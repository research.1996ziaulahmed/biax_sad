-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2022 at 06:55 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akij_biax`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_settings`
--

CREATE TABLE `application_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `app_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `superadmin_enable_register_tc` tinyint(1) NOT NULL DEFAULT 0,
  `superadmin_register_tc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `landmark` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` char(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_DRIVER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_HOST` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_PORT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_USERNAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_PASSWORD` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_ENCRYPTION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_FROM_ADDRESS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_FROM_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_email_settings_to_businesses` tinyint(1) NOT NULL DEFAULT 0,
  `enable_new_business_registration_notification` tinyint(1) NOT NULL DEFAULT 0,
  `enable_new_subscription_notification` tinyint(1) NOT NULL DEFAULT 0,
  `enable_welcome_email` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application_settings`
--

INSERT INTO `application_settings` (`id`, `app_name`, `app_title`, `superadmin_enable_register_tc`, `superadmin_register_tc`, `landmark`, `country`, `state`, `city`, `zip_code`, `mobile`, `alternate_number`, `email`, `MAIL_DRIVER`, `MAIL_HOST`, `MAIL_PORT`, `MAIL_USERNAME`, `MAIL_PASSWORD`, `MAIL_ENCRYPTION`, `MAIL_FROM_ADDRESS`, `MAIL_FROM_NAME`, `allow_email_settings_to_businesses`, `enable_new_business_registration_notification`, `enable_new_subscription_notification`, `enable_welcome_email`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 1, '<h2 style=\"margin: 0px 0px 10px; padding: 0px; font-weight: 400; font-family: DauphinPlain; font-size: 24px; line-height: 24px;\">What is Lorem Ipsum?</h2>\r\n<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker includin</p>', '1/A East Tejturi Bazar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'smtp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, '2022-01-29 13:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `business_id`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Apex', 7, 4, NULL, '2021-04-27 12:35:25', '2021-04-27 12:35:25'),
(3, 'Bata', 7, 4, NULL, '2021-04-27 12:35:30', '2021-04-27 12:35:30'),
(19, 'Loto', 7, 4, NULL, '2021-05-03 15:43:13', '2021-05-03 15:43:13'),
(20, 'Orion', 7, 4, NULL, '2021-05-04 11:37:53', '2021-05-04 11:37:53');

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `last_package_id` int(11) DEFAULT NULL,
  `nos_of_location` int(11) NOT NULL DEFAULT 1,
  `nos_of_user` int(11) DEFAULT NULL,
  `nos_of_invoice` int(11) DEFAULT NULL,
  `nos_of_product` int(11) DEFAULT NULL,
  `end_trial_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `use_system_mail` tinyint(1) NOT NULL DEFAULT 0,
  `MAIL_DRIVER` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_HOST` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_PORT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_USERNAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_PASSWORD` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_ENCRYPTION` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_FROM_ADDRESS` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MAIL_FROM_NAME` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `businesses`
--

INSERT INTO `businesses` (`id`, `business_name`, `email`, `logo_url`, `is_active`, `last_package_id`, `nos_of_location`, `nos_of_user`, `nos_of_invoice`, `nos_of_product`, `end_trial_date`, `expiry_date`, `use_system_mail`, `MAIL_DRIVER`, `MAIL_HOST`, `MAIL_PORT`, `MAIL_USERNAME`, `MAIL_PASSWORD`, `MAIL_ENCRYPTION`, `MAIL_FROM_ADDRESS`, `MAIL_FROM_NAME`, `deleted_at`, `created_at`, `updated_at`) VALUES
(7, 'Dynamic Technology Solution', NULL, NULL, b'1', NULL, 1, 2, 0, 0, '2021-05-11', '2022-04-26', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-26 03:47:27', '2021-04-26 03:47:27');

-- --------------------------------------------------------

--
-- Table structure for table `business_locations`
--

CREATE TABLE `business_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `landmark` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` char(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_locations`
--

INSERT INTO `business_locations` (`id`, `location_name`, `business_id`, `landmark`, `country`, `state`, `city`, `zip_code`, `mobile`, `alternate_number`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-25 15:09:42', '2021-04-25 15:09:42'),
(2, 'ABFA', NULL, NULL, 'Bangladesh', 'Tejgoan', 'Dhaka', '1215', NULL, NULL, NULL, '2021-04-26 03:26:13', '2021-04-26 03:26:13'),
(3, NULL, NULL, NULL, 'Bangladesh', 'Dhaka', 'Dhaka', '1215', '01846263663', NULL, NULL, '2021-04-26 03:26:56', '2021-04-26 03:26:56'),
(4, NULL, NULL, NULL, 'Bangladesh', 'Dhaka', 'Dhaka', '1215', '01846263663', NULL, NULL, '2021-04-26 03:33:11', '2021-04-26 03:33:11'),
(5, NULL, NULL, NULL, 'Bangladesh', 'Dhaka', 'Dhaka', '1215', '01846263663', NULL, NULL, '2021-04-26 03:33:53', '2021-04-26 03:33:53'),
(6, 'ss', 7, NULL, 'Bangladesh', 'Tejgoan', 'Dhaka', '1215', '01846263663', NULL, NULL, '2021-04-26 03:47:27', '2021-04-26 03:47:27');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2021_04_18_183413_create_packages_table', 2),
(7, '2021_04_19_104024_create_application_settings_table', 3),
(8, '2021_04_19_202828_create_payment_methods_table', 4),
(14, '2021_04_24_175016_create_businesses_table', 5),
(17, '2021_04_25_095652_create_payment_information_table', 5),
(18, '2021_04_24_205839_create_user_access_lists_table', 6),
(20, '2021_04_25_090823_create_business_locations_table', 7),
(21, '2021_04_26_153319_create_categories_table', 8),
(24, '2021_04_26_182933_create_variations_table', 9),
(28, '2021_04_27_120007_create_units_table', 11),
(29, '2021_04_27_175601_create_brands_table', 12),
(37, '2021_04_27_110659_create_products_table', 13),
(38, '2021_05_04_081205_create_product_details_table', 13),
(39, '2021_05_04_083655_create_product_locations_table', 13),
(40, '2021_05_04_175441_create_product_sku_calculators_table', 14),
(41, '2021_05_04_184118_create_product_sub_sku_calculators_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `intcusid` bigint(11) NOT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `strname` varchar(100) DEFAULT NULL,
  `straddress` varchar(2000) DEFAULT NULL,
  `strphone` varchar(200) DEFAULT NULL,
  `strpropitor` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcustomer`
--

INSERT INTO `tblcustomer` (`intcusid`, `intunitid`, `strname`, `straddress`, `strphone`, `strpropitor`, `created_at`, `updated_at`, `deleted_at`) VALUES
(358289, 91, 'Nazia Enterprise (W)', 'Trishal Mymensingh', '01713680723', 'Abdul Malek', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(375195, 91, 'Krishibid Packaging Ltd', 'Choto Kaliakoir, Birulia, Savar, Dhaka', '01938849408', 'Md. Sabbir', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376425, 91, 'Sample Delivery', '198, Bir Uttam Mir Shawkat Sarak, Tejgaon, Dhaka-1208', '01913858378', 'Akij Biax', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376426, 91, 'Akij Match Factory Limited', 'West Muktarpur, Munshigonj', '01913858378', 'Akij Group', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376450, 91, 'Akij Food & Beverage Limited', '198, Bir Uttam Mir Shawkat Sarak, Tejgaon, Dhaka-1208', '01719268334', 'Mr Nasir Uddin', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376491, 91, 'ESR International', '65, Banglamotor, Dhaka-1000', '01711301584', 'Md. Rasheduzzaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376507, 91, 'Al Mostofa Printing & Packaging Ltd', 'Islampur,Meghnaghat', '01755543935', 'Nur Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376508, 91, 'Bashundhara Paper Mill', 'Anarpura,Gazaria,Munshiganj', '01729077383', 'Mr. Saiful Islam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376509, 91, 'The Merchants Ltd.', 'A-99 BSCIC Industrial Estate Tongi Gazipur, Bangladesh', '01712230225', 'Harun Ur Rashid', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376510, 91, 'Premiaflex Plastics Ltd.', 'Kewa Proschim Khondo', '01707950406', 'Chan Mia', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376512, 91, 'Robin Printing & Packages', 'Teknagpara, Gazipur Sadar, Gazipur, Joydebpur, Dorgabazar, PS-Gazipur-1700, Bangladesh', '01711634824', 'Md. Rafiqul Islam ( GM Purchase)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376514, 91, 'Shajinaz Exim Pack LTD.', 'Bscis Industrial Estate,Block B, Plot- C-9,Nasirabad,Chittagong', '01987666000', 'MD. Kamruzzaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376525, 91, 'Akij Printing & Packages Limited', '198, Bir Uttam Mir Shawkat Sarak, Tejgaon, Dhaka-1208', '01728403022', 'Md. Nasirul Islam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376526, 91, 'Bengal Flexipak Ltd', 'Rajfulbaria, Savar PS, Dhaka-1347, Bangladesh', '01313450021', 'Mr. Iqbal Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376620, 91, 'Bright Plastic Industries', '132, Tejgaon Industrial Area', '01713458679', 'MR. Khatib', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376621, 91, 'Famous Printing & Packaging Ltd', '93,Tongi I/A, Tongi, Gazipur', '01731082988', 'Habibur Rahaman Joarder', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(376634, 91, 'Unity Poly Industries Limited', '38 No. Haider Box Lean, Urdu Road, Dhaka-1211', '01746252222', 'MD Asraful Islam (Director)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376642, 91, 'Mecca Multilayer Limited', 'Baghoir (Rajhalat), Post: Poshchimdi, Union: Tegharia, Thana: South Kewraniganj, Dhaka-1310', '01919523479', 'Mohammad Ali', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376643, 91, 'Madina Multilayer', '82, Aga Sadek Road, Satrowza, Bongshal, Dhaka-1100', '01683852576', 'Mohammad Abul Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376681, 91, 'Nitol Print', '148 Arambag Motijheel,Dhaka-1000', '01673781938', 'Rasel', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(376682, 91, 'Mondal Group', 'Konabari Mouchak(Montrim LTD, Mondal Group)', '01938887254', 'Rupom', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(376697, 91, 'Tampaco Foils Limited', '2-3 BSCIC Industrial estate, Tongi, Gazipur.', '01715475876', 'Md Rejaul Kader', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(376729, 91, 'Mymensingh Agro Ltd', 'RFL Industrial Park, Mulgaon Kaliganj, Gazipur, Dhaka', '01704140797', 'Minhazur Rahaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376777, 91, 'Habiganj Agro Limited', 'Olipur, Shahjibazar, Shaestaganj, Habigonj, Bangladesh.', '0', 'Md. Rafiul Haque', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376778, 91, 'Sam Printing & Packaging Ltd', 'Tapirbari, Shishu Palli, Tengra, Sreepur, Gazipur', '01836604090', 'Md. Aktaruzzaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376794, 91, 'Aristo Accessories Industry', 'Fokirapul, Dhaka', '0', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376842, 91, 'MAK Packaging & Accessories Ind.', 'Mouchak, Gazipur.', '01715016450', 'Kamruzzaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376843, 91, 'Shun Shing Packaging Industries Ltd', 'Land View, 9th Floor,28 Gulshan North C/A, Gulshan 2, Dhaka-1212', '01713015502', 'Md.Faruk Hossain (AGM Production)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376844, 91, 'BD Plast Limited', 'E-06, Mouchak road,(Near mouchak bus stop), Mizmizi, Siddirginj, Narayangonj.', '01762668877', 'Noor Mohammad ', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376899, 91, 'Jharna Printers', 'Panir Tanki, Fakirapool, Dhaka.', '01714714012', 'Md. Ibrahim', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376914, 91, 'Thai-Foils & Polymer Industries Ltd.', 'Meghna Industrial Zone, Sonargaon, Narayanganj', '01755543926', 'Nur Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376970, 91, 'Green Pack Industries Ltd.', 'Nurzahan Sharif Plaza (9th floor), 34, Purana Paltan, Dhaka-1000', '01711462257', 'Delower Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376971, 91, 'Ahian Trade International', '4/5 Sumon Villa, Joynag Road, Lalbag, Dhaka-1211, Bangladesh.', '01843502110', 'Hanif Mahmud', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(376986, 91, 'Fresh Pack Industries', '102, Azimpur Road (6th Floor), Dhaka-1205, Bangladesh', '01708428933', 'Md.Ali Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377033, 91, 'Anmon Packages (Pvt.) Ltd', '19, Paribag (1st Floor), Shahbag, Dhaka-1000.', '01711070507', 'Kazi Shawkat Zaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377034, 91, 'A. J. International', 'Urdu Road, Lalbag', '01919088086', 'Parvez Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377035, 91, 'Aliza Packaging', 'Toil Ghat, Char kaliganj, suvadda, South keraniganj, Dhaka 1310', '01715566850', 'Md. GM Dinar', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(377052, 91, 'Brac Printing Pack', 'Laxmipura,Teen Sarak,Joydebpur,Gazipur.', '01685942865', 'Mr. Alomgir', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377053, 91, 'KR Flexipack Ltd', 'South Banshbaria,Sitakunda,Chittagong', '01708432589', 'Shahadat Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377059, 91, 'M/S Shah Enterprise (W)', 'Raymoni, Trishal, Mymensingh', '01704015437', 'Md. Aftab Uddin Master', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(377126, 91, 'Sun Yad Poly Vinyl Ind Ltd.', '13, Bangshal Road (5th Floor), Dhaka-1100, Bangladesh', '01711524785', 'Md. Delowar Hossain Faroque', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377127, 91, 'Jahnson-s Pack', '1/H/3 Middle Bashabo, Modina Shorok, Shabujbag, Dhaka-1214, Bangladesh', '01711525003', 'HM Jahangir', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377136, 91, 'I B Traders', '24, MM Ali Road, Dampara, Chittagong', '01708912181', 'Chandan Chowdhury', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377137, 91, 'Bushra Lamination', '89, Arambag (CulvertRoad), Motijhil, Dhaka', '01711876658', 'Abu Taher Sarkar', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377145, 91, 'Al Plast Bangladesh Ltd', 'Mulgaon, Kaliganj, Gazipur, Dhaka.', '01844609390', 'Mr.Zaman Khan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377239, 91, 'Parvin Corporation', '199 West Ashrafabad, Kamrangir Char, Dhaka', '01815000430', 'Shariful Islam ', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377241, 91, 'Standard Trade Centre', '291, inner cerculer Road, Fokirapul, Dhaka', '01713020292', 'Md. Zakir Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377242, 91, 'Classic Foils Ltd', 'Urdu Road', '0', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377263, 91, 'Sumaya Packaging', '91,Korbanigonj, Chittagong', '01919417128', 'Md.Belal Chowdhury (Imtiaz)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377313, 91, 'Jahangir Lamination', '291, Inner circular Road, Fokirapul, Motijheel, Dhaka 1000', '0170700891', 'Mr. Jahangir', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377318, 91, 'R. Rafi Enterprise', '', '0', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377324, 91, 'Zaman Printing & Packaging Industries', '1/1, Pioneer Road, YMCA Bhaban (2nd floor), Kakraill, Dhaka-1000', '01911405872', 'Md. Hasanuzzzaman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377353, 91, 'Tricepack Ltd.', 'Ka-62, Kuratoli, Khilkhet, Dhaka-1229', '01722256660', 'Contact: Md. Nasimul Islam, Purchase Officer', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377391, 91, 'Well Trade', 'Raowa Complex, (level 9), Mohakhali, Dhaka 1206', '01709640087', 'Faisal Ibne Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377399, 91, 'Nasim Accessories', 'Jamtola, Dhopa Potti, Hira Community Center, Chashara, Narayangonj', '01985009686', 'Mosharaf Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377405, 91, 'Maa-Er-Dua Foil Printers', '262,Al-Amin Complex Fakirapool, Motijheel, Dhaka, Bangladesh', '01725028473', 'Nur Mohammad Nur', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377410, 91, 'M/S EMU ENTERPRISE', '10 Yakub Nagar ,Firingibazar , Chittagong', '01813143177', 'Mr. Piaru', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377413, 91, 'City Foils Limited', 'Ronosthol, Shimulia, Ashulia, Dhhaka', '01711327948', 'Mohd. Shah Alam Newaz', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377425, 91, 'BUD Corporation', 'Rajason, Savar,Dhaka', '01720067367', 'Md. Rezaul Karim', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(377465, 91, 'Mala Traders', '48, Bijoy Nagar. 1st Floor, Dhaka-1000.', '01676179475', 'Md. Borhan Uddin', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(380534, 91, 'Fu-Wang Foods Ltd', 'House 55, Road 17, Banani, Dhaka 1213', '01701222610', 'Arif Ahmed Chowdhury', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(381593, 91, 'Abul Khair Tobacco Co Ltd', 'Fathapur, Daulatgonj, Laksham, Comilla, Bangladesh', '01973163020', 'Mitul Hasan', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(384683, 91, 'Res Label and Painting Industry', '2, R.K Mission Road, Motaleb Mansion(4th Floor), Dhaka', '01743269977', 'Md. Reza-e-Selim', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(384687, 91, 'Fresh Plastic & Printing Packaging', 'Huzurpara, Ashrafabad, Kamrangir Char, Dhaka', '01711542080', 'Mr. Hanif', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(384749, 91, 'Manipal International Printing Press Limited.', 'PO BOX: 39987-00623, Mombasa Road, Next To Mombasa Cement, Nairobi, Kenya', '0919820079241', 'Contact: Mr. Patkar (Agent)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(384750, 91, 'Krakchemia S.A', '30-298, Krakow, Powstanta Listopadowego 14, Poland', '0481200652199', 'Mr. Patkar (Agent)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385772, 91, 'Wahidul Enterprise', '101/1, K.P Ghosh Street, Armanitola, Dhaka', '01713504457', 'Md. Wahidul', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385773, 91, 'Bangladesh Poly Printing International Ltd.', '51, Khan-A-Sabur Road, Khulna', '01718450661', 'Rabiul Islam Tonu', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385787, 91, 'Akij Plastics Ltd.', 'Shampara, Kumna; Chhatak PS; Sunamganj-3082;  Bangladesh', '01781625991', 'Kaisar Ahmmed', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(385795, 91, 'Hossain Store', '13, Bangshal Road (5th Floor), Dhaka-1100, Bangladesh', '0', 'Md. Delowar Hossain Faroque', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385800, 91, 'G.I.P Trade House', '227/1, Fokirapol, Motijheel, Dhaka-1000', '01715940223', 'Md. Mostofa', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385801, 91, 'G.H Traders', '45, Zindabahar, 1st Lane, Urdu Road, Dhaka', '01819495166', 'Abdus Sobhan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385807, 91, 'Khulna Printing and Packaging Limited', 'Khulna-Mongla Road, Katakhali, Shambaghat, Lockpur, Fakirghat, Bagerhat', '01711471618', 'S.M Amzad Hossain', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385810, 91, 'Banga Plastic International Ltd.', 'Bagpara, Palash, Narsingdi', '01841012966', 'Contact: Md. Arif', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385811, 91, 'RFL Plastics Ltd.', 'Bagpara, Palash, Narsingdi', '0', 'Contact: Md. Arif', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385816, 91, 'Maa Film Cutting', '89, Islampur Road, Dhaka-1100', '01715314013', 'Jashim Uddin', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385821, 91, 'Digital Graphics Printing Ltd.', 'Chonertec, Nagorpara, Rupganj, Narayanganj', '01700762249', 'Contact: Md. Atiq', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385823, 91, 'ABFL-Test', 'Test', '01708428331', 'Test', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385851, 91, 'Noor International', '10/2, Sheik Shaheb Bazar, Lalbag, Dhaka', '01904519604', 'Shiek Salman Noor', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385897, 91, 'Kayes Packaging', 'Char Kaliganj, Hakkani Mosjid Road, Olirgar 1No. Goli, Motaleb Tower, South Keraniganj, Dhaka', '01712583817', 'Md. Alinur Hossen Morol', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385934, 91, 'M/S Morshed Enterprise', '3, Komoldoh Road, Urdu Road, Dhaka', '01316992501', 'Morshed Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385948, 91, 'Shotota Enterprise', '98/1, Nazimuddin Road, Chowkbazar, Dhaka', '01725068757', 'Md. Shariful Islam(Ripon)', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(385983, 91, 'Consort Flexi Pack Limited', 'B-47-49, BSCIC Industrial Estate, Kalurghat, Chittagong', '01816249812', 'Anisul Islam Anis', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386012, 91, 'Afsana Enterprise', '29/1, Khaza Dewan, 1st Lane, Lalbag, Dhaka-1211', '01815035137', 'Md. Joynal Abedin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386015, 91, 'Jaman Enterprise (W)', 'Trishal, Mymensingh', '01711130112', 'Anisuzzaman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386019, 91, 'Madina Accessories', '24/2, Tatkhana Lane, Nazimuddin Road, Dhaka', '01843502110', 'Mohammed Tajul Islam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386020, 91, 'M/S Robin Corporation', '98/4, Hosni Dalan, Nazimuddin Road, Dhaka', '01815035137', 'Md. Ratan Ali Shah', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386033, 91, 'Matador Ballpen Industries Ltd.', '102. West Azimpur Road, Dhaka', '01713556869', 'Md. Shah Alam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386079, 91, 'Miami Roto Film Products Ltd.', 'Boro Alampur, Halimanagar, Adorsha Sadar, Comilla', '01815046998', 'Md. Abdul Mannan', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386098, 91, 'Healthcare Label and Tape Ind. Ltd.', '74/58, Rajabari, Sreepur, Gazipur', '01708800111', 'Nadia Haider', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386112, 91, 'Dekko Accessories Ltd.', 'Singair Road, Hemayetpur, Savar, Dhaka', '01817299776', 'Shariful Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386198, 91, 'Allardan Enterprise', '3/1, Zindabazar(2nd Lane), Hazi Paper Market(2nd Floor), Nayabazar, Dhaka', '01675315323', 'Md. Sumon Ahmed', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386225, 91, 'Khan Accessories and Packaging Co. Ltd.', 'B-72/73, BSCIC, Tongi, Gazipur', '01833318866', 'Md. Abdul Kader Khan', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386285, 91, 'Insignia Print Technology LFTZ Enterprise', 'Lagos Free Trade Zone, Itoke Village IBEJU, Lekki Area, Lagos, Nigeria', '02349073019217', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386298, 91, 'Mars Middle East Trading FZC', 'Q4-111, Saifzone, PO Box- 513732, Sharjah, UAE', '0', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386337, 91, 'Sizan Printers', '99, Naya Paltan, Dhaka-1000', '01711800810', 'Md. Mizanur Rahman Khan(Monzu)', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386347, 91, 'Reliance Fibre Inds. Ltd.', '31/C/1, Topkhana Road, Segunbachia, Dhaka-1000', '01678181818', 'Kazi Shawkat Zaman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386355, 91, 'Union Label and Accessories', '44/Kha, Shahjadpur, Bashtola, Gulshan,Dhaka', '01819279699', 'Md. Jahangir Hossain Molla', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386386, 91, 'Mars Middle East Trading FZC(Zambia)', 'Q4-111, Saifzone, PO Box- 513732, Sharjah, UAE', '0', '', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386393, 91, 'Max Poly Printers', 'Ashrafabad, Nabab Char, Kamrangir Char, Dhaka', '01711542604', 'Md. Sirajuddin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386439, 91, 'Meradia Packaging (Pvt.) Ltd.', 'Dhonuhaji, Mijmiji, Siddirganj, Narayanganj', '01711520398', 'Md. Moshiur Rahman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386468, 91, 'Packman Bangladesh Ltd.', '132, Tejgaon Industrial Area, Dhaka-1208', '01717792475', 'Rehan Zafar', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386470, 91, 'Arbab Poly Pack Ltd.', 'Shimrail, Demra Road, Siddhirganj, Narayanganj', '01711319158', 'Zakir Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386509, 91, 'Sumaya Enterprise', 'Niribili, Mirzanagar, Ashulia, Dhaka', '01708428933', 'Mosammat Maleka Begum', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386634, 91, 'Premier Flexible Packaging Ltd', '315, Nelson Mandela Expressway,', '08801755574727', 'Mr. Zahangir Hasan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386636, 91, 'Arvee Industries Limited', 'KM 38 Abeokuta Motor Road, Sango Ota Ogun State, Nigeria', '01755574727', 'Mr. Zahangir Hasan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386641, 91, 'Sample Delivery(Foreign)', '198, Bir Uttam Mir Shawkat Sarak, Tejgaon, Dhaka-1208', '01755574727', 'Mr. Zahangir Hasan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386644, 91, 'M/S Sundorban Traders', '39, Northbroke Hall Road, Banglabazar, Dhaka-1100', '01710150590', 'Md. Hannan Bepari', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386692, 91, 'AIM Corporation', 'A.S.M Khaled Abdullah', '01775568162', '242/A, Fakirapool, Motijheel, Dhaka', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386716, 91, 'Motara Industries Limited', '25 Morison Cresent, Alausa Ikeja, Lagos, Nigeria', '01755574727', 'Contact: Mr. Zahangir Hasan', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386768, 91, 'Victoria Fashion Accessories Industry', 'Dholadia, Vawal, Rajabari, Sreepur, Gazipur', '01708800112', 'Contact: Shushanto Sarkar', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386784, 91, 'Hamim Plastic International (W)', '72, Siraj Nagar, Al hera Community Centre Kamragirchor Dhaka', '01715956665', 'Md. Motaleb Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386851, 91, 'Montrims Ltd.', 'Mouchak, Kaliakoir, Gazipur', '01938887077', 'Md. Ilias Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(386868, 91, 'Shuaiba Industrial Company (K.S.C.)', 'Block No. 3, Str.: 31, Bldg 150, Subhan Industrial Area, 10088 Shuaiba, Code No. 65451, Kuwait', '08809602310327', 'Contact: Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386878, 91, 'Texplast Industries Limited.', 'P.O. Box 43039-00100 GPO, Magana Lane, Off Magana Road, Near Magana Flowers (Rungiri-Regen), Nairobi, Kenya.', '08809602310327', 'Contact: Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386879, 91, 'Creative Packing Limited.', 'PO Box 9621, 16/Mbozi Road, Changombe, Dar Es Salaam, Tanzania', '08809602310327', 'Contact: Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386913, 91, 'Minar Printing & Packaging Ltd.', '37/2, Faynaz Apartment, Box Culvart Road, Dhaka', '01865045502', 'Md. Nizam Uddin', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386920, 91, 'Pace Tobacco Industries (BD) Ltd.', 'Plot#16-17, EPZD, Mongla, Bagerhat', '01716407002', 'Hiten Jash Shah', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(386965, 91, 'Modhuban Packaging Ind.(PVT.) Ltd.', 'BSCIC I/E, Block-B, Plot# C05-06, Sholashahar, Nasirabad, Chittagong', '01711749102', 'Mohammad Nurul Absar', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387012, 91, 'Sun Sine International', '2, Debidash Ghat Lane, Dhaka', '01979905500', 'Shoaib Shahid', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387077, 91, 'Kiam Metal Industries Limited', 'BSCIC I/E, Kustia', '01755582820', 'Md. Mizber Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387215, 91, 'Jamana Printers Limited.', 'Nyerere Road, Plot No. 09, P.O. Box: 5584, Dar Es Salaam, Tanzania.', '08801730300333', 'Contact: Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387225, 91, 'Akij Particle Board Mills Ltd.', '198, Bir Uttam Mir Showket Sarak, Tejgaon Link Road, GMG Mor, Dhaka', '01911810197', 'Mr. Sandip Debnath', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387245, 91, 'Sisk & Ten Company Limited', 'No. 15, Nnamdi Adikwu Street, Abuja, FCT, Nigeria', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387248, 91, 'Amin Corporation', '87, Naya Paltan, Dhaka-1000', '01309314287', 'Md. Ruhul Amin', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387262, 91, 'Asia Foils Limited', '32/A, Mymensing Lane, Bangla Motor, Dhaka', '01712791154', 'Shah Newaz Talukder', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387341, 91, 'Mahtab Flexible Printing Press', '33/1, Sheikh Shaheb Bazar Road, Lalbag, Dhaka', '01678041680', 'Mahtabuddin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387411, 91, 'Panta Plastics Industries Ltd.', 'Block#A, Plot# B/4, BSCIC Industrial Area, Bayezid, Nasirabad, Chittagong', '01713162939', 'Mohiuddin Md. Aworangazeb', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387452, 91, 'Epic Associates', '304/E, Munda, Uttarkhan, Dhaka-1230', '01730444331', 'Md. Salauddin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387458, 91, 'Banga Plastic International Ltd.(Export)', 'Fuji Trade Center. North Badda, Dhaka. (11th Floor)', '08801841012966', 'Mr. Md. Arif', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387627, 91, 'Unity Enterprise', 'Plot#D-04, 05 BISCIC I/A, Kalurghat, Chittagong', '01746252222', 'Md. Rafiqul Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387639, 91, 'Zas Rotoflex Industries Limited', 'Chatian, Madaripur, Habiganj', '01705477181', 'Contact: Mr. Mehedi', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387679, 91, 'Pace Tobacco Industries(BD) Ltd.(Export)', 'Plot#16-17, EPZD, Mongla, Bagerhat', '01716407002', 'Hiten Jash Shah', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387748, 91, 'Paper King Stationery', 'PO Box 22654, Dar Es Salaam, Tanzania', '08801730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387800, 91, 'Akthar Enterprise Ltd.', 'P.O. Box 4051, Zanzibar, Tanzania.', '08801730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(387801, 91, 'Arbab Pack Limited', 'Shimrail,Demra road, Shiddirgonj,Narayangonj', '01711319158', 'Zakir Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387865, 91, 'Mahiyat Enterprise', '1/9, Sher-e-Banla Market, 2, No. Rail Gate, Narayanganj', '01985009686', 'Md. Mosharrar Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387877, 91, 'Time Pack Industries', 'Dalda Road, Panchabati, Narayanganj', '01924745262', 'Md. Anwar Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387883, 91, 'M Corporation', '38, Hayder Box Lane, Chawakbazar, Dhaka-1211', '01746252222', 'Tanveer Alam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387932, 91, 'M/S Mukta Printing & Packaging', 'House#7, Road#5, Block#A, Chunkutia Southpara, Suvadda, Keraniganj, Dhaka', '01670149965', 'Md. Haji Motiur Rahman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387951, 91, 'Taj Accessories', '74, Rahmatbag, Kamrangirchar, Dhaka', '0', 'Md. Parvez Alam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387986, 91, 'AGI Flex Ltd', '69/8, Maddha Rajashon, Savar, Dhaka-1340', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(387988, 91, 'Test-2', '', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389072, 91, 'Ghorashal Multilayer Plastic Packaging Ltd.', 'Bhagdi, Ghorasal; Palash PS; Narshingdi-1613; Bangladesh', '01717308767', 'Mr Nowshad', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389081, 91, 'Magdata S.P.A', 'Strada Della Selva, 100/2, 43052 Colorno (PR), Partita, Italy.', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389100, 91, 'Al-Arafah Packaging Ind.Ltd.', '1/1,Bashura,Board Bazar, National University, Gazipur Sadar,Gazipur-1704', '01740919108', 'Mohammad Ali Mazumder Nipu ', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389157, 91, 'Ghandoura Industrial Group Co. Ltd.', 'Jeddah Industrial City-2, PO Box 30495, Jeddah', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389159, 91, 'Flour Mills of Nigeria', '34 Eric Moore Road, Lagos Nigeria', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389178, 91, 'Greycom Resourses LLP', 'Unit 5, Olympia Industrial Estate, Coburg Road London, England N22 6TZ, UK', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389272, 91, 'CDM Sp. Z O. O.', 'Cegieniana 7, 95-054 Ksawerów, POLAND', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389299, 91, 'Avera Technologies Limited.', '40, Adisa Bashua Street, Surulere, Lagos, Nigeria', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389300, 91, 'Waled Kilani & Partners Ind. Co.', 'Al-Muaqqar Industrial Zone, PO Box: 950747, Amman 11195, Jordan', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389302, 91, 'Color Print & Packaging Industries', 'Dhopa Para, Dokkin Kawli, Pahartoli, Chittagong', '01713403660', 'Gopal Nath', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389304, 91, 'Habib Enterprise (W)', 'Trishal, Mymensingh', '01759101971', 'Mizanur Rahman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389354, 91, 'Sun Packaging Co (Mauritius) Ltd.', 'Warehouse No. 4, Freeport Zone 6, Mer Rouge, Port Louts Mauritius.', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389374, 91, 'Rotogravure Industrial Investment', '6th of October City, 3rd Industrial Zone, N025, Giza, Egypt', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389375, 91, 'Coatall Films Pvt. Ltd.', 'Plot No: 10, Sector 1, AURIC Shendra, Aurangabad- 431 154 India', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(389389, 91, 'Mastul Graphics & Printers', 'House# 2/7, Block#A, N.S Road, Rampura, Dhaka', '01713049965', 'Mohammad Abdul Awal', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389478, 91, 'M.P. Packeging', '31, Hosnidalan, Dhaka', '01721768749', 'Ripon Chandra Saha', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(389806, 91, 'Tara Traders Link', 'Kader Tower 1st Floor, 128/13 Jubilee Road, Chottogram', '01716040855', 'Abdus Sabur Liton', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(391007, 91, 'H Plastiques CI', 'Anyama Carrefour Kouresh, 09 Bp 1886, Abidjan 09, Ivory Coast', '01730300333', 'Md. Mortuza Kashem', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(391088, 91, 'Taher Packaging', 'Plot # M-36, Notun Sonakanda, Biscic, Keraniganj', '01711463950', 'Md. Abu Taher', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(391243, 91, 'Arafat Eenterprise', '101/1 K.P Gosh Street, Armanitola, Dhaka-1100', '01914282806', 'Md. Motaher Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(391329, 91, 'Al-Faruque Packaging', 'Shantahar Road Shikor Narhot, Kahalu, Bogura', '01926998891', 'Md. Redwanul Kabir Chowdhury', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(391365, 91, 'Vision Carton & Accessories Ind. Ltd.', '160/2, South Salna, Salna Bazar, Joydevpur, Gazipur', '01724400169', 'Md. Tariqul Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(391532, 91, 'Infinity Flexi Pack Ltd.', 'Plot # A-44 Bscic I/A, Mukterpur, Munshiganj', '01730937204', 'Samaresh Saha Bappi', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392609, 91, 'Qatar Development Bank', 'PO Box 22789, Grand Hamad Street, Doha, Qatar', '08801713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(392634, 91, 'Ideal Fibre Industries Ltd.', '13/3 (Chiarman Bhaban) B.B. Road, Nitaigonj, Narayangonj.', '01711537154', 'Mohammad Zakir Hossen', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392666, 91, 'Jordan Shareef Plastic Factory', 'B#5, Al Riba St, Marka Ash, Shamaliyya, Amman, Jordan', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(392715, 91, 'Abul Khair Match Factory Ltd.', '259 BSCIC Road, Charipur, Feni Sadar, Feni-3900', '0', 'A.K.M. Shafique Chowdhury', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392725, 91, 'H P Packaging', '107/43, Rangarajapuram Main Road, Kodambakkam, Chennai-600024, India', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(392743, 91, 'Nasim Enterprise (W)', 'Raymoni, Trishal, Mymensingh', '01742893016', 'Md. Faruk', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392785, 91, 'Akij Biri Factory Ltd.', 'Navaron, Jhikargachha, Jessore', '01711827923', 'Sk. Mohiuddin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392795, 91, 'Canton Paper Converting & Packaging', 'Plot: B-72, 73, Bsic Kanchpur, Sonargaon, Narayangonj.', '01713037960', 'Anwar Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392818, 91, 'NRB Desiccant Industries', 'Holding No.-18, Word-02, Kaundia Bazarpara, Mirpur Bazar, Savar, Dhaka', '01926263363', 'Md. Abdul Motin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392819, 91, 'Labib Trading', '15/6, Block - F, Tekka Para, Adabor, Mohammedpur', '01700984978', 'Md. Ali Abed', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392880, 91, 'Metro Trade & Commercial Service Ent', 'Plot No. 20, Block No. 7 Garry Free Zone, Sudan', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(392916, 91, 'Power Pack Corporation', 'Akram Tower (8th Floor), 1515, Bijoy Nagar, Dhaka', '01711196584', 'Alok Sen Gupta', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(392917, 91, 'Fazlu Enterprise', '31, Hosni Dalan, Chawk Bazar, Dhaka-1211', '01820008229', 'Md. Fazlu', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(393000, 91, 'M Corporation (W)', '38, Hayder Box Lane, Chawakbazar, Dhaka-1211', '001746-252222', 'Tanveer Alam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(393048, 91, 'M/S SINTHIA ENTERPRISE', '60, Purana Paltan; Paltan PS; Dhaka-1000; Bangladesh', '0', 'Sajjad Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(395839, 91, 'Casio Trims & Accessories Ltd.', 'Kakab, Birulia, Savar, Dhaka', '01740927584', 'Sheikh Nasir Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(395892, 91, 'K-Flex Limited', '34, Purana Paltan (8th Floor), Dhaka', '01714096140', 'Kazi Monsur Ul Haque', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(395924, 91, 'S Alam Agency', 'C-159, Bscic Industrial Estate, Tongi, Monnunagar, Gazipur-1710', '01863638590', 'Kabir Hossain Shimul', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(395927, 91, 'Technoval Laminados Plasticos LTDA', 'Rua Leonardo Costa Gonclaves 1100-Parque Monte Verde Minas Gerais, Brazil', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(395933, 91, 'Nurab Limited', '81, Kathaldia, Tongi, Gazipur-1710', '01716010376', 'Md. Amran Hossen Juwel', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396116, 91, 'Soretrac', 'Zac de la Villette aux Aulnes, Rue rene Cassin, 77290 Mitry-Morry, France,', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(396256, 91, 'Al-Amin Enterprise (W)', '425, Ashrafabad, Kamrangir Char, Dhaka', '01839291882', 'Gazi Abdulla Al-Amin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396302, 91, 'Rajasthan Flexible Packaging Ltd.', 'Pathredi Road, Village- Patherdi, Tehsil Kotputli, Dist.: Jaipur (Rajasthan) 303107, India', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(396304, 91, 'Shree Shakti Poly Films Pvt. Ltd.', 'Unit No. 1, Bldg No. 8, Abhilasha Industrial Estate, Near Burma Shell Petrol Pump, Opp Western Express Highway, Village-waliv, Vasai (East). Dist. Thane, India', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(396332, 91, 'Hamim Plastic International', '72, Siraj Nagar, Al hera Community Centre Kamragirchor Dhaka', '01715956665', 'Md. Motaleb Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396608, 91, 'Glorious Enterprise', 'Alif Biponi Bitan, 31 N.A. Chowdhury Road, Chattogram', '01740988463', 'Toma Dey', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396609, 91, 'Amenaz Concept', '98/1, Nazimuddin Road, Chawkbazar, Dhaka', '01716139215', 'Md. Ahsan Habib', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396610, 91, 'Al-Amin Enterprise', '425, Ashrafabad, Kamrangir Char, Dhaka', '01839291882', 'Gazi Abdulla Al-Amin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396612, 91, 'Suncoast Incubuos Limited', '40 Adisa Bashua Street, Surulere, Lagos, Nigeria', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(396781, 91, 'A.H. Enterprise', '20/3, KM Azam Lane, Shatrowza, Chwakbazar, Dhaka', '01953021565', 'Answer Ali', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396782, 91, 'Jahanara Packaging Ind.', 'City Corporation Road, Matuail, Jatrabari, Dhaka-1362', '01925615163', 'Md. Mosharaf Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396828, 91, 'Meghna Foil Packaging Ltd.', 'Meghna Industrial Economic Zone, Tipordi, Mograpara, Sonargaon, Narayangonj', '01713141974', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(396920, 91, 'Abu Dhabi Islamic Bank', 'P.O. Box 313 Abu Dhabi United Arab Emirates', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(398509, 91, 'De United Foods Industries Limited', '44 Jimoh Odutola Stree, Off Eric Moore Road, Surulere, Lagos, Nigeria', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(398586, 91, 'Shahjalal Trading', '8 No. Arambagh, Motijheel, Dhaka', '01739079069', 'Md. Nur Nobi', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(398811, 91, 'Kishwan Agro Products Limited', 'S-Salam Tower (5th Floor), 57 Agrabad I/A, Chittagong', '01755695826', 'Md. Shahidul Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(398997, 91, 'Bombay Sweets and Co. Ltd. (Unit - 2)', 'Ka-63, Kuratoli, Khilkhet, Dhaka-1229', '0', 'Barkat Ali Habib', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399033, 91, 'Accl to Ifad Auto Services Ltd.', 'NULL', '0NULL', 'NULL', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(399140, 91, 'Nafim Enterprise (W)', 'Raymoni, Trishal, Mymensingh.', '01713680748', 'Md. Moniruzzaman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399337, 91, 'Well Accessories Ltd.', 'Vill-Baniarchala, Post-Bhabanipur, Mirzapur, Gazipur Sadar, Gazipur-1740', '01730790348', 'Syed Nurul Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399473, 91, 'Sumal Foods Limited', 'Oluyole estate, Ring Road, Ibadan, Oyo state, Nigeria', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(399509, 91, 'Shatadal Printers', '85/A, (1st Floor) Arambag, Motijheel C/A, Dhaka-1000.', '01924940408', 'Abul Kalam Azad', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399515, 91, 'Amber Packaging Industries LLC', 'P.O. Box: 33294, Sharjah, UAE, Phone: 065500737', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(399516, 91, 'Hotpack Packaging LLC', 'P. O. Box: 7154, Umm Al Quwain, UAE', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(399650, 91, 'F. S. Trading', '15/7, Zindabahar 2nd Lane (Under Ground), Dhaka.', '01915702337', 'Md. Ekram Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399656, 91, 'Al-Modina Trading', 'Madrasa Para, Chowrasta More, Kamrangichar, Dhaka', '01726750749', 'Md. Abul Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399712, 91, 'Orchid Printers Limited', '32/A, 33, Mymensingh Lane, Banglamotor, Shahbagh, Dhaka-1000', '01671992386', 'Hasina Newas ', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(399733, 91, 'Rajko Traders (Hong Kong) Ltd.', '17/Floor, Tern Centre Tower1, 237/ Queen-s Road Central Hong  Kong', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(399734, 91, 'Hamim International', '72, Sirajnagar, Kamrangichar, Dhaka-1211', '0171595665', 'Md. Motaleb Hossain', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(400771, 91, 'Akij Paints & Adhesives Limited', 'Raimoni, Trishal, Mymensingh', '01781391973', 'Md. Shoyeb Kabir', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(400772, 91, 'Matador Writing Instruments Ltd.', '1/1, West Rosulpur, Kamrangirchar, Dhaka-1211', '01730789599', 'Anisur Rahman', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(400893, 91, 'Akij Plastics Ltd. (W)', 'Shampara, Kumna; Chhatak PS; Sunamganj-3082;  Bangladesh', '01781625991', 'Kaisar Ahmmed', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(400943, 91, 'Magnum Steel Industries Limited (W)', 'Madhayam Baushia, Baushia, Gazaria, Munshiganj', '01819745981', 'Akij Resources Ltd.', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401004, 91, 'M/S. Adarsh Agencies', '2nd Floor,88,Kousualya Bai Building , G Street, Jogupalya, Ulsoor,Bengaluru - 560008', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401054, 91, 'M/S Mostafiz Traders', '', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401064, 91, 'Sonali Lamination & Spot', '193/A, Fakirapul, Motijheel, Dhaka-1000', '01816007735', 'Md. Joynal Abdin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401065, 91, 'Al Khidmah Traders', '34 Northbruk Hall Road, 2nd Floor, Mohammad Ali Market, Banglabazar, Dhaka', '01819383200', 'Md. Golam Murtuza', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401117, 91, 'N A Enterprise', '101/A, Rahmatbag, Kamrangirchar, Dhaka-1211', '01717079760', 'Mojammel Hossain Shaon', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401118, 91, 'Akij Ceramics Limited', 'Akij House, 198, Bir Uttam Mir Shawkat Sarak, Tejgaon Link Road', '0', 'Sk Bashir Uddin', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401130, 91, 'Rapid Pack Limited', '492, Dhitpur, Rajabazar, Keranigonj, Dhaka-1310', '01716280820', 'Md. Shafiqul Islam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401166, 91, 'Epyllion Ltd.', 'Kutubpur, Kanchpur Bazar, Sonargaon, Narayangonj-1430', '01730725872', 'Reazuddin-Al-Mamun', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401167, 91, 'Epyllion Ltd. (Export)', 'Kutubpur, Kanchpur Bazar, Sonargaon, Narayangan-1430', '01730725872', 'Md. Mohimenul Islam', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401171, 91, 'Emirates Technopack LLC', 'Jabel ali industrial area 2, P.O. Box 50912, Dubai - UAE.', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401175, 91, 'Western Paper Industries (BD) Pvt Ltd.', 'Plot #140, EPZ, Comilla, Dhaka, Dhaka, Bangladesh', '001614 473 270', 'BISHAWJIT SAHA', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401179, 91, 'Western Paper Industries (BD) Pvt Ltd. (Export)', 'Plot #140, EPZ, Comilla, Dhaka, Dhaka, Bangladesh', '01614473270', 'BISHAWJIT SAHA', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401183, 91, 'Gum Tape Manufacturer', '', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401184, 91, 'Garments Film Trader', '', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401187, 91, 'Lamination Film Traders', '', '0', '', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401219, 91, 'Hexxa Flexible Packaging LLC', 'P.O BOX 86261 , RAS AL KHAIMAH, AL JAZEERA AL HAMRA INDUSTRIAL AREA, RAKIA , UAE, TEL : +971-7-2444051', '01713998738', 'Md. Mujeeb Ur Rahman', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401228, 91, 'Creative Packaging & Paper Converting', '431/1 Tejgaon I/A, Dhaka-1208', '01819556612', 'Shefaul Alam', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401242, 91, 'S.S. Printing & Packaging Limited', 'Plot-63/A, Block-C, Tongi I/A, Monnunagar PS, Gazipur-1710', '01676179475', 'Kabir Hossain Shimul', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL),
(401243, 91, 'A', 'C', '0', 'B', '2022-01-30 01:28:17', '2022-01-30 01:28:17', NULL),
(401244, 91, 'Abul Biri Factory Limited', 'Saralkha, Shaptibari, Aditmari PS, Lalmonirhat-5510', '01973163020', 'Mr. Mitul Hasan', '2022-01-30 01:28:18', '2022-01-30 01:28:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomercategory`
--

CREATE TABLE `tblcustomercategory` (
  `intcustomercategory` bigint(20) NOT NULL,
  `strcustomercategory` varchar(200) DEFAULT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `ysnactive` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcustomercategory`
--

INSERT INTO `tblcustomercategory` (`intcustomercategory`, `strcustomercategory`, `intunitid`, `ysnactive`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Credit\r\n', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL),
(2, 'Cash', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL),
(3, 'Local L/C', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL),
(4, 'Gum Tape Manufacturer', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL),
(5, 'Garments Traders', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL),
(6, 'Lamination Traders', 91, 1, '2022-01-30 00:01:47', '2022-01-30 00:01:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbldispoint`
--

CREATE TABLE `tbldispoint` (
  `intdispointid` int(11) NOT NULL,
  `intunitid` int(11) NOT NULL,
  `strname` varchar(200) NOT NULL,
  `straddress` varchar(500) DEFAULT NULL,
  `strcontactperson` varchar(200) DEFAULT NULL,
  `strcontactno` varchar(200) DEFAULT NULL,
  `intcustomerid` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbldispoint`
--

INSERT INTO `tbldispoint` (`intdispointid`, `intunitid`, `strname`, `straddress`, `strcontactperson`, `strcontactno`, `intcustomerid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(69492, 91, 'Res Label and Painting Industry [384683]', 'Nekrose Bag , Khalindi ,KERANIGANJ,DHAKA', 'Rahat Reza Omi', '01751632740', 384683, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69495, 91, 'Fresh Plastic & Printing Packaging [384687]', 'Hazurpara, Asrafabad,Kamrangirchar,Dhaka', 'Md. Wakil Ahamed', '01711542080', 384687, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69539, 91, 'Manipal International Printing Press Limited. [384749]', 'PO BOX: 39987-00623, Mombasa Road, Next To Mombasa Cement, Nairobi, Kenya', 'Mr. Patkar (Agent)', '0919820079241', 384749, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69540, 91, 'Krakchemia S.A [384750]', '30-298, Krakow, Powstanta Listopadowego 14, Poland', 'Mr. Patkar (Agent)', '0481200652199', 384750, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69566, 91, 'Wahidul Enterprise [385772]', '101/1, K.P Ghosh Street, Armanitola, Dhaka', 'Md. Wahidul', '01713504457', 385772, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69569, 91, 'Bangladesh Poly Printing International Ltd. [385773]', 'Nawapara (Katakhali), Fokirhat, Bagerhat.', 'Rabiul Islam Tonu', '01718450661', 385773, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69570, 91, 'Bangladesh Poly Printing International Ltd. [385773]', '51, Khan-A-Sabur Road, Khulna', 'Rabiul Islam Tonu', '01718450661', 385773, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69581, 91, 'Akij Plastics Ltd. [385787]', 'Chhatak, Sunamganj', 'Md. Al-Amin Hossin', '01635883599', 385787, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69582, 91, 'Sample for Mymensing Agro Ltd.', 'Mulgaon,Kaligonj, Gazipur, Bangladesh', 'Md. Sadrul Hasan, Asst. Manager, Supply Chain Management, PRAN Group', '01704132980', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69583, 91, 'Sample-Mymensing Agro Ltd.', 'Mulgaon,Kaligonj, Gazipur, Bangladesh', 'Md. Sadrul Hasan, Asst. Manager, Supply Chain Management, PRAN Group', '01704132980', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69585, 91, 'Hossain Store [385795]', '13, Bangshal Road (5th Floor), Dhaka-1100, Bangladesh', 'Md. Sohel Ahmed', '01714164002', 385795, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69591, 91, 'G.I.P Trade House [385800]', '227/1, Fokirapol, Motijheel, Dhaka-1000', 'Md. Mostofa', '01715940223', 385800, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69592, 91, 'G.H Traders [385801]', 'Islampur Road, Zindabahar, Dhaka.', 'Abdus Sobhan', '01819495166', 385801, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69593, 91, 'G.H Traders [385801]', '45, Zindabahar, 1st Lane, Urdu Road, Dhaka', 'Abdus Sobhan', '01819495166', 385801, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69597, 91, 'Khulna Printing and Packaging Limited [385807]', 'Bir Shrestha Ruhul Amin Sharak , East Rupsha , Khulna ', 'Md. Parvez Reza ', '01716607321', 385807, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69601, 91, 'Banga Plastic International Ltd. [385810]', 'Bagpara, Palash, Narsingdi', 'Contact: Mr. Masud', '01841014694', 385810, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69602, 91, 'RFL Plastics Ltd. [385811]', 'Bagpara, Palash, Narsingdi', 'Mr. Masud', '01841014694', 385811, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69605, 91, 'Maa Film Cutting [385816]', 'Islampur Road, Zindabahar, Dhaka.', 'Jashim Uddin', '01715314013', 385816, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69608, 91, 'Digital Graphics Printing Ltd. [385821]', 'Chonertec, Nagorpara, Rupganj, Narayanganj', 'Contact: Md. Atiq', '01700762249', 385821, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69630, 91, 'Noor International [385851]', '10/2, Sheik Shaheb Bazar, Lalbag, Dhaka', 'Shiek Salman Noor', '01904519604', 385851, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69661, 91, 'Kayes Packaging [385897]', 'Char Kaliganj, Hakkani Mosjid Road, Olirgar 1No. Goli, Motaleb Tower, South Keraniganj, Dhaka', 'Md. Alinur Hossen Morol', '01712583817', 385897, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69710, 91, 'M/S Morshed Enterprise [385934]', '3, Komoldoh Road, Urdu Road, Dhaka', 'Morshed Alam', '01815035137', 385934, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69726, 91, 'Shotota Enterprise [385948]', '98/1, nazimddin Road (Husney Dalan),Chauk Bazar, Dhaka-1211', 'Md. Shariful Islam Ripon', '01725068757', 385948, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69753, 91, 'Consort Flexi Pack Limited [385983]', 'B 47-49BICIC Industrial area,Kalurghat,Chittagong', 'Mr. Munna ', '01816249812', 385983, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69792, 91, 'Afsana Enterprise [386012]', '29/1, Khaza Dewan, 1st Lane, Lalbag, Dhaka-1211', 'Md. Joynal Abedin', '01815035137', 386012, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69793, 91, 'Afsana Enterprise [386012]', '29/1, Khaza Dewan, 1st Lane, Lalbag, Dhaka-1211', 'Md. Joynal Abedin', '01815035137', 386012, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69800, 91, 'Madina Accessories [386019]', '24/2, Tatkhana Lane, Nazimuddin Road, Dhaka', 'Mr. Mohiuddin', '01843502110', 386019, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69801, 91, 'M/S Robin Corporation [386020]', '98/4, Hosni Dalan, Nazimuddin Road, Dhaka', 'Md. Ratan Ali Shah', '01815035137', 386020, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69832, 91, 'Matador Ballpen Industries Ltd. [386033]', '1/1 west rosulpur, kamraggirchor, Dhaka', 'Asaf-uddala ', '01713556869', 386033, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69873, 91, 'Miami Roto Film Products Ltd. [386079]', 'Boro Alampur, Halimanagar, Adorsha Sadar, Comilla', 'Joshim uddin ', '01815046998', 386079, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69944, 91, 'Healthcare Label and Tape Ind. Ltd. [386098]', '74/58, Rajabari, Sreepur, Gazipur', 'Sushanta Sarker', '01708800112', 386098, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(69957, 91, 'Dekko Accessories Ltd. [386112]', 'Singair Road, Hemayetpur, Savar, Dhaka', 'Mr. Monir', '01817299776', 386112, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70023, 91, 'Allardan Enterprise [386198]', '3/1, Zindabazar(2nd Lane), Hazi Paper Market(2nd Floor), Nayabazar, Dhaka', 'Md. Sumon Ahmed', '01675315323', 386198, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70024, 91, 'Allardan Enterprise [386198]', '3/1, Zindabazar(2nd Lane), Hazi Paper Market(2nd Floor), Nayabazar, Dhaka', 'Md. Sumon Ahmed', '01675315323', 386198, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70037, 91, 'Khan Accessories and Packaging Co. Ltd. [386225]', 'B-72/73 Basic Industrial Area, Tangi', 'Rezaul Kabir Shujon', '01833318866', 386225, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70066, 91, 'Sample for Abul Khair Tobacco Co Ltd', 'Fathapur, Daulatgonj, Laksham, Comilla, Bangladesh', 'Mitul Hasan', '01973163020', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70068, 91, 'Sample for Famous Printing & Packaging Ltd', '93,Tongi I/A, Tongi, Gazipur', 'Habibur Rahaman Joarder', '01731082988', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70072, 91, 'Insignia Print Technology LFTZ Enterprise [386285]', 'Lagos Free Trade Zone, Itoke Village IBEJU, Lekki Area, Lagos, Nigeria', '', '02349073019217', 386285, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70096, 91, 'Mars Middle East Trading FZC(Tanzania)', 'Q4-111, Saifzone, PO Box- 513732, Sharjah, UAE', '', '0', 386298, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70131, 91, 'Sizan Printers [386337]', '99, Naya Paltan, Dhaka-1000', 'Md. Mizanur Rahman Khan(Monzu)', '01711800810', 386337, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70139, 91, 'Reliance Fibre Inds. Ltd. [386347]', '31/C/1, Topkhana Road, Segunbachia, Dhaka-1000', 'Kazi Shawkat Zaman', '01678181818', 386347, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70141, 91, 'Union Label and Accessories [386355]', 'Baniarchala, Vabanipur,Gajipur Sadar, Gajipur', 'Md. Rafiqul Islam', '01819279699', 386355, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70171, 91, 'Max Poly Printers [386393]', 'Ashrafabad, Nabab Char, Kamrangir Char, Dhaka', 'Mr Rasel', '01711665021', 386393, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70195, 91, 'Meradia Packaging (Pvt.) Ltd. [386439]', 'Dhonuhaji, Mijmiji, Siddirganj, Narayanganj', 'Md. Moshiur Rahman', '01711520398', 386439, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70208, 91, 'Packman Bangladesh Ltd. [386468]', '132, Tejgaon Industrial Area, Dhaka-1208', 'Md. Bashir Uddin', '01717792475', 386468, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70210, 91, 'Arbab Poly Pack Ltd. [386470]', 'Shimrail, Demra Road, Siddhirganj, Narayanganj', 'Iqbal Bin Habib', '01711319158', 386470, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70245, 91, 'Sumaya Enterprise (Savar)', 'Niribili mirja nagar , 1344,Ashilia Savar , Dhaka', 'Shahinur Rahman ', '01708428933', 386509, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70258, 91, 'ABFL-Testt', 'Akij House', 'Mr. Shariful', '01708428331', 385823, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70270, 91, 'I B Traders(Chittagong)', '24, MM Ali Road, Dampara, Chittagong', 'Chandan Chowdhury', '01708912181', 377136, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70271, 91, 'Krishibid Packaging Ltd [375195]', 'Choto Kaliakoir, Birulia, Savar, Dhaka', 'Md Alamin ', '01700729780', 375195, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70272, 91, 'Akij Match Factory Limited [376426]', 'West Muktarpur, Munshigonj', 'Md. Nurul Islam', '01913858378', 376426, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70273, 91, 'Abul Khair Tobacco Co Ltd [381593]', 'Fathapur, Daulatgonj, Laksham, Comilla, Bangladesh', 'Mitul Hasan', '01973163020', 381593, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70274, 91, 'Sun Yad Poly Vinyl Ind Ltd. [377126]', '13, Bangshal Road (5th Floor), Dhaka-1100, Bangladesh', 'Md. Delowar Hossain Faroque', '01711524785', 377126, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70275, 91, 'A. J. International [377034]', 'Urdu road, lalbag, Dhaka', 'Mr. Shakil', '01788860004', 377034, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70276, 91, 'Bushra Lamination [377137]', '89, Arambag (CulvertRoad), Motijhil, Dhaka', 'Md Rafiqul Islam', '01700776103', 377137, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70277, 91, 'KR Flexipack Ltd [377053]', 'South Banshbaria,Sitakunda,Chittagong', 'MD Salah Uddin', '01709399310', 377053, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70278, 91, 'Famous Printing & Packaging Ltd [376621]', '93,Tongi I/A, Tongi, Gazipur', 'Rezaul Karim Shopon', '01816746005', 376621, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70279, 91, 'Nitol Print [376681]', '148 Arambag Motijheel,Dhaka-1000', 'Musa Ahmed', '01673781938', 376681, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70280, 91, 'Mondal Group [376682]', 'Konabari Mouchak(Montrim LTD, Mondal Group)', 'Rupom', '01938887254', 376682, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70281, 91, 'Aliza Packaging [377035]', 'Toil Ghat, Char kaliganj, suvadda, South keraniganj, Dhaka 1310', 'Md. GM Dinar', '01715566850', 377035, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70282, 91, 'Thai-Foils & Polymer Industries Ltd. [376914]', 'Islampur, Meghnaghat, Narayangonj', 'Nur Alam', '01755543935', 376914, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70283, 91, 'Zaman Printing & Packaging Industries [377324]', '2/3 Pathanbari , Pagar ,BSCIC , Tongi ,Gazipur ', 'Md. Hasanuzzzaman', '01911405872', 377324, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70284, 91, 'Well Trade [377391]', 'Baniarchala,vobonipur, Gazipur ', 'Md. Rokon Uddin', '01730790348', 377391, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70285, 91, 'Nasim Accessories [377399]', 'Jamtola, Dhopa Potti, Hira Community Center, Chashara, Narayangonj', 'Mosharaf Hossain', '01985009686', 377399, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70286, 91, 'M/S EMU ENTERPRISE [377410]', '10 Yakub Nagar ,Firingibazar , Chittagong', 'Mr. Piaru', '01813143177', 377410, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70287, 91, 'City Foils Limited [377413]', 'Ronosthol, Shimulia, Ashulia, Dhhaka', 'Mohd. Shah Alam Newaz', '01711327948', 377413, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70288, 91, 'BUD Corporation [377425]', 'Rajason, Savar,Dhaka', 'Md. Rezaul Karim', '01720067367', 377425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70289, 91, 'Tricepack Ltd. [377353]', 'Khashpara, Kanchpur, Sonargaon, Narayanganj.', 'Md. Rashedul', '01731402804', 377353, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70290, 91, 'Habiganj Agro Limited(RIP)', 'RIP-MAL-PML Store, Mulgaon,Kaliganj,Gazipur', 'Mr. Md. Razaul Karim Khan', '01313071123', 376777, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70291, 91, 'Sam Printing & Packaging Ltd [376778]', 'Tapirbari, Shishu Palli, Tengra, Sreepur, Gazipur', 'Md. Aktaruzzaman', '01836604090', 376778, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70292, 91, 'ESR International [376491]', '65, Banglamotor, Dhaka-1000', 'Md. Rasheduzzaman', '01711301584', 376491, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70293, 91, 'Al Mostofa Printing & Packaging Ltd [376507]', 'Islampur, Meghnaghat, Narayangonj', 'Nur Alam', '01755543935', 376507, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70294, 91, 'Bashundhara Paper Mill (Unit # 03)', 'Rotogravure Plant, Joya, Meghna ghat,  Sonargaon,  Narayanganj-1441.', 'Mr. Jakir Hussain ', '01729077383', 376508, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70295, 91, 'Shajinaz Exim Pack LTD. [376514]', 'BSCIC Industrial Estate,Block B, Plot- C-9 & 10,Nasirabad, Bayjid Bostami PS, Chittagong-4210', 'Md Monirul Islam', '01991136115', 376514, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70296, 91, 'Akij Printing & Packages Limited [376525]', 'Morkun, Tongi, Gazipur.', 'Fazlur Rahman', '01771106689', 376525, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70297, 91, 'Bengal Flexipack Ltd Unit-2 [376526]', 'Domna, Kashimpur, Gazipur', 'Dinesh Sarker', '01313450031', 376526, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70298, 91, 'The Merchants Ltd. [376509]', 'A-99 BSCIC Industrial Estate Tongi Gazipur, Bangladesh', 'Mr. Harun', '01712230225', 376509, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70299, 91, 'Premiaflex Plastic Ltd [376510]', 'Mawna, shripur, Gazipur ', 'Md. Hasan Rashid', '01704114470', 376510, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70300, 91, 'Robin Printing & Packages [376512]', 'Tech Nag Para , Bashon , Gazipur ', 'Dider Alam', '01712649265', 376512, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70301, 91, 'Bright Plastic Industries [376620]', '132, Tejgaon Industrial Area', 'MR. Khatib', '01713458679', 376620, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70302, 91, 'Unity Poly Industries Limited [376634]', 'Urdu road, lalbag, Dhaka', 'MD Asraful Islam (Director)', '01746252222', 376634, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70303, 91, 'Macca Multilayer Ltd.', 'AGHOIR (RAJHALAT), POSHCHIMDI, TEGHARIA,KERANIGANJ,DHAKA', 'Md Mosharaf Hossain ', '01747058263', 376642, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70304, 91, 'Madina Multilayer [376643]', '82, Aga Sadek Road, Satrowza, Bongshal, Dhaka-1100', 'Mohammad Biplob', '01735377807', 376643, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70305, 91, 'Maa-Er-Dua Foil Printers [377405]', '262,Al-Amin Complex Fakirapool, Motijheel, Dhaka, Bangladesh', 'Nur Mohammad Nur', '01725028473', 377405, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70306, 91, 'Mala Traders [377465]', 'Nirashpara, Amtoli, Tongi, Gazipur', 'Mr. Borhan', '01676179475', 377465, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70307, 91, 'Fu-Wang Foods Ltd [380534]', 'House 55, Road 17, Banani, Dhaka 1213', 'Arif Ahmed Chowdhury', '01701222900', 380534, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70308, 91, 'R. Rafi Enterprise [377318]', 'Urdu road, lalbag, Dhaka', 'Md. Saddam Hossain', '01912083191', 377318, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70309, 91, 'Tampaco Foils Limited [376697]', '2-3 BSCIC Industrial estate, Tongi, Gazipur.', 'Mr Abdus Salam', '01731636664', 376697, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70310, 91, 'Mymensingh Agro Ltd [376729]', 'RFL Industrial Park, Mulgaon Kaliganj, Gazipur, Dhaka', 'Minhazur Rahaman', '01704140797', 376729, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70311, 91, 'Aristo Accessories Industry(Narayanganj)', 'Shimrail, Shiddirgonj,Narayangonj', 'Md. Deloar', '01728908749', 376794, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70312, 91, 'Shun Shing Packaging Industries Ltd [376843]', 'Char Mirpur,Kaligang ,Gazipur ', 'Ziaur Rahman', '01755515982', 376843, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70313, 91, 'Jharna Printers [376899]', '193/D Box Calbart Rood ,Panir Tanki, Fakirapool, Dhaka.', 'Md. Ibrahim Khalil', '01714714012', 376899, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70314, 91, 'Green Pack Industries Ltd. [376970]', '34 , West Dharmagonj , Folullah, Narayangonj.', 'Md. Amin Hossain', '01922138871', 376970, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70315, 91, 'Ahian Trade International(Lalbag)', 'Urdu road, lalbag, Dhaka', 'Mr. Mohiuddin', '01843502110', 376971, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70316, 91, 'MAK Packaging & Accessories Ind. [376842]', '111, Borabo ,P.O ,Shakashor ,Mouchak ,Kaliakor , Gazipur ', 'Md Zillur Rahman', '01975016454', 376842, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70317, 91, 'BD PLAST LIMITED [376844]', 'E-06, Mouchak road,(Near mouchak bus stop), Mizmizi, Siddirginj, Narayangonj.', 'Md Shayem Bhuiya', '01835909747', 376844, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70318, 91, 'ANMON PACKAGES (PVT.) LTD [377033]', 'Shimrail, Siddirgonj, Narayanganj.', 'M.N. Sikder Partha', '01678181818', 377033, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70319, 91, 'Standard Trade Centre [377241]', '291, inner cerculer Road, Fokirapul, Dhaka', 'Md. Zakir Hossain', '01713020292', 377241, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70320, 91, 'Fresh Pack Industries(Savar)', 'Niribili, Mirzanagor ,Ashilia, Savar, Dhaka-1344', 'Shahinur Rahman', '01708428933', 376986, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70321, 91, 'Brac Printing Pack [377052]', 'Gazipur Chowrasta ,Shibbari ', 'Komol Kurar Dey', '01718613894', 377052, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70322, 91, 'Johnson-s Pack (Khilkhet)', 'Reliance Printing & Packaging. Barua, Bagan Bari, Khilkhet, Dhaka.', 'Mr. Shabuddin', '01719813216', 377127, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70323, 91, 'Sumaya Packaging [377263]', '91,Korbanigonj, Chittagong', 'Md.Belal Chowdhury (Imtiaz)', '01919417128', 377263, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70324, 91, 'Al Plast Bangladesh Ltd [377145]', 'Store: RIP-MAT-PML. RFL Industrial Park, Mulgaon Kaliganj Gazipur, Dhaka.', 'Mr. Zaman', '01844609390', 377145, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70325, 91, 'Jahangir Lamination [377313]', '291, Inner circular Road, Fokirapul, Motijheel, Dhaka 1000', 'Mr. Jahangir', '0170700891', 377313, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70326, 91, 'Akij Food & Beverage Limited [376450]', 'Barobariya,Krisnapura, Dhamrai,Dhaka.', 'Mr. A.K.M Amdadul Haque', '01728217138', 376450, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70327, 91, 'Parvin Corporation [377239]', '199 West Ashrafabad, Kamrangir Char, Dhaka', 'Shariful Islam', '01815000430', 377239, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70328, 91, 'Classic Foils Ltd [377242]', 'Urdu Road, Dhaka', '', '0', 377242, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70400, 91, 'Premier Flexible Packaging Ltd', '315, Nelson Mandela Expressway,', 'Md. Deloar Hossain', '01771959659', 386634, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70401, 91, 'Arvee Industries Limited(Abeokuta Motor Road)', 'KM 38 Abeokuta Motor Road, Sango Ota Ogun State, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386636, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70402, 91, 'Arvee Industries Limited (Ijoko Road)', 'KM 1 Ijoko Road, Sango Ota, Ogun State, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386636, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70405, 91, 'Sample-Arvee Industries Limited(Motor Road)', 'KM 38 Abeokuta Motor Road, Sango Ota Ogun State, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70406, 91, 'Sample-Abvee Industries Limited(Ijoko Road)', 'KM 1 Ijoko Road, Sango Ota, Ogun State, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70409, 91, 'M/S Sundorban Traders [386644]', '39, Northbroke Hall Road, Banglabazar, Dhaka-1100', 'Md. Hannan Bepari', '01710150590', 386644, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70442, 91, 'Sample for Abul Khair Match Factory Ltd', 'D.T. Road, Pahartali. Chitttagong.', 'A.K.M. Shafique Chowdhury', '01911698003', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70464, 91, 'Sample for Tricepack Ltd', 'Khashpara ,kanchpur,Sonargaon. Narayangong, Bangladesh', 'Mr. Anoop Singh', '08801713431058', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70465, 91, 'Sample for KR Flexipack Ltd', 'South Banshbaria. Sitakunda, Chittagong.', 'Md. Salah Uddin', '08801709399310', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70469, 91, 'AIM Corporation [386692]', '242/A, Fakirapool, Motijheel, Dhaka', 'A.S.M Khaled Abdullah', '01775568162', 386692, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70494, 91, 'Motara Industries Limited [386716]', '25 Morison Cresent, Alausa Ikeja, Lagos, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386716, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70497, 91, 'Sample Delivery(Foreign)-Motara Industries Limited', '25 Morison Cresent, Alausa Ikeja, Lagos, Nigeria', 'Mr. Zahangir Hasan', '01755574727', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70512, 91, 'Sample for Premiaflex Plastics Ltd', 'Kewea, Poshim Khando, Mowna, Sripur. Gazipur', 'Faridul Huq', '08801730373899', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70513, 91, 'Sample for Kalyar Replica Limited', 'H-2, R-54/A, Gulshan 2, Dhaka.', 'Hasan', '08801777707048', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70555, 91, 'Victoria Fashion Accessories Industry', 'Dholadia, Vawal, Rajabari, Sreepur, Gazipur', 'Contact: Shushanto Sarkar', '01708800112', 386768, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70569, 91, 'I B Traders(Dhaka)', 'Fokirapul, Paltan, Dhaka', 'Mr. Chandan', '01708912181', 377136, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70639, 91, 'Bashundhara Paper Mill [Unit # 01)', 'Meghna Ghat Bara Nagar, Sonargaon,  Narayanganj-1441.', ' Mr. Mahfuz', '01991197780', 376508, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70674, 91, 'Montrims Ltd. [386851]', 'Mouchak, Kaliakoir, Gazipur', 'Md. Ilias Hossain', '01938887077', 386851, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70686, 91, 'Shuaiba Industrial Company (K.S.C.) [386868]', 'Block No. 3, Str.: 31, Bldg 150, Subhan Industrial Area, 10088 Shuaiba, Code No. 65451, Kuwait', 'Contact: Md. Mortuza Kashem', '08809602310327', 386868, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70689, 91, 'Sample-Shuaiba Industrial Company (K.S.C.)', 'Block No. 3, Str.: 31, Bldg 150, Subhan Industrial Area, 10088 Shuaiba, Code No. 65451, Kuwait', 'Md. Mortuza Kashem', '08809602310327', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70692, 91, 'Sample for A.J. International', 'Urdu Road', 'Md. Parvez Alam', '0', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70709, 91, 'Texplast Industries Limited. [386878]', 'P.O. Box 43039-00100 GPO, Magana Lane, Off Magana Road, Near Magana Flowers (Rungiri-Regen), Nairobi, Kenya.', 'Contact: Md. Mortuza Kashem', '08809602310327', 386878, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70710, 91, 'Creative Packing Limited. [386879]', 'PO Box 9621, 16/Mbozi Road, Changombe, Dar Es Salaam, Tanzania', 'Contact: Md. Mortuza Kashem', '08809602310327', 386879, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70747, 91, 'Aristo Accessories Industry(Fakirapol)', 'Bijoy Nagar Dhaka.', 'Md. Amzad Hossain', '01680369252', 376794, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70784, 91, 'Minar Printing & Packaging Ltd. [386913]', 'Village: Joynagar, P.O: Alishwar, U.P: Lalmai, Comilla Sadar, Comilla', 'Nizam Uddin', '01865045502', 386913, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70790, 91, 'Pace Tobacco Industries(BD) Ltd. [386920]', 'Plot#16-17, EPZD, Mongla, Bagerhat', 'Fardoush Zahan', '01716407002', 386920, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70796, 91, 'Sample for Pace Tobacco Industries (BD) Ltd.', 'Plot No: 16-17, Mongla EPZ, Mongla, Bagerhath', 'Mr. Fardous Zahan', '01716407002', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70799, 91, 'Sample for Baly Integrated Solutions Ltd.', 'Raj-Fulbaria, Savar, Dhaka', 'Kazi Arikul Alam', '08801918294266', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70800, 91, 'Sample for M.M. Ispahani Limited', 'Ispahani Building, 7th Floor, 14-15, Motijheel C/A.', 'Chandan Saha', '08801937900024', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70819, 91, 'Modhuban Packaging Ind.(PVT.) Ltd. [386965]', 'BSCIC I/E, Block-B, Plot# C05-06, Sholashahar, Nasirabad, Chittagong', 'Sahidul Islam', '01716109011', 386965, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70881, 91, 'Sample-ALVAPACK SRL', 'Centro Direzionale, 40016 Interporto Bologna, San Giorgio DI', 'Md. Mortuza Kashem', '08809602310327', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70890, 91, 'Sun Sine International [387012]', 'Plot# B-38, BSCIC I/A, Ruhitpur, Keraniganj, Dhaka', 'Masab Shahid', '01911367654', 387012, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70909, 91, 'Sample for Akij Printing & Packages Limited', 'Morkun, Tongi, Gazipur.', 'Mr. Md. Mamunul Islam', '08801731470690', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70911, 91, 'Sample for Habiganj Agro Limited', 'Olipur, Shahjibazar, Shaestaganj, Habiganj, Bangladesh.', 'Mr. Rafiul Haque', '08801704132975', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70913, 91, 'Sample for Sun Packaging Co (Mauritius) Ltd.', '', '', '0', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70914, 91, 'Sample-Sun Packaging Co (Mauritius) Ltd.', 'Bpml warehouse no 4. Free Port Zone, Port Louis, Mauritius.', 'Mr. Ramdas Patkar', '02302413763', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70916, 91, 'Sample-Sun Packaging Company SAOC', 'Road 23, Rusay Industrial Estate. PO BOX 125 Russel, Postal Code 124, Sultanate of oman.', 'Mr. Ramdas Patkar', '096824446864', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(70945, 91, 'Ahian Trade International(Chittagong)', 'Shahi Jame Moshjid', 'Md. Monsur Ali', '01816343631', 376971, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71012, 91, 'Sample for Al Mostofa Printing & Packaging Ltd.', 'Islampur,Meghnaghat', 'Nur Alam', '08801755543935', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71013, 91, 'Sample for Dekko Accessories Ltd.', 'Singair Road, Hemayetpur, Savar, Dhaka', 'Shariful Islam', '08801833167964', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71014, 91, 'Sample for Matador Ballpen Industries Ltd.', '102. West Azimpur Road, Dhaka', 'Md. Shah Alam', '08801713556869', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71015, 91, 'Sample for Matador Ballpen Industries Ltd.', '102. West Azimpur Road, Dhaka', 'Md. Shah Alam', '01713556869', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71019, 91, 'Sample for Standard Trade Centre', '291, inner cerculer Road, Fokirapul, Dhaka', 'Md. Zakir Hossain', '08801713020292', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71020, 91, 'Kiam Metal Industries Limited(Dhaka)', '10/B, Road-6, Dhanmondi, Dhaka', 'Mr. Sajedur Rahman Sujon', '01755582820', 387077, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71070, 91, 'Sample for Sumaya Enterprise', 'Niribili mirja nagar , 1344,Ashilia Savar ', 'Shahinur Rahman ', '08801708428933', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71072, 91, 'Sample for British American Tobacco', 'Mohakhali, Dhaka', '', '0', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71081, 91, 'Sample for Johnson-s Pack', '1/H/3 Middle Bashabo, Modina Shorok, Shabujbag, Dhaka-1214, Bangladesh', 'HM Jahangir', '08801711525003', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71153, 91, 'Kiam Metal Industries Limited-Kustia', 'BSCIC I/A , Kustia', 'Mr. Sajedur Rahman Sujon', '01755582820', 387077, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71156, 91, 'Sample for RES Label and Painting Industry', 'Nekrose Bag, Khalindi, Keraniganj, Dhaka-1310', 'Rahat Reza Omi', '08801751632740', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71157, 91, 'Sample-Suma Foods Ltd.', 'Oluyole Estate Ring Road, Ibadan Oyo State, Nigeria', 'V P Sudarshan', '02357056664000', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71197, 91, 'Jamana Printers Limited. [387215]', 'Nyerere Road, Plot No. 09, P.O. Box: 5584, Dar Es Salaam, Tanzania.', 'Contact: Md. Mortuza Kashem', '08801730300333', 387215, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71210, 91, 'Akij Particle Board Mills Ltd. [387225]', 'Akij Economic Zone, Khagatipara, Trishal, Mymensing', 'Mr. Sandip Debnath', '01911810197', 387225, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71231, 91, 'Sisk & Ten Company Limited [387245]', 'No. 15, Nnamdi Adikwu Street, Abuja, FCT, Nigeria', 'Md. Mortuza Kashem', '01730300333', 387245, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71233, 91, 'Amin Corporation [387248]', 'Char Golgolia, South Keraniganj, Dhaka', 'Md. Ruhul Amin', '01309314287', 387248, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71241, 91, 'Sample for Asia Foils', 'Kainjanul, Vowal Mirzapur, Gazipur Sadar, Gazipur-1703', '', '0', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71242, 91, 'Sample for Asia Foils', 'Kainjanul, Vowal Mirzapur, Gazipur Sadar, Gazipur-1703', 'Monirul Islam Ayub', '08801720585902', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71244, 91, 'Asia Foils Limited [387262]', 'Kainjanul, Vowal Mirzapur, Sadar, Gazipur', 'Sharif Ahmed', '01712791154', 387262, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71271, 91, 'Sample for Jharna Printers', 'Panir Tanki, Fakirapool, Dhaka.', 'Md. Ibrahim', '01714714012', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71282, 91, 'Sample for Robin Printing & Packages', 'Robin tower,42/43 Puran Paltan, Dhaka, Bangladesh', 'Md. Rafiqul Islam ( GM Purchase)', '01711634824', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71283, 91, 'Sample for Tampaco Foils Ltd.', '2-3 BSCIC Industrial estate, Tongi, Gazipur.', 'Md Rejaul Kader', '01715475876', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71310, 91, 'Sample-Creative Packing Limited', 'PO Box 9621, 16/Mbozi Road, Changombe, Dar Es Salaam, Tanzania', 'Md. Mortuza Kashem', '08809602310327', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71312, 91, 'Mahtab Flexible Printing Press [387341]', '291, Ashrafabad, Kamrangirchar, Dhaka', 'Mahtabuddin', '01678041680', 387341, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71400, 91, 'Panta Plastics Industries Ltd. [387411]', 'Block#A, Plot# B/4, BSCIC Industrial Area, Bayezid, Nasirabad, Chittagong', 'Md. Nuruzzaman', '01817220337', 387411, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71446, 91, 'Sample for Krishibid Packaging Ltd', 'Choto Kaliakoir, Birulia, Savar, Dhaka', 'Md Alamin ', '01700729780', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71475, 91, 'Sample for BD PLAST LIMITED', 'E-06, Mouchak road,(Near mouchak bus stop), Mizmizi, Siddirginj, Narayangonj.', 'Noor Mohammad ', '01762668877', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71476, 91, 'Sample for  Max Poly Printers ', 'Ashrafabad, Nabab Char, Kamrangir Char, Dhaka', 'Md. Sirajuddin', '01711542604', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71477, 91, 'Sample-Image Masters Ltd.', 'Je Je Depot Gpdown 1, Kipawa Nyerere Road, P.O. BOX-38273, Dar Es Salaam, Tanzania.', 'Ramdas Patkar', '0', 386641, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71478, 91, 'Epic Associates [387452]', '304/E, Munda, Uttarkhan, Dhaka-1230', 'Md. Salauddin', '01730444331', 387452, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71483, 91, 'Banga Plastic International Ltd.(Export) [387458]', 'RFL Plastics Ltd. Bagpara, Palash, Narshingdi.', 'Mr. Masud', '01841014694', 387458, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71489, 91, 'Sample for NRG Printing & Packaging Ltd.', 'Jamirdia, Masterbari, Valuka, Mymonsingh', 'Md. Mojahedul Islam', '08801720566909', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71523, 91, 'Sample for Fresh Pack Industries', '102, Azimpur Road (6th Floor), Dhaka-1205, Bangladesh', 'Md.Ali Hossain', '0880170188822', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71603, 91, 'Unity Enterprise(Dhaka)', '38, Haider Box Lane, Urdu Road, Dhaka-1211', 'Tanvir Alam', '01746252222', 387627, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71614, 91, 'Zas Rotoflex Industries Limited [387639]', 'Chatian, Madaripur, Habiganj', 'Mr. Aziz', '01710178851', 387639, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71665, 91, 'Unity Enterprise(Chittagong)', 'Plot#D-04, 05 BISCIC I/A, Kalurghat, Chittagong', 'Tanvir Alam	', '001746252222	', 387627, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71696, 91, 'Pace Tobacco Industries(BD) Ltd.(Export) [387679]', 'Plot#16-17, EPZD, Mongla, Bagerhat', 'Fardoush Zahan', '01716407002', 387679, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71768, 91, 'Paper King Stationery [387748]', 'PO Box 22654, Dar Es Salaam, Tanzania', 'Md. Mortuza Kashem', '08801730300333', 387748, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71818, 91, 'Akthar Enterprise Ltd. [387800]', 'P.O. Box 4051, Zanzibar, Tanzania.', 'Md. Mortuza Kashem', '08801730300333', 387800, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71819, 91, 'Arbab Pack Limited [387801]', 'Shimrail,Demra road, Shiddirgonj,Narayangonj', 'Zakir Hossain', '01711319158', 387801, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71866, 91, 'Sample for Amin Corporation', '87, Naya Paltan, Dhaka-1000', 'Md. Ruhul Amin', '01309314287', 376425, '2022-01-30 15:42:20', '2022-01-30 15:42:20', NULL),
(71869, 91, 'Sample for The Merchants Ltd.', 'A-99 BSCIC Industrial Estate Tongi Gazipur, Bangladesh', 'Harun Ur Rashid', '01712230225', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71870, 91, 'Sample for Arbab Pack Limited', 'Shimrail,Demra road, Shiddirgonj,Narayangonj', 'Zakir Hossain', '01711319158', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71905, 91, 'Mahiyat Enterprise [387865]', '1/9, Sher-e-Banla Market, 2, No. Rail Gate, Narayanganj', 'Md. Mosharrar Hossain', '01985009686', 387865, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71906, 91, 'Habiganj Agro Limited (HIP)', 'M M Store,Habiganj Industrial Park, Olipur,Shahjibazar,Shastagong,Habigonj', 'Md. Foyez', '01704132383', 376777, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71912, 91, 'Time Pack Industries [387877]', 'Dalda Road, Panchabati, Narayanganj', 'Md. Anwar Hossain', '01924745262', 387877, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71914, 91, 'M Corporation (Dhaka)', '38, Hayder Box Lane, Chawakbazar, Dhaka-1211', 'Tanveer Alam', '01746252222', 387883, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71930, 91, 'M/S Mukta Printing & Packaging [387932]', 'House#7, Road#5, Block#A, Chunkutia Southpara, Suvadda, Keraniganj, Dhaka', 'Md. Haji Motiur Rahman', '01647683058', 387932, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71940, 91, 'Taj Accessories [387951]', '74, Rahmatbag, Kamrangirchar, Dhaka', '', '0', 387951, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71968, 91, 'AGI FLEX LTD [387986]', '69/8, maddha rajason,saver,Dhaka', 'Nur-a-alam', '01727761528', 387986, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71976, 91, 'Sample-Express Flexi Pack', 'Nirobi', '', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71977, 91, 'Sample-H Plastiques CI', 'Nirobi', '', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(71978, 91, 'Sample-Trinity Trading', 'Srilanka', '', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72001, 91, 'Sample-Mars Middle East Trading FZC', 'Q4-111, Saif Zone, PO BOX:7491, Sharjah, UAE', 'Mr. Ramesh Arvined', '0971504622783', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72052, 91, 'GHORASHAL MULTILAYER PLASTIC PACKAGING LTD. [389072]', 'Bhagdi, Ghorasal; Palash PS; Narshingdi-1613; Bangladesh', 'Mr Nowshad', '01717308767', 389072, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72060, 91, 'Magdata S.P.A [389081]', 'Strada Della Selva, 100/2, 43052 Colorno (PR), Partita, Italy.', 'Contact: Md. Mortuza Kashem', '01730300333', 389081, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72062, 91, 'Mars Middle East Trading FZC (Kenya)', 'CIF Mombasa, Kenya', '', '0', 386298, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72063, 91, 'Mars Middle East Trading FZC(Saudi Arabia)', 'CIF Zeddah, Saudi Arabia', '', '0', 386298, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72076, 91, 'Al-Arafah Packaging Ind.Ltd. [389100]', '1/1,Bashura,Board Bazar, National University, Gazipur Sadar,Gazipur-1704', 'Mohammad Ali Mazumder Nipu ', '01740919108', 389100, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72169, 91, 'Ghandoura Industrial Group Co. Ltd. [389157]', 'Jeddah Industrial City-2, PO Box 30495, Jeddah', 'Md. Mortuza Kashem', '01730300333', 389157, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72171, 91, 'Sample for Nestle Bangladesh', 'Dhaka', '', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72172, 91, 'Sample for Japan Tobacco International Bangladesh', 'Gazipur', 'Md.  Shoyeb Kabir', '08801713080147', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72173, 91, 'Flour Mills of Nigeria [389159]', '34 Eric Moore Road, Lagos, Nigeria', 'Md. Mortuza Kashem', '01730300333', 389159, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72185, 91, 'Greycom Resourses LLP [389178]', 'Unit 5, Olympia Industrial Estate, Coburg Road London, England N22 6TZ, UK', 'Md. Mortuza Kashem', '01730300333', 389178, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72192, 91, 'Sample-Flour Mills of Nigeria', '34 Eric Moore Road, Lagos, Nigeria', 'Rajat Rath', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72255, 91, ' Mars Middle East Trading FZC-United Arab Emirates', 'Q4-111, Saif Zone, PO Box: 7941 Sharjah, United Arab Emirates.', 'Md. Mortuza Kashem', '01730300333', 386298, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72266, 91, 'CDM Packaging', 'CDM Sp. Z O. O. Cegieniana 7, 95-054 Ksawerów, POLAND', 'Md. Mortuza Kashem', '01730300333', 389272, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72276, 91, 'Sample-CDM Sp. Z O. O.', 'Cegieniana 7, 95-054 Ksawerów, POLAND', 'Mortaza Kashem', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72277, 91, 'Sample-Paper King Stationery', 'P.O.Box 22654, Dar Es Salaam, Tanzania ', 'Mortaza Kashem', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72282, 91, 'Sample for Akij Poly Fibre Industries Ltd', 'APFIL Factory', 'Mr. Masudur Rahman', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72283, 91, 'Sample for Kishwan Food', 'Dhaka', '', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72297, 91, 'Avera Technologies Limited. [389299]', '40, Adisa Bashua Street, Surulere, Lagos, Nigeria', 'Md. Mortuza Kashem', '01730300333', 389299, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72298, 91, 'Waled Kilani & Partners Ind. Co. [389300]', 'Al-Muaqqar Industrial Zone, PO Box: 950747, Amman 11195, Jordan', 'Md. Mortuza Kashem', '01730300333', 389300, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72300, 91, 'Color Print & Packaging Industries [389302]', 'dhop', '', '0', 389302, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72301, 91, 'Color Print & Packaging Industries [389302]', 'Dhopa Para, Dokkin Kawli, Pahartoli, Chittagong', 'Gopal Nath', '01713403660', 389302, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72397, 91, 'Sun Packaging Co (Mauritius) Ltd. [389354]', 'Warehouse No. 4, Freeport Zone 6, Mer Rouge, Port Louts Mauritius.', 'Md. Mortuza Kashem', '01730300333', 389354, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72426, 91, 'Rotogravure Industrial Investment [389374]', '6th of October City, 3rd Industrial Zone, N025, Giza, Egypt', 'Md. Mortuza Kashem', '01730300333', 389374, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72427, 91, 'Coatall Films Pvt. Ltd. [389375]', 'Plot No: 10, Sector 1, AURIC Shendra, Aurangabad- 431 154 India', 'Md. Mortuza Kashem', '01730300333', 389375, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72428, 91, 'Sample-Integrated Plastics Packaging LLC', 'National Industries Park, Jabal Ali, PO Box: 37607, Dubai, UAE', 'Md. Mortuza Kashem', '01730300333', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72429, 91, 'Sample-First Pack Factory W.L.L', 'New Industrial Area, Jahil Factory No. 20, PO Box: 22197, Doha, Qatar', 'Md. Mortuza Kashem', '01730300333', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72430, 91, 'Sample-Soft Packaging', 'Andera, Gasabo, Kigali Town, Kigali, Rawanda', 'Md. Mortuza Kashem', '01730300333', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72440, 91, 'Mastul Graphics & Printers [389389]', 'Tepir Bari, Shishu Polli Road, Tengra, Sreepur, Gazipur', 'Mohammad Abdul Awal', '01713049965', 389389, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72450, 91, 'Sample for Danish', 'Dhaka', '', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72472, 91, 'Sumaya Enterprise (Kamrangirchar)', 'Plot-8, Jhawchar, Kamrangirchar, Dhaka-1211', 'Mr. Shahin', '01703644444', 386509, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72525, 91, 'M.P. Packeging [389478]', '31, Hosnidalan, Dhaka', 'Ripon Chandra Saha', '01721768749', 389478, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72546, 91, 'Johnson-s Pack (Demra)', 'Basherpool, Eastern Housing, Near Shadhur Matth, Demra, Dhaka-1362', 'Mr. Tuhin', '01712555623', 377127, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72551, 91, 'Sample for Virgo Tobacco Limited', 'House#178, Road#02, Baridhara DOHS, Dhaka-1206', 'Md. Akter Hossen', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72719, 91, 'Sample-Flexible Industrial Packages Company (S.A.O.C)', 'PO Box: 60, PC:124,Muscat-Sultanate of Oman', 'Amit Kumar Singh', '096824449121', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72720, 91, 'Sample for Bengal Flexipack Ltd', 'Domna, Kashimpur, Gazipur', 'Dinesh Sarker', '01313450031', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72721, 91, 'Sample for Tara Group', 'SA Paribahan, Bhairab Bazar', 'Mr. Sharif Jony', '01788898904', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72728, 91, 'Sample for Akij Food & Beverage Limited', 'Barobariya,Krisnapura, Dhamrai,Dhaka.', 'Mr. A.K.M Amdadul Haque', '01728217138', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72810, 91, 'Nazia Enterprise\r\n', 'Trishal, Mymansing\r\n', 'NULL', '001713680723\r\n', 6228, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(72821, 91, 'General Shop M/S Shah Enterprise (W)', 'ABCD', 'NA', '00', 377059, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73059, 91, 'Sample for Global Leaf Tobacco Co. Ltd.', 'House-26, Road-28, Block-K, 2nd Floor, Banani, Dhaka-1213', 'Suman Barua', '01714087625', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73113, 91, 'H Plastiques CI', 'Anyama Carrefour Kouresh, 09 Bp 1886, Abidjan 09, Ivory Coast', 'Md. Mortuza Kashem', '01730300333', 391007, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73141, 91, 'Tara Traders Link', 'Eidgah, Kacha Rasta, Yaqub Ali School Road, Rampur, Halishahar, Chittagong.', 'Imran Chaudhury', '01305599949', 389806, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73205, 91, 'Mars Middle East Trading FZC-Zimbabwe', 'Harare, Zimbabwe', 'Md. Mortuza Kashem', '01730300333', 386298, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73207, 91, 'Ahian Trade International(Ashuliya)', 'Tamim Enterprise, Hemayetpur Bus Stand, Savar, Ashuliya', 'Mr. Tamim', '01715527351', 376971, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73209, 91, 'Taher Packaging', 'Plot # M-36, Notun Sonakanda, Biscic, Keraniganj', 'Md. Afjal', '01722203506', 391088, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73211, 91, 'Sample for PRAN-RFL Group', 'PIP, Bagpara, Palash, Narshingdi', 'Mr. Fahim', '01313071112', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73245, 91, 'M/S Shah Enterprise (W)', 'Raymoni, Trishal, Mymensingh', 'Md. Aftab Uddin Master', '001704015437	', 377059, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73325, 91, 'Sample-MAG Data S.P.A', 'Strada Della Selva, 100/2, 43052 Colorno (PR), Partita, IVA/Cod. Fiscale 02551830348, Italy', 'Mujeebur Rahman', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73326, 91, 'Sample-M/S Sim Films', 'Plot No. A-99, Sector: N-1, SIDCO, Aurangabad, 431003, India', 'Dr. Mishra', '0919881711225', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73327, 91, 'Sample-FSD-Foil-Service-Dtl. GmbH', 'Im Bulloh 25, 29331 Lachendorf', 'Mr. Mujeeb', '0495145285950', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73399, 91, 'Sample-Soretrac', 'ZAC de la Villette aux Aulnes 77290 Mitry-Mory, France', 'Paul Emile Kahn', '0Tel: +33 (0) 160217100, GSM: +33 (0) 664478800', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73417, 91, 'Sample for Montrims Ltd.', 'Mouchak, Kaliakoir, Gazipur', 'Md. Ilias Hossain', '01938887077', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73472, 91, 'Arafat Enterprise', '101/1 K.P Ghosh Street, Armanitola, Dhaka-1100', 'Arafat Hossain', '01914282806', 391243, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73703, 91, 'Vision Carton & Accessories Ind. Ltd.', '160/2, South Shalna, Shalna Bazar, Joydebpur, Gazipur', 'Md Aminul Islam', '01724400169', 391365, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73722, 91, 'Sample for Rapid Pack', 'Dhaka, Bangladesh', 'Mr. Mizan', '01722946044', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73747, 91, 'Al-Faruque Packaging', 'Shantahar Road Shikor Narhot, Kahalu, Bogura', 'Md. Redwanul Kabir Chowdhury', '01926998891', 391329, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73911, 91, 'Sample-Faris Matooq Maseeri Trading', 'Ali Ibn Abi Talib Street Mlaz, Saudi Arabia, Riyadh', 'Abdul Manaf', '0966558835399', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73912, 91, 'Sample for Bright Plastic Industries', '132, Tejgaon Industrial Area', 'Mr. Khatib', '01713458679', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73962, 91, 'Sample for Consort Flexi Pack Limited', 'B-47-49, BSCIC Industrial Estate, Kalurghat, Chittagong', 'Anisul Islam Anis', '01816249812', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(73963, 91, 'Sample for Unilever Tea Factory', '83, Nasirabad I/A, Bayized Bostami Road, Chittagong', 'Md Anwarul Islam', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74075, 91, 'Infinity Flexi Pack Ltd.', 'Plot # A-44 Bscic I/A, Mukterpur, Munshiganj', 'Samaresh Saha Bappi', '01730937204', 391532, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74107, 91, 'Qatar Development Bank', 'PO Box 22789, Grand Hamad Street, Doha, Qatar', 'Md. Mujeeb Ur Rahman', '08801713998738', 392609, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74108, 91, 'Sample for Moslema Traders', '7/9-A South Mugda, PS-Shabujbagh, P.O.-Basaboo, Dhaka-1214.', 'Md. Asad Miah', '01680564433', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74124, 91, 'Jordan Shareef Plastic Factory', 'B#5, Al Riba St, Marka Ash, Shamaliyya, Amman, Jordan', 'Md. Mujeeb Ur Rahman', '01713998738', 392666, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74129, 91, 'Ideal Fibre Industries Ltd.', '13/3 (Chiarman Bhaban) B.B. Road, Nitaigonj, Narayangonj.', 'Mohammad Zakir Hossen', '01711537154', 392634, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74159, 91, 'Sample-Fujairah Plastic Industries', 'PO BOX: 83568, Al Ain, UAE', 'Mr. Gurpreet', '097137847388', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL);
INSERT INTO `tbldispoint` (`intdispointid`, `intunitid`, `strname`, `straddress`, `strcontactperson`, `strcontactno`, `intcustomerid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(74177, 91, 'Abul Khair Match Factory Ltd.', 'Sonali Tobacco, Tongi, gazipur', 'Mr Rojder', '01728049532', 392715, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74188, 91, 'H P Packaging', '107/43, Rangarajapuram Main Road, Kodambakkam, Chennai-600024, India', 'Md. Mujeeb Ur Rahman', '01713998738', 392725, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74200, 91, 'Bengal Flexipack Ltd. Unit-1', 'Rajfulbaria, Savar, Dhaka', 'Mr. Faruk', '01313450046', 376526, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74244, 91, 'Akij Biri Factory Ltd.', 'Navaron, Jhikargachha, Jessore', 'Mr. Shanto Kumar Shaha', '01735436317', 392785, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74248, 91, 'Canton Paper Converting & Packaging', 'Shitakunda, Chittagong, Bangladesh', 'Mitul Hasan', '01973163020', 392795, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74262, 91, 'Hamim Plastic International', '72, Siraj Nagar, Al hera Community Centre Kamragirchor Dhaka', 'Md. Motaleb Hossain', '01715956665', 386784, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74294, 91, 'Metro Trade & Commercial Service Ent', 'Plot No. 20, Block No. 7 Garry Free Zone, Sudan', 'Md. Mujeeb Ur Rahman', '01713998738', 392880, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74298, 91, 'Sample for Mala Traders', 'Nirashpara, Amtoli, Tongi, Gazipur', 'Mr. Borhan', '01676179475', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74325, 91, 'Fazlu Enterprise', '31, Hosni Dalan, Chawk Bazar, Dhaka-1211', 'Md. Fazlu', '01771590366', 392917, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74341, 91, 'NRB Desiccant Industries', 'Tansy Tape Industries, 18/2/A, Goran Chotbari, Mirpur-1 Beribadh (Near Tamanna Park)', 'Mr Akbar Hossain', '01674914719', 392818, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74342, 91, 'Power Pack Corporation', 'Baliakhora, Ghior, Manikganj', 'Mahidul Bhuyan', '01711196584', 392916, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74356, 91, 'Sample-Al Tayar Plastic & Rubber Manufacturing Co. Ltd', 'Industiral Area 1, Phase 5, Jeddah 21428, Saudi Arab', 'Mr. Ali Ibrahim Shehata', '0966126086917', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74357, 91, 'Sample-Flexible Industrial Packaging Co. (SAOC)', 'Plot # 225/226, Road# 21, Rusayl Industrial Estate, Oman', 'Amit Kumar', '096824446847', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74358, 91, 'Sample-Dhalumal Packaging Industries LLC', 'Plot 114 (A), Al Ghail Industrial Park, Ras Al Khaimah, UAE', 'Mr. Naresh Kumar', '0-72219840', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74359, 91, 'Sample-Middle East Plastic Bags Industries', 'PO Box: 36604, Al Ghail Industrial Zone, NFZ, Plot # R121', 'Mr. Robin', '0# 00971526590350', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74360, 91, 'Sample for Ispahani', 'Dhaka', '', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74393, 91, 'M Corporation (W)', '38, Hayder Box Lane, Chawakbazar, Dhaka-1211', 'Alim', '01719624193', 393000, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74396, 91, 'Sample for Thai-Foils & Polymer Industries Ltd.', 'Islampur, Meghnaghat, Narayanganj', 'Nur Alam', '01755543935', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74397, 91, 'Sample for Al Plast Bangladesh Ltd.', 'Store: RIP-MAT-PML, RFL Industrial Park, Mulgaon, Kaliganj, Gazipur', 'Mr. Zaman', '01844609390', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74419, 91, 'Sample for G.H Traders', 'Islampur Road, Zindabahar, Dhaka', 'Abdus Sobhan', '01819495166', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74486, 91, 'M/S SINTHIA ENTERPRISE', '75, South Mugdapara, Dhaka-1214', 'Sajjad Hossain', '01712170535', 393048, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74522, 91, 'M Corporation (Chittagong)', 'Plot#D-04, 05 BISCIC I/A, Kalurghat, Chittagong', 'Tanvir Alam', '001746252222	', 387883, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74567, 91, 'Sample-Al Nojoom', 'Al Saulai, Behind the driving School, Dallah, Kingdom of Saudi Arabia, Riyadh', 'Mr. Ayoub', '0(+) 966504197620', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(74568, 91, 'Sample-Nisreen Akram Al Kahlout Trading Est', '6953. Al Bateha Valley Al Malqa Ar Riyadh 13521, Saudi Arabia, Rami Salah', '', '0(+) 966557955002', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75643, 91, 'Sample-Nouvelle Mici-Embaci', '11 BP 2551 Abidjan 11, Zone Industrielle De Koumassi, Cote D-Ivoire (Ivory Coast)', 'Mr. Benoit Levais', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75733, 91, 'Casio Trims & Accessories Ltd.', 'Kakab, Birulia, Savar, Dhaka', 'Sheikh Nasir Hossain', '01740927584', 395839, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75767, 91, 'K-Flex Limited', 'Boro Bamonda Road, Kotchandpur, Jhenidah, Bangladesh', 'Bishwajit Kumar Dutt', '01714096140', 395892, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75774, 91, 'Technoval Laminados Plasticos LTDA', 'Rua Leonardo Costa Gonclaves 1100-Parque Monte Verde Minas Gerais, Brazil', 'Md. Mujeeb Ur Rahman', '01713998738', 395927, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75860, 91, 'Sample for A-One International', '19/A, Joy Chandra Nag Road, Chakbazar, Dhaka (Akij House)', 'Mr. Wasim Ahmed', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75861, 91, 'Sample for A-One International', '19/A, Joy Chandra Nag Road, Chakbazar, Dhaka (Akij House)', 'Mr. Wasim Ahmed', '01715200358', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(75934, 91, 'Soretrac', 'Zac de la Villette aux Aulnes, Rue rene Cassin, 77290 Mitry-Morry, France', 'Md. Mujeeb Ur Rahman', '01713998738', 396116, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76081, 91, 'Al-Amin Enterprise', '425, Ashrafabad, Kamrangir Char, Dhaka', 'Gazi Abdulla Al-Amin', '01839291882', 396256, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76094, 91, 'Shree Shakti Poly Films Pvt. Ltd.', 'Unit No. 1, Bldg No. 8, Abhilasha Industrial Estate, Near Burma Shell Petrol Pump, Opp Western Express Highway, Village-waliv, Vasai (East). Dist. Thane, India', 'Md. Mujeeb Ur Rahman', '01713998738', 396304, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76098, 91, 'Rajasthan Flexible Packaging Ltd.', 'Pathredi Road, Village- Patherdi, Tehsil Kotputli, Dist.: Jaipur (Rajasthan) 303107, India', 'Md. Mujeeb Ur Rahman', '01713998738', 396302, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76124, 91, 'Hamim Plastic International', '72, Siraj Nagar, Al hera Community Centre Kamragirchor Dhaka', 'Md. Motaleb Hossain', '01715956665', 396332, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76290, 91, 'Sample for Meghna Foils', 'Narayanganj, Dhaka', '', '0', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76291, 91, 'Sample for I B Traders', 'Fokirapul, Paltan, Dhaka', 'Mr. Chandan', '01708912181', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76292, 91, 'Sample for G.I.P Trade House', '227/1, Fokirapol, Motijheel, Dhaka-1000', 'Md. Mostofa', '01715940223', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76504, 91, 'Suncoast Incubuos Limited', '40 Adisa Bashua Street, Surulere, Lagos, Nigeria', 'Md. Mujeeb Ur Rahman', '01713998738', 396612, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76506, 91, ' S Alam Agency ', 'C-159, Bscic Industrial Estate, Tongi, Monnunagar, Gazipur-1710', 'Kabir Hossain Shimul', '01863638590', 395924, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76510, 91, 'Al-Amin Enterprise', '425, Ashrafabad, Kamrangir Char, Dhaka', 'Gazi Abdulla Al-Amin', '01839291882', 396610, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76514, 91, 'Glorious Enterprise', 'Alif Biponi Bitan, 31 N.A. Chowdhury Road, Chattogram', 'Mr. Shonjoy', '01740988463', 396608, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76556, 91, 'Amenaz Concept', '98/1, Nazimuddin Road, Chawkbazar, Dhaka', 'Md. Ahsan Habib', '01716139215', 396609, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76678, 91, 'Meghna Foil Packaging Ltd.', 'Meghna Industrial Economic Zone, Tipordi, Mograpara, Sonargaon, Narayangonj', '', '01713141974', 396828, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76782, 91, ' Flexible Packaging Company Ltd. ', '1st Industrial City, Phase-4, PO Box: 4029, Jeddah 21491, Saudi Arabia', 'Md. Mujeeb Ur Rahman', '01713998738', 396920, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(76783, 91, 'Abu Dhabi Islamic Bank', 'P.O. Box 313 Abu Dhabi United Arab Emirates', 'Md. Mujeeb Ur Rahman', '01713998738', 396920, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(77194, 91, 'De United Foods Industries Limited', '44 Jimoh Odutola Stree, Off Eric Moore Road, Surulere, Lagos, Nigeria', 'Md. Mujeeb Ur Rahman', '01713998738', 398509, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(77218, 91, 'Shahjalal Trading', '8 No. Arambagh, Motijheel, Dhaka', 'Md. Nur Nobi', '01739079069', 398586, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(77256, 91, 'Sample for ABFL HO', 'Akij House, 198, Bir Uttam Mir Shawkat Sharak, Tejgaon Link Road, Dhaka-1203.', 'Sk. Md. Saadullah', '01713998743', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(77554, 91, 'Kishwan Agro Products Limited', 'Koripara, Chandpur, Natore', 'Md. Shahidul Islam', '01755695826', 398811, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(78314, 91, 'Bombay Sweets and Co. Ltd. (Unit - 2)', 'Khashpara, Kanchpur, Sonargaon, Narayanganj.', 'Md. Rashedul', '01731402804', 398997, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(78388, 91, 'Sample-Focke & Co. (GmbH & Co. KG)', 'Max-Planck-Strasse 110, 27283 Verden, Germany', 'Jan-Harm (Complex No. F3_350-401-404)', '0', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(78405, 91, 'Sample for Brac Printing Pack', 'Gazipur Chowrasta ,Shibbari', 'Komol Kurar Dey', '01718613894', 376425, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL),
(78419, 91, 'Sample for AM Logistics', 'ITC LTD #93/1, Karl Marx Sarani, Kidderpore, Kolkata,700043, India', 'Mr. Sanjay Paul', '09831149442', 386641, '2022-01-30 15:42:21', '2022-01-30 15:42:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblemployees`
--

CREATE TABLE `tblemployees` (
  `id` int(11) NOT NULL,
  `enroll` int(20) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `parmanentaddress` text DEFAULT NULL,
  `presentaddress` text DEFAULT NULL,
  `contactno` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblemployees`
--

INSERT INTO `tblemployees` (`id`, `enroll`, `code`, `name`, `email`, `parmanentaddress`, `presentaddress`, `contactno`, `created_at`, `updated_at`, `deleted_at`) VALUES
(218, 448123, 'ABFL-360', 'Ahasanul Tariq Ibne Hoq', '0', 'Mymensingh', 'ABFL, Trishal', '01622755767', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(219, 476643, 'ABFL-547', 'Md. Amirul Islam', 'Na', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(220, 476683, 'ABFL-559', 'Md Riaj Howlader', 'Na', 'Borguna', 'm', '01719373179', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(221, 476684, 'ABFLC-560', 'Abdul Baten', '0', 'Mymensingh', 'ABFL, ', '01982705606', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(222, 527700, 'ABFL-917', 'Mohindro Nath Ray', 'NA', 'Lalmonirhat', 'ABFL, Trishal', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(223, 527701, 'ABFL-918', 'Md. Kawsar Ahmed', 'Na', 'Tangail ', 'ABFL, Trishal', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(224, 527702, 'ABFL-919', 'Jasim Uddin', 'NA', 'Trishal', 'ABFL, Trishal', '01703337052', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(225, 527087, 'ABFL-862', 'Ismail Hossen', 'NA', 'Trishal', 'ABFL, Trishal', '01516019113', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(226, 527088, 'ABFL-863', 'Ashikuzzaman', 'NA', 'Trishal', 'ABFL, Trishal', '01823207734', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(227, 527089, 'ABFL-864', 'Rajib Sorkar', 'NA', 'Trishal', 'ABFL, Trishal', '01734241735', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(228, 416383, 'ABFL (Mass-1)', 'ABFL (Mass-1)', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(229, 416384, ' Mass', ' Mass', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(230, 506436, 'ABFL-688', 'moinul', '', '0', '0', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(231, 527617, 'ABFL-899', 'Md. Sujon Badshah', 'NA', 'Trishal', 'ABFL, Trishal', '01401504520', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(232, 527619, 'ABFL-900', 'Halim Fakir', 'Na', 'Mymensingh ', 'ABFL, Trishal', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(233, 490026, 'ABFLC-608', 'Minhaz Uddin', 'n', 'Vill: MagurJor,PO:Horirampur,Trishal,Mymensingh', 'N', '01741648422', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(234, 490031, 'ABFLC-609', 'Md Ubidullah', 'Na', 'Vill:Magurjora,PO:Horirampur,Trishal,Mymensingh', 'N', '01793441299', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(235, 490033, 'ABFLC-611', 'Rakibul Hasan', 'N', 'Vill:Khaghati,PO: Raymoni,PS:Trishal,Mymensingh', 'N', '01749561200', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(236, 423457, 'ABFL-222', 'Md. Faruk Ahmed', '0', 'Cumilla', 'ABFL', '01726374084', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(237, 471391, 'ABFL-484', 'Md. Reggak', 'Na', 'Raymoni, Trishal', 'ABFL', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(238, 527629, 'ABFL-906', 'Omit', '0', 'KKK', 'ABFL', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(239, 490032, 'ABFLC-610', 'Md Washim Mia', 'N', 'Vill:Magurjor,PO:Horirampur,Trishal,Mymensingh', 'N', '01753686956', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(240, 458677, 'ABFL-403', 'Bishwajit das', '0', 'Rajshahi', 'ABFL, Trishal', '01745355765', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(241, 458681, 'ABFL-404', 'Nazmul Hossain', '0', 'Trishal', 'ABFL, Trishal', '01833455209', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(242, 458683, 'ABFL-405', 'Md. Rana Miah(3)', '0', 'Trishal', 'ABFL, Trishal', '01991272922', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(243, 458686, 'ABFL-406', 'Md. Baddol Hossain', '0', 'Trishal', 'ABFL, Trishal', '01784261809', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(244, 458689, 'ABFL-407', 'Sabbir Ahmed ', '0', 'Trishal', 'ABFL', '01777959065', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(245, 366383, 'ABFL-146', 'Md. Al-Amin Islam', '0', 'Gaibanda.', 'ABFL, Trishal, Mymensingh.', '01748743545', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(246, 490085, 'ABFLC-612', 'Md Azizul Huq', 'N', 'Vill:Gopalpur,PO:Kashiganj,Trishal,Mymensingh', 'N', '01850555665', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(247, 501481, 'ABFL-661', 'Md Helal Mia', 'N', 'Vill:Hurara Kul,PO:Sakir,PS:Chwnarghat,Hobiganj.', 'ABFL Factory', '01767833384', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(248, 415016, 'R2073', 'Mohammad Ahsanuzzaman', 'ahsan.abfl@akij.net', 'House: 161, Actroy Mor, Kazla, Rajshahi.', 'House No # 23, Road No#03, Srctor no#11, Airport Road, Uttara, Dhaka', '01717015511', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(249, 523125, 'ABFL-778', 'Shahajan', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(250, 473780, 'ABFL-506', 'Md Afzal Hosain', 'n', 'Vill:Komorpur,PO: Pirgacha Bazar,Noldanga,Nator.', 'ABFL', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(251, 490101, 'ABFL-613', 'Md. Sobuj Hossain', '0', 'Natore', 'ABFL, Factory', '00', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(252, 491937, 'ABFL-627', 'Arzabul Islam', 'Na', 'Jhenaidah', 'ABFL, Factory', '01905271829', '2022-01-29 23:34:28', '2022-01-29 23:34:28', NULL),
(253, 527730, 'ABFL-920', 'Hasanul Mashrur', 'Na', 'Faridpur', 'ABFL, Trishal', '01789214856', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(254, 490103, 'ABFL-614', 'Minhazul Islam', '0', 'Trishal', 'ABFL, Trishal', '01893286236', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(255, 490187, 'ABFL-615', 'Rehana Begum-1', 'NA', 'Bagerhat ', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(256, 416385, 'APFL Store', 'APFL Store', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(257, 416386, 'APBML Store ', 'APBML Store mass', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(258, 416387, 'ABFL (Mass-5)', 'ABFL (Mass-5)', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(259, 416389, 'ABFL (Mass-6)', 'ABFL (Mass-6)', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(260, 522958, 'ABFL-GT-774', 'Md. Habebur Rahman', 'Na', 'Mymensingh', 'ABFL, Trishal', '01791387620', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(261, 515302, 'ABFL-739', 'Mohammudullah', 'Na', 'Trishal', 'ABFL,Trishal', '01632417187', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(262, 515326, 'ABFL-741', 'Tanvir Saiful Shah Tuhin', 'tanvir.abfl@akij.net', 'Munshigonj', 'ABFL, Trishal', '01674793631', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(263, 507543, 'R2622', 'Md. Morshed Alam', '', 'Vill: Varpara, P.O: Dottapara, P.S: Lakhipur Sadar, Dist: Lakhipur.\r\n', 'Vill: Varpara, P.O: Dottapara, P.S: Lakhipur Sadar, Dist: Lakhipur.\r\n', '01718912812', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(264, 458553, 'C195', 'Nowrose Jahan', 'nowrose.corp@akij.net', 'Vill: 10 b Fakirbari, P.O: Mirpur, P.S: Mirpur, Dist: Dhaka.', 'Vill: 10 b Fakirbari, P.O: Mirpur, P.S: Mirpur, Dist: Dhaka.', '01852003423', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(265, 379355, 'ABFL-171', 'Md. Jahirul Islam', '0', 'Naryanpur, Raimoni, Trishal, Mymensingh.', 'ABFL, Trishal.', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(266, 467782, 'ABFL-476', 'Halal Uddin', '0', 'Trishal', 'ABFL, Trishal', '01743369295', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(267, 433286, 'ABFL-298', 'Md. Abdul Aziz', 'abdul.abfl@akij.net', 'Brhamanbaria', 'ABFL, Trishal', '01309342694', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(268, 352517, 'ABFL-130', 'Md. Suhel', '', 'ABFL', 'ABFL', '01852068488', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(269, 402908, 'ABFL-207', 'Sheuli Begum', '0', 'Trishal, Mymensingh', 'Trishal, Mymensingh', '01754658134', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(270, 431526, 'ABFL-253', 'Sekandar Ali Mamun', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(271, 431535, 'R2148', 'Mr. Sk. Md. Saadullah', 'saadullah.abfl@akij.net', '\"\"120-B, Malibagh Chowdhury Para,\r\nDhaka-1210\"\"\r\n', '\"\"120-B, Malibagh Chowdhury Para,\r\nDhaka-1210\"\"\r\n', '01750420900', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(272, 422997, 'ABFL-219', 'Fakhrul Islam', '0', 'Trishal,Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(273, 498671, 'ABFL-656', 'Md. Khurshed Alam', 'Na', 'Trishal, Mymensingh', 'Trishal', '01750251087', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(274, 527287, 'ABFL-877', 'Abu Nayeem', 'Na', 'Trishal', 'ABFL, Trishal', '01304996196', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(275, 527288, 'ABFL-878', 'Md. Mehedi Hasan2', 'NA', 'Rangpur', 'ABFL, Trishal', '01735078489', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(276, 431531, 'R2150', 'Mr. Md. Monowarul Islam', 'monowarul.abfl@akij.net', '\"\"Vill:  Parbotipur, P.O: Rangpur  Upasahar-5401,\r\nP.S: Rangpur Sadar, Dist: Rangpur\"\"\r\n', '\"\"Flat No: 5M, Maltiplan Red crescent City,\r\nZoo road, Mirpur 1, Dhaka1216\"\"\r\n', '01722668394', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(277, 431534, 'R2149', 'Md. Mahedi Hasan ', 'mehedi.abfl@akij.net', '\"\"Vill: Hugli, P.O: Brommaour, \r\nP.S: Shodor, Dist: Noakhali\"\"\r\n', '\"\"Vill: Hugli, P.O: Brommaour, \r\nP.S: Shodor, Dist: Noakhali\"\"\r\n', '01708378392', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(278, 431536, 'R2151', 'Ershadul Rahman Refat', 'refat.abfl@akij.net', '\"\"Vill: Station Road, P.O: Netrokona, \r\nP.S: Netrokona, Dist: Netrokona.\"\"\r\n', '\"\"Vill: Station Road, P.O: Netrokona, \r\nP.S: Netrokona, Dist: Netrokona.\"\"\r\n', '01727920606', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(279, 349978, 'ABFL-122', 'Md. Sekandar Ali Mamun', '', 'Horirampur, Trishal, Mymensingh.', 'ABFL, Trishal.', '01999870586', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(280, 527352, 'ABFL-879', 'Arif Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01610724539', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(281, 443710, 'ABFL-354', 'Md. Kabir Hossain', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(282, 527442, 'ABFL-882', 'Md. Abdullah Al Mamun', 'Na', 'Sherpur', 'ABFL, Trishal', '01927681572', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(283, 431567, 'ABFL-254', 'Muksadul Momin', '0', 'Trishal ', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(284, 431574, 'ABFL-256', 'Alhaj Mia', '0', 'trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(285, 431582, 'ABFL-257', 'Amirul Islam', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(286, 448277, 'ABFL-363', 'Ibrahim Khalil', '0', 'Trishal, ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(287, 365343, 'ABFL-144', 'Md. Masud Rana', '', 'ABFL', 'ABFL', '01788784786', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(288, 443708, 'ABFL-353', 'Md. Jayed Bin Shorif', '0', 'Faridpur', 'ABFL, Trishal', '01315057275', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(289, 423092, 'ABFL-220', 'Md. Asikur Rahman', '0', 'Trishal', 'ABFL, Trishal', '01775027638', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(290, 423118, 'ABFL-221', 'Md. Khalil Foraygi', 'khalil.abfl@akij.net', 'Madaripur', 'ABFL', '01782464063', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(291, 431570, 'ABFL-255', 'Abul Kalam', '0', 'Trishal ', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(292, 488791, 'ABFL-606', 'Al Mahen', 'Na', 'Vill:Bagan,PO:Bagan,Trishal,Mymensingh', 'DO', '01953212788', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(293, 519390, 'ABFL-BOPET-761', 'Rabiul Alam', 'Na', 'Sreepur, Gazipur', 'ABFL, Trishal', '01925905720', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(294, 519391, 'ABFL-BOPET-762', 'Md. Feroj Kabir ', 'Na', 'Dinajpur', 'ABFL, Trishal', '01878753046', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(295, 501546, 'ABFL-662', 'Sarder Md.Billal Hossan', 'n', 'Vill:Kumarkhali,PO:Bashtoli,PS: Rampal,Bgerhat,', 'N', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(296, 502914, 'ABFL-671', 'Md Suzon Mia', 'N', 'Vill:Dighulia,Po:Atharo Bari,PS:Issorganj,Mymensingh', 'ABFL Factory', '01790159353', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(297, 319377, 'ABFL-54', 'Md.Nahid Mustak', '0', 'Dolia, Baluka, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01779337623', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(298, 324093, 'ABFL-84', 'Tanvir Ahmed', '0', 'ABFL, Trishal, Mymensingh.', 'Kushtia.', '01737707297', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(299, 320394, 'ABFL-67', 'Md.Badsha Mia', '', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(300, 307286, 'ABFL-34', 'Md. Mukbul Hossen Mukter', '0', 'Ghatkoer, Manda, Naogaon.', 'ABFL, Trishal, Mymensingh.', '01750415577', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(301, 301562, 'ABFL-25', 'Mst. Nazma Khatun', 'Na', 'Raer gram, Mymensingh.', 'ABFL-Trishal.', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(302, 319459, 'ABFL-55', 'Md. Zaoharul Islam', '0', 'Fukra, Kasiani, Gupalganj.', 'ABFL, Trishal, Mymensingh.', '01719723772', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(303, 319524, 'ABFL-56', 'Md. Abu Bokkar Chiddik', '', 'Magur jora, Horirampur, Trishal, Mymensingh.', 'ABFL', '01759754288', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(304, 291950, 'ABFL(1)-13', 'Md. Asadur Rahman Khan Fahim', '0', 'Village- Madha Bhagulpur, Post-Bajitpur, Dist-kishoregong', 'AEZ, Raimony, Trishal, Mymensing.', '01750990883', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(305, 320393, 'ABFL-66', 'Md. Juel mia', '0', 'ABFL', 'ABFL', '01615837405', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(306, 324622, 'ABFL-86', 'Tahajjul Hossen', 'Na', 'ABFL', 'ABFL', '01971705684', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(307, 289259, 'ABFL-11', 'Md. Sumon Hossain ', '0', 'Dokkin  Fukra, Kasiani, Gopalganj', 'ABFL, Raimony, Trishal, Mymensingh', '01729883186', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(308, 295168, 'ABFL(1)-16', 'Md. Shamimuzzaman', 'shamimuzzaman.abfl@akij.net', 'Vill-Banshgram, P.O-Durbachara, P.S-kumarkhali, Dist-Kustia.', 'Raymoni, Trishal, Mymensingh.', '01732885508', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(309, 320341, 'ABFL-61', 'Md. Afzal Hossain', 'Na', 'Korigram,', 'ABFL', '01735965421', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(310, 525254, 'ABFL-805', 'Mohammed Safiqul Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01759543170', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(311, 11568, '11568121386', 'Md. Rafiqul Islam', 'Na', 'n/a', 'n/a', '01714566347', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(312, 502918, 'ABFL-672', 'Nazmul Hoqun', 'Na', 'Mymensingh', 'ABFL, Trishal', '01305098167', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(313, 479385, 'ABL-210', 'Md. Sadekur Islam', '', 'C/O: Mosir Uddin,Vill- Kulanandapur,Bolgari,Ghoraghat, Dinajpur', 'abl factory', '01788195665', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(314, 325382, 'ABFL-90', 'Md.Abdus Sobhan', '', 'ABFL,Trishal, Mymensingh.', 'Sirajganj.', '01723866767', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(315, 321133, 'ABFL-81', 'Md. Arif Mia', '0', 'ABFL', 'ABFL', '01933718228', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(316, 320983, 'ABFL-74', 'Kazi Shariar Hossain', 'Na', 'ABFL', 'ABFL', '01878963563', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(317, 320837, 'ABFL-73', 'Md. Shariful Islam', '', 'ABFL', 'ABFL', '01770491396', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(318, 310908, 'ABFL-41', 'kamrul Islam', '0', 'Noapara, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01633007307', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(319, 321122, 'ABFL-77', 'Md. Suhaq Miah', '0', 'ABFL', 'ABFL', '01999212315', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(320, 321066, 'ABFL-75', 'Md. Jahangir Alam', '0', 'ABFL', 'ABFL', '01629852034', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(321, 324623, 'ABFL-87', 'Shoumik Hossen', '', 'ABFL', 'ABFL', '01971706139', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(322, 325892, 'ABFL-92', 'Md. Sazibul Haque', 'Na', 'Meherpur.', 'ABFL, Trishal, Mymensingh.', '01773050824', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(323, 324958, 'ABFL-89', 'Md. Ruman', 'Na', 'ABFL', 'ABFL', '01710158101', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(324, 325441, 'ABFL-91', 'Md. Akram', '', 'ABFL, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01709049478', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(325, 298982, 'ABFL-20', 'Md. Abdul Hamid', '0', 'Mirergau, Sadar, Sylet.', 'ABFL', '01709261392', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(326, 319566, 'ABFL-57', 'Md.Asiqur Rahman', '', 'Noupara, Trishal, Mymensingh.', 'ABFL', '01791104600', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(327, 320408, 'ABFL-68', 'Shahinur Rahman', '0', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(328, 321134, 'ABFL-82', 'Sree Nikhil Tilok Das', '0', 'ABFL', 'ABFL', '01924843026', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(329, 460393, 'ABFL-422', 'Md. Farhad Khan', 'Na', 'Trishal', 'ABFL, Trishal', '01756356217', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(330, 519436, 'ABFL-GT-764', 'Hannan Ahmed', 'Na', 'Trishal', 'ABFL, Trishal', '01723452365', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(331, 519461, 'ABFL-BOPET-766', 'Amir Faysal', 'Na', 'Shariatpur', 'ABFL, Trishal', '01865259259', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(332, 206912, 'APBML(2)-136', 'Sahanur Rahman', '', 'Paikgasa,Khulna', 'APBML(2),Trishal', '01946530886', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(333, 502967, 'ABFL-673', 'Md. Alimul Haq', '', 'Jhenaidah', 'ABFL, Trishal', '01760289641', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(334, 321132, 'ABFL-80', 'Sree Ratan Chandra Bormon', '0', 'ABFL', 'ABFL', '01792605603', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(335, 321124, 'ABFL-78', 'Md. Shahin Mia', '', 'ABFL', 'ABFL', '01777997907', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(336, 320832, 'ABFL-72', 'Mir Shafiul Alam', 'shafiul.abfl@akij.net', 'Vill -Khila, Atpara, Netrokona.', 'ABFL, Trishal, Mymensingh.', '01716217098', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(337, 320604, 'ABFL-71', 'Md. Asikur Rahman', '', 'ABFL', 'ABFL', '01775027638', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(338, 320391, 'ABFL-65', 'Anu Chandra Das', '0', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(339, 317385, 'ABFL-49', 'Md. Anarul', '', 'Raimoni, Trishal, Mymensingh.', 'ABFL ,Raimoni, Trishal, Mymensingh.', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(340, 324604, 'ABFL-85', 'Md. Anwar Hossain', '', 'ABFL', 'ABFL', '01712879733', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(341, 324624, 'ABFL-88', 'Shohan Mondol', '', 'ABFL', 'ABFL', '01770742490', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(342, 320357, 'ABFL-62', 'Mohammad Shanakidder', 'Na', 'Sejia , Mohespur, Jinaidha.', 'ABFL', '01989247304', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(343, 325672, 'R1787', 'Abu Sayeed Mohd Ishaq', 'ishaq.abfl@akij.net ', '167/4th Floor, South Kafrul, Dhaka Cantinment, Dhaka-1206.', '167/4th Floor, South Kafrul, Dhaka Cantinment, Dhaka-1206.', '01756080195', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(344, 320289, 'ABFL-59', 'Md. Nazmul Hoque', 'Na', 'Cohoni, Hatibandha, lalmonirhat.', 'ABFL, Trishal, Mymensingh.', '01793186187', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(345, 456683, 'ABFL-384', 'Md. Masum Miah (Line)', '0', 'Baluka', 'Baluka', '01781454513', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(346, 519416, 'ABFL-763', 'Shariful', 'Na', 'Trishal', 'ABFL, Trishal', '01645273870', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(347, 460804, 'ABFL-430', 'Md. Azizul Hakim', 'Azizul.abfl@akij.net', 'Mymensingh', 'ABFL, Trishal', '01682842807', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(348, 460809, 'ABFL-431', 'Omar Faruk', 'Na', 'Naogaon', 'ABFL, Trishal', '01799751804', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(349, 460812, 'ABFL-433', 'Bishwajit Das', 'bishwajit.abfl@akij.net', 'Dhaka', 'ABFL, Trishal', '01745355765', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(350, 519460, 'ABFL-BOPET-765', 'Rubel', 'Na', 'Narayanganj', 'ABFL, Trishal', '01746999690', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(351, 431851, 'ABFL-261', 'Ariful Islam', '0', 'Trishal', 'Trishal', '01760926838', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(352, 432931, 'ABFL-291', 'Md. Nayem', '0', 'Sonamganj', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(353, 504549, 'ABFL-686', 'Md. Masud Rana', 'Na', 'Gaibandha', 'ABFL, Trishal', '0193339391', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(354, 428813, 'ABFL-238', 'Md. Saddam Hossain', '0', 'Sirajganj', 'ABFL', '01764568597', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(355, 431835, 'ABFL-259', 'Anwar Pervez', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(356, 431840, 'ABFL-260', 'Rana Ahmed (Slitter)', '', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(357, 525019, 'ABFL-BOPET-795', 'Md. Rubel Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01878423842', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(358, 342464, 'ABFL-106', 'Md. Mozammel Hossain', 'Na', 'ABFL, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01679157850', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(359, 431904, 'ABFL-262', 'Emun Hosen ', '0', 'trishal ', 'trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(360, 431906, 'ABFL-263', 'Abu Sayem', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(361, 431909, 'ABFL-264', 'Nurul Islam', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(362, 342753, 'ABFL-108', 'Md. Faruk Ahmaed', '', 'Amir bari, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01785984707', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(363, 431913, 'ABFL-265', 'Ziadul  Haque', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(364, 431916, 'ABFL-266', 'Sahin Molla (Slitter)', '0', 'Rajbari', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(365, 431925, 'ABFL-267', 'Md. Mamun', 'Na', 'Mymensingh', 'ABFL', '01752050810', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(366, 504026, 'ABFL-679', 'Md. Hasibur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01782783048', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(367, 504089, 'ABFL-680', 'Md. Kamrul Hasan', 'na', 'Mymensingh', 'ABFL, Trishal', '01633689907', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(368, 459590, 'ABFL-412', 'Md. Shariful Islam', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(369, 431929, 'ABFL-268', 'Touhid Ahmed', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(370, 433310, 'ABFL-299', 'Jahidul Islam (Line)', '0', 'Kisharganj', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(371, 525028, 'ABFL-BOPET-798', 'Sheikh Mehedi Hasan', 'Na', 'Bagerhat', 'ABFL, Trishal', '01621427168', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(372, 398154, 'ABFL-199', 'Md. Tufazzal Hossain', '0', 'Mymensingh , Sadar', 'ABFL, Trishal', '01884787385', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(373, 504091, 'ABFL-681', 'Md. Shepon Miah', 'Na', 'Sirajganj', 'ABFL, Trishal', '01768143370', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(374, 504092, 'ABFL-682', 'Md. Pabel mia', 'na', 'Trishal', 'ABFL, Trishal', '01912731187', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(375, 504094, 'ABFL-683', 'Alauddin', '0', 'Bula', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(376, 431931, 'ABFL-269', 'Md. Arif Mia', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(377, 431933, 'ABFL-270', ' Md. Shawn', '0', 'Jamalpur', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(378, 432001, 'ABFL-271', 'Asaduzzaman', '0', 'Mymensigh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(379, 520618, 'ABFL-800', 'Abdullah Al-Khalid', 'khalid.abfl@akij.net', 'Barishal', 'ABFL, Trishal', '01918834429', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(380, 443859, 'ABFL-355', 'Mufajjal Hossain', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(381, 465908, 'ABFL-471', 'Md. Sajib Mia ', '', 'Netrokona ', 'ABFL, Trishal', '01878026365', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(382, 370933, 'R1908', 'Sonia Biswas Rodela', 'rodela.corp@akij.net', 'Rajoir, Madaripur, Dhaka', 'House No: E-9/B, T&T Colony, Road No: 5, Banani, Dhaka-1213', '01750977530', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(383, 470117, 'C202', 'Fahmida Rahman Eva', 'fahmida.corp@akij.net', 'Vill: Khasha, P.O; Beani Bazar, P.S: Beani Bazar, Dist: Sylhet. ', 'House No: 10/12, Madrasha Road, Block-F, Mohadmmadpur, Dhaka-1207. ', '01775540999', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(384, 458039, 'ABFL-397', 'Md. Rubel Miah (Slitter)', '0', 'Mymensingh', 'ABFL, Trishal', '01315303881', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(385, 458058, 'ABFL-398', 'Md. Asaduzaman (M)', '0', 'Mymensingh ', 'Trishal', '01910208807', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(386, 527121, 'ABFL-865', 'Md. Akram Hossain', 'Na', 'Trishal', 'ABFL, Trishal', '01763524640', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(387, 527122, 'ABFL-866', 'Md. Elias Ahmed', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(388, 349799, 'ABFL-119', 'Arif Hossain', '', 'ABFL', 'AFBL', '01734923667', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(389, 349803, 'ABFL-120', 'Mst. Parvin Akter', 'Na', 'ABFL', 'AFBL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(390, 415163, 'ABFL-215', 'Utjjal Mia', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(391, 415166, 'ABFL-216', 'Shoaib Talukder', 'shoaib.abfl@akij.net', 'Barishal', 'ABFL, Trishal', '01751234204', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(392, 349804, 'AFBL-121', 'Mst. Halima Khatun', 'Na', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(393, 526659, 'ABFL-837', 'Md. Ebna Asad', 'Na', 'Gazaria,Fulchhari-5760,Fulchhari,Gaibandha', 'ABFL', '01904196361', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(394, 465966, 'ABFL-372', 'Md. Halal Uddin', '0', 'Trishal', 'ABFL, Trishal', '01743369295', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(395, 521515, 'ABFL-770', 'Md. Abu Talib ', 'Na', 'Trishal', 'ABFL, Trishal', '01786316807', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(396, 501969, 'ABFL-664', 'K. M. Tanvir Haidar', 'Na', 'Gaibandha', 'ABFL, Trishal', '01746999232', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(397, 527215, 'ABFL-875', 'Easir Arafat', 'Na', 'Trishal', 'ABFL, Trishal', '01629453686', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(398, 520748, 'ABFL-801', 'Tonmon Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01782498646', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(399, 411145, 'ABFL-213', 'Md. Shahidul Islam', 'shahidul1.abfl@akij.net', 'Bogra', 'ABFL', '01737336157', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(400, 422191, 'ABFL-218', 'Dilip Horijon', '0', 'Baraduba, Valuka', 'ABFL', '01304866632', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(401, 495125, 'ABFL-635', 'Feroza Khatun', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(402, 476555, 'ABFLC-520', 'Md. Burhan (Slitter)', '', 'Mymensingh', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(403, 476557, 'ABFLC-521', 'Md. Mahbubul Alam', 'Na', 'Mymensingh', 'ABFL', '01704961014', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(404, 476559, 'ABFLC-522', 'Mijanur Rahman', '0', 'Netrokuna', 'ABFL, Trishal', '01983065920', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(405, 511776, 'ABFL-713', 'Rehena', 'Na', 'Trishal', 'ABFL, Trishal', '01405329556', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(406, 511778, 'ABFL-714', 'Hasina Begum', 'Na', 'Trishal', 'ABFL, Trishal', '01917092204', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(407, 511779, 'ABFL-715', 'Hurena Akter', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(408, 511780, 'ABFL-716', 'Md. Imrul Kayas', 'Na', 'Trishal', 'ABFL, Trishal', '01791215551', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(409, 476563, 'ABFLC-523', 'Md. Shipon Mia', '0', 'Mymensingh', 'ABFL', '01917421852', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(410, 476568, 'ABFLC-524', 'Bikash Chandra', '0', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(411, 476569, 'ABFLC-525', 'Nazmul Haque', '0', 'Mymensingh', 'ABFL', '01745842795', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(412, 476576, 'ABFLC-527', 'Shajahan', '0', 'Mymensingh', 'ABFL', '01726483719', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(413, 511603, 'ABFL-', 'Shohag', 'Na', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(414, 386798, 'ABFL-180', 'Md. Abul kashem', 'kashem.abfl@akij.net', 'Mymensingh', 'ABFL', '01684674045', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(415, 460893, 'ABFL-435', 'Md. Shaheb Ali', '0', 'Pabna ', 'ABFL, Trishal', '01727918694', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(416, 460901, 'ABFL-437', 'Misbah Uddin', 'Na', 'Barishal', 'ABFL, Factory', '01710387936', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(417, 514225, 'ABFL-731', 'Sree Polash Chandra', 'Na', 'Ranjpur', 'ABFL, Trishal', '01723368192', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(418, 476572, 'ABFLC-526', 'Asraful Islam', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(419, 476599, 'ABFL-533', 'Hasibul Hasan', '0', 'Mymensingh', 'ABFL,Trishal', '01625911229', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(420, 524296, 'ABFL-785', 'Nazmul Haque', 'Na', 'Chawladi, Horirampur,-2220, Trishal, Mymensingh.', 'Home:01765348763', '01750124268', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(421, 525979, 'ABFL-821', 'Rakibul2 Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01731393317', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(422, 525981, 'ABFL-823', 'Md. Abu Obida Sarker', 'Na', 'Kurigram', 'ABFL, Trishal', '01822871668', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(423, 525983, 'ABFL-825', 'Mst. Aklima Khatun', 'Na', 'Trishal', 'ABFL, Trishal', '01312199080', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(424, 450366, 'ABFL-364', 'Md. Robin Mia', '0', 'Netrokona', 'ABFL', '01739912790', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(425, 514220, 'ABFL-730', 'Md. Abdur Rashid', 'Na', 'Rajshahi', 'ABFL, Trishal', '01836287272', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(426, 514233, 'ABFL-732', 'Md. Morsalim Mia', 'Na', 'Rangpur', 'ABFL, Trishal', '01761106701', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(427, 514234, 'ABFL-733', 'Alauddin Jamaddar', 'Na', 'Jalukhathi', 'ABFL, Trishal', '01710207957', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(428, 495133, 'ABFL-636', 'Razaul ', '0', 'Bogura', 'ABFL, Factory', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(429, 476579, 'ABFLC-528', 'Johirul Islam', '0', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(430, 476584, 'ABFLC-529', 'Mehedi Hasan Toshar', '0', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(431, 476588, 'ABFLC-530', 'Md. Abdur Razzak', '0', 'Mymensingh', 'ABFL', '01904359773', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(432, 476598, 'ABFL-532', 'Md. Jahirul Islam', '0', 'Mymensinhg', 'ABFL, Trishal', '01941529502', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(433, 476616, 'ABFL-538', 'Md. Faisal Mahmud', '0', 'Mirpur , Dhaka', 'ABFL, Trishal', '01781469910', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(434, 476620, 'ABFL-539', 'Rana Ahmed', 'Na', 'Mymensingh', 'ABFL', '01629474024', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(435, 433006, 'ABFL-293', 'Zahirul Islam', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(436, 476642, 'ABFL-546', 'Md Sahabi Alam', 'Na', 'Mymensingh', 'n', '01318061116', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(437, 476647, 'ABFL-551', 'Md Ali Hossain', 'Na', 'Mymensingh', 'n', '01715374769', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(438, 525022, 'ABFL-BOPET-796', 'Noman Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01942580046', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(439, 525026, 'ABFL-BOPET-797', 'Md. Sajjad Hossain', 'Na', 'Pabna', 'ABFL, Trishal', '01742369343', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(440, 526724, 'ABFL-844', 'Md. Limu Sardar', 'Na', 'Kurigram', 'ABFL, Trishal', '01861375255', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(441, 460899, 'ABFL-436', 'Md. Rejaul Karim', '0', 'Nilfamari', 'ABFL, Trishal', '01773261884', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(442, 476595, 'ABFL-531', 'Md. Abu Bokkar Chiddik', '0', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(443, 476601, 'ABFL-534', 'Md. Habib', 'Na', 'Mymensingh', 'ABFL, Trishal', '01874122201', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(444, 476607, 'ABFL-535', 'Sahin Molla', '0', 'Rajbari', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(445, 476614, 'ABFL-537', 'Md. Sayem Ahmmad', '0', 'Netrokuna', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(446, 476628, 'ABFL-541', 'Md. Khadimul Islam', 'Na', 'Mymensingh', 'ABFl, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(447, 476632, 'ABFL-542', 'Md Jahidul Islam', 'Na', 'Kishorganj', 'n', '01713618045', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(448, 476634, 'ABFL-543', 'Md. Alamgir Hosen', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(449, 476638, 'ABFL-544', 'Md Tufael Ahamed Tohin', 'Na', 'Mymensingh', 'n', '01759067439', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(450, 476644, 'ABFL-548', 'Md. Nurul Islam', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(451, 476645, 'ABFL-549', 'Md Ashikur Rahman', 'Na', 'Mymensingh', 'n', '01775027638', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(452, 476648, 'ABFL-552', 'Atiqul Mia', 'Na', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(453, 476649, 'ABFL-553', 'Md Harun', 'Na', 'Mymensingh', 'n', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(454, 476650, 'ABFL-554', 'Shohel Khan', 'Na', 'Mymensinhg', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(455, 433100, 'ABFL-295', 'Kolpona Akter', 'Na', 'Trishal', 'Trishal', '01302424234', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(456, 476611, 'ABFL-536', 'Md.Jashim Uddin', '0', 'Mymensingh', 'ABFL, Trishal', '01623951282', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(457, 476651, 'ABFL-555', 'Abu Hanif', 'n', 'Mymensingh', 'n', '01782904671', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(458, 476652, 'ABFL-556', 'Shishir Kumar ', 'Na', 'Nilphamari', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(459, 476654, 'ABFL-558', 'Md. Shahin ', '0', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(460, 433062, 'ABFL-294', 'Md. Titu Bapery ', 'titu.abfl@akij.net', 'Chandpur', 'ABFL, Trishal', '01306598622', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(461, 460920, 'ABFL-438', 'Md. Abdul Majid', 'Na', 'Trishal', 'ABFL, Trishal', '01924214197', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(462, 476623, 'ABFL-540', 'Md. Asraful Islam', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(463, 476640, 'ABFL-545', 'Didarul Islam', '0', 'Mymensingh', 'ABFL, Trishal', '01718406040', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(464, 476646, 'ABFL-550', 'Md.Ismaeel Hossan', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(465, 476653, 'ABFL-557', 'Shorif Husen', 'n', 'Mymensingh', 'ABFL, Trishal', '01905336845', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(466, 511752, 'ABFL-711', 'Nur Shaid Murshed', 'Na', 'Natore', 'ABFL, Trishal', '01740266211', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(467, 511756, 'ABFL-712', 'Amdadul Haque ', 'Na', 'Baluka', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(468, 434656, 'ABFL-314', 'Ariful Miah', '0', 'Trshal', 'Trishal', '01837973131', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(469, 525762, 'ABFL-819', 'Sojeb Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01749299958', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(470, 495761, 'ABFL-638', 'Md Rhiday Miah', 'Na', 'Vill:Bamnakhali,PO:Raymoni,Trishal', 'N', '01768210532', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(471, 496228, 'ABFL-648', 'Md. Sumon', 'na', 'Barishal', 'ABFL, Trishal', '01984413885', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(472, 471910, 'ABFLC-491', 'Md. Rafiqul Islam', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(473, 527077, 'ABFL-855', 'Md. Hanif Ahmed', 'Na', 'Trishal', 'ABFL, Trishal', '01952837573', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(474, 527078, 'ABFL-856', 'Zannat Mia', 'NA', 'Sherpur', 'ABFL, Trishal', '01645182113', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(475, 527079, 'ABFL-857', 'Mofakkharul', 'NA', 'Trishal', 'ABFL, Trishal', '01722078541', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(476, 527080, 'ABFL-858', 'Masud Rana', 'Na', 'Trishal', 'ABFL, Trishal', '01780159442', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(477, 527081, 'ABFL-859', 'Shahinur Rahman', 'NA', 'Trishal', 'ABFL, Trishal', '01649117106', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(478, 376040, 'ABFL-168', 'Md. Sebow', '0', 'Chilahati, Nilpamari.', 'ABFL, Trishal.', '01749976910', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(479, 379499, 'ABFL-172', 'Nazmul Huda', '0', 'Golabita, Horirampur, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01631679649', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(480, 379503, 'ABFL-173', 'Ali Kausar', '0', 'Golabita, Trishal, Mymensingh.', 'AEZ, Raymoni, Trishal.', '01630527540', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(481, 496232, 'ABFL-649', 'Md. Omar Ibna Shahid', 'omar.abfl@akij.net', 'Munshi Ganj', 'ABFL, Trishal', '01670132467', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(482, 473837, 'ABFL-508', 'Ajoy Candra Dabnath', 'Na', 'Mymensingh', 'ABFL, Trishal', '01923140331', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(483, 456051, 'ABFL-383', 'Md. Zabed Ali ', '0', 'Baluka', 'ABFL, Trishal', '01758825748', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(484, 331413, 'ABFL-98', 'Md. Pikul Mondol', '', 'ABFL', 'ABFL', '01961648193', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(485, 503107, 'ABFL-674', 'Md. Alamgir', 'Na', 'Trishal', 'ABFL, Trishal', '01609850128', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(486, 396146, 'ABFL-189', 'Md. Ripon Ali', 'Na', 'Baradi, Meherpur', 'ABFL, Trishal', '01966876414', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(487, 526999, 'APAL-0023', 'Md. Firoj Mia', '0', 'Kutobpur, Kanchpur, Sonargoan, Narayanganj', 'Kutobpur, Kanchpur, Sonargoan, Narayanganj', '01755276871', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(488, 527651, 'ABFL-916', 'Md. Faharia Islam Fahim', 'NA', 'Gaibandha', 'ABFL, Trishal', '01740036713', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(489, 484000, 'ABFLC-590', 'Md. Rubel Mia', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(490, 360187, 'ABFL-141', 'Md. Tariqul Islam', 'Na', 'ABFL', 'ABFL', '01719473337', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(491, 515883, 'ABFL-744', 'Md. Younus Ali', 'younus.abfl@akij.net', 'Trishal', 'ABFL, Trishal', '01727206509', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(492, 368612, 'ABFL-142', 'Ballal Hossain', '', 'Jonota Koloni, Firozshah, Chittagong', 'APBML-2, Trishal, Mymensingh', '01777245451', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(493, 389922, 'ABFL-185', 'Reaz Mahammad Imran', '0', 'Jinaidah', 'ABFL,Trishal .Mymensingh', '01921680334', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(494, 444919, 'ABFL-356', 'Md. Shoriful Islam(Erema)', '0', 'Trishal', 'ABFL', '0645178051', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(495, 491755, 'ABFL-624', 'Rifad Hosan', 'Na', 'Jamalpur', 'ABFL,Factory', '01742017073', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(496, 463255, 'ABFL-461', 'Md. Hasan ', 'Na', 'Trishal', 'ABFL, Trishal', '01831945050', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(497, 459886, 'ABFL-417', 'Md. Shohel Rana (Line)', '0', 'Panchagarth ', 'ABFL, Trishal', '01783213041', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(498, 525980, 'ABFL-BOPET-822', 'Md. Rakibul Islam', 'Na', 'Ranjpur', 'ABFL, Trishal', '01954683500', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(499, 525982, 'ABFL-824', 'Rima Rani Sharkar', 'Na', 'Trishal', 'ABFL, Trishal', '01887254429', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(500, 463257, 'ABFL-462', 'Md. Mamun Miah', '0', 'Kirigram ', 'Trishal', '01983631645', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(501, 463267, 'ABFL-463', 'Amdadul Huq', '0', 'Trishal', 'ABFL, Trishal', '01708064945', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(502, 525043, 'ABFL-799', 'Md. Forhad Hossain2', 'Na', 'Mymensingh', 'ABFL, Trishal', '01791316164', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(503, 491740, 'ABFL-622', 'Riyat Hossan', 'Na', 'Trishal', 'ABFL,Trishal', '01998214143', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(504, 491783, 'ABFL-625', 'Awwal ', 'Na', 'Trishal', 'ABFL, Trishal', '01950173705', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(505, 491694, 'ABFL-620', 'Shamol Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01910214970', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(506, 526335, 'ABFL-830', 'Abu Daud ', 'Na', 'Manikganj', 'ABFL, Trishal', '01611678966', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(507, 491786, 'ABFL-626', 'Md Rofiq Prodan', 'n', 'Vill: Narinda,PO:Narinda-3519,PS:Titas,Comilla', 'n', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(508, 463284, 'ABFL-464', 'Suhrab Hossain', '0', 'Trishal', 'Trishal ', '01647299161', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(509, 500464, 'ABFL-659', 'Md. Masum Reza', 'Na', 'Sharijganj', 'ABFL, Trishal', '01718735805', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(510, 423779, 'ABFL-223', 'Md. Shahidul Islam', '0', 'Trishal , ', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(511, 525079, 'ABFL-802', 'Md. Nurjaman Munshi', 'Na', 'Chittagang', 'ABFL, Trishal', '01965585395', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(512, 525117, 'ABFL-804', 'Anisur Rahman', 'Na', 'Natore', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(513, 510374, 'ABFL-699', 'Md. Kawsar', 'na', 'Barishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(514, 491748, 'ABFL-623', 'Md. Shahjahan ', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(515, 391824, 'ABFL-186', 'Md. Shofiul Bashar', 'shofiul.abfl@akij.net', 'Comilla', 'ABFL', '01749989470', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(516, 500551, 'ABFL-660', 'Md. Aslam Ali', 'Na', 'Natore ', 'ABFL, Trishal', '01740120585', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(517, 369388, 'ABFL-152', 'Md. Burhan Uddin', '', 'Nargarchara, Trishal, Mymensingh.', 'ABFL, Trishal.', '01770271743', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(518, 513533, 'ABFL-728', 'Ashraful Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01625691123', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(519, 485175, 'ABFLC-592', 'Ismail Hossain', 'n', 'Vill:Chak Madu,PO:Fasiladanga,PS:Dinajpur,Dinajpur', 'N', '01717493988', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(520, 435869, 'ABFL-320', 'Shorif Hossen', '0', 'Trishal', 'trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(521, 471794, 'ABFL-489', 'Md Ahad Ali', 'n', 'Vill: Bil Boalia,PO:Boro Ghangdia,PS: Dowlatpur,Kustia', 'ABFL', '01718771600', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(522, 519063, 'R2735', 'Tarikul Islam Tutul', 'tarikul.abfl@akij.net', 'Vill: Durgarampur, P.O: Birgaon, P.S: Nabinagar, Dist: Brahmanbaria', 'Dhaka', '01799411564', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(523, 377722, 'ABFL-169', 'Md. Nazrul Islam', 'nazrul.abfl@akij.net', 'Dewan bag, modonpur, bandar, Narayanganj.', 'ABFL, Trishal.', '01713998741', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(524, 519099, 'ABFL-758', 'Alamgir', 'Na', 'Comillah', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(525, 516837, 'ABFL-747', 'Suma Akter', 'Na', 'Mymensingh', 'ABFL, Trishal', '01986253575', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(526, 516850, 'R2712', 'Md. Hasan Ali Mridha', '', 'Vill+P.O: Alojdia Bazzar, P.S: Magura sadar, Dist: Magura. \r\n', '369/C, Hazi Worci khan, Poshim Ahmad nagor, Mirpur -1, Dhaka-1216.\r\n', '01913761178', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(527, 471897, 'ABFL-490', 'Prem Horizon', 'n', 'Bhaluka,Mymensingh', 'Bhaluka', '01782624768', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(528, 511627, 'ABFL-706', 'Md. Siddiqur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01949724940', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(529, 483626, 'ABFC-589', 'Siddiqur Rahman', '0', 'Raymoni', 'ABFL', '01312498045', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(530, 479162, 'ABFLC-575', 'Md. Nazmul Huda', '0', 'Mymensingh', 'ABFL,Factory', '01636521396', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(531, 365707, 'ABFL-145', 'Md. Habibur Rahman', '', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(532, 511609, 'ABFL-703', 'Md. Sohag Rana', 'Na', 'Sirajganj', 'ABFL', '01935794666', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(533, 511616, 'ABFL-704', 'Md. Monirul Islam', 'Na', 'Pabna', 'ABFL, Trishal', '01741049711', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(534, 511619, 'ABFL-705', 'Md. Shahin Alam', 'Na', 'Natore', 'ABFL, Trishal', '01714827661', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(535, 519133, 'ABFL-GT-759', 'Md. Mostofa Kamal', 'Na', 'Jossore', 'ABFL, Trishal', '01748237309', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(536, 360190, 'ABFL-142', 'Md. Mafuj Molla', '0', 'ABFL', 'ABFL', '01776764639', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(537, 458691, 'ABFL-407', 'Md. Sabbir Ahmed', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(538, 524281, 'ABFL-784', 'Munna Islam', 'munna.abfl@akij.net', 'Vill: Masimpur, Post: Chalongahat, P.S: Shibgonj, Dist: Bagra.', 'AEZ Security Dormitory, Mother Contact: 01767016234', '01580596729', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(539, 480394, 'ABFLC-583', 'Mukhlasur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01713598378', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(540, 429234, 'ABFL-239', 'Rasel Mahmud ', '0', 'Sirajgonj ', 'ABFL, Trishal', '01762220823', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(541, 525406, 'ABFL-BOPET-807', 'Md. Monir Hossain Topadar ', 'NA', 'Vill: Projapordi, P/S: Chadpur, District: Chadpur', 'Home Contact: 01843710318', '01883561271', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(542, 429675, 'ABFL-245', 'Md. Hasan Ali (Slitter)', '0', 'Trishal, Mymensingh', 'Trishal', '01960571107', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(543, 469563, 'ABFL-480', 'Khokan Reza Mridha', 'Reza.abfl@akij.net', 'Madaripur', 'ABFL,Trishal', '01776153518', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(544, 442366, 'ABFL-352', 'Shekh Afrad Ahmed', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(545, 435991, 'ABFL-321', 'Md. Real Mia', '0', 'Mymensingh', 'ABFL', '01751441467', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(546, 343931, 'ABFL-109', 'Md. Didarul Alam', 'didarul.abfl@akij.net', '1166 East Shewrapar, Mirpur-1216', 'ABFL', '01739384348', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(547, 517291, 'ABFL-751', 'Md. Bashar Sikder', 'Na', 'Bagerhat', 'ABFL, Trishal', '01785801631', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(548, 453305, 'ABFL-368', 'Muklesur Rahaman', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL);
INSERT INTO `tblemployees` (`id`, `enroll`, `code`, `name`, `email`, `parmanentaddress`, `presentaddress`, `contactno`, `created_at`, `updated_at`, `deleted_at`) VALUES
(549, 453306, 'ABFL-369', 'Md. Monir Hossen', '', 'Trishal', 'ABFL, Trishal', '01955967501', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(550, 426713, 'ABFL-235', 'Md. Faisal Mahmud (Line)', '0', 'Mirpur, Dhaka.', 'ABFL', '01781469910', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(551, 333599, 'ABFL-99', 'Roidul Haq', '', 'ABFL', 'ABFL', '01950416779', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(552, 517288, 'ABFL-750', 'Sabuj Ray', 'Na', 'Jashore', 'ABFL, Trishal', '01732402890', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(553, 517299, 'ABFL-752', 'Md. Abu Talab', 'Na', 'Gazipur', 'ABFL, Trishal', '01735893932', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(554, 475814, 'ABFLC-518', 'Md Shahidul Alam', 'n', 'Trishal,Mymensingh', 'n', '01750472515', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(555, 1106, 'R186', 'Md. Jakir Hossen', '', 'n', 'n', '01742508506', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(556, 527609, 'ABFL-896', 'Md. Abdulla Al Mamun', 'NA', 'Savar Dhaka', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(557, 439869, 'ABFL-349', 'Abdul Aziz ', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(558, 527613, 'ABFL-897', 'Siddiqur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01611702215', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(559, 527615, 'ABFL-898', 'Md. Abdur Rahim', 'Na', 'Jhenaidha', 'ABFL, Trishal', '01949147611', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(560, 426050, 'ABFL-224', 'Md. Nabi Hoseen ', '', 'Trishal', 'ABFL', '01904975417', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(561, 512405, 'ABFL-722', 'Sanaul Haque', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(562, 512407, 'ABFL-723', 'Abdul Kuddus', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(563, 527620, 'ABFL-901', 'Shohel Rana', 'NA', 'Mymensingh', 'ABFL, Trishal', '01987312342', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(564, 527621, 'ABFL-902', 'Md. Khairul Amin', 'NA', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(565, 527623, 'ABFL-903', 'Md. Rashid', 'NA', 'Mymensingh, Netrokuna', 'ABFL, Trishal', '01315068774', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(566, 527624, 'ABFL-904', 'Imran Ali', 'Na', 'Natore', 'ABFL, Trishal', '01824484550', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(567, 527625, 'ABFL-905', 'Md. Monirul Islam', 'NA', 'Mymensingh', 'ABFL, Trishal', '01995126113', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(568, 115390, 'ABFL-4', 'Golam Mostofa Farid ', 'Na', 'Late Abdul Malek & Late Goleza Begum, Vill-Chorvobosur, P.O-D.S. Mills, P.S-Dewanganj, Dist-Jamalpur', 'Family Mob No-01759-977102', '01735121308', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(569, 468145, 'ABFL-479', 'Md. Amir Ali ', '0', 'Sunamganj ', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(570, 512404, 'ABFL-721', 'Abu Taher ', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(571, 512409, 'ABFL-724', 'Uzzal', 'Na', 'Gaibandha', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(572, 486616, 'ABFL-603', 'Subashi Rani', '0', 'Baluka', 'ABFL, Trishal', '01707316584', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(573, 403088, 'ABFL-208', 'Md. Shakim Ahamed', '0', 'Mymensingh,Trishal', 'ABFL', '01715469949', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(574, 455795, 'ABFL-279', 'Md. Arif Miah(2)', '0', 'Trishal', 'ABFL', '01995287294', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(575, 455862, 'ABFL-381', 'Md. Motiur Rahman', '0', 'Trishal', 'ABFL', '01918652066', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(576, 439926, 'ABFL-350', 'Md. Shahidul Islam(Slitter)', '0', 'Trishal', 'ABFL', '01763310100', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(577, 522959, 'ABFL-775', 'Md. Imran Ali', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(578, 369227, 'ABFL-151', 'Mst. Rahima Khatun', 'Na', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(579, 510823, 'ABFL-700', 'Md. Tofazzal Hoque', 'na', 'Netrokuna', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(580, 354342, 'ABFL-139', 'Mst. Julakha', 'Na', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(581, 505257, 'ABFL-687', 'Rafiqul Islam', 'Na', 'Mymensingh', 'ABFL, Trishal', '01991163440', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(582, 426278, 'ABFL-229', 'Asadul Islam (Line)', '0', 'Mymensingh', 'ABFL', '01940353084', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(583, 426290, 'ABFL-230', 'Firoz Ahmmed ', '0', 'Tangail', 'ABFL', '01715898177', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(584, 353794, 'ABFL-132', 'Md. Shariful Islam', 'Na', 'ABFL', 'ABFL', '01628111661', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(585, 44789, '447894291402', 'MD. MEHEDI HASAN', 'mehedi2.accl@akij.net', 'Md. Mobarok Hossain, Vill: Nazirpur, P.O. Ambor Pur, PS: Chandina, Dist: Comilla', 'Family Mob No-01712-205934/01719-197260', '01684674045', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(586, 467776, 'ABFL-474', 'Md. Alamgir 3', '0', 'Trishal', 'ABFL,Trishal', '01764237980', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(587, 467777, 'ABFL-475', 'Md. Anamul Huq', '0', 'Trishal', 'ABFL, Trishal', '01718109874', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(588, 523043, 'ABFL-776', 'Sipon Miah', 'Na', 'Tangail', 'ABFL, Tangail', '01771017529', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(589, 369297, 'ABFL-151', 'Md. Naem', '', 'Kasiganj, Trishal, Mymensingh', 'ABFL, Trishal.', '01868968601', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(590, 467774, 'ABFL-473', 'Md. Chan Mia', '0', 'Trishal, Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(591, 368434, 'ABFL-147', 'Md. Shamim Mia', '', 'Tangail.', 'ABFL, Trishal.', '01732422041', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(592, 510826, 'ABFL-701', 'Md. Shohidul Islam', 'Na', 'Kurigram', 'ABFL, Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(593, 367172, 'APL-318', 'Abdul Alim', 'N/A', 'S/O: Miyar Uddin; Village: Dotter Gaon Poshchim Para; P.O. Dotter Gaon; Shibpur, Narsingdi', 'S/O: Miyar Uddin; Village: Dotter Gaon Poshchim Para; P.O. Dotter Gaon; Shibpur, Narsingdi', '01751065374', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(594, 515487, 'ABFL-742', 'Md. Musa Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01734577359', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(595, 455887, 'ABFL-382', 'Asikur Rahman', '0', 'Trishal', 'ABFL', '01719732578', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(596, 463243, 'ABFL-459', 'Mahabur', 'Na', 'Trishal', 'Trishal', '01875166052', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(597, 453307, 'ABFL-370', 'Md. Faridur Rahman', 'faridur.abfl@akij.net', 'Mymensingh', 'ABFL, Trishal', '01710066616', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(598, 439590, 'R2181', 'Md. Maniruzzaman Akash', 'akash.corp@akij.net', 'Vill: Enayetpur P.o: Kurshipara, P.S: Ishwarganj, Dist: Mymensingh\r\n', 'Vill: Enayetpur P.o: Kurshipara, P.S: Ishwarganj, Dist: Mymensingh\r\n', '01951233084', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(599, 432063, 'ABFL-272', 'Abdur Rohman', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(600, 432072, 'ABFL-273', 'Sadhin Hossain', '0', 'Tangail', 'ABFL', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(601, 463244, 'ABFL-460', 'Abdullah', 'Na', 'Trishal', 'Trishal', '01308619290', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(602, 439600, 'ABFL-338', 'Md. Deloar Hossen', 'deloar.abfl@akij.net', 'Sherpur, Mymensingh', 'ABFL, Trishal', '01771959659', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(603, 455852, 'ABFL-380', 'Sohel Khan', '0', 'Trishal', 'Trishal', '01759641961', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(604, 525488, 'ABFL-809', 'Md. Forhad2 Mia', 'Na', 'Mymensingh', 'ABFL, Trishal', '01733427680', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(605, 479357, 'ABFLC-579', 'Sakibul Hasan', 'Na', 'Jamalpur', 'ABFL, Trishal', '01306076866', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(606, 328721, 'R1793', 'Mohammad Nazrul Islam Asique', '', 'Sardaganj, Kashimpur, Gazipur.', 'Sardaganj, Kashimpur, Gazipur.', '01724606252', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(607, 432555, 'ABFL-277', 'Shahidul Islam', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(608, 509285, 'ABFL-690', 'Al Amin Sumon', 'Na', 'Jamalpur', 'ABFL', '01837934360', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(609, 432596, 'ABFL-278', 'Ashikur Rahman', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(610, 478781, 'ABFLC-567', 'Fokhrul', 'Na', 'Trishal,Mymensingh', 'n', '01726703397', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(611, 478784, 'ABFLC-568', 'Md Juel Mia', 'n', 'Trishal,Mymensingh', 'n', '01991231779', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(612, 509293, 'ABFL-691', 'Md. Uzzal Mia', 'Na', 'Jamalpur', 'ABFL, Trishal', '01933260673', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(613, 511885, 'ABFL-717', 'Noun Mia', 'Na', 'Mymensingh', 'ABFL, Trishal', '01999742586', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(614, 511890, 'ABFL-718', 'Rubel', 'Na', 'Trishal', 'ABFL, Trishal', '01409343494', '2022-01-29 23:34:29', '2022-01-29 23:34:29', NULL),
(615, 453063, 'R2207', 'Md. Nasir Howlader', '', 'Vill: Neyamoti, P.O; Neyamoti, P.S: Mohaspur, Dist: Barishal', 'House/ Holding No: 19, Road No: 01, Mirpur-1216, Sha Ali, Dhaka City Corporation, Dhaka. ', '01991690914', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(616, 354259, 'ABFL-138', 'Md. Faysal Ahamed', '0', 'ABFL', 'ABFL', '01674220770', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(617, 377797, 'ABL-82', 'Md.Shahidul Islam', 'Na', 'Vill & Post- Tapa Podumshohor, Shaghata, Gaibandah', 'ABL factory', '01715596986', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(618, 527123, 'ABFL-867', 'Md. Abdul Baten', 'NA', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(619, 524566, 'ABFL-787', 'Md. Jehad ', 'Jehed.abfl@akij.net', 'Noapara,Ovainagar,Jashore,', 'AEZ', '01960246241', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(620, 453311, 'ABFL-371', 'Md. Mehedi Hasan', 'Na', 'Rangpur', 'ABFL, Trishal', '01306827784', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(621, 448204, 'ABFL-361', 'Md. Ziaul Haqe', '0', 'Manikganj ', 'ABFL, Trishal', '01675557033', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(622, 511897, 'ABFL-719', 'Md. Habibullah Robin', 'Na', 'Sirajganj', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(623, 527124, 'ABFL-868', 'Ripan Majumder', 'Na', 'Bagerhat', 'ABFL, Trishal', '01315450434', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(624, 527125, 'ABFL-869', 'Md. Monour Hossain Minu', 'Na', 'Habigonj', 'ABFL, Trishal', '01959227165', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(625, 387566, 'ABFL-181', 'Md. Mukhlesur Rahman', '0', 'Shakua , Trishal,', 'ABFL, Trishal.', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(626, 527641, 'ABFL-913', 'Alauddin', 'Na', 'Shatkhira', 'ABFL, Trishal', '01953374160', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(627, 448215, 'ABFL-362', 'Md. Khairul Islam', 'Na', 'Munshigonj', 'ABFL, Trishal', '01301529917', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(628, 1246, 'R011', 'Md. Ashraf-uz-zaman Khan', 'azkhan@akij.net', 'Holding No. ka-34, South Badda, Dhaka-1214.', 'Holding No. ka-34, South Badda, Dhaka-1214.', '01713080161', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(629, 526856, 'APAL-0021', 'Md. Robiul Islam', '', 'Dogair Ziro Point, Sarulia, Demra, Jalkhati', 'Dogair Ziro Point, Sarulia, Demra, Jalkhati', '01303348105', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(630, 330912, 'ABFL-95', 'A.S.M. Nasim', 'nasim.abfl@akij.net', 'Trishal, Mymensingh.', 'Trishal, Mymensingh.', '01724093522', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(631, 460625, 'ABFL-423', 'Md. Imran Hasan', 'imran.abfl@akij.net', 'Sirajganj', 'ABFL', '01841261777', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(632, 527129, 'ABFL-870', 'Md. Jahangir Alam', 'Na', 'Trishal', 'ABFL, Trishal', '01833941832', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(633, 527633, 'ABFL-907', 'Mst. Fatema Khatun', 'NA', 'Trishal', 'ABFL, Trishal', '01961081641', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(634, 352350, 'ABFL- 128', 'Md. Kabir Hossain', '', 'Mahamudpur, Valuka, Mymensingh', 'mahamudpur, Valuka, Mymensingh', '01761490936', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(635, 526149, 'ABFL-826', 'Md. Nasir Uddin', 'Na', 'Trishal', 'ABFL, Trishal', '01790156485', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(636, 511968, 'ABFL-720', 'Sohel Oreng', 'Na', 'Netrokuna', 'ABFL, Trishal', '01863294640', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(637, 526689, 'ABFL-839', 'Md. Al Amin', 'Na', 'Mymensingh', 'ABFL, Trishal', '01743456488', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(638, 526690, 'ABFL-840', 'Rubel Paul', 'Na', 'Trishal', 'ABFL, Trishal', '01736252996', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(639, 526691, 'ABFL-841', 'Abdour Rahman', 'NA', 'Trishal', 'ABFL, Trishal', '01989922537', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(640, 526692, 'ABFL-842', 'Kamruzzaman', 'Na', 'Trishal', 'ABFL, Trishal', '01723132848', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(641, 526693, 'ABFL-843', 'Sakib Mahmud', 'NA', 'Trishal', 'ABFL, Trishal', '01538422215', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(642, 77699, 'ABFL-2', 'Md. Sayed', 'Na', 'Vill+P.O- Harikumaria, P.S+Dist- Madaripur.', 'Bogra Warehouse', '01673383553', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(643, 490200, 'ABFL-617', 'Md. Noorul Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01624711946', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(644, 518882, 'ABFL-757', 'Amdadul', 'Na', 'Bhaluka, Mymensingh', 'ABFL, Trishal', '01609340764', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(645, 453132, 'ABFL-367', 'Sadiqul Hassan (Slitter)', '0', 'Tangail', 'ABFL, Trishal', '01757541030', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(646, 439526, 'ABFL-329', 'Md. Shahin Khan', '0', 'Rajbari', 'ABFL, Trishal', '01875856818', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(647, 439530, 'ABFL-330', 'Md. Rakibul Islam', '0', 'Meherpur', 'ABFL', '01725078632', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(648, 439533, 'ABFL-331', 'Md. Mubarak Hossain', '0', 'Mymensingh', 'ABFL, Trishal', '01852767331', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(649, 439535, 'ABFL-332', 'Md. Erahad khan', '0', 'Trishal', 'ABFL', '01716754828', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(650, 513308, 'ABFL-727', 'Faruk Ahammad', 'Na', 'Trishal', 'ABFL, Trishal', '01760943003', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(651, 490199, 'ABFL-616', 'Md. Omar Faruk', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(652, 199884, 'APBMLP1352', 'Md. Mozibur Rahman', '', 'Lakhikundo, Joydia, Cotchadpur, Jhinaidah.', 'Manikganj.', '01935791682', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(653, 439544, 'ABFL-333', 'Akramul Islam', '0', 'Trishal', 'ABFL', '01306631505', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(654, 439546, 'ABFL-334', 'Md. Joshim Uddin', '0', 'Mymensingh', 'ABFL', '01823887207', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(655, 439547, 'ABFL-335', 'Hafizul Islam', '0', 'Trishal  ', 'ABFL, Trishal', '01731592025', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(656, 527584, 'ABFL-887', 'Md. Sumon Ahmed', 'Na', 'Madharipur', 'ABFL, Trishal', '01998067485', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(657, 512601, 'ABFL-726', 'Md. Babul Miah', 'Na', 'Nilphamary ', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(658, 473628, 'ABFL-503', 'Zalal Uddin', 'N', 'Vill:Uttor Gazipur,PO:Dokhin Gazipur,PS: Motlob,Chadpur', 'N', '01825515670', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(659, 460654, 'ABFL-426', 'Md. Rasel Mia', 'Na', 'Trishal', 'ABFL', '01936843893', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(660, 460660, 'ABFL-427', 'Md. Mahbul Hoque', 'Na', 'Trishal', 'ABFL, Trishal', '01764925840', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(661, 439551, 'ABFL-336', 'Sujon Ali', '0', 'Meherpur', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(662, 439554, 'ABFL-337', 'Md. Imran Hossan', '0', 'Trishal', 'ABFL, Trishal', '01924315275', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(663, 527586, 'ABFL-888', 'Md. Rakibul 3 Islam', 'NA', 'Chattogram', 'ABFL, Trishal', '01612223255', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(664, 527587, 'ABFL-889', 'Md. Osikur Rahman', 'NA', 'Khulna', 'ABFL, Trishal', '01911649338', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(665, 460673, 'ABFL-428', 'Md. Rasel Mia2', '0', 'Trishal', 'ABFL,Trishal', '01307592502', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(666, 461001, 'ABFL-439', 'Md. Apple Mahmud', '0', 'Pabna, Shathia ', 'ABFL, Trishal', '01796617443', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(667, 439582, 'Intern-105', 'Shamme Akter Mim', '', 'Vill: Tami, P.O: Tamai, P.S: Belkuchi, Dist: Sirajgonj. ', 'Dhanmondi 15, Dhaka. ', '01902721756', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(668, 527084, 'ABFL-860', 'Rajib Sorkar', 'NA', 'Trishal', 'ABFL, Trishal', '01734241735', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(669, 527085, 'ABFL-861', 'Rafiul Hasan', 'NA', 'Trishal', 'ABFL, Trishal', '01716818969', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(670, 527593, 'ABFL-890', 'Md. Saruar Hossain', 'NA', 'Putoakhali', 'ABFL, Trishal', '01716747163', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(671, 473654, 'ABFL-504', 'Md. Ujjal Hossain', '0', 'Bogra', 'ABFL, Trishal', '01785537320', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(672, 473655, 'ABFL-505', 'Md. Hedatuil Islam', '0', 'Sirajganj ', 'ABFL, Trishal', '01712927220', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(673, 298627, 'ABFL-12', 'Sohel Rana', 'rana.abfl@akij.net', 'N', 'N', '01719428093', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(674, 527050, 'ABFL-852', 'Md. Parbas Hasan', 'NA', ' Kona Bari ,Bagana-2220, Trishal Mymensingh', 'ABFL', '01615835170', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(675, 527595, 'ABFL-891', 'Md. Ramijan Ali', 'Na', 'Trishal', 'ABFL, Trishal', '01830398581', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(676, 527598, 'ABFL-892', 'Md. Nurul 2 Alam', 'NA', 'Mymensingh', 'ABFL, Trishal', '01924319958', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(677, 429551, 'ABFL-243', 'Md.Didarul Islam', '0', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(678, 429562, 'ABFL-244', 'Md. Sahabi Alam', '0', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(679, 499964, 'ABFL-657', 'Md. Jewel Rana', 'Na', 'Trishal', 'Trishal', '01781531533', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(680, 367176, 'ABL-53', 'Tushar Saha', '', 'Satiachara,Jamurki,Mirzapur,Tangail', 'Shilmon, Tongi, Gazipur', '01710690544', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(681, 516029, 'ABFL-745', 'Bodiuzzaman', 'bodiuzzaman.abfl@akij.net', 'Rajshahi', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(682, 527052, 'ABFL-853', 'Shaun Farazi', 'Na', 'Trishal', 'ABFL, Trishal', '01754714105', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(683, 357907, 'ABFL-140', 'Md. Amir Khan', '', 'ABFL', 'ABFL', '01764042226', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(684, 526669, 'ABFL-838', 'Md. Zafu Mia', 'Na', 'Father Name: Md. Akterul Islam, Vill: Rakhal Bruz, P/S: Gobindo Gonj, Dist: Gaibandha', 'Contact Number: 01318915029', '01765591410', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(685, 459553, 'ABFL-409', 'Md. Jahidul Haque', '0', 'Baluka.Mymensingh', 'ABFL, Trishal', '01712600055', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(686, 459587, 'ABFL-410', 'Md. Jashim Uddin ', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(687, 527159, 'ABFL-873', 'Md. Habibullah Bahar', 'Na', 'Sunamganj', 'ABFL, Trishal', '01719845712', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(688, 434448, 'ABFL-303', 'Arif Miah', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(689, 434451, 'ABFL-304', 'Md. Sebow', '0', 'Nilphamary', 'ABFL', '01749976910', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(690, 421278, 'ABFL-217', 'Md. Shohid ', '0', 'Trishal ', 'ABFL', '01733442382', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(691, 502295, 'ABFL-668', 'Md. Abdullah Al Mamun', 'Na', 'Trishal', 'ABFL, Trishal', '01518337694', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(692, 382694, 'ABFL-176', 'Motiure Rahman', 'motiure.abfl@akij.net', 'Digha, Gafargoan, Mymensingh.', 'ABFL, Trishal', '01728690638', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(693, 396591, 'ABFL-197', 'Md. Hazrat Ali', 'Na', 'Muktagacha, Mymensingh', 'ABFL', '01755969773', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(694, 527638, 'ABFL-910', 'Shakhawat Hossen', 'NA', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(695, 527639, 'ABFL-910', 'Shakhawat hosen2', 'NA', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(696, 527157, 'ABFL-871', 'Md. Liton Mia ', 'Na', 'Tangail', 'ABFL, Trishal', '01794758539', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(697, 527158, 'ABFL-872', 'Md. Mamun Mia', 'Na', 'Sylhet', 'ABFL, Trishal', '01704133079', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(698, 525050, 'ABFL-801', 'Md. Sumon Mhamud', 'Na', 'Sherpur', 'ABFL, Trishal', '01601778910', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(699, 527475, 'ABFL-884', 'Md. Roysul Islam Rubal', 'NA', 'Sherpur', 'ABFL, Trishal', '01954038211', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(700, 1059, 'R479', 'Md. Jahirul Islam', 'jahir@akij.net', 'n/an', 'n/a', '01726640200', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(701, 527493, 'ABFL-885', 'Md. Saiful Islam', 'Na', 'Tongi, Gazipur', 'ABFL, Trishal', '01740576319', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(702, 477909, 'ABFLC-561', 'Md. Mahbub Alam', '0', 'Trishal', 'Mymensingh', '01999074247', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(703, 502542, 'ABFL-670', 'Naymul Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01931743503', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(704, 479259, 'ABFLC-576', 'Md. Ahsan Habib', '0', 'Trishal', 'ABFL, Factory', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(705, 479260, 'ABFLC-577', 'Ariful Islam 2', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(706, 479261, 'ABFLC-578', 'Al-Amin Sumon', '0', 'Jamalpur', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(707, 459658, 'ABFL-414', 'Bishojit Kumar', 'Na', 'Trishal', 'ABFL', '01716299990', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(708, 455184, 'ABFL-372', 'Md. Mehedi Hasan ', '0', 'Rangpur', 'ABFL', '01306827784', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(709, 527161, 'ABFL-874', 'Md. Ashikur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01957390051', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(710, 527263, 'ABFL-876', 'Md.Obaidullah', 'NA', 'Raymoni Trishal Mymensingh', 'ABFL', '01301218887', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(711, 527425, 'ABFL-881', 'Md. Akmal Hossain', 'NA', 'Pabna', 'ABFL, Trishal', '01738452770', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(712, 480598, 'ABFLC-584', 'Md. Ripon Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01741808084', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(713, 526518, 'ABFL-831', 'Joynal Abedin', 'Na', 'Trishal', 'ABFL, Trishal', '01409343494', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(714, 526526, 'ABFL-832', 'Ali Hossain', 'Na', 'Mymensingh', 'ABFL, Trishal', '01625840150', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(715, 459651, 'ABFL-413', 'Md. Shova Mia', '0', 'Trishal', 'ABFL, Trishal', '01706935626', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(716, 455233, 'ABFL-374', 'Md. Ikhlas Hossain', 'Na', 'Jhenaidha', 'ABFL', '01777510222', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(717, 502347, 'ABFL-669', 'Ashadus Jaman', 'Na', 'Gaibandha', 'ABFL, Trishal', '01517861936', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(718, 523473, 'ABFL-780', 'Md. Shariful Islam', 'Na', 'Bhaluka', 'ABFl, Trishal', '01756856406', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(719, 504507, 'ABFL-684', 'Md Sukhon Sarker', 'Na', 'Vill.Kabila Para,PO: Porabari,PS& Dist: Tangail', 'ABFL factory', '01722485767', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(720, 526640, 'ABFL-836', 'Md. Abu Sayed', 'Na', 'ConaBarri , Bagan-2220,Trishal,Mymensingh', 'ABFL', '01723235480', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(721, 515668, 'ABFL-743', 'Foysal Ahmed', 'Na', 'Trishal', 'ABFL, Trishal', '01757793837', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(722, 524574, 'ABFL-788', 'Md. Kawser Sheikh', 'Na', 'Jonasur, jonasur-8133\r\nKashiani.Gopalganj', 'AEZ', '01913707247', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(723, 504521, 'ABFL-685', 'Anowar Ahamed', 'Na', 'Trishal', 'ABFL, Trishal', '01764812723', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(724, 519353, 'ABFL-760', 'Ibrahim Khalilullah', 'NA', 'Natore', 'ABFL, Trishal', '017', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(725, 455232, 'ABFL-373', 'Md. Tarequl Islam', 'tarequl.abfl@akij.net', 'Brammanbaria', 'ABFL', '01722625190', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(726, 455234, 'ABFL-375', 'Md. Abdul Malek', 'Na', 'Gaibandha ', 'ABFL', '01744400909', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(727, 526993, 'ABFL-851', 'Md. Mahmud Hasan', 'Na', 'Trishal', 'ABFL, Trishal', '01648083555', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(728, 515184, 'ABFL-738', 'Rifat Hossain', 'Na', 'Magura', 'ABFL, Trishal ', '01902045254', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(729, 397791, 'ABFL-198', 'Kamrul Islam', 'Na', 'Trishal, Mymensingh', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(730, 432674, 'ABFL-283', 'Md. Nirob Baga', '0', 'Barishal', 'ABFL', '01703040914', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(731, 495956, 'ABFL-639', 'Arif Hossain', 'n', 'Vill:Baliapara,PO:Hodder Vita-2220,Trishal,Mymensingh', 'N', '01609334577', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(732, 388648, 'ABFL-184', 'Md. Shahidullah', '0', 'Gollabita, Horirampur, Trishal, Mymensingh.', 'Trishal, Mymensingh.', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(733, 459797, 'ABFL-416', 'Md. Ali Azam Miah', '0', 'Trishal', 'ABFL', '01616234190', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(734, 474257, 'ABFLC-510', 'Md. Abu Sayed', 'Na', 'Trishal', 'Mymensingh', '01302045449', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(735, 491695, 'ABFL-621', 'Robel Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01630310586', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(736, 525480, 'ABFL-808', 'Md. Delower Hossain', 'Na', 'Dakshinkhan, Dhaka 1230', 'ABFL', '0191115441', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(737, 405379, 'ABFL-209', 'Gayatri Biswas', 'gayatri.abfl@akij.net', 'Mirpur, Dhaka', 'ABFL,Trishal', '01746360154', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(738, 395615, 'ABFL-188', 'Md . Abdul Karim ', '0', 'Shek Bazar, Trishal Mymensingh', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(739, 432658, 'ABFL-280', 'Md. Asraful Islam (Slitter)', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(740, 432662, 'ABFL-281', 'Md. Marful Islam', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(741, 495958, 'ABFL-640', 'Golam Mostofa', 'Na', 'Vill:Binnokuri,PO:Dullah,PS:Muktagacha,Mymensingh', 'ABFL Factory', '01919474751', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(742, 474259, 'ABFLC-511', 'Md. Khadimul Haque', '', 'Trishal', 'ABFL, Trishal', '01912430814', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(743, 523598, 'ABFL-782', 'Md. Rejuan Hasan', '', 'Jamalpur', 'ABFL, Trishal', '01687117636', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(744, 473217, 'ABFL-498', 'Najmul Husain', 'Na', 'Ghana Shampur,PO:DO,Godagari,Rajsahi', 'ABFL', '01748600631', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(745, 473218, 'ABFL-499', 'Md Nazrul Islam Shafik', 'n', 'Vill:Dewna,PO:Vuleshar,PS: Kapasia,Gazipur', 'N', '01302257635', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(746, 525610, 'ABFL-812', 'MD.Kamrul Khan', 'N/A', 'Pailanpur,Manumukh,-3202,\r\nBalaganj,Sylhet.', 'ABFL', '01317870528', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(747, 462897, 'ABFL-455', 'Md. Shohel Miah', 'Na', 'Trishal', 'ABFL, Trishal', '01933716761', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(748, 432665, 'ABFL-282', 'Md. Alek Mia', 'Na', 'Valuka', 'Valuka', '01997759647', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(749, 459787, 'ABFL-415', 'Alamgir Hossen', '0', 'Trishal', 'ABFL', '01686874862', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(750, 463155, 'ABFL-457', 'Shamim Sarwar', '0', 'Trishal', 'Trishal', '01710175556', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(751, 525582, 'ABFL-810', 'Subroto Biswas', 'Na', 'Narail', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(752, 432690, 'ABFL-284', 'Md. Ismaeel Hossan', '0', 'Trishal', 'Trishal', '01777959725', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(753, 432691, 'ABFL-285', 'Md. Ali Hossin ', '0', 'Trishal', 'Trishal', '01715374769', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(754, 432695, 'ABFL-287', 'Hazrat Billal', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(755, 495964, 'ABFL-641', 'Md.Omar Faruk', 'faruk.abfl@akij.net', 'Vill:Dhonshara,PO:Golpasha,PS:Chaddogramme,Comilla', 'ABFL-Factory', '01817548398', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(756, 523555, 'ABFL-781', 'Sajedul Haq', '', 'Mymensingh', 'ABFL, Trishal', '01648368478', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(757, 510198, 'ABFL-695', 'Shahed Hasan Anik', 'Na', 'Vill:Enayetpur,PO:BAU madrasha,PS & Dist:Tangail', 'ABFL Factory', '01641111680', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(758, 432694, 'ABFL-286', 'MD. Atiqul Mia', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(759, 432698, 'ABFL-288', 'Md. Harun ', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(760, 463174, 'ABFL-458', 'Md. Omar Faruk', 'Na', 'Gazipur , Sadar', 'ABFL, Trishal', '01717477236', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(761, 510245, 'ABFL-696', 'Md Sharif Robbani', 'Na', 'Vill:Dholia,PO: Dholia,PS: Bhaluka,Mymensingh', 'N', '01934830762', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(762, 510251, 'ABFL-697', 'Jakir Hossain', 'Na', 'Vill:Raymoni,PO:Raymoni,PS: Trishal,Mymensingh', 'N', '01741485754', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(763, 437511, 'ABFL-325', 'Md. Sahidul Islam(Slitter)', '0', 'Trishal', 'Trishal', '01703206834', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(764, 437515, 'ABFL-326', 'Homayun kabir', '0', 'Trishal', 'Trishal', '01789439222', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(765, 496053, 'ABFL-642', 'Md Mofijul Islam', 'Na', 'Vill-Dopar Char,PO-Chaksahadi,PS-Sherpur,Sherpur', 'n', '01916543267', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(766, 496074, 'ABFL-645', 'Md. Rubel Howlader', '', 'Barishal', 'ABFL, Trishal', '01748422567', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(767, 474347, 'ABFLC-513', 'Md. Sariful', '0', 'Mymensingh', 'ABFL, Trishal', '01727740255', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(768, 510259, 'ABFL-698', 'Monir Hossain', 'na', 'Trishal', 'Trishal', '01611377132', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(769, 496063, 'ABFL-643', 'Humayun Kabir', 'n', 'Vill:Birampur Uzan Para,PO:Trishal,PS: Trishal,Mymensingh', 'N', '01729534330', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(770, 496064, 'ABFL-644', 'Md Faruque Hossain', 'n', 'Vill:Dhanikhola,PO:South Vatipara-2220,PS: Trishal,Mymensingh', 'N', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(771, 459888, 'ABFL-418', 'Md. Hasem Ali', '0', 'Jamalpur ', 'ABFL', '01741153941', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(772, 459889, 'ABFL-419', 'Md. Josim Uddin', 'Na', 'Trishal', 'Trishal', '01777213765', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(773, 474346, 'ABFLC-512', 'Abdulla Al Mamun', 'Na', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(774, 481571, 'ABFLC-587', 'Md Rakib Ali Sarker', 'N', 'Vill: Kismot Jallah,PO: Islampur,Islampur,Jamalpur', 'N', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(775, 517330, 'ABFL-753', 'Nazmul Huda ', '0', 'Bagora', 'ABFL, Factory', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(776, 517336, 'ABFL-754', 'Md. Abu Taohid Tarafder', 'taohid.abfl@akij.net', 'Noagan', 'ABFL, Trishal', '01701021699', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(777, 339310, 'ABFL-100', 'Abul Hossain', '', 'Sibaloy,Manikgonj', 'ABFL', '01747811689', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(778, 416392, 'ABFL (Mass-8)', 'ABFL (Mass-8)', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(779, 472867, 'ABFL-497', 'Md Mubarak Hossain', 'N', 'Vill: vabukhali,PO: Vabukhali Bazar,Mymensingh', 'N', '01714957943', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(780, 507429, 'ABFL-689', 'Moinul Islam', 'Na', 'Sarajganj', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(781, 368956, 'ABFL-149', 'Md. Sirazul Islam Shahin', 'shahin.abfl@akij.net', 'ABFL-1', 'ABFL-1', '01819545664', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(782, 485479, 'ABFL-600', 'Md. Jakir Hossen (Slitter)', '0', 'Nandail, Mymensingh', 'ABFL, Factory', '01748714472', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(783, 485484, 'ABFL-601', 'Md. Billal Hossain(Erema)', '0', 'Trishal, Mymensingh', 'ABFL, Factory', '01645275420', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(784, 527466, 'ABFL-883', 'Md. Sohag Mia2', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(785, 527645, 'ABFL-915', 'Imran Akanda', 'NA', 'Trishal', 'ABFL, Trishal', '01836759819', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(786, 492652, 'ABFL-631', 'Emam Mahedi', 'emam.abfl@akij.net', 'Vill:Khilpara,PO:Santanpara,PS:Polash,Norshingdi', 'ABFL Factory', '01739900220', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(787, 484886, 'ABFL-591', 'Nasiruzzaman(Erema)', '', 'Mymensingh', 'ABFL, Trishal', '01952036897', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(788, 513921, 'ABFL-729', 'Md. Abdul Malek', 'Na', 'Dhaka', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(789, 387845, 'ABFL-182', 'Md. Zalal Uddin(Erema)', '0', 'Trishal, Mymensingh', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(790, 485563, 'ABFL-602', 'Md. Ridoy Raihan', '0', 'Trishal', 'ABFL, Factory', '01632619105', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(791, 435998, 'ABFL-322', 'Md. Forhad Hossain', '', 'Nilphamari ', 'ABFL, Trishal', '01774405894', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(792, 436003, 'ABFL-323', 'Md. Sefatullah', '0', 'Joypurhat', 'ABFL, Trishal', '01740314064', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(793, 388245, 'ABFL-183', 'Md. Aminul Islam', '0', 'Maghurjora, Trishal, Mymensingh', 'ABFL, Trishal.', '01675867714', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(794, 494510, 'ABFL-632', 'Jakirul Islam Riad', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(795, 460627, 'ABFL-424', 'Md. Nadim Hasan', '0', 'Trishal, Mymensingh, ', 'ABFL, Trishal', '01745812821', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(796, 475624, 'ABFLC-517', 'Md. Alam Mia', '0', 'Mymensingh', 'ABFL, Trishal', '01648104094', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(797, 460632, 'ABFL-425', 'Md. Mahidul Islam', 'Na', 'Naogaon ', 'Trishal, Abfl', '01316071052', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(798, 524606, 'ABFL-789', 'Md. Mujahid Mia', 'Na', 'Hobiganj', 'ABFL, Trishal', '01320977145', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(799, 524608, 'ABFL-790', 'Mst. Nurjahan Begum', 'Na', 'Trishal', 'ABFL, Trishal', '01792664663', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(800, 469503, 'R2365', 'Robin Chandro Mozumder', 'robin.abfl@akij.net', 'Vill: South Fashion, P.O: Char Fashion, Dist: Bhola.', 'Vill: South Fashion, P.O: Char Fashion, Dist: Bhola.', '01681134996', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(801, 44250, 'ABFL-1', 'Md.Golam Mowla', 'mowla.acrl@akij.net', 'Vill- Duler Char, PO- BAU Madrasa, Ps& Dist- Tangail', 'ACRL, Trishal, Mymenshing', '01732117827', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(802, 496583, 'ABFL-650', 'Md. Mahfuzl Hossain', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(803, 496585, 'ABFL-651', 'Shariful Islam', '0', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(804, 525590, 'ABFL-811', 'Tuhin', 'Na', 'Gazipur', 'ABFL, Trishal', '01680998639', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(805, 460175, 'Intern-128', 'Atahar Mansur Talukder', '', 'Vill: Birbug Gualy, P.O: Char Gualy, P.S: Daudkhandi, Dist: Comilla', 'Vill: Birbug Gualy, P.O: Char Gualy, P.S: Daudkhandi, Dist: Comilla', '01682745289', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(806, 465198, 'R2315', 'Md. Sohel Mia', 'sohel.abfl@akij.net', 'Vill: Barabashlia, P.O: Barabashalia, P.S: Tangail, Dist: Tangail', 'Vill: Barabashlia, P.O: Barabashalia, P.S: Tangail, Dist: Tangail', '01743511887', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(807, 523606, 'R2782', 'Sadia Farzana', 'sadia.corp@akij.net', 'Vill: Terkhadia Uttarpara, P.O: Shenanibash -6202, Rajshai.\r\n', 'House: 336, Dokkhinkhan, P.O: Ashkona, P.S: Ashkona, Dhaka.\r\n', '01629093200', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(808, 460198, 'ABFL-420', 'Md. Monjur Hosan (Line)', '0', 'Trishal', 'ABFL', '01729327155', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(809, 482337, 'ABFL-588', 'Md Anisur Rahman', 'Na', 'Vill: Baysdi, Po: Baysdi,PS:Modhukhali,Faridpur', 'ABFL', '01728527784', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(810, 431774, 'ABFL-258', 'Md. Shahin Mia', 'Na', 'Trishal', 'Trishal', '01777997907', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(811, 501889, 'ABFL-663', 'Md. Raihan Mia', 'Na', 'Gujiam, Trishal', 'ABFL, Trishal', '01631679089', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(812, 342433, 'ABFL-103', 'Suranjan Roy', '0', 'Ghorait, Birishiri, Durgapur, Netrokona.', 'ABFL, Trishal, Mymensingh.', '01716519454', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(813, 349515, 'ABFL-112', 'Mst. Nurjahan Begum', '', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(814, 518806, 'ABFL-GT-755', 'Ishtiak Akanda', 'Na', 'Mymensingh', 'ABFL, Trishal', '01797372170', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(815, 518807, 'ABFL-GT-756', 'Fiyadul Islam', 'Na', 'Lakhmipur', 'ABFL, Trishal', '01400164573', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(816, 527414, 'ABFL-880', 'Anarol', 'NA', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(817, 431372, 'ABFL-242', 'Abdul Hamid Shek ', '', 'N', 'N', '01911682292', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(818, 527637, 'ABFL-909', 'Shilpy', 'NA', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(819, 431319, 'ABFL-250', 'Md. Eman Ali', '0', 'Sirajgang', 'ABFL', '01752468250', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(820, 396481, 'ABFL-191', 'Ariful Alam', '0', 'Jinaidaha', 'ABFL', '01307430393', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(821, 396482, 'ABFL-192', 'Mehedy Hasan', '0', 'Jinaidha', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(822, 396484, 'ABFL-193', 'Sujan Alam', '0', 'Jinaidha', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(823, 396485, 'ABFL-194', 'Habibur Rahaman', '0', 'Janaidha', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(824, 396486, 'ABFL-195', 'Rubel Hossain', '0', 'Jinaidha', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(825, 497699, 'ABFL-654', 'Md. Munkir Ali', 'Na', 'Rajshahi', 'ABFL, Trishal', '01867069609', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(826, 502174, 'ABFL-665', 'Md. Jahid Hasan', 'Na', 'Jamalpur', 'ABFL', '01945352775', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(827, 396491, 'ABFL-196', 'Monia ', '0', 'Sunamganj', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(828, 451507, 'ABFL-365', 'Md. Al-Amin', '0', 'Md. Abdul Razzak, Vill: Bamnakhali, P/O: Raymoni, P/S: Trishal, Dist: Mymenshing', 'Cont: 01776130174', '01764251402', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(829, 503630, 'ABFL-675', 'Md. Tanvir', 'Na', 'Jhenaidah, Moheshpur', 'ABFL, Trishal', '01709889351', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(830, 502178, 'ABFL-666', 'Md. Emanur Rahman', 'Na', 'Rangpur', 'ABFL, Factory', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(831, 526954, 'ABFL-847', 'Md. Masud Gazi', 'Na', 'Trishal', 'ABFL, Trishal', '01989335422', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(832, 526955, 'ABFL-848', 'Ashraful Alam', 'Na', 'Mymensingh', 'ABFL, Trishal', '01300604504', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(833, 526956, 'ABFL-849', 'Owahidul Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01623951282', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(834, 526957, 'ABFL-850', 'Azizul Hakim', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(835, 437518, 'ABFL-327', 'Md. Forhad Mia', '0', 'Trishal', 'Trishal', '01980359352', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(836, 380490, 'ABFL-174', 'Abdul Kuddos', '0', 'Trishal, Mymensingh.', 'ABFL, Trishal.', '01717294690', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(837, 502251, 'ABFL-667', 'Md Abdur Razzak', 'N', 'Vill:Mohabazpur,PO:Raniganj,PS+Dist:Dinajpur', 'ABFL Factory', '01735020770', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(838, 526949, 'ABFL-846', 'Md. Mustak Ahmed', 'Na', 'Trishal', 'ABFL, Trishal', '01760076558', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(839, 495412, 'ABFL-637', 'Md. Asaduzzaman', 'asaduzzaman.abfl@akij.net', 'Jessore', 'ABFL, Factory', '01714958907', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(840, 434519, 'ABFL-306', 'Md. Rana Miah', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(841, 434522, 'ABFL-307', 'Minhas Fakir', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(842, 434528, 'ABFL-308', 'Waskuruni', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(843, 461139, 'ABFL-440', 'Md. Shohag Hasan', 'shohag.abfl@akij.net', 'Jessore ', 'ABFL, Trishal', '01786105639', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(844, 520581, 'ABFL-768', 'Farhan Shahrear', 'Na', 'Trishal', 'ABFL, Trishal', '01737992622', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(845, 433407, 'ABFL-301', 'Dil Mohammmad ', '0', 'Trishal', 'Trishal', '01317578514', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(846, 454681, 'R2211', 'Md. Morshad Alam', '', 'Vill: Baro para, P.O; Datta Para, P.S: Lakshmipur Sadar, Dist: Lakshmipur. ', 'Vill: Rana Vola, P.O: Nishat Nogor, P.S: Turag, Dist: Dhaka. ', '01718912812', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(847, 434518, 'ABFL-305', 'Md. Arif Miah', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(848, 525616, 'ABFL-BOPET-813', 'Md. Sabbir Hossain ', 'Na', 'Raymoni, Trishal', 'ABFL, Trishal', '01794620154', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(849, 525618, 'ABFL-BOPET-814', 'Kausar Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01919408693', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(850, 525621, 'ABFL-815', 'Md. Parbez Iqbal ', 'Na', 'Churkhai', 'ABFL, Trishal', '01312750304', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(851, 371027, 'ABFL-155', 'Md. Razzak Ali', '0', 'Meherpur.', 'ABFL, Trishal, Mymensingh,', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(852, 461179, 'ABFL-441', 'Md. Khairul Hasan', 'khairul.abfl@akij.net', 'Santhia, Pabna', 'ABFL, Trishal', '01796925871', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(853, 433398, 'ABFL-300', 'Shahadat Hosen', '0', 'Trishal', 'Trishal', '0794769422', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(854, 503777, 'ABFL-677', 'Sanjit Sikder', 'sanjit.abfl@akij.net', 'Vill:Puraton Bandura,PO: Hasnabad,PS:Nawabganj,Dhaka', 'ABFL Factory', '01854140993', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(855, 371079, 'ABFL-156', 'Md. Saidul Islam', '', 'ABFL, Trishal.', 'ABFL, Trishal.', '01850186652', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(856, 371081, 'ABFL-157', 'Md. Gulam Sarwar', '0', 'ABFL, Trishal.', 'ABFL, Trishal.', '01755729929', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(857, 349753, 'ABFL-118', 'Md.Rahel Imam sajib', '', 'ABFL', 'ABFL', '01992889071', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(858, 503790, 'ABFL-678', 'Al Imran', 'na', 'Trishal', 'ABFL, Trishal', '01638330231', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(859, 434541, 'ABFL-309', 'Rana Mia', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(860, 434542, 'ABFL-310', 'Alamgir Hosen (Slitter)', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(861, 371088, 'ABFL-158', 'Md. Roni Mia', '0', 'ABFL, Trishal.', 'ABFL, Trishal.', '01818372153', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(862, 457865, 'ABFL-396', 'Md. Mostafizur Rahman', 'mostafizur.abfl@akij.net', 'Gazipur', 'ABFL, Trishal', '01957321646', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(863, 497498, 'ABFL-653', 'Md. Emtiaz Hossain', 'NA', 'Trishal', 'Trishal', '01845139801', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(864, 349668, 'ABFL-116', 'Prodip kumar sarkar', '', 'ABFL', 'ABFL', '01725951216', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(865, 492191, 'ABFL-630', 'Md. Minto', 'Na', 'Trishal', 'ABFL, Trishal', '01734509324', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(866, 461218, 'ABFL-442', 'Md. Jamil Hossen', 'Na', 'Pabna', 'ABFL,Trishal', '01753572775', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(867, 426736, 'ABFL-236', 'Shishir Kumar ', '0', 'Nilphamari', 'ABFL', '01717806795', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(868, 426743, 'ABFL-237', 'Krishna Chandro Ray', '0', 'Nilphamari', 'ABFL', '01788202720', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(869, 436049, 'ABFL-324', 'Shamim Mia', '0', 'Trishal', 'Trishal', '01757275127', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(870, 436051, 'ABFL-325', 'Anamul Haque', '0', 'Trishal', 'Trishal', '01840300010', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(871, 520567, 'ABFL-767', 'Md. Sydul Islam', 'Na', 'Trishal, Mymensingh', 'ABFL, Trishal', '01557360139', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(872, 349637, 'ABFL-114', 'Dilip kumar Dash', '', 'ABFL', 'ABFL', '01687813282', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(873, 435789, 'ABFL-318', 'Hamidur Rahman', '0', 'Trishal', 'Trishal', '01739033012', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(874, 509948, 'ABFL-694', 'Shanta kumar Sutradhar', '0', 'Magura, Shreepur', 'ABFL, Trishal', '01315695662', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(875, 382456, 'ABFL-175', 'Saimon Khan', '0', 'Torpurchondi, Baburhat, Chadpur,', 'ABFL-2', '01721573759', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(876, 503748, 'ABFL-676', 'Md. Anowar Hossain', 'Na', 'Naogaon', 'ABFL, Trishal', '01724283928', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(877, 434573, 'ABFL311', 'Mahabubur Rahman', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(878, 434649, 'ABFL-312', 'Sharif Husen ', '0', 'Fulbaria', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL);
INSERT INTO `tblemployees` (`id`, `enroll`, `code`, `name`, `email`, `parmanentaddress`, `presentaddress`, `contactno`, `created_at`, `updated_at`, `deleted_at`) VALUES
(879, 434655, 'ABFL-313', 'Moniruzzaman', '0', 'Trishal', 'Trishal', '01710198925', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(880, 371133, 'ABFL-159', 'Md. Abdul Ali', '', 'ABFL, Trishal.', 'ABFL, Trishal.', '01714312181', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(881, 349657, 'ABFL-115', 'Hasibul Hasan(Erema)', '0', 'Gujiram, Kasiganj, Trishal, Mymensingh.', 'ABFL', '01625911229', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(882, 460810, 'ABFL-432', 'Md. Rafiqul Islam', 'Na', 'Mumensingh', 'Trishal', '01823761712', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(883, 372503, 'ABFL-167', 'Md. Shofiqul Islam', '0', 'Maqurzora, Trishal, Mymensingh.', 'ABFL, Trishal.', '01792617541', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(884, 349504, 'ABFL-111', 'Md. Kalam', '', 'ABFL', 'ABFL', '01754047068', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(885, 457861, 'ABFL-395', 'Md. Suruz Hossain ', 'suruz.abfl@akij.net', 'Jenaidah  ', 'ABFL, Trishal', '01701011876', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(886, 520586, 'ABFL-769', 'Md. Sajibul Islam', 'NA', 'Kishoreganj', 'ABFL, Trishal', '01780177494', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(887, 349680, 'ABFL-117', 'Md. Mehedey Hasan', 'Na', 'ABFL', 'ABFL', '01799305809', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(888, 460821, 'ABFL-434', 'Shajahan (Slitter)', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(889, 460825, 'ABFL-435', 'Md. Rezaul Karim', '0', 'Nilfamari', 'ABFL, Trishal', '01773261884', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(890, 497497, 'ABFL-652', 'Md. Jahirul Islam', 'NA', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(891, 416390, 'ABFL (Mass-7)', 'ABFL (Mass-7)', 'NULL', 'NULL', 'NULL', '0NULL', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(892, 461297, 'ABFL-443', 'Md. Nazmul Haque ', '0', 'Trishal', 'ABFL, Trishal', '01944158927', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(893, 458495, 'ABFL-401', 'Md. Asadul ', '', 'Trishal', 'ABFL', '01790193527', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(894, 458690, 'ABFL-408', 'Md. Sumon Mia', '0', 'Trishal', 'ABFL, Trishal', '01734312093', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(895, 510853, 'ABFL-702', 'Alfaj Khan', 'Na', 'Trishal', 'ABFL', '01982056349', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(896, 526623, 'ABFL-835', 'Md. Kakon ', 'Na', 'Jamalpur', 'ABFL, Trishal', '01323817231', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(897, 354067, 'ABFL-136', 'Md. Ariful Islam', 'Na', 'ABFL', 'ABFL', '01942525994', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(898, 371976, 'ABFL-164', 'Rakibul Hasan', '0', 'ABFL, Trishal.', 'ABFL, Trishal.', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(899, 353787, 'ABFL-131', 'Md. Bozlur Rahman', '', 'ABFL', 'ABFL', '01735553925', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(900, 474422, 'ABFLC-515', 'Md. Shalim', 'Na', 'Trishal', 'Trishal', '01714775677', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(901, 474427, 'ABFLC-516', 'Md. Rajib mia', '0', 'Gaforgaon, Mymensingh', 'ABFL, Trishal', '01771191465', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(902, 434661, 'ABFL-315', 'Md. Omit Hasan', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(903, 354048, 'ABFL-134', 'Md. Habib (Slitter)', '', 'ABFL', 'ABFL', '01874122201', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(904, 474419, 'ABFLC-514', 'Md. Sifad Hossain', '0', 'Mymensingh', 'ABFL. Trishal', '01723407164', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(905, 527636, 'ABFL-908', 'Sanuar Hosen', 'Na', 'Trishal', 'ABFL, Trisha', '01718129027', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(906, 354064, 'ABFL-135', 'Md. Jahangir Alam', 'Na', 'ABFL', 'ABFL', '01937932407', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(907, 354116, 'ABFL-137', 'Ariful Islam', '', 'ABFL', 'ABFL', '01762861886', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(908, 524563, 'ABFL-786', 'Chanchal Kumar Shil', 'Na', 'Shantagoroni,Satinali-5910,Pachbibi,Joypurhat', 'AEZ Security Dormitory', '01738290162', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(909, 473573, 'ABFLC-502', 'Md. Shahid Mia', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(910, 515310, 'ABFL-740', 'Md. Shiblu Miah', 'Na', 'Sirajganj', 'ABFL, Trishal', '01984534385', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(911, 473567, 'ABFLC-500', 'Md. Suruj Ali', 'Na', 'Trishal', 'ABFL,Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(912, 473568, 'ABFLC-501', 'Md. Hasikul Islam', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(913, 523410, 'ABFL-779', 'Md. Abu Taher2', 'Na', 'Mymensingh', 'ABFL, Trishal', '01764263391', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(914, 432976, 'ABFL-292', 'Asrafur Rahman ', 'asrafur.abfl@akij.net> ', 'Camilla', 'ABFL', '01631308580', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(915, 290605, 'R1564', 'Kh. Tanvir Hossain', 'tanvir.apl@akij.net', 'Vill: Banira, P.O: Baniara, P.S: Mirzapur, Dist: Tangail', 'Dhaka', '01772021534', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(916, 463554, 'ABFL-465', 'Md. Tonmon Mia(Erema)', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(917, 472368, 'R2403', 'Md. Asikuzzaman Chowdhury', 'asikuzzaman.abfl@akij.net', 'Vill: Pakulla, P.O; Jamurki, P.S: Mirzapur, Dist: Tangail', '202/B, Mohammaddia Housing Ltd, Road No-6, Mhoammadpur, Dhaka-1207', '01688229502', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(918, 430751, 'ABFL-246', 'Biplob Hossain', '0', 'Kurigram', 'ABFL', '01980829888', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(919, 524777, 'ABFL-792', 'Md. Mahmodul Hasan Faysal ', 'Na', 'Sunamganj', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(920, 401719, 'ABFL-201', 'Abu Nayem Md.Nasurullah', 'nayem.abfl@akij.net', 'Mymensingh', 'ABFL', '01717309904', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(921, 523628, 'ABFL-BOPET-783', 'Fuzail Shad', '0', 'Gaforgaon, Mymensingh', 'ABFL, Trishal', '01748884880', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(922, 525303, 'ABFL-BOPET-806', 'Md. Nazmul Mondol', 'Na', 'Jaypurhat', 'ABFL, Trishal', '01929284079', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(923, 401706, 'ABFL-200', 'Md. Ahsanul Haque Khan', 'ahsanul.abfl@akij.net', 'Mymensingh', 'ABFL', '01723779332', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(924, 401725, 'ABFL-202', 'Md. Abu Rashed Talukder', 'rashed.abfl@akij.net', 'Jamalpur', 'ABFL', '01712695854', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(925, 401740, 'ABFL-204', 'Md. Bakku Mondol', 'Na', 'Pabna', 'ABFL', '01770041328', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(926, 488619, 'ABFL-604', 'Md. Khairul ', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(927, 525678, 'ABFL-816', 'Md. Farhad Hasan', 'Na', 'Gaibandha', 'ABFL, Trishal', '01707557756', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(928, 523569, 'R2776', 'Md. Serajum Monir', 'monir.abfl@akij.net', '363 Pirerbag, Mirpur, Dhaka', '363 Pirerbag, Mirpur, Dhaka', '01686057700', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(929, 524760, 'ABFL-791', 'Md. Rezaul Haque', 'Na', 'Nelfamary', 'ABFL, Trishal', '01788029911', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(930, 401735, 'ABFL-203', 'Md. Rahmat Ali', 'Na', 'Kurigram', 'ABFL', '01718758544', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(931, 401742, 'ABFL-205', 'Md. Kawsar Hossain', 'Na', 'Patuakhali', 'ABFL', '01935957320', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(932, 430850, 'ABFL-247', 'Md. Naymul Islam', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(933, 488622, 'ABFL-605', 'Tufazzul Hossain', '0', 'Mymensingh', 'ABFL, Trishal', '01400271335', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(934, 463691, 'ABFL-466', 'Md. Khairul Bashar', '0', 'Trishal', 'ABFL', '01633007210', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(935, 350022, 'ABFL-123', 'Nur Mohammod.', '', 'ABFL', 'ABFL', '01998115664', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(936, 472519, 'ABFLC-492', 'Md. Mahadi Hasan', '0', 'Mymensingh', 'Trishal', '01748571080', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(937, 472520, 'ABFLC-493', 'Md. Abu Taher', '0', 'Mymensingh', 'ABFL', '01756812134', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(938, 433199, 'ABFL-297', 'MD. Arif Miah 2', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(939, 429406, 'ABFL-240', 'Dolon Mutsuddi', '0', 'Chittagong', 'ABFL', '01836423475', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(940, 429420, 'ABFL-241', 'Priotush Kumer Roy', 'Na', 'Pabna', 'ABFL', '01709060253', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(941, 458314, 'ABFL-400', 'Minhaj Abedin (Slitter)', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(942, 433198, 'ABFL-296', 'Owahiduzzaman', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(943, 432516, 'ABFL-274', 'Md. Risul Islam', '0', 'Gaibandha', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(944, 432517, 'ABFL-275', 'Ashikur Rahaman', '0', 'Mymensingh', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(945, 458299, 'ABFL-399', 'Md. Ibrahim ', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(946, 468032, 'ABFL-477', 'Md. Afjal Hossan', '0', 'Jamalpur', 'ABFL< Trishal', '01646320350', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(947, 350103, 'ABFL-124', 'Mohammad Tashnuf Ali', 'tashnuf.abfl@akij.net', 'ABFL', 'ABFL', '01729997238', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(948, 495024, 'ABFLC-634', 'Mst Parul Akter', 'N', 'Vill-Raymoni,PO:Raymoni-2220,Trishal,Mymensingh', 'N', '01744379984', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(949, 429427, 'ABFL-242', 'Tanjimul Islam ', '0', 'Rangpur ', 'ABFL', '01866420240', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(950, 350127, 'ABFL-125', 'Md. Asraful Islam', '', 'ABFL', 'ABFL', '01626093875', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(951, 350128, 'ABFL-126', 'Md. Sabbir Ahamed', '', 'ABFL', 'ABFL', '01777959065', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(952, 350210, 'ABFL-127', 'Md. Monirul Islam', '', 'ABFL', 'ABFL', '01985801380', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(953, 527630, 'ABFL-906', 'Md. Firoj Ahmed', 'Na', 'Bagora', 'ABFL, Trishal', '01798729646', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(954, 472670, 'ABFLC-494', 'Md. Dulal ', '0', 'Trishal', 'Trishal', '01402293319', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(955, 472695, 'ABFLC-495', 'Md. Zuel Hossain (Slitter)', '0', 'Jossare ', 'ABFL, Factory', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(956, 432551, 'ABFL-276', 'Md. Shahid ', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(957, 509672, 'ABFL-693', 'Khalek Jihadi', 'Na', 'Jhenaidah', 'ABFL, Trishal', '01904561482', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(958, 490004, 'ABFL-607', 'Md Helaluddin Kari', 'n', 'Vill: Parbotipur,PO: Thutia Pakur,PS: PolashBari, Gaibanda', 'N', '01721348004', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(959, 517030, 'ABFL-749', 'Shafiqul Islam', 'Na', 'Trishal', 'ABFL, Trishal', '01911789521', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(960, 432617, 'ABFL-279', 'Monjurul Alam ', '0', 'Hobigonj', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(961, 431473, 'ABFL-252', 'Md. Sumon Miah', '0', 'Sirajganj', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(962, 480069, 'ABFLC-580', 'Md. Ariful Islam', 'Na', 'Trishal', 'ABFL,Trishal', '01945659001', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(963, 444927, 'ABFL-357', 'Azizul Islam ', '0', 'Gazipur', 'ABFL. Trishal', '01742996564', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(964, 444947, 'ABFL-359', 'Gulam Mustufa', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(965, 431465, 'ABFL-251', 'Shahan Mia', '0', 'Netrokuna', 'ABFL', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(966, 478171, 'ABFLC-565', 'Sahidul Islam', '0', 'Mymensingh', 'ABFL, Trishal', '00', '2022-01-29 23:34:30', '2022-01-29 23:34:30', NULL),
(967, 444929, 'ABFL-358', 'Abdul Karim', 'Na', 'Mymensingh', 'ABFL, Trishal', '01721292043', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(968, 480300, 'ABFLC-581', 'Md. Ahasan Habib', 'Na', 'Trishal', 'ABFL, Trishal', '01997415564', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(969, 451738, 'ABFL-366', 'Rajon Basful', '0', 'Dhanikhula, Trishal, Mymenshingh. 01407226460', 'Family Contact: 01', '01955235767', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(970, 481345, 'ABFL-586', 'Mohosin', 'Na', 'Kalihati,Tangail', 'ABFL', '01738093787', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(971, 525732, 'ABFL-BOPET-817', 'Md. Nuruzzaman2', 'Na', 'Sherpur', 'ABFL, Trishal', '01933952712', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(972, 480709, 'ABFLC-585', 'Md Abdullah Al Mamun(Slitter)', 'NA', 'Vill:Panchpara,PO:Chak Panchpara,Trishal,Mymensingh', 'N', '01308014834', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(973, 370952, 'ABFL-154', 'Md. Asraful', '', 'ABFL, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01851893935', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(974, 527640, 'ABFL-912', 'Md. Abul Bashar', 'Na', 'Trishal', 'ABFL, Trishal', '01626528470', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(975, 463094, 'ABFL-456', 'Mohamed Mujeeb Ur Rahman', 'mujeeb.abfl@akij.net', 'India', 'AEZ, ABFL, Mymensingh.', '01713998738', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(976, 329252, 'ABFL-94', 'Anup Kumar Kundu', '', 'Fultola, Khulna.', 'ABFL, Trishal, Mymensingh.', '01755905850', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(977, 480393, 'ABFLC-582', 'Md. Abu Shihab Khan', 'Na', 'Trishal', 'ABFL, Trishal', '01732500561', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(978, 433448, 'ABFL-302', 'Md. Abu Sayem ', '0', 'Trishal', 'Trishal', '01728825923', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(979, 511631, 'ABFL-707', 'Md. Kajal Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01756105647', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(980, 511633, 'ABFL-708', 'Md. Rasel Hosen', 'Na', 'Jamalpur', 'ABFL, Trishal', '01780353982', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(981, 410699, 'ABFL-211', 'Md. Saddam Hossan', 'saddam.abfl@akij.net', 'Netrokhona, Mymensingh', '0', '01701010757', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(982, 410718, 'ABFL-212', 'Julhas Mia', '0', 'Trishal, Mymensingh', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(983, 462646, 'ABFL-452', 'Md. Rakib Ahamad', 'Na', 'Trishal', 'ABFL, Trishal', '01628044017', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(984, 514734, 'ABFL-734', 'Sadman Hafiz', 'Na', 'Kushtia ', 'ABFL, Trishal', '01650060603', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(985, 471657, 'ABFL-486', 'Md. Sopan Mia', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(986, 440039, 'ABFL-351', 'Md. Dulal Miah', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(987, 509352, 'ABFL-692', 'Md. Hehedi Hasan Tushar', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(988, 459588, 'ABFL-411', 'Md. Riton Mia', '', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(989, 462673, 'ABFL-453', 'Nur Mohammad', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(990, 320417, 'ABFL-69', 'Shah Kamal', '0', 'ABFL', 'ABFL, Trishal, Mymensingh.', '01771449125', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(991, 298505, 'R1623', 'Masum Ahmed', '', 'Vill: South Kandigaon, Post: Baghapargana, P.S: Golapganj, Dist: Sylhet', 'ABFL, Trishal, Mymensingh.', '01789405807', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(992, 298975, 'ABFL-18', 'Md. Amir Hamza', '0', 'Gopalpur, Kasinathpur, Shatia, Pabna.', 'ABFL', '01737759518', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(993, 298976, 'ABFL-19', 'Md. Shakil Hosen', '', 'Shathoda, Raigonj, Shirajganj', 'ABFL', '01928140562', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(994, 204898, 'ABFL-5', 'Md.Ohedul Sikder', '', 'Md. Shahid Sikder, Chorkorfa, Kamthana, Lohgora, Norail', 'Family Mob No-01760-818665', '01761159444', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(995, 462698, 'ABFL-454', 'Sajib Miah', 'Na', 'Trishal', 'ABFL, Trishal', '01791484348', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(996, 435740, 'ABFL-316', 'Lutfor Rahman', '0', 'Trishal', 'Trishal', '01753692235', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(997, 435765, 'ABFL-317', 'Md. Amir Hamja', 'amir.abfl@akij.net', 'Sirajganj', 'ABFL, Trishal', '01710110016', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(998, 298991, 'ABFL-21', 'Rubel Mia', '0', 'Mokotpur, Rajnagar, Molobibajar.', 'ABFL', '01763699261', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(999, 321118, 'ABFL-76', 'Rushena Khatun', 'Na', 'ABFL', 'ABFL', '01994730238', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1000, 310559, 'ABFL-39', 'Md. Aminul Islam', '0', 'Chor bhelamari, Mohongonj, Rajibpur, korigram.', 'ABFL, Raimoni, Trishal, Mymensingh.', '01760250511', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1001, 304185, 'ABFL-32', 'Md.Mohosin', '0', 'Modhopara, Dolodpur, Khalihadi, Tangial.', 'ABFL, Trishal, Mymensing.', '01738093787', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1002, 302148, 'ABFL-29', 'Md. Shahidul Islam ', 'shahidul.abfl@akij.net', 'Vill: Narayonpur, Post: Jadobpur\r\nP.S: Sha rsha, Dist: Jessore.', 'ABFL, Trishal, Mymensingh.', '01778050595', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1003, 320384, 'ABFL-64', 'Md. Sana Ullah Rana', '', 'ABFL', 'ABFL', '01723244731', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1004, 320374, 'ABFL-63', 'Md. Shamim', '', 'ABFL', 'ABFL', '01791296762', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1005, 281692, 'ABFL-10', 'Ehsanul Haque', 'Na', 'Vill- Damodar. PS-Phultala. Dist-Khulna.', 'ABFL(1)-Raymoni , Trishal, Mymensingh.', '01722247203', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1006, 272683, 'ABFL-9', 'Hosan Ali', 'Na', 'Bagduli, Pangsha, Rajbari', 'APBML-2', '01725119009', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1007, 309863, 'ABFL-37', 'Md.Nazmul Hossain', 'nazmul1.abfl@akij.net', 'Doriapur, Mojibnagar, Meherpur.', 'ABFL', '01611661206', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1008, 310761, 'ABFL-40', 'Md. Sourav Mia', 'sourav.abfl@akij.net ', 'Goghat 3.No unit, Kamarjani, Gaibandha.', 'ABFL, Trishal, Mymensingh.', '01767014620', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1009, 310983, 'ABFL-43', 'Md. Kabir Hossain', '0', 'Domuritala, Barishal.', 'ABFL', '01736103685', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1010, 278949, 'ABFL(01)-04', 'Shekh Monuwar Hossain', '', 'Kuschubunia,Moralgonj,Bagarhat.', 'ABFL-01', '01747808024', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1011, 312022, 'ABFL-45', 'Md.Masud Rana', '0', 'Dokhin Domdoma, Natore.', 'ABFL , Trishal, Mymensingh.', '01712470260', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1012, 312101, 'ABFL-46', 'Md. Salim', 'Na', 'Moddo fara, Bogura.', 'ABFL , Trishal, Mymensingh.', '01718369393', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1013, 284390, 'ABFL(1)-07', 'Md.Milon Hossen', '', 'Kanchonpur, nepa,mohespor,Jhinaidha', 'Trishal,mymenshingh.', '01994685645', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1014, 290520, 'ABFL(1)-12', 'K. M. Maroof Faisal', 'maroof.abfl@akij.net', '60(7-B), Lake Circus, Kalabagan, Dhaka, Bangladesh', 'AEZ, Raymoni, Trishal, Mymensingh, Bangladesh', '01819453234', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1015, 287921, 'ABFL(1)-09', 'Abdulla All Faruk', '0', 'Baniadi,Shibpur,Narsingdi.', 'Raimoni,Trishal,Mymensingh.', '01719350270', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1016, 252182, 'APBML(2)-210', 'Sree Nirmal Sarkar', '0', 'Lalmonirhat', 'APBML-2', '01798196692', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1017, 272398, 'ABFL(1)-1', 'Rahim Shaikh', '', 'Khulna', 'ABFL-2', '01980945078', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1018, 293080, 'ABFL(1)-14', 'Md. Joynal Abedin', '0', 'Chor kumaria, Dhani Khola, Trishal, Mymensingh.', 'AEZ, Trishal, Mymensingh.', '01718668601', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1019, 299255, 'ABFL-24', 'Md. Maruf Mia', '0', 'Narionpur, Raymony, Trishal, Mymensingh.', 'ABFL', '01739601866', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1020, 299193, 'ABFL-23', 'Md. Uzzal Shak', '0', 'Ishordi, Dorompur, Rajshai.', 'ABFL', '01739412156', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1021, 298056, 'ABFL-17', 'Sisir Ranjan.', 'Sisir.abfl@akij.net', 'Murshidabad WB', 'ABFL, Trishal, Mymensingh.', '01787287252', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1022, 269376, 'ABFL-7', 'Md. Zahangir Hasan', 'hasan.abfl@akij.net', 'Vill: Kanchonpur, Post: Nepa, P.S: Moheshpur, Dist: Jhenaidah', 'Dhaka.', '01755574727', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1023, 321128, 'ABFL-79', 'Anamul Haque', '0', 'ABFL', 'ABFL', '01905841892', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1024, 270358, 'ABFL-8', 'Lokman Shaikh', 'lokman.abfl@akij.net', 'Shahebermet. Mongla, Bagherhat', 'ABFL-1', '01933630299', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1025, 231819, 'ABFL-6', 'Ashim Kumer', 'ashim.abfl@akij.net   ', 'Vill: Pukuria, Post: Pukuria, Upozilla; Dhunat, Dist: Bogra', 'Trishal, Mymensingh.', '01711136596', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1026, 316647, 'ABFL-47', 'Md.Azizul Hoque.', '0', 'Solimpur, Bashpara, Trishal, Mymensingh.', 'ABFL- Trishal.', '01981264130', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1027, 317774, 'ABFL-51', 'Shahanur', '0', 'Dokin Khan, Dhaka.', 'ABFL', '01999602217', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1028, 309699, 'ABFL-36', 'Muntasir Mahfuz', '0', 'Food Office fara, Nilphamari.', 'ABFL', '01991419993', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1029, 311764, 'ABFL-44', 'Istiak Ahmed', 'istiak.abfl@akij.net', 'Vill: Chungapasha, P.O: Cholisha, Pirojpur.', 'ABFL, Trishal. Mymensingh.', '01918174297', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1030, 277747, 'ABFL(01)03', 'Md.Salman Sarder', '0', 'Vill-Uttor Aiar kandi, P.O-Ramjanpur P.S-Kalkini,  madaripur.', 'ABFL-01', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1031, 288633, 'ABFL(1)-10', 'Md. Shiful Islam', '', 'Vill Digolia, Faridpur ,Pabna.', 'AEZ,Raimony,Trishal,Mymensing', '01789184826', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1032, 325950, 'ABFL-93', 'Md. Ahsan Habib', '', 'ABFL', 'ABFL', '01941388317', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1033, 320326, 'ABFL-60', 'Anjumanara', 'Na', 'Jagritari, Chilmari, Korigram.', 'ABFL', '01918442109', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1034, 293682, 'ABFL(1)-15', 'Mst. Rahima Begum', '0', 'Tirail, Boraigram, Natore.', 'Raymoni, Trishal, Mymensingh.', '01765901936', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1035, 301697, 'ABFL-26', 'Kh. Abdul Aziz', 'Na', 'Bagan , Trishal , Mymensingh.', 'AEZ, Trishal.', '01818444341', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1036, 301698, 'ABFL-27', 'Md. Nazmul Haque', 'Na', 'Bagan, Trishal, Mymensingh.', 'AEZ, Trishal.', '01965021570', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1037, 342449, 'ABFL-104', 'Md Imam Hasan', '0', 'Allhori, Joida, Raniganj, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01922472540', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1038, 369160, 'ABFL-150', 'Md.Nasir Uddin', '', '0', 'Family-01737555305', '01708395209', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1039, 301729, 'ABFL-28', 'Md. Islam', '0', 'Vill: Narayonpur, Post: Jadobpur\r\nP.S: Sha rsha, Dist: Jessore.', 'ABFL, Trishal, Mymensingh.', '01778050595', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1040, 320486, 'ABFL-70', 'Md. Solaiman', 'Na', 'ABFL', 'ABFL', '01953290001', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1041, 305312, 'ABFL-33', 'Md.Amirul Islam', '0', 'Trishal, Mymensingh.', 'ABFL-Trishal.', '01775961181', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1042, 307463, 'ABFL-35', 'Sabina Yeasmin', '0', 'Trishal, Mymensingh.', 'Trishal, Mymensingh.', '01782794164', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1043, 316748, 'ABFL-48', 'Laxmi Rani Barmon', '0', 'Konabari, Bagan, Trishal, Mymensingh.', 'ABFL', '01636116692', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1044, 310967, 'ABFL-42', 'Md. Rukan Uddin', '', 'Trishal, Mymensingh.', 'Trishal, Mymensingh.', '01771348168', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1045, 299042, 'ABFL-22', 'Imrul Kaish', 'imrul.abfl@akij.net', 'Char Shalimpur, Taragunia, Daulatpur, Kustia.', 'ABFL', '01780147213', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1046, 303017, 'ABFL-31', 'Nur Mohammod.', '0', 'Shejia Aminnagar, Mohespur, Jiniadha.', 'ABFL, Trishal, Mymensingh.', '01947317854', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1047, 309870, 'ABFL-38', 'Md. Alamin', '0', 'Rarjbari, ', 'ABFL', '01767919968', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1048, 324088, 'ABFL-83', 'Md.Nur-E-Alahi', 'nur.abfl@akij.net', 'ABFL, Trishal, Mymensingh.', 'Mymensingh.', '01611178755', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1049, 317402, 'ABFL-50', 'Md. Khirul Islam', '', 'Magur jora, Horirampur, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01732385538', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1050, 317775, 'ABFL-52', 'Mst. Fatema', '0', 'Raymoni, Trishal, Mymensingh.', 'ABFL', '01624401637', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1051, 319662, 'ABFL-58', 'Md. Gauser Rahman Rifat', '0', 'Ujanpara, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01742425604', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1052, 514816, 'ABFL-735', 'Golam Rabbani', 'Na', 'Kurigram', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1053, 511652, 'ABFL-709', 'Sharmin ', 'Na', 'Trishal', 'ABFL, Trishal', '017787', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1054, 511653, 'ABFL-710', 'Mahfuj Ahamed', 'Na', 'Trishal', 'ABFL, Trishal', '01766066781', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1055, 471757, 'ABFL-487', 'Md Lutfar Rahman', 'N', 'Vill:Gannar Para,PO: Gongar Char,PS: Ganngar Char,Rongpur.', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1056, 471773, 'ABFL-488', 'Arif Hossain', 'n', 'Vill: Charpara,PO: Gofaguri,Trishal,Mymensingh', 'n', '01633786467', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1057, 491647, 'ABFL-618', 'Aulad Hossen Fahad', 'Na', 'Trishal', 'ABFL, Trishal', '01612265777', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1058, 491648, 'ABFL-619', 'Rubayat Kaysar', 'Na', 'Trishal', 'ABFL, Trishal', '01719992827', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1059, 500123, 'ABFL-658', 'Md. Rabbi Hossain', 'Na', 'Trishal', 'ABFL, Trishal', '01989457378', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1060, 514863, 'ABFL-736', 'Roshon Ali', 'Na', 'Rangpur', 'ABFL, Trishal', '01304254482', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1061, 457453, 'ABFL-390', 'Md. Sohel Khan', '0', 'Trishal', 'ABFL', '01709133363', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1062, 457454, 'ABFL-391', 'Md. Abdul Hannan ', 'Na', 'Trishal', 'ABFL', '01988110339', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1063, 386567, 'ABFL-177', 'Sanjoy Kumar Pal', 'sanjoy.abfl@akij.net', 'Tower 6, Flat 11 Mik Sarani, Bidhannager, Durgapur.', 'ABFL, Trishal.', '01713998738', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1064, 457039, 'ABFL-385', 'Md. Shahin Islam', '0', 'Nilphamari', 'ABFL', '0177385424', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1065, 349597, 'ABFL-113', 'Md. Akter Hossan', 'Na', 'ABFL', 'ABFL', '01818410836', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1066, 457511, 'ABFL-392', 'Roni Roy ', '0', 'Bogra', 'ABFL', '01777186639', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1067, 287683, 'R1535', 'Md. Ismail (MD House)', 'Na', 'Vill:Mailagi, P.O: Ghior, P.S:Ghior, Dist: Manikganj.', 'Dhaka', '01928869627', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1068, 526542, 'ABFL-833', 'Taslima Khatun', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1069, 526543, 'ABFL-834', 'Parul Akter 3', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1070, 438522, 'ABFL-328', 'Md. Monjurul Alam', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1071, 439618, 'ABFL-339', 'Md. Rashid', 'Na', 'Trishal', 'Trishal', '01714220064', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1072, 526160, 'ABFL-827', 'Akram Hossain2', 'Na', 'Trishal', 'ABFL, Trishal', '01936328605', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1073, 478094, 'ABFLC-562', 'Md. Tanvir Ahamed', '0', 'Khulna', 'ABFL, Trishal', '01709889351', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1074, 457572, 'ABFL-393', 'Md. Maruf Ahmed ', 'maruf.abfl@akij.net', 'Jessore', 'ABFL, Trishal', '01716511721', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1075, 339859, 'ABFL-101', 'Md. Alamgir Hosen', '0', 'Sirajganj.', 'ABFL', '01731324984', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1076, 457600, 'ABFL-394', 'Md. Abu Tomal (Slitter)', '0', 'Panchagarh ', 'ABFL, Trishal', '01706735240', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1077, 331408, 'ABFL-97', 'Shaon Mondol', '', 'ABFL', 'ABFL', '01971706345', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1078, 331402, 'ABFL-96', 'Asik Tarafder', '', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1079, 342471, 'ABFL-107', 'Md. Tuhin', '0', 'Tolshibita, Mirzapur, Gazipur.', 'ABFL, Trishal, Mymensingh.', '01680998639', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1080, 478124, 'ABFLC-563', 'Abdus Salam', '0', 'Trishal', 'ABFL, Trishal', '01876600241', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1081, 396435, 'ABFL-190', 'Shahadat Hossan ', '0', 'Comilla', 'ABFL', '01845210291', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1082, 339941, 'ABFL-102', 'Md. Majharul Islam', '0', 'kisurganj.', 'ABFL, Trishal, Mymensingh.', '01746759698', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1083, 478169, 'ABFLC-564', 'Shariful Islam', 'n', 'Trishal,Mymensingh', 'n', '01730282236', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1084, 478172, 'ABFLC-566', 'Ariful Islam', 'n', 'Trishal,Mymensingh', 'n', '01750585136', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1085, 494580, 'ABFL-633', 'Rahat Hasan', 'na', 'Trishal', 'ABFL', '01904274268', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1086, 525771, ' ABFL-BOPET-820', 'Md. Mamun Sheikh', 'Na', 'Norail', 'ABFL, Trishal', '01954958189', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1087, 525760, 'ABFL-BOPET-818', 'Refat Hossain', 'Na', 'Trishal', 'ABFL, Trishal', '01629180717', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1088, 426181, 'ABFL-225', 'Jashim Uddin (Line)', '0', 'Trishal', 'ABFL', '01623951282', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1089, 426208, 'ABFL-228', 'Habibur Rahman', 'Na', 'Balika, Mymensingh', 'ABFL', '01629083302', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1090, 426199, 'ABFL-225', 'Sanuar Hossain', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1091, 426206, 'ABFL-227', 'Sanuar Hossain (Erema)', '0', 'Trishal, Mymensingh', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1092, 432899, 'ABFL-290', 'Khadimul Islam (Line)', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1093, 474095, 'C209', 'Farzana Akter Beauty', 'beauty.corp@akij.net', 'Vill: Humzapur, P.O; Pulhat, P.S: Sadar, Dist: Dinajpur.', 'Dhaka. ', '01762996859', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1094, 460373, 'ABFL-421', 'Md. Nazrul Islam', '0', 'Trishal', 'ABFL, Trishal', '01627653899', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1095, 474109, 'ABFLC-509', 'Kamrul Islam', 'n', 'Trishal', 'N', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1096, 517028, 'ABFL-748', 'Eyasin Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01625658507', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1097, 432892, 'ABFL-289', 'Azharul Islam', 'Na', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1098, 455236, 'ABFL-376', 'Md. Al-Hasib', '0', 'Chapainawabganj', 'ABFL', '01755636204', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1099, 437492, 'ABFL-324', 'Md. Monayem Mia', '0', 'Mymenshing', 'ABFL, Trishal', '01735989155', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1100, 371637, 'ABFL-160', 'Md. Babul Mia(Erema)', '0', 'ABFL, Trishal.', 'ABFL, Trishal.', '01766045331', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1101, 462498, 'ABFL-445', 'Firoz Ahmed ', '0', 'Tangil ', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1102, 401860, 'ABFL-206', 'Gulam Rabbani', 'rabbani.abfl@akij.net', 'Mymensingh', 'ABFL, Trishal', '01918568529', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1103, 455269, 'ABFL-377', 'Tufael Ahamed (Line)', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1104, 512494, 'ABFL-725', 'Mehedi Hasan ', 'Na', 'Trishal', 'ABFL, Trishal', '01770091852', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1105, 496103, 'ABFL-646', 'Md. Abdur Rouf', 'Na', 'Gatail, Tangile', 'ABFL, Factory', '01734073879', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1106, 457339, 'ABFL-389', 'Md. Gias Uddin 2', '0', 'Trishal', 'ABFL', '01923751491', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1107, 462502, 'ABFL-446', 'Md. Shamim ', '0', 'Trishal', 'ABFL', '01926971583', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1108, 371671, 'ABFL-161', 'Md.Tuhin', 'Na', 'Tangile.', 'ABFL, Trishal, Mymensingh,', '01709004719', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1109, 431126, 'ABFL-248', 'Harun or Rashid', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1110, 462531, 'ABFL-448', 'Md. Amdadul Haque', 'Na', 'Mymensingh', 'ABFL, Trishal', '01714241410', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1111, 462535, 'ABFL-449', 'Md. Nazmul Hasan', '0', 'Trishal', 'ABFL, Trishal', '01930279070', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1112, 462538, 'ABFL-450', 'Shariful Islam', 'Na', 'Trishal', 'ABFL', '01767500027', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1113, 462540, 'ABFL-451', 'Md. Fazlul Hoque', '0', 'Trishal', 'ABFL, Trishal', '01406351382', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1114, 342452, 'ABFL-105', 'Mst. Hamida Khatun', '', 'Bagan, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01742132896', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1115, 462458, 'R2278', 'Sizan Mahmud', 'sizan.abfl@akij.net', 'House: 211/2, P.O: Khilkhet, P.S: Vatara, Dist: Dhaka.\r\n', 'House: 211/2, P.O: Khilkhet, P.S: Vatara, Dist: Dhaka.\r\n', '01736463601', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1116, 466553, 'ABFL-472', 'Chandrapaul Das', 'chandrapaul.abfl@akij.net', 'Mymensingh', 'ABFL, Trishal', '01757173140', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1117, 496192, 'ABFL-647', 'Md. Mukter Hossain', 'Na', 'Trishal', 'ABFL, Trishal', '01910115213', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1118, 455600, 'ABFL-378', 'Md. Nurzaman', 'Na', 'Dinajpur', 'ABFL, Trishal', '01722564165', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1119, 462469, 'ABFL-444', 'Shamim Mia', 'shamim.abfl@akij.net ', 'Jamnalpur', 'ABFL', '01722378069', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1120, 431156, 'ABFL-249', 'Rashel Rana ', '0', 'Sirajganj', 'ABFL', '01780428056', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1121, 465863, 'ABFL-469', 'Md. Nur Alam', '0', 'Jamalpur,Mymensingh', 'ABFL, Factory', '01781973249', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1122, 465906, 'ABFL-470', 'Md. Tuhiddozzaman', '0', 'Trishal', 'ABFL, Trishal', '01734451392', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1123, 471125, 'ABFL-482', 'Ridoy Chondro Bisso', '0', 'Netrokuna', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1124, 491945, 'ABFL-628', 'Khalilur Rahman', 'Na', 'Hobiganj', 'ABFL, Factory', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1125, 526174, 'ABFL-828', 'Mustafizur Rahman', 'Na', 'Trishal', 'ABFL, Trishal', '01713826629', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1126, 406379, 'ABFL-210', 'Akramul Islam', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1127, 435867, 'ABFL-319', 'Josim Howlader', '0', 'Barishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1128, 439756, 'ABFL-340', 'Md. Sharif Miah', '', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1129, 439758, 'ABFL-341', 'Md. Gias Uddin', '0', 'Trishal', 'Trishal', '01782731325', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1130, 439760, 'ABFL-342', 'Minhaz Uddin', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1131, 439764, 'ABFL-343', 'Md. Abu Zahid Rifat', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1132, 491976, 'ABFL-629', 'Md. Juwel Rana', 'Na', 'Kustia', 'ABFL, Factory', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1133, 525015, 'ABFL-BOPET-794', 'Md. Milon Uddin', 'Na', 'Bogra', 'ABFL, Trishal', '01707916909', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1134, 526229, 'ABFL-829', 'Md. Naeem Mia', 'Na', 'Trishal', 'ABFL, Trishal', '01761987823', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1135, 475865, 'ABFL-519', 'Md Faruk Mia', 'n', 'Vill:Gozaria,PO:Fulchori,PS: Fulshori,Gaibanda', 'n', '01767058695', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1136, 439775, 'ABFL-344', ' Minhaz', '0', 'Trishal', 'Trishal', '01765201173', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1137, 439837, 'ABFL-347', 'Robin Hossain', 'Na', 'Trishal', 'ABFL', '01794134013', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1138, 439838, 'ABFL-348', 'Asraful Kadir', '0', 'Mymensingh', 'ABFL', '01721872477', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1139, 368746, 'ABFL-148', 'Md. Ahosan Habib', '', 'Ragulia, PolushBari,Gaibanda', 'APBML-2', '01737581122', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1140, 364264, 'ABFL-143', 'Md. Nazmul Hossen', '', 'ABFL', 'ABFL', '01636999680', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1141, 439829, 'ABFL-345', 'Imtiaj Imran (Slitter)', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1142, 392070, 'ABFL-187', 'Md Shamsil Arifin', 'shamsil.abfl@akij.net', 'Kazipara, Mirpur, Dhaka', 'ABFL, Trishal', '01704449644', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1143, 353987, 'ABFL-133', 'Mst. Shufia Khatun', 'Na', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1144, 439836, 'ABFL-346', 'Rakibul Hasan', '0', 'Trishal', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1145, 478805, 'ABFLC-570', 'Md Nazmul Haque', 'n', 'raymoni,Trishal', 'n', '01639046146', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1146, 471088, 'ABFL-481', 'Shahinur Rahman', 'n', 'Vill:Pochim Balapara,PO:Balagramm,PS: Joldhaka,Nilphamari', 'Akij economic zone', '01734204856', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1147, 522528, 'ABFL-772', 'Abu Bakar Siddik', 'Na', 'Trishal', 'ABFl, Trishal', '01766262262', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1148, 478796, 'ABFL-569', 'Syed Taifur Rahman', 'taifur.abfl@akij.net', 'Dhamurhat ,Nowgan', 'ABFL', '01768798820', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1149, 498414, 'ABFL-655', 'Rafiul Islam Rabby', 'Na', 'Raymoni, Trishal', 'Raymoni, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1150, 478936, 'ABFLC-573', 'Md. Tajul Islam', '0', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1151, 379091, 'ABFL-170', 'Md. Suroz Ali', '0', 'Bagan, Trishal, Mymensingh.', 'ABFL, Trishal.', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1152, 369647, 'ABFL-153', 'Asaduzzaman Houlader', '0', 'Pochim Baligram, Duwasar, Kalkini, madaripur,', 'ABFL-1', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1153, 527604, 'ABFL-895', 'Lukman Hossain', 'Na', 'Habiganj', 'ABFL, Trishal', '01313170216', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1154, 478827, 'ABFLC-571', 'Md Ashiqur Rahman Asik', 'n', 'Trishal,Mymensingh', 'n', '01633121860', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1155, 522530, 'ABFL-773', 'Mohammad Shova mia', 'Na', 'Trishal', 'ABFL, Trishal', '01706935626', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1156, 527051, 'R2901', 'Md. Rahat Howlader', 'rahat.abfl@akij.net', 'Vill: Chrica, P.O: Khanpur, P.S: Barishal Sadar, Dist: Barishal.\r\n', '35, West Tejturi Bazar, Firmgate, Dhaka.\r\n', '01682815611', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1157, 527062, 'ABFL-854', 'Md. Jewel Mia 2', 'Na', 'Trishal', 'ABFL, Trishal', '01645851656', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1158, 478872, 'ABFLC-572', 'Nazmul Haque', 'Na', 'Trishal,Mymensingh', 'n', '01726978859', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1159, 471173, 'ABFL-483', 'Potul ', '', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1160, 479015, 'ABFLC-573', 'Mst Parul Akter-1', 'n', 'Sakua ,Trishal', 'n', '01777375246', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1161, 526920, 'ABFL-845', 'Uzzal Hossain', 'NA', 'Trishal', 'ABFL, Trishal', '01781248749', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1162, 343999, 'ABFL-110', 'Md. Faisal Alam Siddique', '', 'Japan Garden city, Muhammodpur, Dhaka', 'ABFL', '01673087792', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1163, 479016, 'ABFLC-574', 'Mst Parul Akter -2', 'n', 'Gondokhola,Trishal', 'n', '01799895485', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1164, 371793, 'ABFL-162', 'Md. Azaduzzaman', '0', 'Trishal, Mymensingh.', 'ABFL, Trishal.', '01989345106', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1165, 371796, 'ABFL-163', 'Emdadul Hoque', '0', 'ABFL, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '01998528160', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1166, 527531, 'ABFL-886', 'Md. Azahar Ali', 'Na', 'Trishal', 'ABFL, Trishal', '01916821919', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1167, 527603, 'ABFL-893', 'Sagor Sarkar', 'Na', 'Sunamganj', 'ABFL, Trishal', '01761509898', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1168, 457130, 'ABFL-386', 'Md. Shujat Ali ', 'Na', 'Trishal', 'Trishal', '01925410283', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1169, 457136, 'ABFL-387', 'Md. Asadul Islam', '0', 'Mymensingh', 'ABFL', '01724599031', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1170, 524970, 'ABFL-793', 'Shamim Hosen', 'Na', 'Jamalpur', 'ABFL, Trishal', '01932206472', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1171, 352504, 'ABFL-129', 'Md. Rashel Mia', '', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1172, 485292, 'ABFLC-593', 'Karima Akter', 'N', 'Vill:Koragacha,PO:Raniganj,Trishal,Mymensingh', 'N', '01948219985', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1173, 485321, 'ABFL-594', 'Md. Rakibul Hasan', 'Na', 'Trishal, Mymensingh', 'ABFL, Trishal', '01703289009', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1174, 485322, 'ABFL-595', 'Md. Ariful Islam', '0', 'Trishal, Mymensingh', 'ABFL, Factory', '01759741520', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1175, 426439, 'ABFL-231', 'Md. Abu Hanif ', '0', 'Trishal', 'ABFL', '01782904671', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1176, 485323, 'ABFL-596', 'Tanvir Hasan (Erema)', '0', 'Trishal, Mymensingh', 'ABFL, Factory ', '01767883010', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1177, 485328, 'ABFL-597', 'Md. Sabbir Hossain', 'Na', 'Trishal, Mymensingh', 'ABFL, Factory', '01836819401', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1178, 485331, 'ABFL-598', 'Abdul Hannan', '0', 'Trishal, Mymensingh', 'ABFL, Factory ', '01741294164', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1179, 485340, 'ABFL-599', 'Md. Mahabub Shah', '0', 'Trishal, Mymensingh', 'ABFL, Factory', '01747942561', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1180, 426490, 'ABFL-232', 'Md. Sayem Ahmmad (Line)', '0', 'Netrokona', 'ABFL', '01734200000', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1181, 463725, 'ABFL-467', 'Faruk Ahmed', '0', 'Trishal', 'ABFL, Trishal', '01871246416', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1182, 463727, 'ABFL-468', 'Md. Bathsha Mia', '0', 'Trishal', 'ABFL, Trishal', '01408411011', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1183, 386687, 'ABFL-178', 'Obidul Hossain', 'obidul.abfl@akij.net', 'BRAHAMANBARIA', 'ABFL, ', '01734275765', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1184, 386693, 'ABFL-179', 'Sharifa Khatun', 'sharifa.abfl@akij.net', 'abfl', 'ABFL', '01758426724', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1185, 426494, 'ABFL-233', 'Jannatul Islam ', '0', 'Sherpur', 'ABFL', '01957133805', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1186, 515036, 'ABFL-737', 'Tanjir Hasan', 'tanjir.abfl@akij.net', 'Cumilla', 'ABFL, Trishal', '01712082211', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1187, 371982, 'ABFL-165', 'Uzzal Mia', '0', 'ABFL, Trishal, Mymensingh.', 'ABFL, Trishal, Mymensingh.', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1188, 371985, 'ABFL-166', 'Md. Faruk', '0', 'ABFL', 'ABFL', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1189, 426499, 'ABFL-234', 'Md. Azmul Hossen', 'azmul.abfl@akij.net', 'Brahmanbaria-3410', 'ABFL', '01831113574', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1190, 457316, 'ABFL-388', 'Anarul Islam', '0', 'Trishal', 'Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1191, 516139, 'ABFL-746', 'Faruk Ahmed2', 'Na', 'Trishal', 'ABFL, Trishal', '01785984707', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL),
(1192, 471655, 'ABFL-485', 'Shamim Rony', 'Na', 'Trishal', 'ABFL, Trishal', '00', '2022-01-29 23:34:31', '2022-01-29 23:34:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblflimcoreid`
--

CREATE TABLE `tblflimcoreid` (
  `intflimcoreid` bigint(20) NOT NULL,
  `strflimcoreid` varchar(200) DEFAULT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `ysnactive` tinyint(4) DEFAULT NULL,
  `dteentrytime` datetime(3) DEFAULT current_timestamp(3),
  `intinsertby` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblitem`
--

CREATE TABLE `tblitem` (
  `intID` bigint(20) NOT NULL,
  `intUnitID` int(11) NOT NULL DEFAULT 91,
  `strProductName` text NOT NULL,
  `ysnActive` tinyint(4) NOT NULL DEFAULT 1,
  `intInsertedBy` int(11) DEFAULT NULL,
  `strProductCode` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblitem`
--

INSERT INTO `tblitem` (`intID`, `intUnitID`, `strProductName`, `ysnActive`, `intInsertedBy`, `strProductCode`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 91, 'BOPP Film-Non Heat Sealable Transparent BOPP Film, Both Side Treated-NF-100-B', 1, NULL, 'NF-100-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(2, 91, 'Both Side Treated, Slip Modified Non-Heat Sealable Transparent BOPP Film-NF-150-B', 1, NULL, 'NF-150-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(3, 91, 'Heat Sealable Low SIT BOPP Film (90 C SIT)-HF-190-I\r\n', 1, NULL, 'HF-190-I', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(4, 91, 'Heat Sealable Low SIT BOPP Film (85C SIT)-HF-185-I\r\n', 1, NULL, 'HF-185-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(5, 91, 'Two Side Heat Sealable, One Side Matte, Other Side low Glossy BOPP Film-MF-205-B', 1, NULL, 'MF-205-B', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(6, 91, 'Two Side Heat Sealable, One Side Matte, Other Side Glossy BOPP Film-MF-200-B\r\n', 1, NULL, 'MF-200-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(7, 91, 'Both side treated Matte Surface Film-MF-203-B', 1, NULL, 'MF-203-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(8, 91, 'Wrap around Label Pearlized White voided BOPP film-LW-300-I\r\n', 1, NULL, 'LW-300-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(9, 91, 'Heat Sealable High Slip BOPP Film (115 C SIT)-HF-140\r\n', 1, NULL, 'HF-140\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(10, 91, 'Metalized grade heat sealable BOPP film-HM-400-I\r\n', 1, NULL, 'HM-400-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(11, 91, 'Heat Sealable Metalized BOPP film-MF-400-I\r\n', 1, NULL, 'MF-400-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(12, 91, 'Metallized High Barrier BOPP Film-MF-410-I\r\n', 1, NULL, 'MF-410-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(13, 91, 'Metallized Low SIT BOPP Film (90 C SIT)-MF-490-I\r\n', 1, NULL, 'MF-490-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(14, 91, 'Metallized Low SIT BOPP Film (85 C SIT)-MF-485-I\r\n', 1, NULL, 'MF-485-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(15, 91, 'Both Side Heat Seatable Anti Fog Film-ATF-1010-B\r\n', 1, NULL, 'ATF-1010-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(16, 91, 'Wrap Around Label White Voided Bopp Film-PFL-3000', 1, NULL, 'PFL-3000\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(17, 91, 'Acrylic Polymers In Primary Forms-ABX-PSA\r\n', 1, NULL, 'ABX-PSA\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(18, 91, 'BOPP Film-Metallized White Voided Film-MF-403-I\r\n', 1, NULL, 'MF-403-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(19, 91, 'MPET Film ( VAT On Process Materials and Services Charge )-MPET-421\r\n', 1, NULL, 'MPET-421\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(20, 91, 'Transparent LW Bopp Film-LW-100-I', 1, NULL, 'LW-100-I', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(21, 91, 'MCPP Film ( VAT On Process Materials and Services Charge )-MCPP-422\r\n', 1, NULL, 'MCPP-422\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(22, 91, 'Wastage-Plastic Waste Lumps-WL-700', 1, NULL, 'WL-700\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(23, 91, 'Wastage-Plastic Waste Thick Rolls-CF-800', 1, NULL, 'CF-800\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(24, 91, 'Solid White Heat Sealable BOPP Film-HF-600-I\r\n', 1, NULL, 'HF-600-I\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(25, 91, 'Non Heat Sealable Transparent Film\r\n-NF100', 1, NULL, 'NF100', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(26, 91, 'Pearllized White Voided Heat Sealable Film-PF300', 1, NULL, 'PF300\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(27, 91, 'Non Heat Sealable Metallized Film-ABX-MF-401\r\n', 1, NULL, 'MF-401\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(28, 91, 'Metallized High Barrier Film with the Alu Bond Unique Metallizing Process-MF-420\r\n', 1, NULL, 'MF-420\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(29, 91, 'BOPP Coating Jumbo Roll-AT-100\r\n', 1, NULL, 'AT-100\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(30, 91, 'BOPP Adhesive Tape (48mm)-AT-903948\r\n', 1, NULL, 'AT-903948\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(31, 91, 'BOPP Gum Tape Jumbo Roll (39 micron)-AT-10039\r\n', 1, NULL, 'AT-10039\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(32, 91, 'BOPP Film-Non- Heat Sealable Silky Matt Film-MT-215-B\r\n', 1, NULL, 'MT-215-B\r\n', '2022-01-29 15:00:52', '2022-01-29 15:00:52', NULL),
(33, 91, 'BOPP Film-Heat Sealable Silky Matt Film-MT-220-B\r\n', 1, NULL, 'MT-220-B\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(34, 91, 'Textile Grade BOPP Film- NTX-110-I\r\n', 1, NULL, 'NTX-110-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(35, 91, 'Non Heat Sealable Tape Base Film-NT100', 1, NULL, 'NT100', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(36, 91, 'BOPP Film-Transparent One Side Treated Film-TL-1003\r\n', 1, NULL, 'TL-1003\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(37, 91, 'Matt-Both Side Treated Film-MTL-206\r\n', 1, NULL, 'MTL-206\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(38, 91, 'Transparent Non Heat Sealable BOPP Film (Premium)-NF-106-I\r\n', 1, NULL, 'NF-106-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(39, 91, 'Both Side Treated Matte Surface Film- MT-210-B\r\n', 1, NULL, 'MT-210-B\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(40, 91, 'Both Side Treated, Slip Modified Non-Heat Sealable Transparent BOPP Film-NF-120-B\r\n', 1, NULL, 'NF-120-B\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(41, 91, 'BOPP Film-Pearllized White Voided BOPP Film-PF-300-I\r\n', 1, NULL, 'PF-300-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(42, 91, 'BOPP Film-Transparent Non Heat Sealable Bopp Film-NF-100-I\r\n', 1, NULL, 'NF-100-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(43, 91, 'BOPP Film-Heat Sealable Bopp Film(Both Side Heat Sealable)-HF-100-I\r\n', 1, NULL, 'HF-100-I', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(44, 91, 'BOPP Film-Tape Grade Bopp Film-NT-100-I', 1, NULL, 'NT-100-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(45, 91, 'BOPP Film-Matt Bopp Film (One Side Heat Sealable)-MT-201-I', 1, NULL, 'MT-201-I\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(46, 91, 'BOPP Film-Heat shrinkable (standard) both side sealable BOPP-HF-SC\r\n', 1, NULL, 'HF-SC\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(47, 91, 'Transparent One Side Heat sealable film-HF-1005\r\n', 1, NULL, 'HF-1005\r\n', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL),
(48, 91, '1 Side Heat Sealable Matt Surface Film-MT-201', 1, NULL, 'MT-201', '2022-01-29 15:00:53', '2022-01-29 15:00:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblitemfullspecification`
--

CREATE TABLE `tblitemfullspecification` (
  `intitemfullspecificationid` int(11) NOT NULL,
  `intitemid` int(11) DEFAULT NULL,
  `decthickness` decimal(18,2) DEFAULT NULL,
  `decwidth` decimal(18,2) DEFAULT NULL,
  `declength` decimal(18,2) DEFAULT NULL,
  `intcoreid` int(11) DEFAULT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `ysnactive` tinyint(4) DEFAULT 1,
  `inttreatmentdirectionid` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblocf`
--

CREATE TABLE `tblocf` (
  `intocfid` int(11) NOT NULL,
  `strocfno` varchar(200) DEFAULT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `dteocfdate` datetime DEFAULT NULL,
  `strpono` varchar(200) DEFAULT '',
  `dtepodate` datetime DEFAULT NULL,
  `intsalepersonid` int(11) DEFAULT NULL,
  `strsalepersonname` varchar(200) DEFAULT NULL,
  `intcustomercategory` int(11) DEFAULT NULL,
  `strcustomercategory` varchar(500) DEFAULT NULL,
  `intpackagingmode` int(11) DEFAULT NULL,
  `strpackagingmode` varchar(200) DEFAULT NULL,
  `dectorrancerate` decimal(18,2) DEFAULT NULL,
  `intcustomerid` int(11) DEFAULT NULL,
  `strcustomername` varchar(200) DEFAULT NULL,
  `intdelpointid` int(11) DEFAULT NULL,
  `strdelpointname` varchar(200) DEFAULT NULL,
  `ysnactive` tinyint(4) DEFAULT 1,
  `intinsertby` int(11) DEFAULT NULL,
  `strocfsatus` varchar(50) DEFAULT 'active',
  `strflimtype` varchar(100) DEFAULT NULL,
  `ysnqutation` tinyint(4) DEFAULT 0,
  `ysndo` tinyint(4) DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblocf`
--

INSERT INTO `tblocf` (`intocfid`, `strocfno`, `intunitid`, `dteocfdate`, `strpono`, `dtepodate`, `intsalepersonid`, `strsalepersonname`, `intcustomercategory`, `strcustomercategory`, `intpackagingmode`, `strpackagingmode`, `dectorrancerate`, `intcustomerid`, `strcustomername`, `intdelpointid`, `strdelpointname`, `ysnactive`, `intinsertby`, `strocfsatus`, `strflimtype`, `ysnqutation`, `ysndo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 91, '2022-02-01 00:00:00', '123', '2022-02-11 00:00:00', 520618, NULL, 3, NULL, 4, NULL, '12.00', NULL, NULL, 70258, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 09:33:59', '2022-02-01 09:33:59', NULL),
(2, NULL, 91, '2022-02-01 00:00:00', '123', '2022-02-11 00:00:00', 520618, NULL, 3, NULL, 4, NULL, '12.00', NULL, NULL, 70258, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 09:39:47', '2022-02-01 09:39:47', NULL),
(3, NULL, 91, '2022-02-01 00:00:00', '123', '2022-02-11 00:00:00', 520618, NULL, 3, NULL, 4, NULL, '12.00', NULL, NULL, 70258, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 09:40:14', '2022-02-01 09:40:14', NULL),
(4, NULL, 91, '2022-02-01 00:00:00', '123', '2022-02-11 00:00:00', 520618, NULL, 3, NULL, 4, NULL, '12.00', NULL, NULL, 70258, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 09:40:47', '2022-02-01 09:40:47', NULL),
(5, 'OCF22B4', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:22:31', '2022-02-01 13:22:31', NULL),
(6, 'OCF22B5', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:22:41', '2022-02-01 13:22:41', NULL),
(7, 'OCF22B06', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:01', '2022-02-01 13:26:01', NULL),
(8, 'OCF22B07', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:08', '2022-02-01 13:26:08', NULL),
(9, 'OCF22B08', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:10', '2022-02-01 13:26:10', NULL),
(10, 'OCF22B09', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:12', '2022-02-01 13:26:12', NULL),
(11, 'OCF22B10', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:14', '2022-02-01 13:26:14', NULL),
(12, 'OCF22B11', 91, '2022-02-02 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'active', NULL, 0, 0, '2022-02-01 13:26:16', '2022-02-01 13:26:16', NULL),
(13, 'OCF22B12', 91, '2022-02-19 00:00:00', NULL, NULL, 231819, NULL, 4, NULL, 1, NULL, '12.00', 396920, NULL, 72255, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 14:17:01', '2022-02-01 14:17:01', NULL),
(14, 'OCF22B13', 91, '2022-02-19 00:00:00', NULL, NULL, 231819, NULL, 4, NULL, 1, NULL, '12.00', 396920, NULL, 72255, NULL, 1, 0, 'active', 'gum', 0, 0, '2022-02-01 14:19:02', '2022-02-01 14:19:02', NULL),
(15, 'OCF22B14', 91, '2022-02-03 00:00:00', NULL, NULL, 325672, NULL, 5, NULL, 5, NULL, '12.00', 396920, NULL, 76506, NULL, 1, 0, 'active', 'flim', 0, 0, '2022-02-01 14:34:41', '2022-02-01 14:34:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblocfitems`
--

CREATE TABLE `tblocfitems` (
  `intOCFID` int(11) DEFAULT NULL,
  `strOCFNo` varchar(200) DEFAULT NULL,
  `intOCFItemsID` int(11) NOT NULL,
  `intProductID` int(11) DEFAULT NULL,
  `intProductCode` int(11) DEFAULT NULL,
  `strProductName` varchar(200) DEFAULT NULL,
  `decThickness` decimal(18,2) DEFAULT NULL,
  `decWidth` decimal(18,2) DEFAULT NULL,
  `decLength` decimal(18,2) DEFAULT NULL,
  `strTreatmentLavel` varchar(200) DEFAULT NULL,
  `intTreatmentDirectionID` int(11) DEFAULT NULL,
  `strTreatmentDirection` varchar(200) DEFAULT NULL,
  `intCoreID` int(11) DEFAULT NULL,
  `strCoreID` varchar(200) DEFAULT NULL,
  `strRealOD` varchar(200) DEFAULT NULL,
  `intOrderQty` int(11) DEFAULT NULL,
  `dteDesiredDateofDelivery` datetime(3) DEFAULT NULL,
  `dteTentativeDateofDelivery` datetime(3) DEFAULT NULL,
  `isCreateBoppPlan` tinyint(4) DEFAULT 0,
  `isCompleteBopp` tinyint(4) DEFAULT 0,
  `isCreateSlittingPlan` tinyint(4) DEFAULT 0,
  `isCompleteSlitting` tinyint(4) DEFAULT 0,
  `intUnitId` int(11) DEFAULT NULL,
  `ysnActive` tinyint(4) DEFAULT 1,
  `dteEntryTime` datetime(3) DEFAULT NULL,
  `intInsertBy` int(11) DEFAULT NULL,
  `intUOM` int(11) DEFAULT NULL,
  `strUOM` varchar(100) DEFAULT NULL,
  `strRemarks` varchar(100) DEFAULT NULL,
  `decSlittingQty` decimal(18,2) DEFAULT 0.00,
  `strStatus` varchar(50) DEFAULT 'Pending',
  `strProductCode` varchar(500) DEFAULT NULL,
  `decFGRecieveQty` decimal(18,2) DEFAULT 0.00,
  `decAvailableFGQty` decimal(18,2) DEFAULT 0.00,
  `decDeliveredQty` decimal(18,2) DEFAULT 0.00,
  `intBiaxProductFullSpecificationID` bigint(20) DEFAULT NULL,
  `decStockLotTransferQty` decimal(18,2) DEFAULT 0.00,
  `decSellingRate_per_kg` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblocfvsstockvsrequisition`
--

CREATE TABLE `tblocfvsstockvsrequisition` (
  `intocfvsstockid` bigint(20) NOT NULL,
  `dtedate` datetime(3) DEFAULT current_timestamp(3),
  `intproductfullspecificationid` bigint(20) DEFAULT NULL,
  `strtype` varchar(200) DEFAULT NULL,
  `strremark` longtext DEFAULT NULL,
  `deccreditqty` decimal(18,2) DEFAULT NULL,
  `decdebitqty` decimal(18,2) DEFAULT NULL,
  `intactionby` bigint(20) DEFAULT NULL,
  `intproductoionforocf` bigint(20) DEFAULT NULL,
  `ysnactive` tinyint(4) DEFAULT 1,
  `strocfcode` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblpackagingmode`
--

CREATE TABLE `tblpackagingmode` (
  `intpackagingmodeid` int(11) NOT NULL,
  `strpackagingmodedetails` varchar(500) DEFAULT NULL,
  `dteentrytime` datetime(3) DEFAULT current_timestamp(3),
  `intinsertby` bigint(20) DEFAULT NULL,
  `ysnenable` tinyint(4) DEFAULT NULL,
  `intunitid` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblpackagingmode`
--

INSERT INTO `tblpackagingmode` (`intpackagingmodeid`, `strpackagingmodedetails`, `dteentrytime`, `intinsertby`, `ysnenable`, `intunitid`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Packing should be with bouble wrap, foam & corrugated paper. Edge should be well protected. No edge damage is acceptable.', '0000-00-00 00:00:00.000', 523584, 1, 91, '2022-01-31 06:15:48', '2022-01-31 06:15:48', NULL),
(2, 'Packing should be Vertically Palletized. Edge should be well protected with corrugated paper. No edge damage is acceptable.', '0000-00-00 00:00:00.000', 523584, 1, 91, '2022-01-31 06:15:48', '2022-01-31 06:15:48', NULL),
(3, 'Packing should be Horizontally Palletized with Chip Board and Multiple Layer Bubble Wrap. Edge should be well protected. No edge damage is acceptable.', '0000-00-00 00:00:00.000', 523584, 1, 91, '2022-01-31 06:15:48', '2022-01-31 06:15:48', NULL),
(4, 'Packing should be Horizontally Palletized with Particle Board and Multiple Layer Bubble Wrap. Edge should be well protected. No edge damage is acceptable.', '0000-00-00 00:00:00.000', 523584, 1, 91, '2022-01-31 06:15:48', '2022-01-31 06:15:48', NULL),
(5, 'Standard Tobacco Packaging', '0000-00-00 00:00:00.000', 523584, 1, 91, '2022-01-31 06:15:48', '2022-01-31 06:15:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `business_id`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Pieces', 7, 4, NULL, '2021-04-27 10:49:34', '2021-04-27 10:49:34'),
(2, 'Liter', 7, 4, NULL, '2021-04-27 10:49:56', '2021-04-27 10:54:34'),
(3, 'Kg', 7, 4, NULL, '2021-04-27 10:50:09', '2021-04-27 10:50:09'),
(4, 'Pair', 7, 4, '2021-04-27 11:20:02', '2021-04-27 10:55:18', '2021-04-27 11:20:02'),
(5, 'Pair', 7, 4, NULL, '2021-04-27 11:20:28', '2021-04-27 11:20:28'),
(6, 'ML', 7, 4, '2021-04-29 04:55:04', '2021-04-29 04:38:05', '2021-04-29 04:55:04'),
(7, 'Dazon', 7, 4, '2021-04-29 04:50:50', '2021-04-29 04:42:09', '2021-04-29 04:50:50'),
(8, 'ff', 7, 4, '2021-04-29 04:50:57', '2021-04-29 04:47:44', '2021-04-29 04:50:57'),
(9, 'ee', 7, 4, '2021-04-29 04:50:47', '2021-04-29 04:49:36', '2021-04-29 04:50:47'),
(10, 'eeee', 7, 4, '2021-04-29 04:50:53', '2021-04-29 04:49:45', '2021-04-29 04:50:53'),
(11, 'ddddd', 7, 4, '2021-04-29 04:50:44', '2021-04-29 04:50:29', '2021-04-29 04:50:44'),
(12, 'ML', 7, 4, '2021-04-29 04:58:05', '2021-04-29 04:53:48', '2021-04-29 04:58:05'),
(13, 'll', 7, 4, '2021-04-29 04:57:43', '2021-04-29 04:56:10', '2021-04-29 04:57:43'),
(14, 'dd', 7, 4, '2021-04-29 04:57:39', '2021-04-29 04:57:29', '2021-04-29 04:57:39'),
(15, 'Ml', 7, 4, '2021-04-29 05:04:52', '2021-04-29 05:02:03', '2021-04-29 05:04:52'),
(16, 'er', 7, 4, '2021-04-29 05:04:42', '2021-04-29 05:04:01', '2021-04-29 05:04:42'),
(17, 'll', 7, 4, '2021-04-29 05:18:44', '2021-04-29 05:10:35', '2021-04-29 05:18:44'),
(18, 'h', 7, 4, '2021-04-29 05:32:30', '2021-04-29 05:28:38', '2021-04-29 05:32:30'),
(19, 'ffffff', 7, 4, '2021-04-29 05:32:27', '2021-04-29 05:30:11', '2021-04-29 05:32:27'),
(20, 'hh', 7, 4, '2021-04-29 05:39:26', '2021-04-29 05:39:16', '2021-04-29 05:39:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `surname` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL COMMENT 'user_type',
  `business_id` int(11) NOT NULL,
  `unit_ids` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_user` bit(1) NOT NULL DEFAULT b'0',
  `mobile_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nightmode` tinyint(1) NOT NULL DEFAULT 0,
  `sidebar` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 for open 0 close',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `surname`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `username`, `user_type`, `business_id`, `unit_ids`, `active_user`, `mobile_no`, `nightmode`, `sidebar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Ziaul Ahmed Ovi', NULL, 'ovi@gmail.com', NULL, '$2y$10$XrbGvMmrPojUkij2G8x5s.JI8RDu2RlY2ytXVq9yKlXIonUZUDSVi', 'admin', 0, 0, '[6]', b'0', NULL, 0, 0, NULL, '2021-04-18 02:03:25', '2022-01-30 23:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_lists`
--

CREATE TABLE `user_access_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `location_ids` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accesslist` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_access_lists`
--

INSERT INTO `user_access_lists` (`id`, `business_id`, `location_ids`, `role_name`, `accesslist`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'Admin', '[1]', NULL, '2021-04-25 15:08:10', '2021-04-25 15:08:10'),
(2, 2, '[1]', 'Admin', '[1]', NULL, '2021-04-25 15:09:42', '2021-04-25 15:09:42'),
(3, 3, '[2]', 'Admin', '[1]', NULL, '2021-04-26 03:26:13', '2021-04-26 03:26:13'),
(4, 4, '[3]', 'Admin', '[1]', NULL, '2021-04-26 03:26:56', '2021-04-26 03:26:56'),
(5, 5, '[4]', 'Admin', '[1]', NULL, '2021-04-26 03:33:11', '2021-04-26 03:33:11'),
(6, 6, '[5]', 'Admin', '[1]', NULL, '2021-04-26 03:33:53', '2021-04-26 03:33:53'),
(7, 7, '[6]', 'Admin', '[1]', NULL, '2021-04-26 03:47:27', '2021-04-26 03:47:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application_settings`
--
ALTER TABLE `application_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_locations`
--
ALTER TABLE `business_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`intcusid`);

--
-- Indexes for table `tblcustomercategory`
--
ALTER TABLE `tblcustomercategory`
  ADD PRIMARY KEY (`intcustomercategory`);

--
-- Indexes for table `tbldispoint`
--
ALTER TABLE `tbldispoint`
  ADD PRIMARY KEY (`intdispointid`);

--
-- Indexes for table `tblemployees`
--
ALTER TABLE `tblemployees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblflimcoreid`
--
ALTER TABLE `tblflimcoreid`
  ADD PRIMARY KEY (`intflimcoreid`);

--
-- Indexes for table `tblitem`
--
ALTER TABLE `tblitem`
  ADD PRIMARY KEY (`intID`);

--
-- Indexes for table `tblitemfullspecification`
--
ALTER TABLE `tblitemfullspecification`
  ADD PRIMARY KEY (`intitemfullspecificationid`);

--
-- Indexes for table `tblocf`
--
ALTER TABLE `tblocf`
  ADD PRIMARY KEY (`intocfid`);

--
-- Indexes for table `tblocfitems`
--
ALTER TABLE `tblocfitems`
  ADD PRIMARY KEY (`intOCFItemsID`),
  ADD KEY `FK_tblOCFItems_tblOCF` (`intOCFID`);

--
-- Indexes for table `tblocfvsstockvsrequisition`
--
ALTER TABLE `tblocfvsstockvsrequisition`
  ADD PRIMARY KEY (`intocfvsstockid`);

--
-- Indexes for table `tblpackagingmode`
--
ALTER TABLE `tblpackagingmode`
  ADD PRIMARY KEY (`intpackagingmodeid`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_access_lists`
--
ALTER TABLE `user_access_lists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application_settings`
--
ALTER TABLE `application_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `business_locations`
--
ALTER TABLE `business_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `intcusid` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401245;

--
-- AUTO_INCREMENT for table `tblcustomercategory`
--
ALTER TABLE `tblcustomercategory`
  MODIFY `intcustomercategory` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbldispoint`
--
ALTER TABLE `tbldispoint`
  MODIFY `intdispointid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79956;

--
-- AUTO_INCREMENT for table `tblemployees`
--
ALTER TABLE `tblemployees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1193;

--
-- AUTO_INCREMENT for table `tblflimcoreid`
--
ALTER TABLE `tblflimcoreid`
  MODIFY `intflimcoreid` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblitem`
--
ALTER TABLE `tblitem`
  MODIFY `intID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tblitemfullspecification`
--
ALTER TABLE `tblitemfullspecification`
  MODIFY `intitemfullspecificationid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblocf`
--
ALTER TABLE `tblocf`
  MODIFY `intocfid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tblocfitems`
--
ALTER TABLE `tblocfitems`
  MODIFY `intOCFItemsID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblocfvsstockvsrequisition`
--
ALTER TABLE `tblocfvsstockvsrequisition`
  MODIFY `intocfvsstockid` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpackagingmode`
--
ALTER TABLE `tblpackagingmode`
  MODIFY `intpackagingmodeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_access_lists`
--
ALTER TABLE `user_access_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblocfitems`
--
ALTER TABLE `tblocfitems`
  ADD CONSTRAINT `FK_tblOCFItems_tblOCF` FOREIGN KEY (`intOCFID`) REFERENCES `tblocf` (`intocfid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
