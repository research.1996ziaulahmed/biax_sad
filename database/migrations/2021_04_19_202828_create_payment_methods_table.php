<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 256)->nullable();
            $table->string('number', 20)->nullable();
            $table->text('description')->nullable();
            $table->string('type',30)->nullable()->comment("Personal/Agent");
            $table->boolean('enable_extra_charge')->default(0);
            $table->decimal('extra_charge')->default(0);
            $table->text('instructions')->nullable();
            $table->text('image_url')->nullable();
            $table->boolean('is_active')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
