<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('surname')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->integer('user_type')->nullable();
            $table->integer('business_id')->nullable();
            $table->text('location_ids')->default('[]');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('mobile_no')->nullable();
            $table->boolean('active_user')->default(0);
            $table->boolean('nightmode')->default(0)->comment('0 for day mode 1 for night mode');
            $table->boolean('sidebar')->default(1)->comment('1 for open 0 close');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
