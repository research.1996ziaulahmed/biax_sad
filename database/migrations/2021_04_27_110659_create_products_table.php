<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('business_id')->nullable();
            $table->enum('type', ['single', 'variable']);
            $table->integer('unit_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('sub_category_id')->nullable();
            // $table->integer('parent_id')->nullable()->comment("if single product= null ; for variable it's not null");
            $table->boolean('enable_stock')->default(1);
            $table->decimal('alert_quantity', 22, 2)->default(0)->nullable();
            $table->string('sku');
            $table->integer('created_by')->nullable();
            $table->text('details')->nullable();
            // $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
