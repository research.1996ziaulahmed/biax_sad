<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_information', function (Blueprint $table) {
            $table->id();
            $table->integer('business_id')->nullable();
            $table->integer('payment_method_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_trial_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->integer('package_id')->nullable();
            $table->integer('amount')->nullable();
            $table->text('transaction_id')->nullable();
            $table->enum('status', ['Waiting', 'Approved', 'Declined'])->default('Waiting');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_information');
    }
}
