<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->string('business_name', 256)->nullable();
            $table->string('email')->nullable();
            $table->text('logo_url')->nullable();
            $table->integer('last_package_id')->nullable();
            $table->integer('nos_of_location')->default(1);
            $table->integer('nos_of_user')->nullable();
            $table->integer('nos_of_invoice')->nullable();
            $table->integer('nos_of_product')->nullable();
            $table->date('end_trial_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->boolean('is_active')->default(0);
            
            $table->boolean('use_system_mail')->default(0);
            $table->string('MAIL_DRIVER')->nullable();
            $table->string('MAIL_HOST')->nullable();
            $table->string('MAIL_PORT')->nullable();
            $table->string('MAIL_USERNAME')->nullable();
            $table->string('MAIL_PASSWORD')->nullable();
            $table->string('MAIL_ENCRYPTION')->nullable();
            $table->string('MAIL_FROM_ADDRESS')->nullable();
            $table->string('MAIL_FROM_NAME')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
