<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_name', 256)->nullable();;;
            $table->text('app_title')->nullable();;;
            $table->boolean('superadmin_enable_register_tc')->default(0);
            $table->text('superadmin_register_tc', 256);
            $table->text('landmark')->nullable();
            $table->string('country', 100)->nullable();;
            $table->string('state', 100)->nullable();;
            $table->string('city', 100)->nullable();;
            $table->char('zip_code', 7)->nullable();;
            $table->string('mobile')->nullable();
            $table->string('alternate_number')->nullable();
            $table->string('email')->nullable();
            $table->string('MAIL_DRIVER')->nullable();
            $table->string('MAIL_HOST')->nullable();
            $table->string('MAIL_PORT')->nullable();
            $table->string('MAIL_USERNAME')->nullable();
            $table->string('MAIL_PASSWORD')->nullable();
            $table->string('MAIL_ENCRYPTION')->nullable();
            $table->string('MAIL_FROM_ADDRESS')->nullable();
            $table->string('MAIL_FROM_NAME')->nullable();
            $table->boolean('allow_email_settings_to_businesses')->default(0);
            $table->boolean('enable_new_business_registration_notification')->default(0);
            $table->boolean('enable_new_subscription_notification')->default(0);
            $table->boolean('enable_welcome_email')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_settings');
    }
}
