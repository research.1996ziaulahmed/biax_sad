<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::resources([
    '/superadmin/package' => 'superadmin\PackageController',
    '/superadmin/settings' => 'superadmin\ApplicationSettingController',
    '/superadmin/payment/method' => 'superadmin\PaymentMethodController',
    '/superadmin/business' => 'superadmin\BusinessManagementController',
    '/product-category' => 'product\CategoryController',
    '/product-variation' => 'product\VariationController',
    '/product-unit' => 'product\UnitController',
    '/product-brand' => 'product\BrandController',
    '/product' => 'product\ProductController',
    '/product-pos' => 'product\PosController',


    
    '/ocfcreate' => 'ocf\OcfCreateController',
    '/contact-employee' => 'hr\EmployeeContactController',
    '/contact-customer' => 'sad\CustomerContactController',
    '/contact-delpoint' => 'sad\DelPointContactController',
    '/item' => 'sad\ItemController',

]);
// SuperAdmin PaymentMethod
Route::get('/superadmin/payment-method/all', 'superadmin\PaymentMethodController@getall');
Route::post('/superadmin/payment-method/store', 'superadmin\PaymentMethodController@store');
Route::post('/superadmin/payment-method/{id}/edit', 'superadmin\PaymentMethodController@update');



// product-category
Route::get('product-subcategory/{id}', 'product\CategoryController@get_subcategory');

//product unit
Route::post('product-add_new_unit', 'product\UnitController@add_new_unit');


//product unit
Route::post('product-add_new_brand', 'product\BrandController@add_new_brand');

// product variable
Route::get('product-get_variable', 'product\VariationController@get_variable');
Route::get('product-get_variable_value/{id}', 'product\VariationController@get_variable_value');








//app setting
Route::get('app/night', 'AppsSettingsController@nightmoode');
Route::get('app/sidebar', 'AppsSettingsController@sidebar');





