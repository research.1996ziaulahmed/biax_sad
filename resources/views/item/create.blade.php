@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')


@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-custom-icons-tree">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{!! isset($item)? 'Edit':'Create'!!} brand</h4>
                            </div>
                            <div class="card-body ">

                                {!! Form::open(array('url' => isset($item)?'item/'.$item->id :'item','id'=>'form', 'class'=> 'needs-validation','method' => isset($item)?'put':'post' , 'novalidate')) !!}

                                <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">
                                    <label for="strProductName">Item Name:</label>
                                    {!! Form::text('strProductName', isset($item->strProductName)?$item->strProductName:'',array('class' => ($errors->first('strProductName'))? 'form-control is-invalid ':'form-control ' ,'id'=>'strProductName', 'placeholder'=>'Item Name', 'required')) !!}

                                    @if ($errors->first('strProductName'))
                                    <div class="alert alert-danger">{!! $errors->first('strProductName') !!}</div>@endif
                                </div>

                                <div class="form-group {!! $errors->first('strProductCode')?'has-error':'' !!} clear">
                                    <label for="strProductCode">Item Code:</label>
                                    {!! Form::text('strProductCode', isset($item->strProductCode)?$item->strProductCode:'',array('class' => ($errors->first('strProductCode'))? 'form-control is-invalid ':'form-control ' ,'id'=>'strProductCode', 'placeholder'=>'Item Code', 'required')) !!}

                                    @if ($errors->first('strProductCode'))
                                    <div class="alert alert-danger">{!! $errors->first('strProductCode') !!}</div>@endif
                                </div>

                                <button type="submit" class="btn btn-primary float-right  ">Add New Product</button>



                                {{ Form::close() }}
                            </div>
                        </div>

                    </div>
                   

                </div>
        </div>
    </div>
</div>

@endsection

@section('extra-js')



@endsection