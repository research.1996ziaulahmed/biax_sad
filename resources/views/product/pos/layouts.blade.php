<!DOCTYPE html>
<!--
Template Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
Author: PixInvent
Website: http://www.pixinvent.com/
Contact: hello@pixinvent.com
Follow: www.twitter.com/pixinvents
Like: www.facebook.com/pixinvents
Purchase: https://1.envato.market/vuexy_admin
Renew Support: https://1.envato.market/vuexy_admin
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.

-->
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Invoice Edit - Vuexy - Bootstrap HTML admin template</title>
    <link rel="apple-touch-icon" href="{{URL::to('')}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::to('')}}/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/bordered-layout.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/semi-dark-layout.min.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/pages/app-invoice.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/assets/css/style.css">

    <link href="{{URL::to('')}}/font-awesome/css/all.css" rel="stylesheet">
    <!--load all styles -->
    <!-- END: Custom CSS-->
    <style>
        body {
            overflow: hidden;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="">


    <!-- BEGIN: Content-->
    <div class="row" style="height: 100%;">

        <!-- Invoice Edit Left starts -->
        <div class="col-md-8 col-12" style="height: 90%; margin-top:2%;">
            <div class="card invoice-preview-card" style="height: 100%;">
                <!-- Header starts -->
                <h2>POs System</h2>
                <div class="row" style="padding: 2%;">

                    <div class="col-md-4 col-12 ">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <span class="input-group-text">

                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="100" aria-label="Amount (to the nearest dollar)">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-plus-circle"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-12 ">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon-search1">
                                    <i class="fa fa-barcode"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search..." aria-label="Search..." aria-describedby="basic-addon-search1">
                        </div>
                    </div>

                    <div class="col-md-2 col-12 ">

                        <input type="text" id="fp-default" class="form-control flatpickr-basic flatpickr-input active" placeholder="YYYY-MM-DD" readonly="readonly">
                    </div>
                    

                </div>
                <!-- Header ends -->
                <div class="col-md-12 col-12">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th class="  col-md-4 ">
                                        Product </th>
                                    <th class="  col-md-2 ">
                                        Price </th>
                                    <th class=" col-md-2">
                                        Quantity </th>

                                    <th class=" col-md-3">
                                        Subtotal </th>
                                    <th class="col-md-1"><i class="fas fa-times" aria-hidden="true"></i></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>


            </div>
        </div>
        <!-- Invoice Note ends -->

        <div class="col-md-4" style="height: 100%; overflow:scroll;  margin-top:2%;" >


            <div class="row">
                @for ($x = 0; $x <= 1000; $x++) <div class="col-md-6 ">
                    <div class="card text-center">
                        <div class="card-body">
                            <h5 class="">Special title treatment</h5>

                            Selling Price: 45400<br>
                            Quantity: 50<br>
                            (44444444444444444)
                        </div>
                    </div>
            </div>
            @endfor


        </div>
    </div>

    <!-- Invoice Edit Left ends -->


    <!-- BEGIN: Vendor JS-->
    <script src="{{URL::to('')}}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{URL::to('')}}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="{{URL::to('')}}/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
    <script src="{{URL::to('')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{URL::to('')}}/app-assets/js/core/app-menu.min.js"></script>
    <script src="{{URL::to('')}}/app-assets/js/core/app.min.js"></script>
    <script src="{{URL::to('')}}/app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{URL::to('')}}/app-assets/js/scripts/pages/app-invoice.min.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>
<!-- END: Body-->

</html>