<div class="modal fade text-left" id="add_unit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel19">Add new Unit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group ">
                    <label for="name">Unit Name:</label>
                    <input type="text" id="unit_name" class="form-control" placeholder="Unit Name" />

                </div>




            </div>
            <div class="modal-footer">

                <button class="btn btn-primary float-right  " onclick="add_new_unit()">Submit</button>
            </div>


        </div>
    </div>
</div>
</div>
<script>
    function add_new_unit() {

        $.ajax({
            url: "{{ URL::to('product-add_new_unit') }}",
            type: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                name: $("#unit_name").val(),
            },
            success: function(data) {

               var $output = '';               
            
                    $output += '<option value="' + data['id'] + '"  selected > ' + data['name'] + '</option>';
                    
                $('#unit_id').append($output);

                $('#add_unit_modal').modal('hide');
                toastr['success']('You Create a new Unit', 'Success!', {
              closeButton: true,
              tapToDismiss: false,
              timeOut: 2000,
              // extendedTimeOut: 400, 

            });


            }


        });

    }
</script>