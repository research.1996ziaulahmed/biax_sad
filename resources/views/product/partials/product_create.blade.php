<script>
    $(document).ready(function() {



        $("#category_id").change(function() {
            $('#sub_category_id').html('<option value=""></option>');
            $.ajax({
                url: "{{ URL::to('product-subcategory') }}" + '/' + $('#category_id').val(),
                type: 'GET',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    var $output = '';
                    $.each(data, function(index, value) {
                        $output += '<option value="' + value['id'] + '"> ' + value['name'] + '</option>';
                    });
                    $('#sub_category_id').append($output);
                }
            });
        });
        $("#type").change(function() {
            if ($("#type").val() == 'variable') {
                $('.manage_price').html('');
                get_variable();
                
                $(".variation").removeClass('hidden');
                $("#variation").attr("required", "true");

            } else {
                $(".variation").addClass('hidden');
                $("#variation").removeAttr('required');
                $('.variable_manage_price').html('');
                $('.manage_price').html(get_basic_info());
                get_variable();
            }
        });

        function get_variable() {
            $.ajax({
                url: "{{ URL::to('product-get_variable') }}",
                type: 'GET',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    var $output = '<option value=""></option>';
                    $.each(data, function(index, value) {
                        $output += '<option value="' + value['id'] + '"> ' + value['name'] + '</option>';
                    });
                    $('#variation').html('');
                    $('#variation').append($output);
                }
            });

        }


        $("#variation").change(function() {

            $('.variable_manage_price').html('');
            $.ajax({
                url: "{{ URL::to('product-get_variable_value') }}" + '/' + $('#variation').val(),
                type: 'GET',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    var myvar = '<table class="table">' +
                        '<tr class="text-center">' +
                        '<td>Values</td>' +
                        '<td>Opening Stock:</td>' +
                        '<td>Purchase Price:</td>' +
                        '<td>Selling Price:</td>' +
                        '' +
                        '</tr>';
                    $.each(data, function(index, value) {

                        myvar +=
                            '<tr>' +
                            '<td class="text-center"> <label for="name" class="bg-primary text-white" style="padding: 5px; ;">  ' + value['name'] + '</label>    </td>' +
                            '<td> <input type="number"   min="0"  step="any" name="v_quantity[' + value['id'] + ']" class="form-control  " required> </td>' +
                            '<td> <input type="number"   min="0"  step="any" name="v_default_purchase_price[' + value['id'] + ']" class="form-control  " required></td>' +
                            '<td> <input type="number"   min="0"  step="any" name="v_default_sell_price[' + value['id'] + ']" class="form-control  " required></td>' +
                            '</tr>';
                    });
                    myvar += '</table>';
                    $('.variable_manage_price').html(myvar);

                }
            });

        });


        function get_basic_info() {


            var myvar = '<div class="col-md-4 col-12">' +
                ' <div class="form-group  ">' +
                '<label for="name"> Opening Stock:</label>' +
                '<input type="number"   min="0"  step="any" name="quantity" class="form-control  " required>' +
                ' </div>' +
                '</div>' +
                '<div class="col-md-4 col-12">' +
                ' <div class="form-group  ">' +
                '<label for="name"> Purchase Price:</label>' +
                '<input type="number"   min="0"  step="any" name="default_purchase_price" class="form-control  " required> </div>' +
                '</div>' +
                '<div class="col-md-4 col-12">' +
                ' <div class="form-group  ">' +
                '<label for="name"> Selling Price:</label>' +
                ' ' +
                '<input type="number"   min="0"  step="any" name="default_sell_price" class="form-control  " required>  </div>' +
                '</div>';

            return myvar;

        }




    });
</script>