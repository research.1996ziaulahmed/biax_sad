@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/extensions/jstree.min.css">
<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/plugins/extensions/ext-component-tree.css">
@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-custom-icons-tree">
                <div class="row">

                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{!! isset($category)? 'Edit':'Create'!!} Category</h4>
                            </div>
                            <div class="card-body">

                            {!! Form::open(array('url' => isset($category)?'product-category/'.$category->id :'product-category','id'=>'form', 'class'=> 'needs-validation','method' => isset($category)?'put':'post', 'novalidate')) !!}
                              
                                <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">


                                    <label for="name">Category Name:</label>
                                    {!! Form::text('name', isset($category->name)?$category->name:'',array('class' => ($errors->first('name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'name', 'placeholder'=>'category Name', 'required')) !!}

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger">{!! $errors->first('name') !!}</div>@endif


                                </div>

                                <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">


                                    <label for="name">Select parent category: (Optional)</label>
                                    @if(isset($category))
                                    {!! Form::select('parent_id',[0=>'No parents Category']+\App\product\Category::where('parent_id', 0)->where('business_id',Auth::user()->business_id)->where('id','<>',$category->id)->pluck('name', 'id')->toArray(), isset($category->parent_id)?$category->parent_id:'',array('class' => 'select2 form-control ','id'=>'parent_id', '')) !!}
                                  
                                    @else
                                    {!! Form::select('parent_id',[0=>'No Parents Category']+\App\product\Category::where('parent_id', 0)->where('business_id',Auth::user()->business_id)->pluck('name', 'id')->toArray(), isset($category->parent_id)?$category->parent_id:'',array('class' => 'select2 form-control ','id'=>'parent_id', '')) !!}
                                  
                                    @endif
                                    @if ($errors->first('parent_id'))
                                    <div class="alert alert-danger">{!! $errors->first('parent_id') !!}</div>@endif


                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light   float-right ">Submit</button>



                                {{ Form::close() }}
                            </div>
                        </div>

                    </div>
                    <!-- Basic Tree -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Categories List</h4>
                                @if(isset($category))
                                <a type="button" class=" btn btn-outline-primary" href="{{URL::to('product-category')}}"><i data-feather='plus-circle'></i> &nbsp;Create</a>
                @endif
                            </div>
                            <div class="card-body">
                                <div id="jstree-basic">

                               

 

                                    <ul>
                                    @foreach($categories as $category)
                                        <li class=jstree-open data-jstree='{"icon" : "far fa-folder"}'>
                                       
                                            {{$category->name}} &nbsp; 
                                            <label>
                                            
                                            <i   style="cursor: pointer;" onclick="editpage({{$category->id}},this);" class="fa fa-edit text-primary"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i> 
                                            <i   style="cursor: pointer;" onclick='deleteInfo({{$category->id}},this);' class="fa fa-trash text-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i> 
                                            </label>
                                            <?php  $sub=\App\product\Category::where('parent_id', $category->id)->where('business_id',Auth::user()->business_id)->orderby('name')->get() ?>
                                            <ul>
                                            @foreach($sub as $s)
                                                <li  class=jstree-open data-jstree='{"icon" : "far fa-folder"}'>
                                                {{$s->name}} &nbsp;     
                                                <label>
                                                <i   style="cursor: pointer;" onclick="editpage({{$s->id}},this);"  class="fa fa-edit text-primary"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i> 
                                            <i   style="cursor: pointer;" onclick='deleteInfo({{$s->id}},this);' class="fa fa-trash text-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i> 
                                           
                                                </label>                     
                                                </li>
                                         
                                            @endforeach
                                            </ul>
                                        </li>
                                        @endforeach
                                    </ul>


                                </div>
                                
                               
                            </div>
                        </div>
                    </div>
                    <!--/ Basic Tree -->


                </div>
        </div>
    </div>
</div>

@endsection

@section('extra-js')

<script src="{{URL::to('')}}/app-assets/vendors/js/extensions/jstree.min.js"></script>
<script src="{{URL::to('')}}/app-assets/js/scripts/extensions/ext-component-tree.js"></script>
<script>

function editpage(id, e){
    window.location.href = "{{URL::to('')}}/product-category/"+id+"/edit";


}
    function deleteInfo(id, e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: "{{ URL::to('product-category') }}" + '/' + id,
                    type: 'DELETE',
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        location.reload();
                    }
                });

           
            } 
        });
    }
</script>

@endsection