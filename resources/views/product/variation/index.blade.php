@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/extensions/jstree.min.css">
<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/plugins/extensions/ext-component-tree.css">
@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-custom-icons-tree">
                <div class="row">

                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{!! isset($variation)? 'Edit':'Create'!!} Variation</h4>
                            </div>
                            <div class="card-body">

                                {!! Form::open(array('url' => isset($variation)?'product-variation/'.$variation->id :'product-variation','id'=>'form', 'class'=> 'needs-validation','method' => isset($variation)?'put':'post' , 'novalidate')) !!}

                                <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">


                                    <label for="name">Variation Name:</label>
                                    {!! Form::text('name', isset($variation->name)?$variation->name:'',array('class' => ($errors->first('name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'name', 'placeholder'=>'variation Name', 'required')) !!}

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger">{!! $errors->first('name') !!}</div>@endif


                                </div>

                                <div class="form-group  variation_raw {!! $errors->first('name')?'has-error':'' !!} clear">


                                    <label for="name">Add variation values:</label>
                                    @if(!isset($variation))
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="values[]" required>
                                        </div>
                                        <div class="col-md-2">
                                            <span class="btn btn-success " onclick="addrow();"> <span class="fa fa-plus  fa-sm"></span></span>
                                        </div>
                                    </div>


                                    @else
                                    <?php $val = \App\product\Variation::where('parent_id', $variation->id)->get();   ?>

                                    @foreach($val as $key=>$v)
                                    <div class="row" style="margin-top:10px ;">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="old_values[{{$v->id}}]" value="{{$v->name}}" required>
                                        </div>
                                        @if($key==0)
                                        <div class="col-md-2">
                                            <span class="btn btn-success " onclick="addrow();"> <span class="fa fa-plus  fa-sm"></span></span>
                                        </div>
                                        @endif
                                    </div>
                                    @endforeach
                                    @endif


                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary  float-right   ">Create</button>



                                {{ Form::close() }}
                            </div>
                        </div>

                    </div>
                    <!-- Basic Tree -->
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Variations List</h4>
                                @if(isset($variation))
                                <a type="button" class=" btn btn-outline-primary" href="{{URL::to('/product/create')}}"><i data-feather='plus-circle'></i> &nbsp;Submit</a>
                                @endif
                            </div>
                            <div class="card-body">

                                <table class="table" id="myTable">
                                    <thead>
                                        <tr>
                                            <th> Name</th>
                                            <th> Values</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($variations as $var)
                                        <tr>
                                            <td>{{$var->name}}</td>
                                            <td>
                                                <?php $val = \App\product\Variation::where('parent_id', $var->id)->get();   ?>
                                                @foreach($val as $key=>$v){{$v->name}}@if( $key < count($val)-1),&nbsp;@endif @endforeach </td>
                                            <td>
                                                <a href="{{URL::to('')}}/product-variation/{{$var->id}}/edit"><span style="cursor: pointer;" class="fa fa-edit  fa-sm text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></span></a>
                                                <span style="cursor: pointer;" class="fa fa-trash text-danger" onclick='deleteInfo({{$var->id}},this);' data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></span>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <!--/ Basic Tree -->


                </div>
        </div>
    </div>
</div>

@endsection

@section('extra-js')

<script src="{{URL::to('')}}/app-assets/vendors/js/extensions/jstree.min.js"></script>
<script src="{{URL::to('')}}/app-assets/js/scripts/extensions/ext-component-tree.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    function addrow() {


        var newraw = '<div class="row" style="margin-top:10px ;">' +
            '  <div class="col-md-10">' +
            '      <input type="text" class="form-control" name="values[]" required>' +
            '  </div>' +
            '  <div class="col-md-2">' +
            '     <span class=" btn fa fa-minus btn-danger  "  style="cursor:pointer ;" onclick="$(this).parent().parent().remove();"> </span>' +
            '  </div>' +
            '  </div>';
        $('.variation_raw').append(newraw);

    }

    function deleteInfo(id, e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: "{{ URL::to('product-variation') }}" + '/' + id,
                    type: 'DELETE',
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        location.reload();
                    }
                });


            }
        });
    }
</script>

@endsection