@extends('layouts.app')
@section('title'){{$page_title}}@endsection
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right"> </div>
            </div>
        </div>
        <div class="content-body">
            <section id="multiple-column-form">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                {!! Form::open(array('url' => isset($product)?'product/'.$product->id :'product','id'=>'form', 'class'=>'needs-validation','method' => isset($product)?'put':'post', 'novalidate')) !!}

                                <div class="row">
                                    <div class="col-md-4 col-12">
                                        <div class="form-group  ">
                                         <label for="name">Product Name:</label>
                                            {!! Form::text('name', isset($product->name)?$product->name:'',array('class' => ($errors->first('name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'name', 'placeholder'=>'Product Name', 'required')) !!}
                                            @if ($errors->first('name')) <div class="alert alert-danger">{!! $errors->first('name') !!}</div> @endif</div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group {!! $errors->first('sku')?'has-error':'' !!} "> <label for="sku">Sku/Product Code:</label>
                                            {!! Form::text('sku', isset($product->sku)?$product->sku:'',array('class' => ($errors->first('sku'))? 'form-control is-invalid ':'form-control ' ,'id'=>'sku', 'placeholder'=>'Sku', '')) !!}
                                            @if ($errors->first('sku')) <div class="alert alert-danger">{!! $errors->first('sku') !!}</div> @endif</div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="form-group "> <label for="unit_id">Unit:</label>
                                            {!! Form::select('unit_id',[''=>'']+\App\product\Unit::where('business_id',Auth::user()->business_id)->orderby('name')->pluck('name', 'id')->toArray(), isset($product->unit_id)?$product->unit_id:'',array('class' => ' form-control ', 'id'=>'unit_id', 'required')) !!}
                                              
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-12"><br><span style="cursor: pointer;" class=" text-success fas fa-plus-circle  fa-lg" data-toggle="modal" data-target="#add_unit_modal"></span>
                                    </div>
                                    <div class="col-md-3 col-12">
                                        <div class="form-group "> <label for="brand_id">Brand:</label>
                                            {!! Form::select('brand_id',[''=>'']+\App\product\Brand::where('business_id',Auth::user()->business_id)->orderby('name')->pluck('name', 'id')->toArray(), isset($product->brand_id)?$product->brand_id:'',array('class' => 'select2 form-control ', 'id'=>'brand_id', '')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-12"><br><span style="cursor: pointer;" class=" text-success fas fa-plus-circle  fa-lg" data-toggle="modal" data-target="#add_brand_modal"></span>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group {!! $errors->first('category_id')?'has-error':'' !!} "> <label for="category_id">Category:</label>
                                            {!! Form::select('category_id',[''=>'']+\App\product\Category::where('parent_id', 0)->where('business_id',Auth::user()->business_id)->orderby('name')->pluck('name', 'id')->toArray(), isset($product->category_id)?$product->category_id:'',array('class' => 'select2 form-control ', 'id'=>'category_id', '')) !!}
                                            @if ($errors->first('category_id')) <div class="alert alert-danger">{!! $errors->first('category_id') !!}</div> @endif</div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group {!! $errors->first('sub_category_id')?'has-error':'' !!} ">
                                         <label for="sub_category_id">Sub Category:</label> <select class="select2 form-control form-control-lg" name="sub_category_id" id="sub_category_id">
                                                <option value=""></option>
                                            </select></div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group {!! $errors->first('alert_quantity')?'has-error':'' !!} "> <label for="alert_quantity">Alert Quantity:</label>
                                            {!! Form::text('alert_quantity', isset($product->alert_quantity)?$product->alert_quantity:'',array('class' => ($errors->first('alert_quantity'))? 'form-control is-invalid ':'form-control ' ,'id'=>'alert_quantity', 'placeholder'=>'Alert Quantity', '')) !!}
                                        </div>
                                    </div>
                                    <?php $location =   \App\BusinessLocation::whereIn('id', json_decode(Auth::user()->location_ids))->get() ?>
                                   
                                     <div class="col-md-4 mb-1  @if(sizeof( $location)==1) hidden  @endif"> 
                                        <label id="location_txt">Business Location</label>
                                        <select onchange="changelocationQty();" id="location_ids" class="select2 form-control form-control-lg required" name="business_location" id="business_location" multiple> @if(sizeof( $location)==1)
                                             <option value="{{ $location[0]->id}}" selected>{{ $location[0]->location_name}}</option> @else @foreach($location as $l) <option value="{{ $l->id}}">{{ $l->location_name}}</option> @endforeach @endif
                                            </select>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox"> {!! Form::checkbox('enable_stock', '1', isset($product->enable_stock)?$product->enable_stock:1 ,array('class' => 'custom-control-input','id'=>'enable_stock')) !!} <label class="custom-control-label" for="enable_stock">Manage Stock</label> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=" ">
                                   
                                    <label for="type"> Product Details:</label> <textarea name="details" class="tinytextarea "></textarea>
                                </div>
                                <div class="row" style="margin-top:50px;">
                                    <div class="col-md-4 col-12">
                                        <div class="form-group {!! $errors->first('type')?'has-error':'' !!} "> <label for="type"> Product Type:</label>
                                            {!! Form::select('type',array('single'=>'Single','variable'=>'Variable'), isset($product->interval)?$product->interval:'',array('class' => ' form-control ','id'=>'type', '')) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-12 variation hidden">
                                        <div class="form-group"> <label for="variation_id"> Variation Name:</label>
                                            <select name="variation_id" class="select2 form-control" id="variation" >

                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="row  manage_price">
                                    <div class="col-md-4 col-12">
                                        <div class="form-group  quantity_div">
                                         <label for="quantity"> Opening Stock:</label>
                                         <input type="number"  min="0"  step="any" name="quantity" placeholder="Quantity" class="form-control  " required>
                                          </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group  ">
                                         <label for="default_purchase_price"> Purchase Price:</label>
                                           
                                         <input type="number"  min="0"  step="any" name="default_purchase_price" class="form-control  " required> </div>
                                    </div>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group  ">
                                         <label for="default_sell_price"> Selling Price:</label>                                        
                                         <input type="number"  min="0"  step="any" name="default_sell_price" class="form-control  " required>  </div>
                                    </div>
                                </div>

                                <div class="row  variable_manage_price">                              
                             

                                   
                                </div>

                                <div class="col-12">
                                    <br>
                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light float-right ">Submit</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
<script>
    
    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    })
</script>

<div class="app-content content ">

 <!-- Basic multiple Column Form section start -->
                <section id="multiple-column-form">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">OCF Create</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form">
                                        <div class="row">
                                            
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="first-name-column">Date</label>
                                                    <input type="date" id="first-name-column" class="form-control" placeholder="First Name" name="fname-column" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Ocf NO</label>
                                                    <input type="text" disabled id="last-name-column" class="form-control"  name="lname-column" />
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">PO No</label>
                                                    <input type="text" id="last-name-column" class="form-control"  name="lname-column" />
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">PO Date</label>
                                                    <input type="date" id="last-name-column" class="form-control"  name="lname-column" />
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Sales Person</label>
                                                     <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Product Type</label>
                                                        <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Customer Category</label>
                                                        <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Packaging Mode</label>
                                                        <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Torrance Rate(± %)</label>
                                                        <input type="text" id="last-name-column" class="form-control"  name="lname-column" />
                                                </div>
                                            </div>
                                            <table width='100%'>
                                            <tr >
                                                <td width='50%'>
                                                
                                                   <div class="col-md-12 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Customer</label>
                                                        <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Contact At" readonly />
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Address" readonly />
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Phone" readonly />
                                                    
                                                </div>
                                            </div>
                                                    
                                                </td>
                                                
                                                <td width='50%'>
                                                
                                                   <div class="col-md-12 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Del. Point	</label>
                                                        <select class="select2 form-control form-control-lg">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Contact At" readonly />
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Address" readonly />
                                                     <input type="text" id="last-name-column" class="form-control"  name="lname-column" placeholder="Phone" readonly />
                                                    
                                                </div>
                                            </div>
                                                    
                                                </td>
                                                </tr>
                                            </table>
                                            
                                            <div class="col-12">
                                                <button type="reset" class="btn btn-primary mr-1 ">Genarate OCF</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
               </div>




<div class="app-content content ">

 <!-- Basic multiple Column Form section start -->
                <section id="multiple-column-form">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">OCF NO: 000000</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form">
                                        <div class="row">
                                            
                                            
                                                 <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Product Name	</label>
                                                        <select class="select2 form-control form-control-lg" id="s_productcode">
                                                <option value="">Please Select Product</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                 
                                                    
                                                </div>
                                                </div>
                                                     
                                                 <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Thickness	</label>
                                                        <input type="text"  class="form-control"  name="fname-column" id="s_thickness"/>
                                                 
                                                    
                                                </div>
                                                </div>
                                           
                                     
                                                     
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Width	</label>
                                                        <input type="text" id="s_width" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                                     
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Length	</label>
                                                        <input type="text" id="s_length" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                                     
                                            
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Treatment Level	</label>
                                                        <input type="text" id="s_treatment_level" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Treatment Direction	</label>
                                                        <select class="select2 form-control form-control-lg" id="s_treatment_direction">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                 
                                                    
                                                </div>
                                                </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Core ID	</label>
                                                       <input type="text" id="s_core_id" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Reel OD	</label>
                                                       <input type="text" id="s_reel_od" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                            <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">UOM	</label>
                                                      <select class="select2 form-control form-control-lg " id="s_uom">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option>
                                               
                                            </select>
                                                    
                                                </div>
                                                </div>
                                               <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Order Qty	</label>
                                                       <input type="text" id="s_order_qty" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                            
                                               <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Desired Date	</label>
                                                       <input type="Date" id="s_desired_date" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                               <div class="col-md-2 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Tentative Date	</label>
                                                       <input type="Date" id="s_tentative_date" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                               <div class="col-md-7 col-12">
                                                <div class="form-group">
                                                    <label for="last-name-column">Remarks	</label>
                                                       <input type="text" id="s_remarks" class="form-control"  name="fname-column" />
                                                 
                                                    
                                                </div>
                                                </div>
                                            
                                               <div class="col-md-1 col-12">
                                                <div class="form-group">
                                                    <br>
                                                       <span class="btn btn-info" onclick="return additemtoocflist()"><i class="fas fa-plus-circle"></i></span>
                                                 
                                                    
                                                </div>
                                                </div>
                                             <div class="col-12">
                                                 
                                                 
                                                 <br>
                                                 <br>
                                                 
                                                 
                                            <table class="table table-bordered table-hover table-striped table-responsive text-center " style="font-size:12px;" >
                                                <thead class="bg-primary text-white">
                                             
                                                <td>Product Code</td>
                                                <td>Description</td>
                                                <td>Thickness</td>
                                                <td>Width</td>
                                                <td>Length</td>
                                                <td>Treatment Level</td>
                                                <td>Treatment Direction</td>
                                                <td>Core ID</td>
                                                <td>Reel OD</td>
                                                <td>Order Qty</td>

                                                <td>Desired Date</td>
                                                <td>Tentative Date</td>
                                                <td>Remarks</td>
                                                <td></td>
                                                
                                            </thead>
                                        <tbody class="ocfbody">

<!--
                                            <tr>
                                                   
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Product Code" name="fname-column" /></td>
                                                <td>Description</td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Thickness" name="fname-column" /></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Width" name="fname-column" /></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Length" name="fname-column" /></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Treatment Level" name="fname-column" /></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Treatment Direction" name="fname-column" /></td>
                                                <td><select class=" form-control form-control-lg hidden">
                                                <option >Core Id</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="CA">California</option></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Read OD" name="fname-column" /></td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Order Qty" name="fname-column" /></td>
                                                <td><input type="Date" id="first-name-column" class="form-control hidden" placeholder="Desired Date"  name="fname-column" /></td>
                                                <td>Tentative Date</td>
                                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Remarks" name="fname-column" /></td>
                                                <td><span class="fas fa-times text-danger" style="cursor:pointer;" onclick="$(this).parent().parent().remove();"></span></td>                                           

                                            </tr>
-->
                                        

                                        </tbody>
                                            </table>
                                            </div>
                                           
                                          <br>
                                          <br>
                                            <div class="col-12">
                                                <button type="reset" class="btn btn-success mr-1 ">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
    
    
    
    <script>
    function additemtoocflist(){
     
        var variable = '' + 
'			<tr>' + 
'                                                   ' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Product Code" name="productcode['+$("#s_productcode").val()+']" value="'+$("#s_productcode").val()+'"/>'+$("#s_productcode").val()+'</td>' + 
'                                                <td>'+$("#s_productcode").text()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Thickness" name="thickness['+$("#s_productcode").val()+']" value="'+$("#s_thickness").val()+'" />'+$("#s_thickness").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Width" name="width['+$("#s_productcode").val()+']" value="'+$("#s_width").val()+'" />'+$("#s_width").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Length" name="length['+$("#s_productcode").val()+']" value="'+$("#s_length").val()+'" />'+$("#s_length").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Treatment Level" name="treatment_level['+$("#s_productcode").val()+']" value="'+$("#s_treatment_level").val()+'" />'+$("#s_treatment_level").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Treatment Direction" name="treatment_direction['+$("#s_productcode").val()+']" value="'+$("#s_treatment_direction").val()+'" />'+$("#s_treatment_direction").val()+'</td>' + 
'                                                <td> ' + 
'												<input type="text"  class="form-control hidden" placeholder="Treatment Direction" name="core_id['+$("#s_productcode").val()+']" value="'+$("#s_core_id").val()+'" />'+$("#s_core_id").val()+ 
'												</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Reel OD" name="reel_od['+$("#s_productcode").val()+']" value="'+$("#s_reel_od").val()+'" />'+$("#s_reel_od").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Order Qty" name="order_qty['+$("#s_productcode").val()+']" value="'+$("#s_order_qty").val()+'" />'+$("#s_order_qty").val()+'</td>' + 
'                                                <td><input type="Date"  class="form-control hidden" placeholder="Desired Date"  name="desired_date['+$("#s_productcode").val()+']" value="'+$("#s_desired_date").val()+'" />'+$("#s_desired_date").val()+'</td>' +
'                                                <td><input type="Date"  class="form-control hidden" placeholder="Tentative Date"  name="tentative_date['+$("#s_productcode").val()+']" value="'+$("#s_tentative_date").val()+'" />'+$("#s_tentative_date").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Remarks" name="remarks['+$("#s_productcode").val()+']" value="'+$("#s_remarks").val()+'" />'+$("#s_remarks").val()+'</td>' + 
'                                                <td><span class="fas fa-times text-danger" style="cursor:pointer;" onclick="$(this).parent().parent().remove();"></span></td>                                           ' + 
'' + 
'                                            </tr>' + 
'';
       
        $(".ocfbody").append(variable);
        
       
        $("#s_productcode").val("").change();   
        $("#s_thickness").val('');
        $("#s_width").val('');
        $("#s_length").val('');
        $("#s_treatment_level").val('');
        $("#s_core_id").val('');
        $("#s_reel_od").val('');
        $("#s_order_qty").val('');
        $("#s_desired_date").val('');
        $("#s_tentative_date").val('');
        $("#s_remarks").val('');
        return false;
    }
    </script>
               </div>


@include('product.partials.unit_create_modal')
@include('product.partials.brand_create_modal')@endsection
@section('extra-js')
@include('product.partials.product_create')@endsection