@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/extensions/jstree.min.css">
<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/plugins/extensions/ext-component-tree.css">
@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="basic-custom-icons-tree">
                <div class="row">

                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">{!! isset($brand)? 'Edit':'Create'!!} brand</h4>
                            </div>
                            <div class="card-body ">

                                {!! Form::open(array('url' => isset($brand)?'product-brand/'.$brand->id :'product-brand','id'=>'form', 'class'=> 'needs-validation','method' => isset($brand)?'put':'post' , 'novalidate')) !!}

                                <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">
                                    <label for="name">Brand Name:</label>
                                    {!! Form::text('name', isset($brand->name)?$brand->name:'',array('class' => ($errors->first('name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'name', 'placeholder'=>'Brand Name', 'required')) !!}

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger">{!! $errors->first('name') !!}</div>@endif
                                </div>


                                <button type="submit" class="btn btn-primary float-right  ">Submit</button>



                                {{ Form::close() }}
                            </div>
                        </div>

                    </div>
                    <!-- Basic Tree -->
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Brands List</h4>
                                @if(isset($brand))
                                <a type="button" class=" btn btn-outline-primary" href="{{URL::to('/product-brand')}}"><i data-feather='plus-circle'></i> &nbsp;Create</a>
                                @endif
                            </div>
                            <div class="card-body">
                    <table class="table "  id="myTable">
                    <thead>
                    <tr>
                    <th class="col-md-10">brand Name</th>
                    <th class="col-md-2">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($brands as $u)
                    <tr>
                    <td>{{$u->name}}</td>
                    <td>
                    <a href="{{URL::to('')}}/product-brand/{{$u->id}}/edit"><span style="cursor: pointer;" class="fa fa-edit  fa-sm text-primary "   data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></span></a>
                                   <span style="cursor: pointer;" class="fa fa-trash text-danger"   onclick='deleteInfo({{$u->id}},this);'  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></span>
                                  
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>

                            </div>
                        </div>
                    </div>
                    <!--/ Basic Tree -->


                </div>
        </div>
    </div>
</div>

@endsection

@section('extra-js')

<script src="{{URL::to('')}}/app-assets/vendors/js/extensions/jstree.min.js"></script>
<script src="{{URL::to('')}}/app-assets/js/scripts/extensions/ext-component-tree.js"></script>
<script>
  $(document).ready( function () {
    $('#myTable').DataTable();
} );

    function deleteInfo(id, e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: "{{ URL::to('product-brand') }}" + '/' + id,
                    type: 'DELETE',
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(data) {
                        location.reload();
                    }
                });


            }
        });
    }
</script>

@endsection