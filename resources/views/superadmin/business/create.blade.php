@extends('layouts.app')
@section('title'){{$page_title}}@endsection
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="multiple-column-form">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                {!! Form::open(array('url' => isset($business)?'superadmin/business/'.$business->id
                                :'superadmin/business','id'=>'form','method' => isset($business)?'put':'post')) !!}

                                <h3>Business Setting</h3>
                                <div class="row" style="margin-right:0;">

                                    <div class="col-md-4 col-12">

                                        <label for="business_name">Business Name:</label>
                                        {!! Form::text('business_name',
                                        isset($setting->business_name)?$setting->business_name:'',array('class' =>
                                        ($errors->first('business_name'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'business_name', 'placeholder'=>'Business Name', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">
                                        <label for="landmark">Landmark:</label>
                                        {!! Form::text('landmark',
                                        isset($setting->landmark)?$setting->landmark:'',array('class' =>
                                        ($errors->first('landmark'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'landmark', 'placeholder'=>'Landmark', '')) !!}

                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="state">State</label>
                                        {!! Form::text('state', isset($setting->state)?$setting->state:'',array('class'
                                        => ($errors->first('state'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'state', 'placeholder'=>'State', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="country">Country :</label>
                                        {!! Form::text('country',
                                        isset($setting->country)?$setting->country:'',array('class' =>
                                        ($errors->first('country'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'country', 'placeholder'=>'Country', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="city">City :</label>
                                        {!! Form::text('city', isset($setting->city)?$setting->city:'',array('class' =>
                                        ($errors->first('city'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'city', 'placeholder'=>'City', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="zip_code">Zip Code :</label>
                                        {!! Form::text('zip_code',
                                        isset($setting->zip_code)?$setting->zip_code:'',array('class' =>
                                        ($errors->first('zip_code'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'zip_code', 'placeholder'=>'Zip Code', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="mobile">Mobile :</label>
                                        {!! Form::text('mobile',
                                        isset($setting->mobile)?$setting->mobile:'',array('class' =>
                                        ($errors->first('mobile'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'mobile', 'placeholder'=>'Mobile', '')) !!}
                                    </div>

                                    <div class="col-md-4 col-12">

                                        <label for="alternate_number">Alternate Number :</label>
                                        {!! Form::text('alternate_number',
                                        isset($setting->alternate_number)?$setting->alternate_number:'',array('class' =>
                                        ($errors->first('alternate_number'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'alternate_number', 'placeholder'=>'Alternate Mobile Number', '')) !!}
                                    </div>

                                    <!-- <div class="col-md-4 col-12">

                                        <label for="email">Email :</label>
                                        {!! Form::text('email', isset($setting->email)?$setting->email:'',array('class'
                                        => ($errors->first('email'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'email', 'placeholder'=>'Email', '')) !!}
                                    </div> -->


                                    <div class="col-md-4 col-12">

                                        <label for="image">Upload Logo</label>
                                        <div class="custom-file">
                                            <input type="file" name="image" id="image" class="custom-file-input">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>

                                </div>


                                <h3><br>Owner Information</h3>

                                <div class="row" style="margin-right:0;">

                                    <div class="col-md-2 col-12">
                                        <label for="surname"> Prefix:</label>
                                        {!! Form::text('surname',
                                        isset($setting->surname)?$setting->surname:'',array('class' =>
                                        ($errors->first('surname'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'surname', 'placeholder'=>'Mr./Mrs./Miss', '')) !!}
                                    </div>

                                    <div class="col-md-3 col-12">
                                        <label for="first_name"> First Name:</label>
                                        {!! Form::text('first_name',
                                        isset($setting->first_name)?$setting->first_name:'',array('class' =>
                                        ($errors->first('first_name'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'first_name', 'placeholder'=>'First Name', '')) !!}
                                    </div>

                                    <div class="col-md-3 col-12">
                                        <label for="last_name"> Last Name:</label>
                                        {!! Form::text('last_name',
                                        isset($setting->last_name)?$setting->last_name:'',array('class' =>
                                        ($errors->first('last_name'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'last_name', 'placeholder'=>'Last Name', '')) !!}
                                    </div>


                                    <div class="col-md-4 col-12">
                                        <label for="email"> Email:</label>
                                        {!! Form::text('email',
                                        isset($setting->email)?$setting->email:'',array('class' =>
                                        ($errors->first('email'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'email', 'placeholder'=>'Email', '')) !!}
                                        @if ($errors->first('email'))
                                    <div class="alert alert-danger">{!! $errors->first('email') !!}</div>@endif
                                    </div>


                                    <!-- <div class="col-md-4 col-12">
                                        <label for="mobile"> Mobile:</label>
                                        {!! Form::text('mobile',
                                        isset($setting->mobile)?$setting->mobile:'',array('class' =>
                                        ($errors->first('mobile'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'mobile', 'placeholder'=>'Mobile', '')) !!}
                                    </div> -->
                                </div>
                                <div class="row" style="margin-right:0;">

                                    <div class="col-md-4 col-12">
                                        <label for="username"> User Name:</label>
                                        {!! Form::text('username',
                                        isset($setting->username)?$setting->username:'',array( 'class' =>
                                        ($errors->first('username'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'username', 'placeholder'=>'User Name',)) !!}
                                    </div>

                                    <div class="col-md-4 col-12">
                                        <label for="last_name"> Password:</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            {{ Form::password('password',  [ 'class' => ($errors->first('password'))? 'form-control form-control-mergel is-invalid' : 'form-control form-control-mergel', 'placeholder'=>'Enter you password',  'id'=>'login-password'
                                            ,'aria-describedby'=>'login-password',  'tabindex'=>'2']) }}
                                          
                                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                                        </div>
                                    </div>


                                    <div class="col-md-4 col-12">
                                        <label for="last_name">Confirm Password:</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            {{ Form::password('confirm_password',  ['class' => ($errors->first('confirm_password'))? 'form-control form-control-mergel is-invalid' : 'form-control form-control-mergel', 'placeholder'=>'Enter you confirm password',  'id'=>'login-confirm_password'
                                            ,'aria-describedby'=>'login-confirm_password', 'tabindex'=>'2']) }}
                                           
                                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                                        </div>
                                    </div>




                                </div>

                                <h3><br>Package Setting</h3>
                                <div class="row" style="margin-right:0;">

                                    <div class="col-md-6 col-12">
                                        <div class="form-group {!! $errors->first('package_id')?'has-error':'' !!} clear">
                                            <label for="package_id">Package:</label>
                                            {!! Form::select('package_id',\App\superadmin\package::where('is_active',1)->pluck('name', 'id')->toArray(), isset($business->package_id)?$business->package_id:'',array('class' => 'select2 form-control ','id'=>'package_id', '')) !!}

                                            @if ($errors->first('package_id'))
                                            <div class="alert alert-danger">{!! $errors->first('package_id') !!}</div>
                                            @endif
                                        </div>

                                    </div>


                                    <div class="col-md-6 col-12">
                                        <div class="form-group {!! $errors->first('status')?'has-error':'' !!} clear">
                                            <label for="status">Status:</label>
                                            {!! Form::select('status',array(''=>'','Waiting'=>'Waiting' ,'Approved'=>'Approved','Declined'=>'Declined',), isset($business->status)?$business->status:'',array('class' => 'select2 form-control ','id'=>'status', '')) !!}

                                            @if ($errors->first('status'))
                                            <div class="alert alert-danger">{!! $errors->first('status') !!}</div>
                                            @endif
                                        </div>

                                    </div>




                                    <div class="col-md-6 col-12">
                                        <div class="form-group {!! $errors->first('payment_method_id')?'has-error':'' !!} clear">
                                            <label for="payment_method_id">Paid Via:</label>
                                            {!! Form::select('payment_method_id',[''=>'']+\App\superadmin\PaymentMethod::where('is_active',1)->pluck('Title', 'id')->toArray(), isset($business->payment_method_id)?$business->payment_method_id:'',array('class' => 'select2 form-control ','id'=>'payment_method_id', '')) !!}

                                            @if ($errors->first('payment_method_id'))
                                            <div class="alert alert-danger">{!! $errors->first('payment_method_id') !!}</div>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">
                                        <label for="transaction_id"> Transaction ID:</label>
                                        {!! Form::text('transaction_id',
                                        isset($setting->transaction_id)?$setting->transaction_id:'',array('class' =>
                                        ($errors->first('transaction_id'))? 'form-control is-invalid ':'form-control '
                                        ,'id'=>'transaction_id', 'placeholder'=>'Transaction ID', '')) !!}
                                    </div>



                                </div>


                                <div class="col-12">
                                    <br>
                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light  ">Submit</button>

                                </div>
                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>
@endsection