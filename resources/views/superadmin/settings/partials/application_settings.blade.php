<div class="row" style="margin-right:0;">

    <div class="col-md-4 col-12">




            <label for="app_name">Application Name:</label>
            {!! Form::text('app_name', isset($setting->app_name)?$setting->app_name:'',array('class' => ($errors->first('app_name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'app_name', 'placeholder'=>'Application Name', '')) !!}



    </div>

    <div class="col-md-8 col-12">

        <label for="app_title">Application Title:</label>
        {!! Form::text('app_title', isset($setting->app_title)?$setting->app_title:'',array('class' => ($errors->first('app_title'))? 'form-control is-invalid ':'form-control ' ,'id'=>'app_name', 'placeholder'=>'Application Title', '')) !!}


    </div>
    <div class="row">

        <div class="col-md-12 col-12">
            <div class="demo-inline-spacing">
                <div class="custom-control custom-checkbox">
                    {!! Form::checkbox('superadmin_enable_register_tc', '1', isset($setting->superadmin_enable_register_tc)?$setting->superadmin_enable_register_tc:0 ,array('class' => 'custom-control-input','id'=>'superadmin_enable_register_tc')) !!}
                    <label class="custom-control-label" for="superadmin_enable_register_tc">Enable Terms & Conditions in Registration</label>
                </div>
            </div>

        </div>
        <div class="col-md-12 col-12">
        <textarea name="superadmin_register_tc" class="tinytextarea" >{!! isset($setting->superadmin_register_tc)?$setting->superadmin_register_tc:'' !!}</textarea>
        </div>
    </div>




</div>