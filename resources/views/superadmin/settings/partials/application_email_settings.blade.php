<div class="row" style="margin-right:0;">

<div class="col-md-4 col-12">
            <label for="MAIL_DRIVER">MAIL DRIVER:</label>
            {!! Form::select('MAIL_DRIVER',array('smtp'=>'SMTP' ,'sendmail'=>'Sendmail','mailgun'=>'Mailgun','mandrill'=>'Mandrill','ses'=>'SES','sparkpost'=>'Sparkpost'), isset($setting->MAIL_DRIVER)?$setting->MAIL_DRIVER:'',array('class' => 'select2 form-control ','id'=>'MAIL_DRIVER', '')) !!}

    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_HOST">MAIL HOST:</label>
            {!! Form::text('MAIL_HOST', isset($setting->MAIL_HOST)?$setting->MAIL_HOST:'',array('class' => ($errors->first('MAIL_HOST'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_HOST', 'placeholder'=>'MAIL HOST', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_PORT">MAIL PORT:</label>
            {!! Form::text('MAIL_PORT', isset($setting->MAIL_PORT)?$setting->MAIL_PORT:'',array('class' => ($errors->first('MAIL_PORT'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_PORT', 'placeholder'=>'MAIL PORT', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_USERNAME">MAIL USERNAME:</label>
            {!! Form::text('MAIL_USERNAME', isset($setting->MAIL_USERNAME)?$setting->MAIL_USERNAME:'',array('class' => ($errors->first('MAIL_USERNAME'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_USERNAME', 'placeholder'=>'MAIL USERNAME', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_PASSWORD">MAIL PASSWORD:</label>
            {!! Form::text('MAIL_PASSWORD', isset($setting->MAIL_PASSWORD)?$setting->MAIL_PASSWORD:'',array('class' => ($errors->first('MAIL_PASSWORD'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_PASSWORD', 'placeholder'=>'MAIL PASSWORD', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_ENCRYPTION">MAIL ENCRYPTION:</label>
            {!! Form::text('MAIL_ENCRYPTION', isset($setting->MAIL_ENCRYPTION)?$setting->MAIL_ENCRYPTION:'',array('class' => ($errors->first('MAIL_ENCRYPTION'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_ENCRYPTION', 'placeholder'=>'tls / ssl', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_FROM_ADDRESS">MAIL FROM ADDRESS:</label>
            {!! Form::text('MAIL_FROM_ADDRESS', isset($setting->MAIL_FROM_ADDRESS)?$setting->MAIL_FROM_ADDRESS:'',array('class' => ($errors->first('MAIL_FROM_ADDRESS'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_FROM_ADDRESS', 'placeholder'=>'MAIL FROM ADDRESS', '')) !!}
      
    </div>

   
    <div class="col-md-4 col-12">
            <label for="MAIL_FROM_NAME">MAIL FROM NAME:</label>
            {!! Form::text('MAIL_FROM_NAME', isset($setting->MAIL_FROM_NAME)?$setting->MAIL_FROM_NAME:'',array('class' => ($errors->first('MAIL_FROM_NAME'))? 'form-control is-invalid ':'form-control ' ,'id'=>'MAIL_FROM_NAME', 'placeholder'=>'MAIL FROM NAME', '')) !!}
      
    </div>

    <div class="col-md-6 col-12">
            <div class="demo-inline-spacing">
                <div class="custom-control custom-checkbox">
                    {!! Form::checkbox('allow_email_settings_to_businesses', '1', isset($setting->allow_email_settings_to_businesses)?$setting->allow_email_settings_to_businesses:0 ,array('class' => 'custom-control-input','id'=>'allow_email_settings_to_businesses')) !!}
                    <label class="custom-control-label" for="allow_email_settings_to_businesses">Allow businesses to use Superadmin email configuration</label>
                </div>
            </div>

        </div>

        <div class="col-md-6 col-12">
            <div class="demo-inline-spacing">
                <div class="custom-control custom-checkbox">
                    {!! Form::checkbox('enable_new_business_registration_notification', '1', isset($setting->enable_new_business_registration_notification)?$setting->enable_new_business_registration_notification:0 ,array('class' => 'custom-control-input','id'=>'enable_new_business_registration_notification')) !!}
                    <label class="custom-control-label" for="enable_new_business_registration_notification">Enable new business registration email</label>
                </div>
            </div>

        </div>

              <div class="col-md-6 col-12">
            <div class="demo-inline-spacing">
                <div class="custom-control custom-checkbox">
                    {!! Form::checkbox('enable_new_subscription_notification', '1', isset($setting->enable_new_subscription_notification)?$setting->enable_new_subscription_notification:0 ,array('class' => 'custom-control-input','id'=>'enable_new_subscription_notification')) !!}
                    <label class="custom-control-label" for="enable_new_subscription_notification"> Enable new subscription email</label>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-12">
            <div class="demo-inline-spacing">
                <div class="custom-control custom-checkbox">
                    {!! Form::checkbox('enable_welcome_email', '1', isset($setting->enable_welcome_email)?$setting->enable_welcome_email:0 ,array('class' => 'custom-control-input','id'=>'enable_welcome_email')) !!}
                    <label class="custom-control-label" for="enable_welcome_email">Enable welcome email to new business</label>
                </div>
            </div>

        </div>

</div>

