<div class="row" style="margin-right:0;">

    <div class="col-md-4 col-12">
            <label for="landmark">Landmark:</label>
            {!! Form::text('landmark', isset($setting->landmark)?$setting->landmark:'',array('class' => ($errors->first('landmark'))? 'form-control is-invalid ':'form-control ' ,'id'=>'landmark', 'placeholder'=>'Landmark', '')) !!}
      
    </div>

    <div class="col-md-4 col-12">

        <label for="state">State</label>
        {!! Form::text('state', isset($setting->state)?$setting->state:'',array('class' => ($errors->first('state'))? 'form-control is-invalid ':'form-control ' ,'id'=>'state', 'placeholder'=>'State', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="country">Country :</label>
        {!! Form::text('country', isset($setting->country)?$setting->country:'',array('class' => ($errors->first('country'))? 'form-control is-invalid ':'form-control ' ,'id'=>'country', 'placeholder'=>'Country', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="city">City :</label>
        {!! Form::text('city', isset($setting->city)?$setting->city:'',array('class' => ($errors->first('city'))? 'form-control is-invalid ':'form-control ' ,'id'=>'city', 'placeholder'=>'City', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="zip_code">Zip Code :</label>
        {!! Form::text('zip_code', isset($setting->zip_code)?$setting->zip_code:'',array('class' => ($errors->first('zip_code'))? 'form-control is-invalid ':'form-control ' ,'id'=>'zip_code', 'placeholder'=>'Zip Code', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="mobile">Mobile :</label>
        {!! Form::text('mobile', isset($setting->mobile)?$setting->mobile:'',array('class' => ($errors->first('mobile'))? 'form-control is-invalid ':'form-control ' ,'id'=>'mobile', 'placeholder'=>'Mobile', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="alternate_number">Alternate Number :</label>
        {!! Form::text('alternate_number', isset($setting->alternate_number)?$setting->alternate_number:'',array('class' => ($errors->first('alternate_number'))? 'form-control is-invalid ':'form-control ' ,'id'=>'alternate_number', 'placeholder'=>'Alternate Mobile Number', '')) !!}
    </div>
   
    <div class="col-md-4 col-12">

        <label for="email">Email :</label>
        {!! Form::text('email', isset($setting->email)?$setting->email:'',array('class' => ($errors->first('email'))? 'form-control is-invalid ':'form-control ' ,'id'=>'email', 'placeholder'=>'Email', '')) !!}
    </div>
   

</div>

