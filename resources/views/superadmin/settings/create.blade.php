@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Vertical Tabs start -->
            <section id="vertical-tabs">
                <div class="row match-height">
                    <!-- Vertical Left Tabs start -->
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <div class="nav-vertical">
                                    <ul class="nav nav-tabs nav-left flex-column" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="baseVerticalLeft-tab1" data-toggle="tab" aria-controls="tabVerticalLeft1" href="#tabVerticalLeft1" role="tab" aria-selected="true">Application Settings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="baseVerticalLeft-tab2" data-toggle="tab" aria-controls="tabVerticalLeft2" href="#tabVerticalLeft2" role="tab" aria-selected="false">Location Settings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="baseVerticalLeft-tab3" data-toggle="tab" aria-controls="tabVerticalLeft3" href="#tabVerticalLeft3" role="tab" aria-selected="false">Email Settings
                                            </a>
                                        </li>
                                    </ul>
                                    {!! Form::open(array('url' => isset($setting)?'superadmin/settings/'.$setting->id :'superadmin/setting','id'=>'form','method' => isset($setting)?'put':'post')) !!}

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tabVerticalLeft1" role="tabpanel" aria-labelledby="baseVerticalLeft-tab1">
                                            @include('superadmin.settings.partials.application_settings')
                                        </div>
                                        <div class="tab-pane" id="tabVerticalLeft2" role="tabpanel" aria-labelledby="baseVerticalLeft-tab2">
                                            @include('superadmin.settings.partials.application_location_settings')
                                        </div>
                                        <div class="tab-pane" id="tabVerticalLeft3" role="tabpanel" aria-labelledby="baseVerticalLeft-tab3">
                                            @include('superadmin.settings.partials.application_email_settings')
                                        </div>
                                    </div>

                                </div>
                                <div class="col-12">
                                    <br>
                                    <button type="submit" class="float-right btn btn-primary mr-1 waves-effect waves-float waves-light  ">Submit</button>

                                </div>

                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <!-- Vertical Left Tabs ends -->


                </div>
        </div>
    </div>
</div>

@endsection

@section('extra-js')





@endsection