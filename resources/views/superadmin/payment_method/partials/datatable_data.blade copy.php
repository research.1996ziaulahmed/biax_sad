<script>
  /**
   * DataTables Basic
   */

  $(function() {
    'use strict';

    var dt_basic_table = $('.datatables-basic'),
      dt_date_table = $('.dt-date'),
      dt_complex_header_table = $('.dt-complex-header'),
      dt_row_grouping_table = $('.dt-row-grouping'),
      dt_multilingual_table = $('.dt-multilingual'),
      assetPath = "{{URL::to('/')}}";

    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
      var dt_basic = dt_basic_table.DataTable({
        ajax: assetPath + '/superadmin/payment-method/all',
        columns: [

          {
            data: 'deleted_at'
          },
          {
            data: 'id' // used for sorting so will hide this column
          },
          {
            data: 'image_url'
          },
          {
            data: 'title'
          },
          {
            data: 'number'
          },
          {
            data: 'type'
          },
          {
            data: 'enable_extra_charge'
          },
          {
            data: 'extra_charge'
          },
          {
            data: 'instructions'
          },
          {
            data: 'description'
          },
          {
            data: ''
          }

        ],

        order: [
          [2, 'ASC']
        ],
        columnDefs: [{
            // For Responsive
            className: 'control',
            orderable: false,
            targets: 0
          },
          {
            targets: 1,
            visible: false
          },
          {
            // Avatar image/badge, Name and post
            targets: 2,
            // responsivePriority: 4,
            render: function(data, type, full, meta) {
              var $user_img = '';
              var $output = '';
              if (full['image_url'] === '' || full['image_url'] === null || full['image_url'] === 'null') {
                $output =
                  '<img src="' + assetPath + '/uploads/no_image/no-image.png"  width="50" height="50">';
              } else {
                $user_img = full['image_url'];
                $output =
                  '<img src="' + assetPath + '/' + $user_img + '"  width="50" height="50">';
              }


              return $output;
            }
          },

          {
            // Actions
            targets: -1,
            title: 'Actions',
            orderable: false,
            render: function(data, type, full, meta) {
              return (

                '<span  style="cursor: pointer" onclick="deleteInfo (' + full['id'] + ',this);"    data-toggle="tooltip" data-placement="top" title="Details"   class="item-info delete-record text-info">' +
                feather.icons['info'].toSvg({
                  class: 'font-small-4'
                }) +
                '</span>' +
                '&nbsp;<span  style="cursor: pointer" onclick="editInfo (' + full['id'] + ',this);"   data-toggle="tooltip" data-placement="top" title="Edit"   class="item-edit text-dark">' +
                feather.icons['edit'].toSvg({
                  class: 'font-small-4'
                }) +
                '</span>' +
                '&nbsp;<span style="cursor: pointer" onclick="deleteInfo (' + full['id'] + ',this);"  class="item-delete pick_record_' + full['id'] + ' text-danger">' +
                feather.icons['delete'].toSvg({
                  class: 'font-small-4'
                }) +
                '</span>'
              );
            }
          }
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 7,
        lengthMenu: [7, 10, 25, 50, 75, 100],
        buttons: [{
            extend: 'collection',
            className: 'btn btn-outline-secondary dropdown-toggle mr-2',
            text: feather.icons['share'].toSvg({
              class: 'font-small-4 mr-50'
            }) + 'Export',
            buttons: [{
                extend: 'print',
                text: feather.icons['printer'].toSvg({
                  class: 'font-small-4 mr-50'
                }) + 'Print',
                className: 'dropdown-item',
                exportOptions: {
                  columns: [3, 4, 5, 6, 7]
                }
              },
              {
                extend: 'csv',
                text: feather.icons['file-text'].toSvg({
                  class: 'font-small-4 mr-50'
                }) + 'Csv',
                className: 'dropdown-item',
                exportOptions: {
                  columns: [3, 4, 5, 6, 7]
                }
              },
              {
                extend: 'excel',
                text: feather.icons['file'].toSvg({
                  class: 'font-small-4 mr-50'
                }) + 'Excel',
                className: 'dropdown-item',
                exportOptions: {
                  columns: [3, 4, 5, 6, 7]
                }
              },
              {
                extend: 'pdf',
                text: feather.icons['clipboard'].toSvg({
                  class: 'font-small-4 mr-50'
                }) + 'Pdf',
                className: 'dropdown-item',
                exportOptions: {
                  columns: [3, 4, 5, 6, 7]
                }
              },
              {
                extend: 'copy',
                text: feather.icons['copy'].toSvg({
                  class: 'font-small-4 mr-50'
                }) + 'Copy',
                className: 'dropdown-item',
                exportOptions: {
                  columns: [3, 4, 5, 6, 7]
                }
              }
            ],
            init: function(api, node, config) {
              $(node).removeClass('btn-secondary');
              $(node).parent().removeClass('btn-group');
              setTimeout(function() {
                $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
              }, 50);
            }
          },
          {
            text: feather.icons['plus'].toSvg({
              class: 'mr-50 font-small-4'
            }) + 'Add New Record',
            className: 'create-new btn btn-primary',
            attr: {
              'data-toggle': 'modal',
              'data-target': '#modals-slide-in'
            },
            init: function(api, node, config) {
              $(node).removeClass('btn-secondary');
            }
          }
        ],
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function(row) {
                var data = row.data();
                return 'Summary of ' + data['title'];
              }
            }),
            type: 'column',
            renderer: function(api, rowIdx, columns) {
              var data = $.map(columns, function(col, i) {
                console.log(columns);
                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                  ?
                  '<tr data-dt-row="' +
                  col.rowIndex +
                  '" data-dt-column="' +
                  col.columnIndex +
                  '">' +
                  '<td>' +
                  col.title +
                  ':' +
                  '</td> ' +
                  '<td>' +
                  col.data +
                  '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table class="table"/>').append(data) : false;
            }
          }
        },
        language: {
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        }
      });

      $('div.head-label').html('<h6 class="mb-0">{{$page_title}}</h6>');
    }


    // Add New record
    // ? Remove/Update this code as per your requirements ?
    // var count = 101;
    $('.data-submit').on('click', function(e) {


      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      e.preventDefault();
      var formData = new FormData();
      var l_img = $('input#l_img').val();
      var $title = $('.add-new-record .dt-title').val(),
        $eid = $('.add-new-record .dt-eid').val(),
        $number = $('.add-new-record .dt-number').val(),
        $type = $('.add-new-record .dt-type').val(),
        $enable_extra_charge = $('.add-new-record .dt-enable_extra_charge').val(),
        $extra_charge = $('.add-new-record .dt-extra_charge').val(),
        $instructions = tinyMCE.editors[$('.add-new-record .dt-instructions').attr('id')].getContent(),
        $description = tinyMCE.editors[$('.add-new-record .dt-description').attr('id')].getContent();

      formData.append("image", $('#image')[0].files[0]);
      formData.append("title", $title);
      formData.append("number", $number);
      formData.append("type", $type);
      formData.append("enable_extra_charge", $enable_extra_charge);
      formData.append("extra_charge", $extra_charge);
      formData.append("instructions", $instructions);
      formData.append("description", $description);



      if ($title == '') {
        $('.add-new-record .dt-title').addClass('is-invalid');


      }
      // update info
      else if ($eid != '' || $eid > 0) {

        $('.add-new-record .dt-title').removeClass('is-invalid');


        $.ajax({
          url: "{{ URL::to('/superadmin/payment-method/') }}" + "/" + $eid + '/edit',
          type: 'POST',
          contentType: false,
          processData: false,
          dataType: "json",
          data: formData,

          success: function(data) {

            var $user_img = '';
            var $output = '';
            if (data['image_url'] === '' || data['image_url'] === null || data['image_url'] === 'null') {
              $output =
                '<img src="' + assetPath + '/uploads/no_image/no-image.png"  width="50" height="50">';
            } else {
              $user_img = data['image_url'];
              $output =
                '<img src="' + assetPath + '/' + $user_img + '"  width="50" height="50">';
            }

            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(1)").html(data['title']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(2)").html($output);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(3)").html(data['title']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(4)").html(data['number']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(5)").html(data['type']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(6)").html(data['enable_extra_charge']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(7)").html(data['extra_charge']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(8)").html(data['instructions']);
            $('.pick_record_' + $eid).parents("tr").find("td:nth-child(9)").html(data['description']);

            $('.modal').modal('hide');
            toastr['success']('You Create a new record', 'Success!', {
              closeButton: true,
              tapToDismiss: false,
              timeOut: 2000,
              // extendedTimeOut: 400, 

            });


            $('.add-new-record .dt-title').val('');
            $('.add-new-record .dt-l_img').val('');
            $('.add-new-record .dt-number').val('');
            $('.add-new-record .dt-type').val('');
            $('.add-new-record .dt-enable_extra_charge').val(0);
            $('.add-new-record .dt-extra_charge').val('');
            $('.add-new-record .dt-instructions').val('');
            $('.add-new-record .dt-description').val('');



          }
        });

      }

      // add new info
      else {
        $('.add-new-record .dt-title').removeClass('is-invalid');

        $.ajax({
          url: "{{ URL::to('/superadmin/payment-method/store') }}",
          type: 'POST',
          contentType: false,
          processData: false,
          dataType: "json",
          data: formData,

          success: function(data) {

            dt_basic.row
              .add({
                deleted_at: null,
                id: data['id'],
                image_url: data['image_url'],
                title: data['title'],
                number: data['number'],
                type: data['type'],
                enable_extra_charge: data['enable_extra_charge'],
                extra_charge: data['extra_charge'],
                instructions: data['instructions'],
                description: data['description'],

              })
              .draw();
            $('.modal').modal('hide');
            toastr['success']('You Create a new record', 'Success!', {
              closeButton: true,
              tapToDismiss: false,
              timeOut: 2000,
              // extendedTimeOut: 400,

            });


            $('.add-new-record .dt-title').val('');
            $('.add-new-record .dt-l_img').val('');
            $('.add-new-record .dt-number').val('');
            $('.add-new-record .dt-type').val('');
            $('.add-new-record .dt-instructions').val('');
            $('.add-new-record .dt-description').val('');
            // $('.add-new-record .dt-enable_extra_charge').val(0);
            $('.add-new-record .dt-extra_charge').val('');
            // tinyMCE.editors[$('.add-new-record .dt-instructions').attr('id')].setContent();
            // tinyMCE.editors[$('.add-new-record .dt-description').attr('id')].setContent();



          }
        });
      }
    });


    // Delete Record


    // Flat Date picker
    if (dt_date_table.length) {
      dt_date_table.flatpickr({
        monthSelectorType: 'static',
        dateFormat: 'd/m/Y'
      });
    }



  });


  function deleteInfo(id, e) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      customClass: {
        confirmButton: 'btn btn-primary',
        cancelButton: 'btn btn-outline-danger ml-1'
      },
      buttonsStyling: false
    }).then(function(result) {
      if (result.value) {

        $.ajax({
          url: "{{ URL::to('superadmin/payment/method') }}" + '/' + id,
          type: 'DELETE',
          data: {
            _token: "{{ csrf_token() }}"
          },
          success: function(data) {
            $s=1;
            // $('.pick_record_' + id).parents('tr').remove().draw();
            //  dt_basic.row($(this).parents('tr')).remove().draw();
            toastr['success']('Successsfully Delete', 'Success!', {
              closeButton: true,
              tapToDismiss: false,
              timeOut: 2000,
              // extendedTimeOut: 400,

            });
          }
        });


        Swal.fire({
          icon: 'success',
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          customClass: {
            confirmButton: 'btn btn-success'
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire({
          title: 'Cancelled',
          text: 'Your imaginary file is safe :)',
          icon: 'error',
          customClass: {
            confirmButton: 'btn btn-success'
          }
        });
      }
    });
   
  }


  function editInfo(id, e) {

    $.ajax({
      url: "{{ URL::to('superadmin/payment/method') }}" + '/' + id + '/edit',
      type: 'GET',
      data: {
        _token: "{{ csrf_token() }}"
      },
      success: function(data) {
        // $('.add-new-record .dt-l_img').val('');



        $('.add-new-record .dt-title').val(data['title']);
        $('.add-new-record .dt-number').val(data['number']);
        $('.add-new-record .dt-type').val(data['type']);
        $('.add-new-record .dt-enable_extra_charge').val(data['enable_extra_charge']);
        $('.add-new-record .dt-extra_charge').val(data['enable_extra_charge']);
        if (data['instructions'] != null)
          tinyMCE.editors[$('.add-new-record .dt-instructions').attr('id')].setContent(data['instructions']);
        if (data['description'] != null)
          tinyMCE.editors[$('.add-new-record .dt-description').attr('id')].setContent(data['description']);
        $('.add-new-record .dt-eid').val(data['id']);

        $('.modal').modal('show');

      }
    });

  }
</script>