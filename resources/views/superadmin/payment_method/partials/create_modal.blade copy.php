<!-- Modal to add new record -->
<div class="modal fade" id="modals-slide-in">
    <div class="modal-dialog modal-dialog-centered modal-lg ">
        <form class="add-new-record modal-content pt-0" action="#" method="POST" role="form" id="form-edit-post" enctype='multipart/form-data'>
        
<input type="hidden" name ="_token" id="_token" value="{{ csrf_token() }}">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            <div class="modal-header mb-1">
                <h5 class="modal-title" id="exampleModalLabel">Create New Payment Method</h5>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-title">Title</label>
                    <input name="title" type="text" name="title" class="form-control dt-title" id="basic-icon-default-title" placeholder="Title" />
                </div>

                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-number">Number</label>
                    <input name="number" type="text" class="form-control dt-number" id="basic-icon-default-number" placeholder="Number" />
                </div>

                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-type">Type</label>
                    <input name="type" type="text" class="form-control dt-type" id="basic-icon-default-type" placeholder="type" />
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-7 col-12">
                            <label class="form-label" for="basic-icon-default-type">Extra Charge (%)</label>
                            <input name="extra_charge" type="number" class="form-control dt-extra_charge" id="basic-icon-default-extra_charge" placeholder="Extra Charge (%)" />

                        </div>
                        <div class="col-md-5 col-12">
                            <div class="demo-inline-spacing">
                                <div class="custom-control custom-checkbox">
                                    <input name="enable_extra_charge" type="checkbox" class="custom-control-input dt-enable_extra_charge" id="basic-icon-default-enable_extra_charge"  value="1"/>
                                    <label class="custom-control-label" for="dt-enable_extra_charge">Enable Extra Charge</label>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="basic-icon-default-instructions">Instructions</label>
                        <textarea name="instructions" class="tinytextarea dt-instructions"></textarea>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="basic-icon-default-description">Description</label>
                        <textarea name="description" class="tinytextarea dt-description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="image">Payment Logo</label>
                        <div class="custom-file">
                            <input  type="file" class="custom-file-input" id="file" />
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                    </div>

                </div>
                <button type="button" class="btn btn-primary data-submit mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>