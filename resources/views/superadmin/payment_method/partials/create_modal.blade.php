<!-- Modal to add new record -->
<div class="modal fadet" id="create-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg ">


        <form class="add-new-record modal-content needs-validation" action="#" method="POST" role="form" id="imageUpload" enctype='multipart/form-data'>

            <!-- <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> -->
            <input type="hidden" name="eid" class=" dt-eid" id="basic-icon-default-eid" placeholder="eid" />


            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Create New Payment Method</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body flex-grow-1">
            
                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-title">Title</label>
                    <input name="title" type="text" name="title" class="form-control dt-title" id="basic-icon-default-title" placeholder="Title" required />
                </div>

                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-number">Number</label>
                    <input name="number" type="text" class="form-control dt-number" id="basic-icon-default-number" placeholder="Number" />
                </div>

                <div class="form-group">
                    <label class="form-label" for="basic-icon-default-type">Type</label>
                    <input name="type" type="text" class="form-control dt-type" id="basic-icon-default-type" placeholder="type" />
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-7 col-12">
                            <label class="form-label" for="basic-icon-default-type">Extra Charge (%)</label>
                            <input name="extra_charge" type="number" class="form-control dt-extra_charge" id="basic-icon-default-extra_charge" placeholder="Extra Charge (%)" />

                        </div>
                        <div class="col-md-5 col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <label class="form-label" for="">Enable Extra Charge</label>
                                <br>
                                <input name="enable_extra_charge" type="checkbox" class="custom-control-input  dt-enable_extra_charge" id="customSwitch111"  />
                                <label class="custom-control-label" for="customSwitch111">
                                    <span class="switch-icon-left"><i data-feather="check"></i></span>
                                    <span class="switch-icon-right"><i data-feather="x"></i></span>
                                </label>
                            </div>



                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="basic-icon-default-instructions">Instructions</label>
                        <textarea name="instructions" class="tinytextarea dt-instructions"></textarea>
                    </div>

                    <div class="form-group">
                        <label class="form-label" for="basic-icon-default-description">Description</label>
                        <textarea name="description" class="tinytextarea dt-description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="image">Payment Logo</label>
                        <div class="custom-file">
                            <input type="file" name="image" id="image" class="custom-file-input">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <!-- <div class="custom-file">

                            <input type="file" name="image" id="image" class="custom-file-input">
                        </div> -->
                    </div>

                </div>
                <button type="button" class="btn btn-primary data-submit mr-1">Submit</button>
                <button type="reset" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>