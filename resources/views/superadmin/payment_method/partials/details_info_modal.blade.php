<div class="modal-size-lg d-inline-block">
    <!-- Button trigger modal -->
  
    <!-- Modal -->
    <div class="modal fade text-left" id="details_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel17">details_info Modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    I love tart cookie cupcake. I love chupa chups biscuit. I love marshmallow apple pie wafer
                    liquorice. Marshmallow cotton candy chocolate. Apple pie muffin tart. Marshmallow halvah pie
                    marzipan lemon drops jujubes. Macaroon sugar plum cake icing toffee.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button>
                </div>
            </div>
        </div>
    </div>
</div>