@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
               
@include('superadmin.payment_method.partials.details_info_modal')
            </div>
        </div>
        <div class="content-body">
            <!-- Basic table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table class="datatables-basic table " width="100%">
                            <thead>
                                    <tr>

                                        <th></th>
                                        <th></th>
                                        <th>Icon</th>
                                        <th>Title</th>
                                        <th>Number</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Extra Charge</th>
                                        <th>Instructions</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>  
                               
                            </table>
                        </div>
                    </div>
                </div>

            </section>
            <!--/ Basic table -->



        </div>
    </div>
</div>

@include('superadmin.payment_method.partials.create_modal')

@endsection

@section('extra-js')
@include('superadmin.payment_method.partials.datatable_data')


@endsection