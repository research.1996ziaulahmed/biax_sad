@extends('layouts.app')
@section('title'){{$page_title}}@endsection


@section('extra-css')

@endsection
@section('content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <a type="button" class=" btn btn-outline-primary" href="{{url('superadmin/package/create')}}"><i data-feather='plus-circle'></i> &nbsp;Add</a>
                </div>
            </div>
        </div>
        <div class="content-body">

            <!-- pricing plan cards -->
            <div class="row pricing-card">
                <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto">
                    <div class="row">
                        @foreach($packages as $package)
                        <!-- basic plan -->
                        <div class="col-12 col-md-4" id="package_{{$package->id}}">
                            <div class="card basic-pricing text-center">
                                <div class="card-body">
                                    <h3>{{$package->name}}</h3>
                                    @if($package->is_active)
                                    <div class="badge badge-success">Active</div>
                                    @else
                                        <div class="badge badge-warning">Deactive</div>
                                        @endif
                                       &nbsp;<a href="{{url('superadmin/package/'.$package->id.'/edit')}}"> <span class=" fa fa-edit"></span></a>
                                        <button type="button" onclick='deleteInfo({{$package->id}},this);' class="btn btn-icon btn-flat-danger" >
                                            <i class="fa fa-trash"></i>
                                         </button>
                                
                                    
                                    <div class="annual-plan">
                                        <div class="plan-price mt-2">
                                            <sup class="font-medium-1 font-weight-bold text-primary">BDT</sup>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{{$package->price}}</span>
                                            <sub class="pricing-duration text-body  font-weight-bold">/{{$package->interval_count}} {{$package->interval}}</sub>
                                        </div>
                                        <small class="annual-pricing d-none text-muted"></small>
                                    </div>
                                    <ul class="list-group list-group-circle text-left">

                                    <li class="list-group-item"> @if($package->installation_fees==0) No  Installation Fees @else  Installation Fees: {{$package->installation_fees}}/- @endif</li>
                                        <li class="list-group-item">{{$package->location_count}} Business Location</li>
                                        <li class="list-group-item">{{$package->user_count}} Users</li>
                                        <li class="list-group-item">@if($package->product_count==0) Unlimited @else {{$package->product_count}} @endif Products</li>
                                        <li class="list-group-item">@if($package->invoice_count==0) Unlimited @else {{$package->invoice_count}} @endif Invoices</li>

                                    </ul>
                                    <p class="card-text">{{$package->description}}</p>
                                </div>
                            </div>
                        </div>
                        <!--/ basic plan -->
                        @endforeach

                    </div>
                </div>
            </div>
            <!--/ pricing plan cards -->


        </div>
    </div>
</div>

@endsection

@section('extra-js')



<script>


        function deleteInfo(id, e) {
            Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        customClass: {
          confirmButton: 'btn btn-primary',
          cancelButton: 'btn btn-outline-danger ml-1'
        },
        buttonsStyling: false
      }).then(function (result) {
        if (result.value) {
            $.ajax({
                    url: "{{ URL::to('superadmin/package') }}" + '/' + id,
                    type: 'DELETE',
                    data: {_token: "{{ csrf_token() }}"},
                    success: function (data) {
                        $("#package_"+id).remove(); //play with data
                    }
                });

        //   Swal.fire({
        //     icon: 'success',
        //     title: 'Deleted!',
        //     text: 'Your file has been deleted.',
        //     customClass: {
        //       confirmButton: 'btn btn-success'
        //     }
        //   });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
            title: 'Cancelled',
            text: 'Your imaginary file is safe :)',
            icon: 'error',
            customClass: {
              confirmButton: 'btn btn-success'
            }
          });
        }
      });
        }


    </script>

@endsection
