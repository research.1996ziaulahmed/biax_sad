@extends('layouts.app')
@section('title'){{$page_title}}@endsection
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        <div class="content-body">
            <section id="multiple-column-form">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                {!! Form::open(array('url' => isset($package)?'superadmin/package/'.$package->id :'superadmin/package','id'=>'form','method' => isset($package)?'put':'post')) !!}
                                <div class="row">

                                    <div class="col-md-6 col-12">




                                        <div class="form-group {!! $errors->first('name')?'has-error':'' !!} clear">


                                            <label for="name">Package Name:</label>
                                            {!! Form::text('name', isset($package->name)?$package->name:'',array('class' => ($errors->first('name'))? 'form-control is-invalid ':'form-control ' ,'id'=>'name', 'placeholder'=>'Package Name', '')) !!}

                                            @if ($errors->first('name'))
                                            <script>

                                            </script>
                                            @endif

                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('description')?'has-error':'' !!} clear">
                                            <label for="description">Package Description:</label>
                                            {!! Form::text('description', isset($package->description)?$package->description:'',array('class' => ($errors->first('description'))? 'form-control is-invalid ':'form-control ','id'=>'description', 'placeholder'=>'Package Description', '')) !!}


                                        </div>

                                    </div>


                                </div>
                                <div class="row">

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('location_count')?'has-error':'' !!} clear">
                                            <label for="location_count">Number of Locations:</label>
                                            {!! Form::number('location_count', isset($package->location_count)?$package->location_count:'',array('class' => ($errors->first('location_count'))? 'form-control is-invalid ':'form-control ','id'=>'location_count', 'placeholder'=>'Number of Locations', '')) !!}

                                            <label class="">&nbsp;&nbsp;&nbsp;&nbsp;0=infinite</label>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('user_count')?'has-error':'' !!} clear">
                                            <label for="user_count">Number of active users:</label>
                                            {!! Form::number('user_count', isset($package->user_count)?$package->user_count:'',array('class' => ($errors->first('user_count'))? 'form-control is-invalid ':'form-control ','id'=>'user_count', 'placeholder'=>'Number of active users', '')) !!}

                                            <label class="">&nbsp;&nbsp;&nbsp;&nbsp;0=infinite</label>

                                        </div>

                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('product_count')?'has-error':'' !!} clear">
                                            <label for="product_count">Number of products:</label>
                                            {!! Form::number('product_count', isset($package->product_count)?$package->product_count:'',array('class' => ($errors->first('product_count'))? 'form-control is-invalid ':'form-control ','id'=>'product_count', 'placeholder'=>'Number of products', '')) !!}

                                            <label class="">&nbsp;&nbsp;&nbsp;&nbsp;0=infinite</label>

                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('invoice_count')?'has-error':'' !!} clear">
                                            <label for="invoice_count">Number of Invoices:</label>
                                            {!! Form::number('invoice_count', isset($package->invoice_count)?$package->invoice_count:'',array('class' => ($errors->first('invoice_count'))? 'form-control is-invalid ':'form-control ','id'=>'invoice_count', 'placeholder'=>'Number of Invoices', '')) !!}

                                            <label class="">&nbsp;&nbsp;&nbsp;&nbsp;0=infinite</label>

                                        </div>

                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-12">
                                        <div class="form-group {!! $errors->first('interval')?'has-error':'' !!} clear">
                                            <label for="interval">Price Interval:</label>
                                            {!! Form::select('interval',array(''=>'' ,'days'=>'Days','months'=>'Months','years'=>'Years'), isset($package->interval)?$package->interval:'',array('class' => 'select2 form-control ','id'=>'interval', '')) !!}

                                            @if ($errors->first('interval'))
                                            <div class="alert alert-danger">{!! $errors->first('interval') !!}</div>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('interval_count')?'has-error':'' !!} clear">
                                            <label for="Interval">Interval</label>
                                            {!! Form::number('interval_count', isset($package->interval_count)?$package->interval_count:'',array('class' => ($errors->first('interval_count'))? 'form-control is-invalid ':'form-control ','id'=>'interval_count', 'placeholder'=>'Interval', '')) !!}



                                        </div>

                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('trial_days')?'has-error':'' !!} clear">
                                            <label for="trial_days">Trial Days:</label>
                                            {!! Form::number('trial_days', isset($package->trial_days)?$package->trial_days:'',array('class' => ($errors->first('trial_days'))? 'form-control is-invalid ':'form-control ','id'=>'trial_days', 'placeholder'=>'Trial Days')) !!}


                                        </div>

                                    </div>
                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('price')?'has-error':'' !!} clear">
                                            <label for="installation_fees">Installation Fees</label>

                                            <div class="input-group ">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">BDT</span>
                                                </div>
                                                {!! Form::number('installation_fees', isset($package->installation_fees)?$package->installation_fees:'',array('class' => ($errors->first('installation_fees'))? 'form-control is-invalid ':'form-control ','id'=>'installation_fees', 'placeholder'=>'Installation Fees', '')) !!}


                                            </div>



                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">

                                        <div class="form-group {!! $errors->first('price')?'has-error':'' !!} clear">
                                            <label for="price">Price:</label>

                                            <div class="input-group ">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">BDT</span>
                                                </div>
                                                {!! Form::number('price', isset($package->price)?$package->price:'',array('class' => ($errors->first('price'))? 'form-control is-invalid ':'form-control ','id'=>'price', 'placeholder'=>'Price', '')) !!}


                                            </div>

                                            <label class="">&nbsp;&nbsp;&nbsp;&nbsp;0=Free Package</label>

                                        </div>

                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-6 col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox">
                                                {!! Form::checkbox('is_private', '1', isset($package->is_private)?$package->is_private:0 ,array('class' => 'custom-control-input','id'=>'is_private')) !!}
                                                <label class="custom-control-label" for="is_private">SuperAdmin Private Only</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox">
                                                {!! Form::checkbox('is_protective', '1', isset($package->is_protective)?$package->is_protective:0 ,array('class' => 'custom-control-input','id'=>'is_protective')) !!}
                                                <label class="custom-control-label" for="is_protective">SuperAdmin + Users Protective Only</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6 col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox">
                                                {!! Form::checkbox('is_active', '1', isset($package->is_active)?$package->is_active:0 ,array('class' => 'custom-control-input','id'=>'is_active')) !!}
                                                <label class="custom-control-label" for="is_active">Active</label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6 col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="custom-control custom-checkbox">
                                                {!! Form::checkbox('is_one_time', '1', isset($package->is_one_time)?$package->is_one_time:0 ,array('class' => 'custom-control-input','id'=>'is_one_time')) !!}
                                                <label class="custom-control-label" for="is_one_time">One Time Subscription</label>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-12">
                                    <br>
                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light  ">Submit</button>

                                </div>
                                {{ Form::close() }}
                            </div>

                        </div>
                    </div>
                </div>
        </div>
        </section>
    </div>
</div>



@endsection