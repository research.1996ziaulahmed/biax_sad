@extends('layouts.app')
@section('title'){{$page_title}}@endsection
@section('content')


<div class="app-content content ocfheader">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">{{$page_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                @foreach ($breadcrumbs as $title=>$url)
                                <li class="breadcrumb-item"><a href="{{$url}}">{{ $title }}</a>
                                </li>
                                @endforeach

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">

                </div>
            </div>
        </div>
        @if($ocf_create_step_no == 1)
 <!-- Basic multiple Column Form section start -->
                <section id="multiple-column-form">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">OCF Create</h4>
                                </div>
                                <div class="card-body">
                               
                                    {!! Form::open(array('url' => isset($ocf_create)?'ocfcreate/'.$ocf_create->id :'ocfcreate','id'=>'form', 'class'=> 'needs-validation','method' => isset($ocf_create)?'put':'post' , 'novalidate')) !!}

                                        <div class="row">
                                            
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >Date</label>
                                                    <input type="date"  class="form-control"  name="dteocfdate" required />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >Ocf NO</label>
                                                    <input type="text" disabled  class="form-control"  name="" />
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >PO No</label>
                                                    <input type="text" class="form-control"  name="strpono"  />
                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >PO Date</label>
                                                    <input type="date"  class="form-control"  name="dtepodate"  />
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label >Sales Person</label>
                                                     <select class="select2 form-control form-control-lg" name="intsalepersonid" required id="intsalepersonid">
                                                     <option value=""></option>
                                                     @foreach ($employees as $emp)
                               
                                                <option value="{{$emp->enroll}}">{{$emp->name}} [{{$emp->enroll}}]</option>
                                                
                                                @endforeach
                                                
                                            </select>
                                               </div>
                                            </div>
                                            
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >Product Type</label>
                                                        <select class="select2 form-control form-control-lg" name="strflimtype" required>
                                                <option value=""></option>
                                                <option value="flim">Flim</option>
                                                <option value="gum">Gum Tape</option>
                                               
                                            </select>

                                                </div>
                                            </div>
                                              <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >Customer Category</label>
                                                        <select class="select2 form-control form-control-lg" name="intcustomercategory" required>
                                                        
                                                        <option value=""></option>
                                                     @foreach ($cuscategory as $data)
                               
                                                <option value="{{$data->intcustomercategory}}">{{$data->strcustomercategory}} </option>
                                                
                                                @endforeach
                                               
                                            </select>
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-12">
                                                <div class="form-group">
                                                    <label >Packaging Mode</label>
                                                        <select class="select2 form-control form-control-lg" name="intpackagingmode" required>
                                                            <option></option>
                                                            @foreach ($pakg as $p)
                                               
                                                            <option value="{{$p->intpackagingmodeid }}">{{$p->strpackagingmodedetails}} </option>
                                                            
                                                            @endforeach
                                                         
                                            </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="form-group">
                                                    <label >Torrance Rate(± %)</label>
                                                        <input type="number"  class="form-control"  name="dectorrancerate" required/>
                                                </div>
                                            </div>
                                           
                                                   <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label >Customer</label>
                                                        <select class="select2 form-control form-control-lg" onchange="cusdetails();" id="intcustomerid" name="intcustomerid" required>
                                                        <option value=""></option>
                                                     @foreach ($customer as $cus)
                               
                                                <option value="{{$cus->intcusid}}">{{$cus->strname}} [{{$cus->intcusid}}] </option>
                                                
                                                @endforeach
                                               
                                            </select> 
                                            <input type="text"  class="form-control hidden"  name="strcustomername" />
                                            @foreach ($customer as $cus)
                               
                               
                               <span class="hidden " id="cus_{{$cus->intcusid}}">

                               Contact at: {{$cus->strpropitor}}<br/>
                               Address: {{$cus->straddress}}<br/>
                               Phone: {{$cus->strphone}}<br/>
                               </span>
                               @endforeach
                                            <br/>
                                            <span class="cus_details"></span>  
                                           
                                                  
                                        </div>
                                            </div>
                                                    
                                                
                                                   <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label >Del. Point	</label>
                                                        <select class="select2 form-control form-control-lg" onchange="disdetails();" id="dis_id" name="intdelpointid" required>
                                                        <option value=""></option>
                                                     @foreach ($dispoint as $dis)
                               
                                                <option value="{{$dis->intdispointid}}">{{$dis->strname}} [{{$dis->intdispointid}}] </option>
                                                
                                                @endforeach
                                               
                                            </select>
                                                     
                                            @foreach ($dispoint as $dis)
                               
                               <span class="hidden " id="dis_{{$dis->intdispointid}}">

Contact at: {{$dis->strcontactperson}}<br/>
Address: {{$dis->straddress}}<br/>
Phone: {{$dis->strcontactno}}<br/>
</span>
@endforeach
             <br/>
             <span class="dis_details"></span>
                                                </div>
                                            </div>
                                                    
                                               
                                            
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary ">Genarate OCF NO</button>
                                            </div>
                                        </div>
                                  
                                {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @endif
 <!-- Basic multiple Column Form section start -->
 @if($ocf_create_step_no == 2)
 <section id="multiple-column-form">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">OCF NO: {{$strOcfNo}}</h4>
                </div>
                <div class="card-body">
                    <form class="form">
                        <div class="row">
                            
                            
                                 <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label >Product Name	</label>
                                        <select class="select2 form-control form-control-lg" id="s_productcode">
                                            <option></option>
                                            @foreach ($items as $it)
                               
                                            <option value="{{$it->strProductCode}}">{{$it->strProductName}} [{{$it->strProductCode}}]</option>
                                            
                                            @endforeach
                               
                            </select>
                                 
                                    
                                </div>
                                </div>
                                     
                                 <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Thickness	</label>
                                        <input type="text"  class="form-control"  name="fname-column" id="s_thickness"/>
                                 
                                    
                                </div>
                                </div>
                           
                     
                                     
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Width	</label>
                                        <input type="text" id="s_width" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                                     
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Length	</label>
                                        <input type="text" id="s_length" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                                     
                            
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Treatment Level	</label>
                                        <input type="text" id="s_treatment_level" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Treatment Direction	</label>
                                        <select class="select2 form-control form-control-lg" id="s_treatment_direction">
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                                <option value="CA">California</option>
                               
                            </select>
                                 
                                    
                                </div>
                                </div>
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Core ID	</label>
                                       <input type="text" id="s_core_id" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Reel OD	</label>
                                       <input type="text" id="s_reel_od" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                            <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >UOM	</label>
                                      <select class="select2 form-control form-control-lg " id="s_uom">
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                                <option value="CA">California</option>
                               
                            </select>
                                    
                                </div>
                                </div>
                               <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Order Qty	</label>
                                       <input type="text" id="s_order_qty" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                            
                               <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Desired Date	</label>
                                       <input type="Date" id="s_desired_date" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                               <div class="col-md-2 col-12">
                                <div class="form-group">
                                    <label >Tentative Date	</label>
                                       <input type="Date" id="s_tentative_date" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                               <div class="col-md-7 col-12">
                                <div class="form-group">
                                    <label >Remarks	</label>
                                       <input type="text" id="s_remarks" class="form-control"  name="fname-column" />
                                 
                                    
                                </div>
                                </div>
                            
                               <div class="col-md-1 col-12">
                                <div class="form-group">
                                    <br>
                                       <span class="btn btn-info" onclick="return additemtoocflist()"><i class="fas fa-plus-circle"></i></span>
                                 
                                    
                                </div>
                                </div>
                             <div class="col-12">
                                 
                                 
                                 <br>
                                 <br>
                                 
                                 
                            <table class="table table-bordered table-hover table-striped table-responsive text-center " style="font-size:12px;" >
                                <thead class="bg-primary text-white">
                             
                                <td>Product Code</td>
                                <td>Description</td>
                                <td>Thickness</td>
                                <td>Width</td>
                                <td>Length</td>
                                <td>Treatment Level</td>
                                <td>Treatment Direction</td>
                                <td>Core ID</td>
                                <td>Reel OD</td>
                                <td>Order Qty</td>

                                <td>Desired Date</td>
                                <td>Tentative Date</td>
                                <td>Remarks</td>
                                <td></td>
                                
                            </thead>
                        <tbody class="ocfbody">

<!--
                            <tr>
                                   
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Product Code" name="fname-column" /></td>
                                <td>Description</td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Thickness" name="fname-column" /></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Width" name="fname-column" /></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Length" name="fname-column" /></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Treatment Level" name="fname-column" /></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Treatment Direction" name="fname-column" /></td>
                                <td><select class=" form-control form-control-lg hidden">
                                <option >Core Id</option>
                                <option value="HI">Hawaii</option>
                                <option value="CA">California</option></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Read OD" name="fname-column" /></td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Order Qty" name="fname-column" /></td>
                                <td><input type="Date" id="first-name-column" class="form-control hidden" placeholder="Desired Date"  name="fname-column" /></td>
                                <td>Tentative Date</td>
                                <td><input type="text" id="first-name-column" class="form-control hidden" placeholder="Remarks" name="fname-column" /></td>
                                <td><span class="fas fa-times text-danger" style="cursor:pointer;" onclick="$(this).parent().parent().remove();"></span></td>                                           

                            </tr>
-->
                        

                        </tbody>
                            </table>
                            </div>
                           
                          <br>
                          <br>
                            <div class="col-12">
                                <button type="reset" class="btn btn-success mr-1 ">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
</div>
</div>
</div>





@endsection
@section('extra-js')

<script>
  
  $(window).on('load', function() {
      if (feather) {
          feather.replace({
              width: 14,
              height: 14
          });
      }
  })
</script>
  
  <script>
  function additemtoocflist(){
   
      var variable = '' + 
'			<tr>' + 
'                                                   ' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Product Code" name="productcode['+$("#s_productcode").val()+']" value="'+$("#s_productcode").val()+'"/>'+$("#s_productcode").val()+'</td>' + 
'                                                <td>'+$("#s_productcode option:selected").text()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Thickness" name="thickness['+$("#s_productcode").val()+']" value="'+$("#s_thickness").val()+'" />'+$("#s_thickness").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Width" name="width['+$("#s_productcode").val()+']" value="'+$("#s_width").val()+'" />'+$("#s_width").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Length" name="length['+$("#s_productcode").val()+']" value="'+$("#s_length").val()+'" />'+$("#s_length").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Treatment Level" name="treatment_level['+$("#s_productcode").val()+']" value="'+$("#s_treatment_level").val()+'" />'+$("#s_treatment_level").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Treatment Direction" name="treatment_direction['+$("#s_productcode").val()+']" value="'+$("#s_treatment_direction").val()+'" />'+$("#s_treatment_direction").val()+'</td>' + 
'                                                <td> ' + 
'												<input type="text"  class="form-control hidden" placeholder="Treatment Direction" name="core_id['+$("#s_productcode").val()+']" value="'+$("#s_core_id").val()+'" />'+$("#s_core_id").val()+ 
'												</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Reel OD" name="reel_od['+$("#s_productcode").val()+']" value="'+$("#s_reel_od").val()+'" />'+$("#s_reel_od").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Order Qty" name="order_qty['+$("#s_productcode").val()+']" value="'+$("#s_order_qty").val()+'" />'+$("#s_order_qty").val()+'</td>' + 
'                                                <td><input type="Date"  class="form-control hidden" placeholder="Desired Date"  name="desired_date['+$("#s_productcode").val()+']" value="'+$("#s_desired_date").val()+'" />'+$("#s_desired_date").val()+'</td>' +
'                                                <td><input type="Date"  class="form-control hidden" placeholder="Tentative Date"  name="tentative_date['+$("#s_productcode").val()+']" value="'+$("#s_tentative_date").val()+'" />'+$("#s_tentative_date").val()+'</td>' + 
'                                                <td><input type="text"  class="form-control hidden" placeholder="Remarks" name="remarks['+$("#s_productcode").val()+']" value="'+$("#s_remarks").val()+'" />'+$("#s_remarks").val()+'</td>' + 
'                                                <td><span class="fas fa-times text-danger" style="cursor:pointer;" onclick="$(this).parent().parent().remove();"></span></td>                                           ' + 
'' + 
'                                            </tr>' + 
'';
     
      $(".ocfbody").append(variable);
      
     
      $("#s_productcode").val("").change();   
      $("#s_thickness").val('');
      $("#s_width").val('');
      $("#s_length").val('');
      $("#s_treatment_level").val('');
      $("#s_core_id").val('');
      $("#s_reel_od").val('');
      $("#s_order_qty").val('');
      $("#s_desired_date").val('');
      $("#s_tentative_date").val('');
      $("#s_remarks").val('');
      return false;
  }
  

    function cusdetails(){
    var id= $('#intcustomerid').val();
    
    $(".cus_details").html($('#cus_'+id).html());
      //$(".cus_details").html($('#cus_'+id).html());
        }

 function disdetails(){

    var id= $('#dis_id').val();
              
      $(".dis_details").html($('#dis_'+id).html());
        }
        
        
    // function salepersonnamechange(){
    
    // $("#strsalepersonname").val($("#intsalepersonid option:selected").text());
  
    // }

    // function strcustomercategoryChange(){
    //     alert();
    
    // $("#strcustomercategory").val($("#intcustomercategory option:selected").text());
  
    // }

    
        </script>


@endsection