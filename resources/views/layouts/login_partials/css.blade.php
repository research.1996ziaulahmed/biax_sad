<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/app-assets/css/pages/page-auth.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('')}}/assets/css/style.css">
    <!-- END: Custom CSS-->
