<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{URL::to('/home')}}"><span class="brand-logo">
                        <!-- <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                                <defs>
                                    <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                        <stop stop-color="#000000" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                    <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                        <g id="Group" transform="translate(400.000000, 178.000000)">
                                            <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill:currentColor"></path>
                                            <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                                            <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                            <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                            <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg></span> -->
                        <h2 class="brand-text">Sales</h2>
                </a>

            </li>

            @if(Auth::user()->sidebar==0)
            <a href="{{URL::to('app/sidebar')}}">
                <!-- <i class="fas fa-lock-open  " style="margin-top:20px;"></i> -->
                <i data-feather='circle'  class=" font-medium-4 " style="margin-top:20px;"></i>
            </a>
            @else
            <a href="{{URL::to('app/sidebar')}}">
                <!-- <i class="fas fa-lock  " style="margin-top:20px;"></i> -->
                
                <i data-feather='disc' class=" font-medium-4 " style="margin-top:20px;"></i>
            </a>
            @endif
            <!-- <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li> -->
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">

        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="index.html"><i data-feather="monitor"></i><span class="menu-title text-truncate">Super Admin</span></a>
                <ul class="menu-content">

                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate">Home</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='superadmin/business') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/superadmin/business')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">All Businesses</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Subscription</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='superadmin/package') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/superadmin/package')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Packages</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='superadmin/settings') active @endif @endif  "><a class="d-flex align-items-center" href="{{url('/superadmin/settings')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Settings</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='superadmin/payment/method') active @endif @endif  "><a class="d-flex align-items-center" href="{{url('/superadmin/payment/method')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Payment Method</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate">Communicator</span></a>
                    </li>

                </ul>
            </li> --}}
            <li  class="@if(isset($url_path)) @if($url_path =='home') active @endif @endif nav-item"><a class="d-flex align-items-center" href="{{url('/home')}}"><i data-feather="home"></i><span class="menu-title text-truncate">Home</span></a>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="user"></i><span class="menu-title text-truncate" data-i18n="User">User</span></a>
                <ul class="menu-content">
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href="app-user-view.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View">View</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href="app-user-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Edit</span></a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i class="far fa-address-book"></i><span class="menu-title text-truncate">Contacts</span></a>
                <ul class="menu-content">
                    <li class="@if(isset($url_path)) @if($url_path =='contact-employee') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/contact-employee')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Employees</span></a>
                    </li>
                    <li  class="@if(isset($url_path)) @if($url_path =='contact-customer') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/contact-customer')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Customers</span></a>
                    </li>
                    <li  class="@if(isset($url_path)) @if($url_path =='contact-delpoint') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/contact-delpoint')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Del Point</span></a>
                    </li>
                    <!-- <li><a class="d-flex align-items-center" href="app-user-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate">Customers Group</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href="app-user-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate">Import Contacts</span></a>
                    </li> -->
                </ul>
            </li>


            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i class="fa fas fa-cubes"></i><span class="menu-title text-truncate">Items</span></a>
                <ul class="menu-content">
                    <li class="@if(isset($url_path)) @if($url_path =='item') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/item')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Item List</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='item/create') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/item/create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Add Item</span></a>
                    </li>
                    <!-- <li class="@if(isset($url_path)) @if($url_path =='product-label') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/product-label')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Print Labels</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='product-variation') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/product-variation')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Variations</span></a>
                    </li> -->
                    <!-- <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate" >Import Products</span></a>
                        </li> -->
                    <!-- <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate" >Import Opening Stock</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate" >Selling Price Group</span></a>
                        </li> -->
                    <!-- <li class="@if(isset($url_path)) @if($url_path =='product-unit') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/product-unit')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Units</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='product-category') active @endif  @endif "><a class="d-flex align-items-center" href="{{URL::to('product-category')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Categories</span></a>
                    </li>
                    <li class="@if(isset($url_path)) @if($url_path =='product-brand') active @endif  @endif "><a class="d-flex align-items-center" href="{{URL::to('product-brand')}}"><i data-feather="circle"></i><span class="menu-item text-truncate">Brands</span></a>
                    </li> -->
                    <!-- <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate" >Warranties</span></a>
                        </li> -->

                </ul>
            </li>




            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i class="fa fas fa-table"></i><span class="menu-title text-truncate">ABFL OCF</span></a>
                <ul class="menu-content">
                    <li  class="@if(isset($url_path)) @if($url_path =='ocfcreate/create') active @endif  @endif "><a class="d-flex align-items-center" href="{{url('/ocfcreate/create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate"> OCF Create</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> OCF List</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Ocf Modifier</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> FG Slit</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> FG Recieve</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> FG Delivered</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Stock Lot Request</span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Transfer to Stock Lot</span></a>
                    </li>
                </ul>
            </li>

           
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='bar-chart'></i><span class="menu-title text-truncate">Report</span></a>
                <ul class="menu-content">
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Stock Lot Report </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Slit Report </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> FG Recieve Report </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> FG Deliver Report </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Sell Payment Report </span></a>


                </ul>
            </li>
          
            <!-- <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='settings'></i><span class="menu-title text-truncate">Settings</span></a>
                <ul class="menu-content">
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Business Settings </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Business Locations </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Invoice Settings </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Barcode Settings </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Receipt Printers </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Tax Rates </span></a>
                    </li>
                    <li><a class="d-flex align-items-center" href=""><i data-feather="circle"></i><span class="menu-item text-truncate"> Package Subscription </span></a>
                    </li>
                </ul>
            </li> -->



        </ul>
    </div>
</div>