@if(Session::has('success'))
<button type="button" class="d-none " id="progress-bar"> {{ Session::get('success') }}</button>
<script> setTimeout(function () {  $('#progress-bar').click();  }, 500); </script>

@endif

@if(Session::has('info'))
<button type="button" class="d-none " id="type-info"> {{ Session::get('info') }}</button>
<script> setTimeout(function () {  $('#type-info').click();  }, 500); </script>
@endif

@if(Session::has('warning'))
<button type="button" class="d-none " id="type-warning"> {{ Session::get('warning') }}</button>
<script> setTimeout(function () {  $('#type-warning').click();  }, 500); </script>
@endif

@if(Session::has('error'))
<button type="button" class="d-none " id="type-error"> {{ Session::get('error') }}</button>
<script> setTimeout(function () {  $('#type-error').click();  }, 500); </script>
@endif


{{--
@if(isset($success))
<button type="button" class="d-none " id="type-success"> {{ $success }}</button>
<script> setTimeout(function () {  $('#type-success').click();  }, 500); </script>
@endif

@if(isset($info))
<button type="button" class="d-none " id="type-info"> {{ $info) }}</button>
<script> setTimeout(function () {  $('#type-info').click();  }, 500); </script>
@endif

@if(isset($warning))
<button type="button" class="d-none " id="type-warning"> {{ $warning) }}</button>
<script> setTimeout(function () {  $('#type-warning').click();  }, 500); </script>
@endif

@if(isset($error))
<button type="button" class="d-none " id="type-error"> {{ $error }}</button>
<script> setTimeout(function () {  $('#type-error').click();  }, 500); </script>
@endif

--}}
