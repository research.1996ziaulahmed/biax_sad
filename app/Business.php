<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'businesses';
}
