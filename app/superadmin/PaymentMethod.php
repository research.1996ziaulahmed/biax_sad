<?php


namespace App\superadmin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'payment_methods';
}
