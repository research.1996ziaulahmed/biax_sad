<?php

namespace App\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Package extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'packages';
}
