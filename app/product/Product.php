<?php

namespace App\product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'products';
}
