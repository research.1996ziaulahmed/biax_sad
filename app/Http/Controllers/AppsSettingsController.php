<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppsSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function nightmoode(){
        $user= User::find( Auth::user()->id);
        if( $user->nightmode ==1 )
        $user->nightmode =0;
        else   $user->nightmode =1;
        $user->save();
        return back();
 
    }
    public function sidebar(){
        $user= User::find( Auth::user()->id);
        if( $user->sidebar ==1 )
        $user->sidebar =0;
        else   $user->sidebar =1;
        $user->save();
        return back();
 
    }
}
