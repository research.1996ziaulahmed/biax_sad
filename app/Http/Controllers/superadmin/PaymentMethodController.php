<?php

namespace App\Http\Controllers\superadmin;

use App\Http\Controllers\Controller;
use App\superadmin\PaymentMethod;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        view()->share('url_path', "superadmin/payment/method");
        view()->share('page_title', "Payment Method");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Payment Method' => url('superadmin/payment/method')));
        return view('superadmin/payment_method/index');
    }
    public function getall()
    {
        // return 1;
        $payment_method = PaymentMethod::all();
        // return   response()->toJson(['data'=>$payment_method]);
        return   ['data' => $payment_method];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = '';
        if ($request->file('image')) {
            $originalName = $request->file('image')->getClientOriginalName();
            $fileName = md5(rand(15000000, 24678543)) . '.' . pathinfo($originalName, PATHINFO_EXTENSION);
            $request->file('image')->move(
                base_path() . '/public/uploads/payment_method',
                $fileName
            );
        }

        $payment_method = new PaymentMethod();
        $payment_method->title = $request->title;
        if ($request->file('image')) {
            $payment_method->image_url = "uploads/payment_method/$fileName";
        }
        $payment_method->number = $request->number;
        $payment_method->type = $request->type;
        $payment_method->enable_extra_charge = ($request->enable_extra_charge == 'on') ? 1 : 0;
        $payment_method->extra_charge = ($request->extra_charge) ? $request->extra_charge : 0;
        $payment_method->instructions = $request->instructions;
        $payment_method->description = $request->description;
        $payment_method->save();

        return $payment_method;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment_method = PaymentMethod::find($id);
        return  $payment_method;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment_method = PaymentMethod::find($id);
        return  $payment_method;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $fileName = '';
        if ($request->file('image')) {
            $originalName = $request->file('image')->getClientOriginalName();
            $fileName = md5(rand(15000000, 24678543)) . '.' . pathinfo($originalName, PATHINFO_EXTENSION);
            $request->file('image')->move(
                base_path() . '/public/uploads/payment_method',
                $fileName
            );
        }

        $payment_method = PaymentMethod::find($id);
        $payment_method->title = $request->title;
        if ($request->file('image')) {
            $payment_method->image_url = "uploads/payment_method/$fileName";
        }
        $payment_method->number = $request->number;
        $payment_method->type = $request->type;
        $payment_method->enable_extra_charge = ($request->enable_extra_charge==1 || $request->enable_extra_charge=='on' ) ?1: 0;
        $payment_method->extra_charge = ($request->extra_charge) ? $request->extra_charge : 0;
        $payment_method->instructions = $request->instructions;
        $payment_method->description = $request->description;
        $payment_method->save();

        return $payment_method;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment_method = PaymentMethod::find($id);
        $payment_method->delete();
        return 1;
    }
}
