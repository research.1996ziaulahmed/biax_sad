<?php

namespace App\Http\Controllers\superadmin;

use App\Http\Controllers\Controller;
use App\superadmin\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        view()->share('url_path', "superadmin/package");
        view()->share('page_title', "Packages List");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Packages' => url('superadmin/package')));
        $packages=Package::all();
        view()->share('packages', $packages);
        return view('superadmin/package/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('url_path', "superadmin/package");
        view()->share('page_title', "Create Package");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Packages' => url('superadmin/package'), 'Add' => url('superadmin/package/create')));
        
        return view('superadmin/package/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'location_count' => 'required',
            'user_count' => 'required',
            'product_count' => 'required',
            'invoice_count' => 'required',
            'interval' => 'required',
            'interval_count' => 'required',
            'trial_days' => 'required',
            'price' => 'required',
            
        ]);

        $package=new Package();
        $package->name=$request->name;
        $package->description=$request->description;
        $package->location_count=$request->location_count;
        $package->user_count=$request->user_count;
        $package->product_count=$request->product_count;
        $package->invoice_count=$request->invoice_count;
        $package->interval=$request->interval;
        $package->interval_count=$request->interval_count;
        $package->trial_days=$request->trial_days;
        $package->price=$request->price;
        $package->is_private=($request->is_private)?$request->is_private:0;
        $package->is_protective=($request->is_protective)?$request->is_protective:0;
        $package->is_active=($request->is_active)?$request->is_active:0;
        $package->is_one_time=($request->is_one_time)?$request->is_one_time:0;
        $package->installation_fees=($request->installation_fees)?$request->installation_fees:0;
        $package->created_by= Auth::user()->id;
        $package->save();

        return redirect('superadmin/package')->with('success', 'Successfully Store');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share('url_path', "superadmin/package");
        view()->share('page_title', "Edit Package");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Setting' => url('superadmin/package'), 'Edit' => url('superadmin/package/'.$id)));
        $package=Package::find($id);
        view()->share('package', $package);
        return view('superadmin/package/create');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'location_count' => 'required',
            'user_count' => 'required',
            'product_count' => 'required',
            'invoice_count' => 'required',
            'interval' => 'required',
            'interval_count' => 'required',
            'trial_days' => 'required',
            'price' => 'required',
            
        ]);

        $package= Package::find($id);
        $package->name=$request->name;
        $package->description=$request->description;
        $package->location_count=$request->location_count;
        $package->user_count=$request->user_count;
        $package->product_count=$request->product_count;
        $package->invoice_count=$request->invoice_count;
        $package->interval=$request->interval;
        $package->interval_count=$request->interval_count;
        $package->trial_days=$request->trial_days;
        $package->price=$request->price;
        $package->is_private=($request->is_private)?$request->is_private:0;
        $package->is_protective=($request->is_protective)?$request->is_protective:0;
        $package->is_active=($request->is_active)?$request->is_active:0;
        $package->is_one_time=($request->is_one_time)?$request->is_one_time:0;
        $package->installation_fees=($request->installation_fees)?$request->installation_fees:0;
        $package->created_by= Auth::user()->id;
        $package->save();

        return redirect('superadmin/package')->with('info', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package=Package::find($id);
        $package->delete();
        return 1;
    }
}
