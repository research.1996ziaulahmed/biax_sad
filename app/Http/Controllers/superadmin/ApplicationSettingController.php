<?php

namespace App\Http\Controllers\superadmin;

use App\Http\Controllers\Controller;
use App\superadmin\ApplicationSetting;
use Illuminate\Http\Request;

class ApplicationSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        view()->share('url_path', "superadmin/settings");
        view()->share('page_title', "Application Settings");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Settings' => url('superadmin/settings')));
        $setting=ApplicationSetting::find(1);
        view()->share('setting', $setting);
        return view('superadmin/settings/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $setting=ApplicationSetting::find($id);        
        $setting-> app_name = $request->app_name;
        $setting->app_title = $request->app_title;
        $setting->superadmin_enable_register_tc = ($request->superadmin_enable_register_tc)?$request->superadmin_enable_register_tc:0;
        $setting->superadmin_register_tc = $request->superadmin_register_tc;
         $setting->landmark = $request->landmark;
        $setting->state = $request->state;
        $setting->country = $request->country;
        $setting->city = $request->city;
        $setting->zip_code = $request->zip_code;
        $setting->mobile = $request->mobile;
        $setting->alternate_number = $request->alternate_number;
        $setting->email = $request->email;
        $setting->MAIL_DRIVER = $request->MAIL_DRIVER;
        $setting->MAIL_HOST = $request->MAIL_HOST;
        $setting->MAIL_PORT = $request->MAIL_PORT;
        $setting-> MAIL_USERNAME = $request->MAIL_USERNAME;
        $setting-> MAIL_PASSWORD = $request->MAIL_PASSWORD;
        $setting-> MAIL_ENCRYPTION = $request->MAIL_ENCRYPTION;
        $setting-> MAIL_FROM_ADDRESS = $request->MAIL_FROM_ADDRESS;
        $setting-> MAIL_FROM_NAME = $request->MAIL_FROM_NAME;
        $setting->allow_email_settings_to_businesses= ($request->allow_email_settings_to_businesses)?$request->allow_email_settings_to_businesses:0;
        $setting->enable_new_business_registration_notification= ($request->enable_new_business_registration_notification)?$request->enable_new_business_registration_notification:0;
        $setting->enable_new_subscription_notification= ($request->enable_new_subscription_notification)?$request->enable_new_subscription_notification:0;
        $setting->enable_welcome_email= ($request->enable_welcome_email)?$request->enable_welcome_email:0;
        $setting->save();
        return redirect('superadmin/settings')->with('info', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
