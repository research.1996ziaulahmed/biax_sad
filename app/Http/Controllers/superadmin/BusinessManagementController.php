<?php

namespace App\Http\Controllers\superadmin;

use App\BusinessLocation;
use App\Http\Controllers\Controller;
use App\Business;
use App\superadmin\Package;
use App\superadmin\PaymentInformation;
use App\User;
use App\UserAccessList;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Location;

class BusinessManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        view()->share('url_path', "superadmin/business");
        view()->share('page_title', "Business List");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Business' => url('superadmin/business')));
        $businesses= Business::all();
        // return $businesses;
        view()->share('businesses', $businesses);
        return view('superadmin/business/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('url_path', "superadmin/business");
        view()->share('page_title', "Create Business");
        view()->share('breadcrumbs', array('Super Admin' => url('/home'), 'Business' => url('superadmin/business'), 'Add' => url('superadmin/business/add')));
        
        return view('superadmin/business/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          ////////////////////////////
           
            $this->validate($request, [
                'business_name' => 'required',
                'email' => 'required|email|unique:users',
                // 'username' => 'required|email|unique:users',
                'password' => 'required',
                'confirm_password' => 'required',
                'package_id' => 'required',
                'status' => 'required'
                
                
            ]);

            // if($request->password != $request->confirm_password)
            //     return ['$pass_err'=>"Password doesn't match"];

        $package=Package::find($request->package_id);
        $date=date('y-m-d');
        $expiry_date= date("y-m-d",strtotime("+$package->interval_count $package->interval"));
        $end_trial_date= date("y-m-d",strtotime("+$package->trial_days days"));
      
        //businesses 
         $business= new Business();
 	     $business->business_name=$request->business_name;
        //  $business->email=$request->email;
         $business->logo_url=$request->logo_url;
         $business->last_package_id=$request->last_package_id;
         $business->nos_of_location=$package->location_count;
         $business->nos_of_user=$package->user_count;
         $business->nos_of_invoice=$package->product_count;
         $business->nos_of_product=$package->invoice_count;
         $business->end_trial_date=$end_trial_date;
         $business->expiry_date=$expiry_date;
         $business->is_active=1;
         $business->save();


               //user_access_lists
          $user_access_lists=new UserAccessList();
          $user_access_lists->business_id=$business->id;
          $user_access_lists->role_name='Admin';
          $user_access_lists->accesslist= json_encode([1]);;
          $user_access_lists->save();

               //business_locations
          $business_locations= new BusinessLocation();
          $business_locations->business_id=$business->id;
          $business_locations->location_name=$business->business_name;
          $business_locations->landmark=$request->landmark;
          $business_locations->country=$request->country;
          $business_locations->state=$request->state;
          $business_locations->city=$request->city;
          $business_locations->zip_code=$request->zip_code;
          $business_locations->mobile=$request->mobile;
          $business_locations->alternate_number=$request->alternate_number;
          $business_locations->save();

          //user
          $user_access_lists->location_ids=json_encode([$business_locations->id]);;
          $user_access_lists->save();


               //payment_information
          $payment_information= new PaymentInformation();
          $payment_information->business_id=$business->id;
          $payment_information->payment_method_id=isset($request->payment_method_id)?$request->payment_method_id:null;
          $payment_information->start_date=$date;
          $payment_information->end_trial_date=$end_trial_date;
          $payment_information->expiry_date=$expiry_date;
          $payment_information->package_id=$request->package_id;
          $payment_information->amount=$request->amount;
          $payment_information->transaction_id=$request->transaction_id;
          $payment_information->status=$request->status;
          $payment_information->save();
        
          // create user

                
                $user=new User();
               $user->surname=     $request->surname;
               $user->first_name=  $request->first_name;
               $user->last_name=   $request->last_name;
               $user->email=       $request->email;
               $user->username=    $business->id.'_'.$request->username;
               $user->user_type=   $user_access_lists->id;
               $user->business_id= $business->id;
               $user->location_ids= json_encode([$business_locations->id]);;
               $user->password=    $request->password;
               $user->active_user= 1;

               $user->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
