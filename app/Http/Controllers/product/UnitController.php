<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\product\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        view()->share('url_path', "product-unit");
        view()->share('page_title', "Units");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Units' => url('product-unit')));
        $units = Unit::where('business_id', Auth::user()->business_id)->orderby('name')->get();
        view()->share('units', $units);
        return view('product/unit/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unit = new Unit();
        $unit->name =  $request->name;
        $unit->business_id =  Auth::user()->business_id;
        $unit->created_by =   Auth::user()->id;
        $unit->save();

        return redirect('product-unit')->with('success', 'Successfully Store');
    }
   
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share('url_path', "product-unit");
        view()->share('page_title', "Units");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Units' => url('product-unit')));
        $units = Unit::where('business_id', Auth::user()->business_id)->orderby('name')->get();
        view()->share('units', $units);
        $unit = Unit::find($id);
        view()->share('unit', $unit);
        return view('product/unit/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit =  Unit::find($id);
        $unit->name =  $request->name;
        $unit->business_id =  Auth::user()->business_id;
        $unit->created_by =   Auth::user()->id;
        $unit->save();

        return redirect('product-unit')->with('success', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::find($id);
        $unit->delete();
        return 1;
    }


    public function add_new_unit(Request $request)
    {
        $unit = new Unit();
        $unit->name =  $request->name;
        $unit->business_id =  Auth::user()->business_id;
        $unit->created_by =   Auth::user()->id;
        $unit->save();

        // $units = Unit::where('business_id', Auth::user()->business_id)->orderby('name')->get();   
        
        // $units['max']=$unit->id;
        return $unit;
    }
}
