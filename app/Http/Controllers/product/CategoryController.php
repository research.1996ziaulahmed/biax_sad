<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        view()->share('url_path', "product-category");
        view()->share('page_title', "Categories");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Category' => url('product-category')));
        $categories = Category::where('parent_id', 0)->where('business_id',Auth::user()->business_id)->orderby('name')->get();
        view()->share('categories', $categories);
        return view('product/category/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->business_id = Auth::user()->business_id;
        $category->save();

        return redirect('product-category')->with('success', 'Successfully Store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }
    public function get_subcategory($id){    

        $sub_category = Category::where('parent_id',$id)->orderby('name')->get();
        return $sub_category;

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share('url_path', "category");
        view()->share('page_title', "Categories");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Edit Category' => url('product-category/'.$id.'/edit')));
        $categories = Category::where('parent_id', 0)->where('business_id',Auth::user()->business_id)->orderby('name')->get();

        $category = Category::find($id);
        view()->share('categories', $categories);
        view()->share('category', $category);
        return view('product/category/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',

        ]);

        $category = Category::find($id);
        if ($request->parent_id != 0)
            if ($category->parent_id == 0) {
                $sub_catgory = Category::where('parent_id', $category->id)->orderby('name')->get();

                foreach ($sub_catgory as $s) {
                    $ss = Category::find($s->id);
                    $ss->parent_id = 0;
                    $ss->save();
                }
            }

        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->business_id = Auth::user()->business_id;
        $category->save();
        return redirect('product-category')->with('success', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub_catgory = Category::where('parent_id', $id)->where('business_id',Auth::user()->business_id)->orderby('name')->get();

        foreach ($sub_catgory as $s) {
            $ss = Category::find($s->id);
            $ss->parent_id = 0;
            $ss->save();
        }

        $category = Category::find($id);
        $category->delete();
        return 1;
    }
}
