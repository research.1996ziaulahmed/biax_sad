<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\product\Product;
use App\product\ProductDetails;
use App\product\ProductLocation;
use App\product\ProductSkuCalculator;
use App\product\ProductSubSkuCalculator;
use App\product\Variation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('url_path', "product/create");
        view()->share('page_title', "Create Product");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Add' => url('product/create')));

        return view('product/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $product = new Product();

        $product->name = $request->name;
        if ($request->sku == null) {
            $p = 0;
            $sku = ProductSkuCalculator::where('business_id', Auth::user()->business_id)->first();
            if ($sku) {
                $p = $sku->max_sku + 1;
                $sku->max_sku = $p;
                $sku->save();
            } else {
                $p = 1;
                $ps = new ProductSkuCalculator();
                $ps->max_sku = $p;
                $ps->business_id = Auth::user()->business_id;
                $ps->save();
            }
            $product->sku = $p;
        } else $product->sku = $request->sku;

        $product->unit_id = ($request->unit_id) ? $request->unit_id : null;
        $product->brand_id = ($request->brand_id) ? $request->brand_id : null;
        $product->category_id = ($request->category_id) ? $request->category_id : null;
        $product->sub_category_id = ($request->sub_category_id) ? $request->sub_category_id : null;
        $product->alert_quantity = ($request->alert_quantity) ? $request->alert_quantity : null;
        $product->enable_stock = ($request->enable_stock) ? $request->enable_stock : 0;
        $product->details = $request->details;
        $product->type = $request->type;
        $product->business_id = Auth::user()->business_id;
        $product->save();

        if ($product->type == 'single') {
            $product_detail = new ProductDetails();
            $product_detail->business_id = Auth::user()->business_id;
            $product_detail->product_id = $product->id;
            // $product_detail->sub_sku =($request->sub_sku)?$request->sub_sku:null;;
            $product_detail->default_purchase_price = $request->default_purchase_price;
            $product_detail->default_sell_price = $request->default_sell_price;
            $product_detail->save();

            $product_location = new ProductLocation();
            $product_location->product_id = $product->id;
            $product_location->product_details_id = $product_detail->id;
            $product_location->business_id = Auth::user()->business_id;
            $product_location->location_id = $request->business_location;
            $product_location->quantity = $request->quantity;
            $product_location->save();
        }
        else{
            $variation_value=  Variation::where('parent_id', $request->variation_id)->where('business_id',Auth::user()->business_id)->orderby('name')->get();

            foreach($variation_value as $v){

                $product_detail = new ProductDetails();
                $product_detail->business_id = Auth::user()->business_id;
                $product_detail->product_id = $product->id;


                if ($request->sku == null) {
                    $p = 0;
                    $sub_sku = ProductSubSkuCalculator::where('business_id', Auth::user()->business_id)->where('sku',$product->sku)->first();
                    if ($sub_sku) {
                        $p = $sub_sku->max_sub_sku + 1;
                        $sub_sku->max_sub_sku = $p;
                        $sub_sku->save();
                    } else {
                        $p = 1;
                        $ps = new ProductSubSkuCalculator();
                        $ps->sku = $product->sku;
                        $ps->max_sub_sku = $p;
                        $ps->business_id = Auth::user()->business_id;
                        $ps->save();
                    }
                    $product_detail->sub_sku = $p;
                } 
                else 
                $product_detail->sub_sku =($request->sub_sku)?$request->sub_sku:null;;
                $product_detail->default_purchase_price = $request->v_default_purchase_price[$v->id];
                $product_detail->default_sell_price = $request->v_default_sell_price[$v->id];
                $product_detail->save();
    
                $product_location = new ProductLocation();
                $product_location->product_id = $product->id;
                $product_location->product_details_id = $product_detail->id;
                $product_location->business_id = Auth::user()->business_id;
                $product_location->location_id = $request->business_location;
                $product_location->quantity = $request->v_quantity[$v->id];
                $product_location->save();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**;
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
