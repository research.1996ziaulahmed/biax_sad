<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\product\Variation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VariationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        view()->share('url_path', "product-variation");
        view()->share('page_title', "Variations");
        view()->share('breadcrumbs', array('Admin' => url('/home'), 'Product' => url('product'), 'Variation' => url('product-variation')));
        $variations = Variation::where('parent_id', null)->where('business_id',Auth::user()->business_id)->orderby('name')->get();
        view()->share('variations', $variations);
        return view('product/variation/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $variations = new Variation();
        $variations->name =  $request->name;
        $variations->parent_id =  null;
        $variations->business_id =  Auth::user()->business_id;
        $variations->created_by =   Auth::user()->id;
        $variations->save();

        foreach ($request->values as  $v) {
            $vv = new Variation();
            $vv->name =  $v;
            $vv->parent_id =  $variations->id;
            $vv->business_id =  Auth::user()->business_id;
            $vv->created_by =   Auth::user()->id;
            $vv->save();
        }


        return redirect('product-variation')->with('success', 'Successfully Store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $variation = Variation::find($id);
        view()->share('url_path', "product-variation");
        view()->share('page_title', "Variations");
        view()->share('breadcrumbs', array('Admin' => url('/home'), 'Product' => url('product'), 'Variation' => url('product-variation')));
        $variations = Variation::where('parent_id', null)->where('business_id',Auth::user()->business_id)->orderby('name')->get();
        view()->share('variations', $variations);
        view()->share('variation', $variation);
        return view('product/variation/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $variations =  Variation::find($id);
        $variations->name =  $request->name;
        $variations->parent_id =  null;
        $variations->business_id =  Auth::user()->business_id;
        $variations->created_by =   Auth::user()->id;
        $variations->save();

        foreach ($request->old_values as $key => $v) {
            $vv = Variation::find($key);
            $vv->name =  $v;
            $vv->parent_id =  $variations->id;
            $vv->business_id =  Auth::user()->business_id;
            $vv->created_by =   Auth::user()->id;
            $vv->save();
        }
        if (isset($request->values))
            foreach ($request->values as  $v) {
                $vv = new Variation();
                $vv->name =  $v;
                $vv->parent_id =  $variations->id;
                $vv->business_id =  Auth::user()->business_id;
                $vv->created_by =   Auth::user()->id;
                $vv->save();
            }


        return redirect('product-variation')->with('success', 'Successfully Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variation =  Variation::find($id);
        $variation->delete();
        $variations= Variation::where('parent_id',$id)->where('business_id',Auth::user()->business_id)->orderby('name')->get();

        foreach ($variations as  $v) {
            $vv = Variation::find($v->id);
            $vv->delete();
        }

        return 1;
    }

    public function get_variable(){
        $variations = Variation::where('parent_id', null)->where('business_id',Auth::user()->business_id)->orderby('name')->get();
       return $variations;

    }
    public function get_variable_value($id){
        $variation = Variation::where('parent_id', $id)->where('business_id',Auth::user()->business_id)->orderby('name')->get();
       return $variation;

    }
}
