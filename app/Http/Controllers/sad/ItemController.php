<?php

namespace App\Http\Controllers\sad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\sad\tblItem; 
use DB;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share('url_path', "item");
        view()->share('page_title', "Item List");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Item List' => url('item')));
       
            
        return view('item/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('url_path', "item/create");
        view()->share('page_title', "Item Create");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Item' => url('item'), 'Create' => url('item/create')));
        // $brands = Brand::where('business_id',Auth::user()->business_id)->orderby('name')->get();
        // view()->share('brands', $brands);
        return view('item/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $item = new tblItem();
        $item->strProductName =  $request->strProductName;
        $item->strProductCode =  $request->strProductCode;
        $item->intUnitID =  91;
        $item->save();
        return redirect('item/create')->with('success', 'Successfully Store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = DB::select( DB::raw("SELECT * FROM `tblItem`  order by strProductName") );
        return   ['data' => $items];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share('url_path', "product-unit");
        view()->share('page_title', "Units");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Product' => url('product'), 'Units' => url('product-unit')));
        $brands = Brand::where('business_id',Auth::user()->business_id)->orderby('name')->get();
        view()->share('brands', $brands);
        $brand = Brand::find($id);
        view()->share('brand', $brand);
        return view('product/brand/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit =  Brand::find($id);
        $unit->name =  $request->name;
        $unit->business_id =  Auth::user()->business_id;
        $unit->created_by =   Auth::user()->id;
        $unit->save();
        return redirect('product-brand')->with('success', 'Successfully Update');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        return 1;
    }

    public function add_new_brand(Request $request)
    {
        $brand = new Brand();
        $brand->name =  $request->name;
        $brand->business_id =  Auth::user()->business_id;
        $brand->created_by =   Auth::user()->id;
        $brand->save();

        // $brands = Brand::where('business_id', Auth::user()->business_id)->orderby('name')->get();   
        
        // $brands['max']=$brand->id;
        return $brand;
    }
}
