<?php

namespace App\Http\Controllers\Ocf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\hr\tblEmployee; 
use App\sad\tblCustomer; 
use App\sad\tblCustomerCategory; 
use App\sad\tblDispoint; 
use App\sad\tblItem; 
use App\sad\tblPackagingMode; 
use App\sad\tblOcf; 
use DB;
use Auth;

class OcfCreateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('url_path', "ocfcreate/create");
        view()->share('page_title', "Create Ocf");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Ocf Create' => url('ocfcreate/create')));
       
        $employees = DB::select( DB::raw("SELECT `enroll`, `name` FROM `tblemployees` WHERE email like '%@%' order by name") );
        $cuscategory =tblCustomerCategory::all();
        $customer =tblCustomer::orderBy('strname')->get(); 
        $dispoint =tblDispoint::orderBy('strname')->get();
       
        $pakg = DB::select( DB::raw("SELECT * FROM `tblpackagingmode`  order by strpackagingmodedetails") );
        
       
        view()->share('pakg', $pakg);
        view()->share('employees', $employees);
        view()->share('customer', $customer);
        view()->share('cuscategory', $cuscategory);
        view()->share('dispoint', $dispoint);
        view()->share('ocf_create_step_no', 1);
        return view('ocf/ocfcreate/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        view()->share('url_path', "ocfcreate/create");
        view()->share('breadcrumbs', array('Home' => url('/home'), 'Ocf Create' => url('ocfcreate/create')));
       
        $ocf = new tblOcf();             
        $ocf->dteocfdate = $request->dteocfdate  ;
        $ocf->strpono = $request->strpono  ;
        $ocf-> dtepodate= $request-> dtepodate ;
        $ocf-> intsalepersonid= $request-> intsalepersonid ;
        $ocf-> strsalepersonname= $request-> strsalepersonname ;
        $ocf-> intcustomercategory= $request-> intcustomercategory ;
        $ocf-> intpackagingmode= $request-> intpackagingmode ;
        $ocf-> strsalepersonname= $request-> strsalepersonname ;
        $ocf-> dectorrancerate= $request-> dectorrancerate ;
        $ocf-> intcustomerid= $request-> intcustomerid ;
        $ocf-> intdelpointid= $request-> intdelpointid ;
        $ocf-> strdelpointname= $request-> strdelpointname ;
        $ocf-> intinsertby= Auth::user()->business_id; ;
        $ocf-> strocfsatus= 'active';
        $ocf-> strflimtype=  $request-> strflimtype ;
        $ocf-> intunitid= 91;
       
        //ocf no Create
        $alphabet = range('A', 'Z');       
        $ocfYear = date("Y",strtotime($request-> dteocfdate));       
        $month=   date("m",strtotime($request-> dteocfdate));;
        $ocfMonth=    $alphabet[(int)$month-1];
        $countSql= "SELECT count(intocfid) as ocfno FROM `tblocf` where year(dteocfdate)= $ocfYear  and month(dteocfdate)= $month";
        $count = DB::select( DB::raw($countSql) );
        $c=$count[0]->ocfno;
        $ocfYear=substr( $ocfYear, -2 );
        if(strlen($c)>1) $strOcfNo="OCF$ocfYear$ocfMonth$c"; 
        else{$pre=0; $strOcfNo="OCF$ocfYear$ocfMonth$pre$c";}
        //ocf no Create

        $ocf-> strocfno= $strOcfNo;
        $ocf->save();

        $items = DB::select( DB::raw("SELECT * FROM `tblItem`  order by strProductName") );  
        
        view()->share('page_title', "OCF NO : $strOcfNo");
        view()->share('strOcfNo', $strOcfNo);
        view()->share('items', $items);
        view()->share('ocf_create_step_no', 2);
        return view('ocf/ocfcreate/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
