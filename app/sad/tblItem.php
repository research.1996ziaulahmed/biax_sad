<?php

namespace App\sad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class tblItem extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'tblitem';
}
