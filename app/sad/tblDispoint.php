<?php

namespace App\sad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class tblDispoint extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'tbldispoint';
}

