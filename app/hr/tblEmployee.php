<?php

namespace App\hr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class tblEmployee extends Model
{
    use Notifiable;

    use SoftDeletes;
    protected $table = 'tblemployees';
}
